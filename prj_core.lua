#!lua
-- Written with TAB as 4 spaces

-- Library
project "libPCO"
	targetname( "PCO" )
	language "C++"

	dofile "compile_filters.lua"

	includedirs { "src/core/hpp" }

	-- Src
	files { "src/core/**.hpp", "src/core/**.cpp" }
	-- Included src
	files { "src/core/**.cppi" }

	-- Doc
	files { "LICENSE", "*.md", "doc/*", "**.txt" }
