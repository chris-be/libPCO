# Noted
There is always some trouble with strings. They can be made of "ascii", encoded with "iso-...", "wide char" or "multi bytes" / "unicode".
Furthermore there is "code points" complexity.

Usual use cases:
- as resource names: file, directory, ... => C function use 'const char*'
- string concatenation: create message/log => char* / std::string => deal with encoding (locale not thread safe...)
- string manipulation: search, replace, parse, ... => char* / std::string => deal with encoding (locale not thread safe...)

So:
- we need a way to be compliant with all C function taking 'char*'
- we need a way to concat strings efficiently
- we would be happy to simplify string manipulation (not deal with encoding)

# Path chosen
## StringC (constant)
- designed as resource that can be read: holds const char* and encoding
- null terminated -> c_str()
- not suitable for direct access, find, ...

## String (dynamic)
- designed for basic needs: concatenation/printing: holds chars and encoding
- to be efficient: not forced to have continuous chars in memory -> can't use c_str() -> no need to be NULL terminated
- can be concatenated
- not suitable for direct access, find, ...
- must be converted when used as resource name

## Char array (string manipulation)
Charray is designed to store "uncompressed" and continuous chars to ensure that any ith char is not related to neighbours.
It provides usual string manipulation (operator[], concat, find, ..).

It should be used when working with chars (parsing, concat, ...).

# Charset conversion
It would be time consuming to write each conversion from one charset to another.
So there are decoders to "expand" to UTF-32:
- ASCII, ISO-??, ... -> UTF-32
- UTF-8, UTF-16, UTF-32 -> UTF-32
And encoders to convert from UTF-32 to whatever:
- UTF-32 -> ASCII, ISO-??, ...
- UTF-32 -> UTF-8, UTF-16, UTF-32
At the time of writing only US-ASCII / ISO-8859-1 / UTF-8 / UTF-16 / UTF-32 are handled.

## CharsetConverter
CharsetConverter use decoder/encoder" to achieve conversion.

# Way to use
## Avoid 'char*' as much as possible, prefer:
- const char* container => StringC
- char* container => String

## E_StringEncoding: extention to work with encoded chars


# E_String: extension to work with C-Style strings (const char*, char16_t*, char32_t*, ...)
Examples:
- strlen -> e_strlen(const PChar*)
- strcpy -> e_stcpy(const PChar*)
- free -> e_free(PChar*&)

# TODO
CStringHolder: check current encoding and transcode at (const_)char_ptr only ?


# Note
nl_langinfo(CODESET)

locale -m : list all locales

https://www.cprogramming.com/tutorial/unicode.html

Official char set
http://www.iana.org/assignments/character-sets/character-sets.xhtml

wchar_t -> utf32 : mbrtoc32
utf32 -> wchar_t : c32rtomb
