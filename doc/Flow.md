# Note

# Inflow
An inflow provides a way to read something sequentially.
Example: read a file from start to end one byte after another.

# Filter
A filter is an inflow that reads another inflow and transform it on the fly.
Example: read a string and remove successive separators.

# Outflow
An outflow provides a way to write something sequentially.
Example: write to a file one byte after another.

# Recommended
Do not transform data in outflows. Better write a filter and do:

while( inflow.read(data) )
{
	outflow.write(data);
}

Why ?
- Transforming something means always reading something first (file, user entries, database, ...).
- Even if it's not natural (zip data may be thought as outflow for example), it's better to have one way to do this.
