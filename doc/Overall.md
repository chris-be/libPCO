# Note
Here is project presented:
- naming convention
- structure

# Naming convention
## Files
- C_Name : constraint definition named "name"
- I_Name : contract (interface) named "name"
- E_Name : extension named "name (holds extension methods. Example: e_findFirst())
- CVS_Name : convenience class named "name" (generally holds services)


## Structure
- crt/ : every constraints
- cvs/ : all convenience/extension classes
- low :
