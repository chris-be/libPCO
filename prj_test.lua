#!lua
-- Written with TAB as 4 spaces

-- Unit tests
project "testLibPCO"
	kind "ConsoleApp"
	language "C++"
	links	{ "libPCO"
		, "c++", "pthread"
		}

	dofile "compile_filters.lua"

	includedirs { "src/core/hpp", "src/test/hpp" }

	-- Src
	files { "src/test/**.hpp", "src/test/**.cpp" }
	-- Included src
	files { "src/test/**.cppi" }

	-- Doc
        files { "src/test/*.md", "src/test/*.txt"  }
