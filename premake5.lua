#!lua
-- Written with TAB as 4 spaces

workspace "LibPCO"
	location( "ide" )
	configurations { "DynDebug", "DynRelease", "StaticDebug", "StaticRelease" }
	-- ARM
	platforms { "x64", "x32" }
	-- Clang
	buildoptions { "-std=c++17", "-stdlib=libc++", "-Wpedantic" }

	-- Some filters
	filter { "platforms:*32" }
		architecture "x86"

	filter { "platforms:*64" }
		architecture "x64"

	filter "configurations:Static*"
		kind "StaticLib"

	filter "configurations:Dyn*"
		kind "SharedLib"
		-- defines { "DLL_EXPORTS" }

	filter "configurations:*Debug"
		targetsuffix( "-dbg" )
		defines { "DEBUG" }
		symbols "on"

	filter "configurations:*Release"
		targetsuffix( "-rel" )
		defines { "NDEBUG", "NPREDICATE" }
		optimize "full"
		-- Clang "-Wall", "-Wextra"
		-- "-Weverything", "-fcomment-block-commands=convenience",  "-Wno-c++98-compat", "-Wno-shadow-field-in-constructor", "-Wno-shadow", "-Wno-date-time", "-Wno-covered-switch-default"
		buildoptions { "-Wall", "-Wextra" }

	-- Not forget to stop filter action
	filter { }

	targetSystem	= "%{os.get()}-%{cfg.architecture}"

	objdir		( "bin/obj/" .. targetSystem .. "/%{prj.name}/%{cfg.buildcfg}" )
	targetdir	( "bin/" .. targetSystem .. "/%{prj.name}" )

	-- Library
	include "prj_core.lua"

	-- Unit tests
	include "prj_test.lua"
