/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"text/TestText.hpp"
#include "text/Regex.hpp"
#include "string/String.hpp"
#include "string/StringConstant.hpp"
#include "text/cvs/CVS_Regex.hpp"
#include "cvs/res/AutoClose.hpp"

/*
#include "text/ACharray.hpp"
#include "text/UCharray.hpp"
namespace NPCO {
	// Force all code generation
	template class Charray<AChar>;
}

#include "text/cfr/TextClassifiers.hpp"
*/

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

// Test data

const char8_t*		glob_a_pattern		= U8_STR("globi_a+_globi");
const char8_t*		test_glob_a1		= U8_STR("globi__globi");
const char8_t*		test_glob_a2		= U8_STR("globi_aaAAaa_globi");

const char*			regProtocol = "(https?://)?";
const char*			regDomain	= "([-_:[:alnum:]\\.]+)";
const char*			regPath		= "([-_/!,'[:alnum:]\\. \\+ \\* \\$ \\( \\) %]*/{0,1})";
const char*			regOptions	= "(\\?.*)?";


const char8_t*		test_protocol	= U8_STR("https://");		// 8
const char8_t*		test_domain		= U8_STR("www.tiptip.fr");	// 13
const char8_t*		test_path		= U8_STR("/test");			// 5
const char8_t*		test_option		= U8_STR("?validate");		// 9


TestText::TestText(void)
{	}

TestText::~TestText(void)
{	}

#include <iostream>

void	TestText::regex(void)
{
	bool	ok;
	Regex	rgx;

	{
		AutoClose	cleaner(rgx);

		rgx.prepare(StringC(glob_a_pattern), false, true);

		ok		= rgx.match( StringC(test_glob_a1) );
		TC_CHECK(!ok);

		ok = rgx.match( StringC(test_glob_a2) );
		TC_CHECK(ok);

		// rgx.close();
	}

	String	regURL	= String("^") + regProtocol + regDomain + regPath + regOptions + "$";

	rgx.prepare(regURL, true, true);

	String	url = String(test_protocol) + test_domain + test_path + test_option;

	RegexSplitResult	res;
	rgx.split(res, url);
	ok = res.found();
	TC_CHECK(ok);

	ok = res.matches().size() == 4;
	TC_CHECK(ok);


	//
	Store<String>	tests;
	ok = CVS_Regex::split(tests, url, rgx);
	TC_CHECK(ok);

	ok = tests.size() == 4;
	TC_CHECK(ok);

	std::cout << std::endl << tests[0] << std::flush;
	ok = (tests[0] == test_protocol);
	TC_CHECK(ok);

	ok = (tests[1] == test_domain);
	TC_CHECK(ok);

	ok = (tests[2] == test_path);
	TC_CHECK(ok);

	ok = (tests[3] == test_option);
	TC_CHECK(ok);

}

void	TestText::correctness(void)
{
	this->regex();
}


/*
void	TestText::testACharray(void)
{
	bool	ok;
	ACharray	left = "Youpi";
	ACharray	right = " :)";

	//TC_CHECK(left != right);
	ACharray	text = left + right;
	ok = ( text == ACharray("Youpi :)") );
	TC_CHECK(ok);

	ATextCIClassifier	classifier;
	ok = classifier.isSame(text, ACharray("YOUPI :)"));
	TC_CHECK(ok);

	ACharray sub;
	sub.set(text, 0, left.size()-1);
	ok = (sub  == left);
	TC_CHECK(ok);

	sub.set(text, left.size(), text.size()-1);
	ok = (sub  == right);
	TC_CHECK(ok);

	sub.set(text);
	sub.mirror();
	ok = ( sub == ACharray("): ipuoY") );
	TC_CHECK(ok);
}

void	TestText::testUCharray(void)
{
	//TODO

}

void	TestText::correctness(void)
{
	this->testACharray();

	this->testUCharray();
}
*/
