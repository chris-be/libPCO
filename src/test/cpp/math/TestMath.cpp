/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"math/TestMath.hpp"

#include "math/TypeNumber.hpp"
namespace NPCO {
	template class TypeNumber<double>;
}

#include "math/Factor.hpp"
namespace NPCO {
	template class Factor<double>;
}

#include "math/Matrix.hpp"
namespace NPCO {
	template class Matrix<float, 5, 9>;
}

#include "math/SquareMatrix.hpp"
namespace NPCO {
	template class SquareMatrix<float, 7>;
}

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

TestMath::TestMath(void)
{	}

TestMath::~TestMath(void)
{	}

void	TestMath::testFactor(void)
{
	Factor<int> f(1, 2);
	int test;

	test = f.mul(4);
	TC_CHECK(test == 2);

	f/= 2;
	test = f.mul(4);
	TC_CHECK(test == 1);
}

void	TestMath::testMatrix(void)
{
	Matrix<int, 2, 2> m1;

	m1.fill(1);
	m1*= 4;

	TC_CHECK(m1(0, 0) == 4);
	TC_CHECK(m1(1, 1) == 4);

	m1/= 2;
	TC_CHECK(m1(0, 1) == 2);
	TC_CHECK(m1(1, 0) == 2);

	Matrix<int, 2, 2> m2;
	m1.fill(1);
	m2.fill(2);

	m1+= m2;
	TC_CHECK(m1(0, 0) == 3);
	TC_CHECK(m1(1, 1) == 3);

	m1-= m2;
	TC_CHECK(m1(0, 0) == 1);
	TC_CHECK(m1(1, 1) == 1);

	auto m3 = m1 + m2;
	TC_CHECK(m3(0, 0) == 3);
	TC_CHECK(m3(1, 1) == 3);

	m3 = m3 - m2;
	TC_CHECK(m3(0, 0) == 1);
	TC_CHECK(m3(1, 1) == 1);

	auto m4 = m1 * m2;
	TC_CHECK(m4(0, 0) == 4);
	TC_CHECK(m4(1, 1) == 4);

	SquareMatrix<int, 4> sm;
	sm.setIdentity();
	for(int i = 0 ; i < 4 ; ++i)
	{
		for(int j = 0 ; j < 4 ; ++j)
		{
			int t = sm(i, j);
			int test = (i == j) ? 1 : 0;
			TC_CHECK(t == test);
		}
	}

	SquareMatrix<int, 2>	sm1;
	SquareMatrix<int, 2>	sm2;
	sm1.fill(1);
	sm2.fill(2);

	sm1*= sm2;
	TC_CHECK(sm1(0, 0) == 4);
	TC_CHECK(sm1(1, 1) == 4);
}

void	TestMath::correctness(void)
{
	this->testFactor();
	this->testMatrix();
}
