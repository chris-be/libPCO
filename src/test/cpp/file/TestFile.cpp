/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/TestFile.hpp"
#include "file/FileMemory.hpp"
#include "file/FileLow.hpp"
#include "file/FileBuffered.hpp"

#include "cvs/res/AutoClose.hpp"
#include "cvs/res/AutoDeleteBlock.hpp"

#include "testUnit/TestMacros.hpp"

using namespace NPCO;
using namespace NTestUnit;

TestFile::TestFile(void)
{	}

TestFile::~TestFile(void)
{	}

bool	TestFile::testPattern(IFile& file) const
{
	TC_PREDICATE(file.isOpened());

	size_t size = this->rwPattern.size();
	AutoDeleteBlock<uw_08> buf( new uw_08[size] );
	file.read(buf._da(), size);

	return this->rwPattern.checkContent(buf._da());
}

void	TestFile::testMemFile(void)
{
	io_open_mode	openOptions = (NPCO::eRead | NPCO::eWrite);
	io_offset		patSize = this->rwPattern.size();
	const uw_08*	pattern = this->rwPattern._da();
	io_offset		size = 2*patSize;

	bool tres;

	FileMemory	memFile;
	AutoClose	autoCloseMem(memFile);

	{	// Write pattern and read back
		memFile.open(nullptr, size, openOptions);
		TC_CHECK(memFile.isOpened());

		memFile.write(pattern, patSize);

		memFile.seek(-patSize, ESeek::fromCurrent);
		tres = testPattern(memFile);
		TC_CHECK(tres);

		memFile.seek(0, ESeek::fromStart);
		tres = testPattern(memFile);
		TC_CHECK(tres);

		memFile.seek(size, ESeek::fromEnd);
		tres = testPattern(memFile);
		TC_CHECK(tres);

		memFile.close();
		TC_CHECK( !memFile.isOpened() );
	}

}

void	TestFile::testBufferedFile(void)
{
	io_open_mode	openOptions = (NPCO::eRead | NPCO::eWrite);
	io_offset		patSize = this->rwPattern.size();
	const uw_08*	pattern = this->rwPattern._da();
	io_offset		size = 2*patSize;

	bool tres;

	FileMemory	memFile;
	AutoClose	autoCloseMem(memFile);

	FileBuffered	bufFile;
	AutoClose		autoCloseBuf(bufFile);

	memFile.open(nullptr, size, openOptions);
	TC_CHECK(memFile.isOpened());

	{	// Write pattern and read back
		bufFile.open(&memFile, 16);
		TC_CHECK(bufFile.isOpened());

		bufFile.write(pattern, patSize);
		bufFile.seek(0, ESeek::fromStart);
		tres = testPattern(bufFile);
		TC_CHECK(tres);

		bufFile.close();
		TC_CHECK( !bufFile.isOpened() );

		memFile.seek(0, ESeek::fromStart);
		tres = testPattern(memFile);
		TC_CHECK(tres);
		memFile.close();
		TC_CHECK( !memFile.isOpened() );
	}
}

void	TestFile::correctness(void)
{
	this->rwPattern.randomFill();

	this->testMemFile();
	this->testBufferedFile();

	FileLow	file;
			// file.open("test_file.md", eRead);
			// file.close();

}
