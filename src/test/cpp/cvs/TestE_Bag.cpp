/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"cvs/TestE_Bag.hpp"
#include "cvs/bag/E_Bag.hpp"
#include "cvs/bag/E_DynBag.hpp"
#include "cvs/bag/E_RowBag.hpp"

#include "col/Store.hpp"
#include "col/SetTree.hpp"
#include "math/MathClassifier.hpp"
#include "string/cvs/E_CStyleString.hpp" // e_memBlock

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

TestE_Bag::TestE_Bag(void)
{	}

TestE_Bag::~TestE_Bag(void)
{	}

void	TestE_Bag::testE_Bag(void)
{
	using TBag = const MemBlock<char>;

	bool ok;
	// areEqual
	TBag left = e_memBlock("This is a test");
	TBag copyLeft= e_memBlock("This is a test");
	ok = e_equalContent(left, copyLeft);
	TC_CHECK(ok);

	// equalContent
	TBag notRight = e_memBlock("This is another test");
	ok = !e_equalContent(left, notRight);
	TC_CHECK(ok);
}

void	TestE_Bag::testE_DynBag(void)
{
	const MemBlock<char>	pattern = e_memBlock("This is a test :)");
	Store<char> store;

	bool ok;
	// addAll
	e_addAll(store, pattern);
	ok = e_equalContent(store, pattern);
	TC_CHECK(ok);

	// replaceAll
	store.add('g');
	e_replaceAll(store, pattern);
	ok = e_equalContent(store, pattern);
	TC_CHECK(ok);
}

void	TestE_Bag::testE_RowBag(void)
{
	const MemBlock<char>	content = e_memBlock("H 1.");
	const MemBlock<char>	exceed = e_memBlock(" \n\t");
	SetTree<char, MathClassifier<char>>	esc = { ' ', '\t', '\r' , '\n' };

	bool ok;
	// trim
	Store<char> toTrim;
	e_addAll(toTrim, content);
	e_addAll(toTrim, exceed);
	e_trimEnd(toTrim, esc);
	ok = e_equalContent(toTrim, content);
	TC_CHECK(ok);

	e_replaceAll(toTrim, exceed);
	e_addAll(toTrim, content);
	e_trimStart(toTrim, esc);
	ok = e_equalContent(toTrim, content);
	TC_CHECK(ok);
}

void	TestE_Bag::correctness(void)
{
	this->testE_Bag();
	this->testE_DynBag();

	this->testE_RowBag();
}
