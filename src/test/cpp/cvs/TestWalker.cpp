/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"cvs/TestWalker.hpp"
#include "mem/MemBlock.hpp"
#include "cvs/iter/E_HasIter.hpp"
#include "cvs/walk/E_Walker.hpp"

#include "math/MathClassifier.hpp"

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

TestWalker::TestWalker(void)
{	}

TestWalker::~TestWalker(void)
{	}

void	TestWalker::testFind(void)
{
	char buffer[10] = { char(0), char(1), char(2), char(3), char(4), 'a', 'b', 'c', 'd', 'e' };
	int size = sizeof(buffer) / sizeof(char);
	MemBlock<char>	mb(buffer, size);

	bool found;

	// Cursor - operator==
	char wanted = 'b';
	auto	cursor = e_cursor(mb);
	found = e_find(cursor, wanted);
	TC_CHECK(found && (cursor.cell() == wanted));

	cursor.toLast();
	found = e_rvsFind(cursor, wanted);
	TC_CHECK(found && (cursor.cell() == wanted));

	// Cursor - selector
	wanted = char(2);
	typename ISelector<char>::SelFunc	selector = [] (const char& t) -> bool { return t == char(2); };

	cursor = e_cursor(mb);
	found = e_find(cursor, selector);
	TC_CHECK(found && (cursor.cell() == wanted));

	cursor.toLast();
	found = e_rvsFind(cursor, selector);
	TC_CHECK(found && (cursor.cell() == wanted));

	// Rider - classifier
	MathClassifier<char> classifier;
	// IClassifier<char>& cl = classifier;

	wanted = 'c';
	auto	rider = e_rider(mb);
	found = e_find(rider, wanted, classifier);
	TC_CHECK(found && (rider.cell() == wanted));

	rider.toLast();
	found = e_rvsFind(rider, wanted, classifier);
	TC_CHECK(found && (rider.cell() == wanted));
}

void	TestWalker::correctness(void)
{
	this->testFind();

}
