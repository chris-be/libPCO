/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"cvs/TestRes.hpp"

#include "cvs/res/AutoDelete.hpp"
#include "cvs/res/AutoDeleteBlock.hpp"
namespace NPCO {
	// Force all code generation
	template class AutoDelete<float>;
	template class AutoDeleteBlock<bool>;
}

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

TestRes::TestRes(void)
{	}

TestRes::~TestRes(void)
{	}

class TestClean {

	bool& alive;
public:
	TestClean(bool& alive) : alive(alive)
	{
		this->alive = true;
	}

	~TestClean(void)
	{
		this->alive = false;
	}
};

void	TestRes::testAutoClean(void)
{
	bool	alive;

	TestClean*	tc = new TestClean(alive);
	TC_CHECK(alive == true);
	{
		AutoDelete<TestClean>	ad(tc);
		TC_CHECK(ad.isValid());
		TC_CHECK(ad._da() == tc);

		TestClean*	t = ad.popOut();
		TC_CHECK(ad.isNull());
		TC_CHECK(t == tc);

		ad = t;
	}
	TC_CHECK(alive == false);

	//
	int* block = new int[10]();
	int* t;
	{
		AutoDeleteBlock	adb(block);
		int* t = adb.popOut();
		TC_CHECK(t == block);
		adb = t;
	}
}

void	TestRes::correctness(void)
{
	this->testAutoClean();
}
