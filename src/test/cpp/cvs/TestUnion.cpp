/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"cvs/TestUnion.hpp"
#include "cvs/Union.hpp"
namespace NPCO {
	// Force all code generation
	template class Union<bool, int>;
}

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

TestUnion::TestUnion(void)
{	}

TestUnion::~TestUnion(void)
{	}

//! Test union set and constructor/destructor calls
class LiveTest {

public:
	static int constructNb;
	static int destructNb;

	char	witness;

	LiveTest(void) : LiveTest(' ')
	{	}

	LiveTest(const char witness) : witness(witness)
	{
		++constructNb;
	}

	~LiveTest(void)
	{
		++destructNb;
	}

};

int	LiveTest::constructNb	= 0;
int	LiveTest::destructNb	= 0;

void	TestUnion::correctness(void)
{
	LiveTest::constructNb	= 0;
	LiveTest::destructNb	= 0;

	using Union_1 = Union<int, LiveTest, bool>;
	bool ok;

	Union_1	u;
	ok = !u.hasValue();
	TC_CHECK(ok);

	u.set((int)(5));
	ok = u.hasValueOf<int>();
	TC_CHECK(ok);

	ok = u.get<int>() == 5;
	TC_CHECK(ok);

	u.set((bool)(false));
	ok = !u.hasValueOf<int>();
	TC_CHECK(ok);
	ok = u.getId() == Union_1::makeId<bool>();
	TC_CHECK(ok);

	const char testChar = '?';
	LiveTest t(testChar);
	TC_CHECK(LiveTest::constructNb == 1);
	TC_CHECK(LiveTest::destructNb == 0);
	TC_CHECK( t.witness == testChar);

	u.set(t);
	TC_CHECK(LiveTest::constructNb == 2);
	ok = u.get<LiveTest>().witness == testChar;
	TC_CHECK(ok);

	u.clear();
	TC_CHECK(LiveTest::destructNb == 1);

	ok = !u.hasValue();
	TC_CHECK(ok);
}
