/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"program/TestProgram.hpp"
#include "program/ExecCommand.hpp"
#include "string/StringConstant.hpp"
#include "string/crt/String_Type.hpp"

#include "testUnit/TestMacros.hpp"

#include <iostream>

using namespace NPCO;

#include "PCO_CompilerPlatform.hpp"
#if defined FOR_POSIX || defined FOR_LINUX
	const char8_t*	exec_cmd	= U8_STR("ls");
	const char8_t*	arg_1		= U8_STR("-l");
#elif defined FOR_WIN32
	const char8_t*	exec_cmd	= U8_STR("dir");
	const char8_t*	arg_1		= U8_STR("/w");
#else
	#error Unspecified or Unknown Platform
#endif

TestProgram::TestProgram(void)
{	}

void	TestProgram::correctness(void)
{
	this->testExecCommand();
}

void	TestProgram::testExecCommand(void)
{
	StringC	exec(exec_cmd);
	StringC	arg1(arg_1);

	ExecCommand	cmd(exec);
				cmd.addArg(arg1);

	bool ok;
	ok = cmd.execute(true, true, true);
	TC_CHECK_MSG(ok, "::fork didn't succeed");

	cmd.wait();

	ok = cmd.getExitStatus() == 0;
	TC_CHECK(ok);

	String out;
	cmd.readStdOut(out);
	ok = out.size() > 0;
	TC_CHECK_MSG(ok, "exec didn't write anything to stdout ?");
	// std::cout << out << std::endl << std::flush;

	String err;
	cmd.readStdErr(err);
	ok = err.size() == 0;
	TC_CHECK_MSG(ok, "exec wrote to stderr ?");
}
