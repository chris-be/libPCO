/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"mem/TestMem.hpp"

#include "mem/MemBlock.hpp"
namespace NPCO {
	template class MemBlock<uw_16>;
}

// #include "mem/MetaHolder.hpp"
#include "mem/Reference.hpp"
namespace NPCO {
	struct Meta { int k; };
	template class MetaHolder<Meta, int>;
	template class Reference<int>;
}

#include "mem/cvs/RefRes.hpp"
namespace NPCO {
	template class RefRes<float>;
}

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

class Mother {
	int		witness;

public:
	static const char*	Who;

	Mother(int w = 0) : witness(w)	{ }
	virtual ~Mother(void) {}

	virtual const char*	whoami(void) const
	{	return Mother::Who;	}

	int	getWitness() const
	{	return witness;		}

	void	setWitness(int w)
	{	witness = w;		}
};

class Child : public Mother {

public:
	static const char*	Who;

	Child(int w = 0) : Mother(w)	{ }

	Child([[maybe_unused]] int w, int o) : Mother(o)	{ }

	virtual const char*	whoami(void) const override
	{	return Child::Who;	}
};

class BigChild : public Child {
	//! Use lot more space than the others
	long int	space[128];

public:
	static const char*	Who;

	BigChild(int w = 0) : Child(w)			{ space[0] = 0; }

	BigChild(int w, int o) : Child(w, o)	{ }

	virtual const char*	whoami(void) const override
	{	return BigChild::Who;	}
};

const char*	Mother::Who		= "Mother::Class";
const char*	Child::Who		= "Child::Class";
const char*	BigChild::Who	= "BigChild::Class";

TestMem::TestMem(void)
{	}

TestMem::~TestMem(void)
{	}

void	TestMem::testReference(void)
{
	Reference<Mother>	m = Reference<Mother>::create<Mother>(10);
	TC_CHECK(m.isValid());
	TC_CHECK(m->whoami() == Mother::Who);
	TC_CHECK(m->getWitness() == 10);

	Reference<Mother>	c = Reference<Mother>::create<Child>(10, 40);
	TC_CHECK(c.isValid());
	TC_CHECK(c->whoami() == Child::Who);
	TC_CHECK(c->getWitness() == 40);

	Reference<Mother>	bc= Reference<Mother>::create<BigChild>(100);
	TC_CHECK(bc.isValid());
	TC_CHECK(bc->whoami() == BigChild::Who);
	TC_CHECK(bc->getWitness() == 100);

	{
		Reference<Mother>	c2 = c;
		TC_CHECK(c2.isValid());
		TC_CHECK(c2 == c);

		c2->setWitness(80);
		TC_CHECK(c->getWitness() == 80);
	}

	TC_CHECK(c.isValid());
}

void	TestMem::testResource(void)
{
/*
	int*	one_int = new int;
	AutoDelete<int>	a1(one_int);

	TC_CHECK(a1.isValid());
	TC_CHECK( a1.operator->() == one_int );

	{
		int*	test_int = a1.popOut();
		TC_CHECK(test_int == one_int);
		TC_CHECK(a1.isNull());
		a1 = test_int;
	}

	int*	another_int = new int;
	a1 = another_int;

	TC_CHECK(one_int == nullptr);

	TC_CHECK(a1.isValid());
	TC_CHECK( a1.operator->() == another_int);
*/
}

void	TestMem::correctness(void)
{
	this->testReference();
	// this->testResource();
}

///////////////////////////////////
// Old idea
// c++11 now...

template<typename T> void	move(T& src, T& dst)
{
	dst = src;
}

template<typename T> void	move(T*& src, T*& dst)
{
	*dst = *src;
	src = 0;
}


template<typename T> class DefaultCopy
{
public:

	static void move(T& src, T& dst)
	{
		dst = src;
	}
};


template<class T> class Test
{
protected:
	T*	t;
	int	nb;

public:
	Test(T* t) : t(t), nb(0)
	{		}

	void	move(Test<T>& dst)
	{
		dst.t = this->t;
		this->t = 0;
		dst.nb = this->nb;
	}

};

template<class T>	void	move(Test<T>& src, Test<T>& dst)
{
	src.move(dst);
}


template<class T, class M = DefaultCopy<T> > class Coincoin
{
public:
	Coincoin(void)
	{
		T f; T p;
		M::move(f, p);
	}

};

#include "cvs/res/AutoDelete.hpp"

void	TestMem::testIdea(void)
{

	int	a = 1, b = 0;
	move(a, b);

	int*	pa = new int;
	int*	pb = new int;
	AutoDelete<int>	clean_pa(pa);	*pa = 1;
	AutoDelete<int>	clean_pb(pb);	*pb = 0;

	move(pa, pb);

	Test<int>	ta(pa), tb(pb);
	move(ta, tb);

}
