/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"mem/TestArrays.hpp"
#include "mem/Array.hpp"
namespace NPCO {
	// Force all code generation
	template class Array<double>;
}
#include "mem/Rack.hpp"
namespace NPCO {
	// Force all code generation
	template class Rack<float>;
}

#include "testUnit/TestMacros.hpp"

using namespace NPCO;
using namespace NTestUnit;

TestArrays::TestArrays(void)
{	}

TestArrays::~TestArrays(void)
{	}

void	TestArrays::testArray(void)
{

}

void	TestArrays::testRack(void)
{
	const int	width = 5;
	const int	height= 10;

	Rack<int>	rack(width, height);
	bool ok;

	ok = (rack.height() == height);
	TC_CHECK(ok);
	ok = (rack.width() == width);
	TC_CHECK(ok);

	// Fill
	for(int j = 0 ; j < height ; ++j)
	{
		for(int i = 0 ; i < width ; ++i)
		{
			rack.cell(i, j) = i;
		}
	}

	// Test data
	for(int j = 0 ; j < height ; ++j)
	{
		for(int i = 0 ; i < width ; ++i)
		{
			ok = (rack.cell(i, j) == i);
			TC_CHECK(ok);
		}
	}

	// Test insert shelfs
	const int	insertShelf = height / 2;
	const int	insertShelfNb= 4;

	rack.insertShelves(insertShelf, insertShelfNb);
	int newHeight = rack.height();
	ok = (newHeight == height + insertShelfNb);
	TC_CHECK(ok);
	ok = (rack.width() == width);	// Width shouldn't change
	TC_CHECK(ok);

	for(int i = 0 ; i < width ; ++i)
	{
		int j = 0;
		for( ; j < insertShelf ; ++j)
		{
			ok = (rack.cell(i, j) == i);
			TC_CHECK(ok);
		}

		for(int j2 = j+insertShelfNb ; j2 < newHeight ; ++j2)
		{
			ok = (rack.cell(i, j2) == i);
			TC_CHECK(ok);
		}
	}

	// Return to normal
	rack.removeShelves(insertShelf, insertShelfNb);
	ok = (rack.height() == height);
	TC_CHECK(ok);
	ok = (rack.width() == width);	// Width shouldn't change
	TC_CHECK(ok);

	// Test data
	for(int j = 0 ; j < height ; ++j)
	{
		for(int i = 0 ; i < width ; ++i)
		{
			ok = (rack.cell(i, j) == i);
			TC_CHECK(ok);
		}
	}

	// Test insert cells
	const int	insertCell = width / 2;
	const int	insertCellNb= 2;

	rack.insertCells(insertCell, insertCellNb);
	int newWidth = rack.width();
	ok = (newWidth == width + insertCellNb);
	TC_CHECK(ok);
	ok = (rack.height() == height);	// Height shouldn't change
	TC_CHECK(ok);

	for(int j = 0 ; j < height ; ++j)
	{
		int i = 0;
		for( ; i < insertCell ; ++i)
		{
			ok = (rack.cell(i, j) == i);
			TC_CHECK(ok);
		}

		for(int i2 = i+insertCellNb ; i2 < newWidth ; ++i2, ++i)
		{
			ok = (rack.cell(i2, j) == i);
			TC_CHECK(ok);
		}
	}

	// Return to normal
	rack.removeCells(insertCell, insertCellNb);
	ok = (rack.width() == width);
	TC_CHECK(ok);
	ok = (rack.height() == height);	// Height shouldn't change
	TC_CHECK(ok);

	// Test data
	for(int j = 0 ; j < height ; ++j)
	{
		for(int i = 0 ; i < width ; ++i)
		{
			ok = (rack.cell(i, j) == i);
			TC_CHECK(ok);
		}
	}

	// Test shrink than grow
	rack.setWidth(0);	rack.setHeight(0);
	rack.setWidth(2);	rack.setHeight(1);

	rack.setWidth(0);	rack.setHeight(0);
	rack.setHeight(1);	rack.setWidth(2);
}

void	TestArrays::correctness(void)
{
	this->testArray();
	this->testRack();
}
