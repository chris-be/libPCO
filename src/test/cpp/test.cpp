/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include "testUnit/TestRunner.hpp"
#include "testUnit/TestMacros.hpp"

#include "Stamp_TestLibPCO.hpp"
#include "Stamp_LibPCO.hpp"

// Test Classes
#include "mem/TestMem.hpp"
#include "cvs/TestRes.hpp"
#include "cvs/TestWalker.hpp"
#include "mem/TestArrays.hpp"
#include "cvs/TestUnion.hpp"

#include "logic/TestCVS_Find.hpp"

#include "col/TestStore.hpp"
#include "col/TestFixedHeap.hpp"
#include "col/TestSortedChunk.hpp"
#include "col/TestChunkList.hpp"

#include "col/trees/TestAVLTree.hpp"
#include "col/TestSets.hpp"

#include "cvs/TestE_Bag.hpp"
#include "string/TestEncoding.hpp"
#include "string/TestString.hpp"

#include "col/TestSTLBags.hpp"

#include "math/TestMath.hpp"
#include "text/TestText.hpp"

#include "logic/TestLogicSets.hpp"
#include "file/TestFile.hpp"

#include "program/TestProgram.hpp"

#include "col/TestColSpeed.hpp"

#include <iostream>

using namespace NPCO;
using namespace NTestUnit;

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
	std::cout << "Running: " << std::endl;
	Stamp_TestLibPCO	testLibPCO;
	testLibPCO.echoInfo();
	std::cout << std::endl << std::flush;

	std::cout << "Test of: " << std::endl;
	Stamp_LibPCO		libPCO;
	libPCO.echoInfo();
	std::cout << std::endl << std::endl << std::flush;

	TestRunner testRunner;
	TestName name;

#ifdef NDEBUG
	name				= ADD_TESTC			(testRunner, TestColSpeed);
	return testRunner.run(ETestMode::speed);
#endif

	name				= ADD_TESTC		(testRunner, TestMem);
	name				= ADD_DEP_TESTC	(testRunner, name, TestRes);

	name				= ADD_DEP_TESTC	(testRunner, name, TestWalker);
	TestName memName	= ADD_DEP_TESTC	(testRunner, name, TestArrays);

	name				= ADD_TESTC		(testRunner, TestUnion);

	TestName fdName		= ADD_TESTC		(testRunner, TestCVS_Find);

	TestName storeName	= ADD_DEP_TESTC	(testRunner, memName, TestStore);

	TestName fhName		= ADD_DEP_TESTC	(testRunner, memName, TestFixedHeap);
	name				= ADD_DEP_TESTC	(testRunner, fhName, TestSortedChunk);
						testRunner.addDependancy(fdName, name);

	name				= ADD_DEP_TESTC	(testRunner, memName, TestChunkList);
	TestName treeName	= ADD_DEP_TESTC	(testRunner, name, TestAVLTree);
	name				= ADD_DEP_TESTC	(testRunner, treeName, TestSets);
						testRunner.addDependancy(fdName, name);

	name				= ADD_DEP_TESTC (testRunner, treeName, TestLogicSets);

	name				= ADD_DEP_TESTC	(testRunner, storeName, TestE_Bag);

	TestName stringEnc	= ADD_DEP_TESTC	(testRunner, name, TestEncoding);
	TestName stringName	= ADD_DEP_TESTC	(testRunner, stringEnc, TestString);

	// Debug note: fileName depends already of memName but there is a bug for ordering
	// tests, so left as this for the moment
	TestName fileName	= ADD_DEP_TESTC	(testRunner, stringName, TestFile);
						// testRunner.addDependancy(memName, fileName);

	TestName mathName	= ADD_TESTC		(testRunner, TestMath);

	TestName textName	= ADD_DEP_TESTC	(testRunner, stringEnc, TestText);

	name				= ADD_DEP_TESTC	(testRunner, stringName, TestProgram);

	TestName stlName	= ADD_TESTC		(testRunner, TestSTLBags);
	name				= ADD_DEP_TESTC	(testRunner, stlName, TestColSpeed);

	int rc = testRunner.run(ETestMode::correctness);
#ifdef NDEBUG
	// Test speed, volumetry in release mode only
	if((rc == 0) && !libPCO.hasPredicate())
	{
		rc = testRunner.run(ETestMode::speed);
	}

	if((rc == 0) && !libPCO.hasPredicate())
	{
		// rc = testRunner.run(ETestMode::volumetry);
	}
#endif

	return rc;
}
