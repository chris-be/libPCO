/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"col/TestSTLBags.hpp"
#include "cvs/stl/Vector.hpp"
// #include "cvs/iter/E_HasIter.hpp"
#include "cvs/bag/E_RowBag.hpp"
#include "col/cvs/E_Head.hpp"
#include "col/cvs/E_Tail.hpp"

#include "testUnit/TestMacros.hpp"

namespace NPCO {
	// Force all code generation
	template class Vector<double>;
}

using namespace NPCO;
using namespace NTestUnit;

TestSTLBags::TestSTLBags(void)
{	}

TestSTLBags::~TestSTLBags(void)
{	}

void	TestSTLBags::correctness(void)
{
	Vector<int> blabla;

	int i;
	// Push 0 -> 9
	for(i = 0 ; i < 10 ; ++i)
	{
		blabla.add(i);
	}
	// Check
	int test = 0;
	for(auto cur = e_cursor(blabla) ; cur.ok() ; ++cur)
	{
		TC_CHECK(*cur == test);
		++test;
	}

	typename decltype(blabla)::TWrapped& stlBag = blabla.getWrapped();
	// Reverse 9 -> 0
	for(auto rid = e_rider(stlBag) ; rid.ok() ; ++rid)
	{
		*rid = --i;
	}
	// Check
	for(auto cur = e_cursor(stlBag) ; cur.ok() ; ++cur)
	{
		--test;
		TC_CHECK(*cur == test);
	}

	test = 4;
	TC_CHECK(test = blabla[test]);

	// Put 0 from 0 -> 5
	int value = 0;
	for(auto rid = e_rider(blabla, 0, 5) ; rid.ok() ; ++rid)
	{
		*rid = value;
	}
	// Check
	for(auto cur = e_cursor(blabla, 0, 5) ; cur.ok() ; ++cur)
	{
		TC_CHECK(*cur == value);
	}

	TC_CHECK(e_head(blabla) == 0);
	TC_CHECK(e_tail(blabla) == 0);
	auto size = blabla.size();
	test = e_pullTail(blabla);
	TC_CHECK(test == 0);
	TC_CHECK(blabla.size() == size-1);

	Vector<Vector<char>> hop;
	Vector<char>	v;	v.add('a');
	hop.add(v);

	TC_CHECK(e_head(hop).size() == 1);
	TC_CHECK(e_tail(hop)[0] == 'a');

	Vector<char>& hello = e_head(hop);
}
