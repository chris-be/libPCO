/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include "col/TestSortedChunk.hpp"

#include "col/SortedChunk.hpp"
#include "math/MathClassifier.hpp"
#include "cvs/cfr/ReverseClassifier.hpp"
#include "cvs/iter/E_HasIter.hpp"

#include "testUnit/TestMacros.hpp"

// #include <iostream>

namespace NPCO {
	// Force all code generation
	template class SortedChunk<float, MathClassifier<float>>;
}

using namespace NPCO;

TestSortedChunk::TestSortedChunk(void)
{	}

TestSortedChunk::~TestSortedChunk(void)
{	}

void	TestSortedChunk::correctness(void)
{
	int size = 10;
	SortedChunk<char, MathClassifier<char>> schunk(size);

	schunk.add(char(4));
	schunk.add(char(6));

	schunk.add(char(2));
	auto it_r = schunk.iter_r(char(2));
	TC_CHECK(it_r.isValid());
	TC_CHECK(it_r.operator->() == schunk._da());
	schunk.removeAt(0);

	bool isOk = schunk.remove(char(6));
	TC_CHECK(isOk);

	char c = 10;
	for(int i = size - schunk.size() ; i > 0 ; --i, ++c)
	{
		schunk.add(c);
	}

	auto		cur = e_cursor(schunk);
	char previous = -1;
	for( ; cur.ok() ; ++cur)
	{
		char t = *cur;
		ClassifyValue cv = schunk.getClassifier().classify(previous, t);
		TC_CHECK( e_isBefore(cv) );
		previous = t;
	}
}

void	TestSortedChunk::speed(void)
{
	this->speed(32768);

}

void	TestSortedChunk::speed(int higherBound)
{
	TC_PREDICATE(higherBound >= 0);

	int lowerBound = 0;
	int midBound = (lowerBound + higherBound) / 2;

	// Reverse to force "mem moves"
	SortedChunk<int, ReverseClassifier<MathClassifier<int>>> sc(higherBound+1);
	for(int i = lowerBound ; i <= higherBound ; ++i)
	{
		sc.add(i);
	}

	for(int i = midBound ; i <= higherBound ; ++i)
	{
		int v = sc.moveOut(midBound);
//		std::cout << v << " ";
	}
//	std::cout << std::endl;

	for(int i = midBound ; i <= higherBound ; ++i)
	{
		sc.add(i);
	}

	for( ; !sc.isEmpty() ; )
	{
		int v = sc.moveOut(0);
//		std::cout << v << " ";
	}

}
