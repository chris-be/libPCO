/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"col/TestFixedHeap.hpp"
#include "col/FixedHeap.hpp"
namespace NPCO {
	// Force all code generation
	template class FixedHeap<float>;
}

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

TestFixedHeap::TestFixedHeap(void)
{	}

TestFixedHeap::~TestFixedHeap(void)
{	}

void	TestFixedHeap::correctness(void)
{
	FixedHeap<char> fh(10);

	fh.add(char(2));
	fh.add(char(4));
	fh.add(char(6));
	array_size i = fh.size()-1;
	TC_CHECK(i == 2);
	fh.removeAt(i);
}

void	TestFixedHeap::speed(void)
{
	this->speed(32768);

}

void	TestFixedHeap::speed(int higherBound)
{
	TC_PREDICATE(higherBound >= 0);

	int lowerBound = 0;
	int midBound = (lowerBound + higherBound) / 2;

	FixedHeap<int> fh(higherBound+1);
	for(int i = lowerBound ; i <= higherBound ; ++i)
	{
		fh.add(i);
	}

	for(int i = midBound ; i <= higherBound ; ++i)
	{
		int v = fh.moveOut(midBound);
//		std::cout << v << " ";
	}
//	std::cout << std::endl;

	for(int i = midBound ; i <= higherBound ; ++i)
	{
		fh.add(i);
	}

	for( ; !fh.isEmpty() ; )
	{
		int v = fh.moveOut(0);
//		std::cout << v << " ";
	}

}
