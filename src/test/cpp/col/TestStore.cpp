/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"col/TestStore.hpp"
#include "col/Store.hpp"
namespace NPCO {
	// Force all code generation
	template class Store<float>;
}

#include "col/Test_DynBag.hpp"

#include "testUnit/TestMacros.hpp"

using namespace NPCO;
using namespace NTestUnit;

TestStore::TestStore(void)
{	}

TestStore::~TestStore(void)
{	}

void	TestStore::correctness(void)
{
	Store<int>				store(128);
	Test_DynBag<Store<int>>	subTest(store);

	MemSample<int, 64>	buffer;
	buffer.randomFill();

	subTest.writeRead(buffer);
}

void	TestStore::speed(void)
{
	// Store<int>				store(128);
	// TestDynBag<Store<int>>	subTest(store);

	// subTest.speed();
}
