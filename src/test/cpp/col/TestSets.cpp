/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"col/TestSets.hpp"
#include "col/SetTree.hpp"
#include "math/MathClassifier.hpp"
namespace NPCO {
	// Force all code generation
	template class SetTree<float, MathClassifier<float>>;
}
#include "col/MapTree.hpp"
namespace NPCO {
	// Force all code generation
	template class MapTree<float, double, MathClassifier<float>>;
}
#include "cvs/iter/E_HasIndexIter.hpp"

#include "col/MapTreeList.hpp"
namespace NPCO {
	// Force all code generation
	template class MapTreeList<float, double, MathClassifier<float>>;
}

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

TestSets::TestSets(void)
{	}

TestSets::~TestSets(void)
{	}

void	TestSets::testSet(void)
{
	SetTree<double, MathClassifier<double>>		s1;
	array_size sz = 0;

	s1.add(1.0);		++sz;
	s1.add(2.5);		++sz;
	s1.add(2.0);		++sz;
	TC_CHECK(s1.size() == sz);

	s1.remove(2.6);
	TC_CHECK(s1.size() == sz);

	s1.remove(2.0);		--sz;
	TC_CHECK(s1.size() == sz);

	s1.add(2.0);		++sz;
	TC_CHECK(s1.size() == sz);

	double prev = -10.0;
	for(auto cur = e_cursor(s1) ; cur.ok() ; ++cur)
	{
		double test = *cur;
		TC_CHECK(prev < test);
		prev = test;
	}

}

void	TestSets::testMap(void)
{
	MapTree<int, char, MathClassifier<int>>	m1;
	array_size sz = 0;

	m1.add(1, 'B');		++sz;
	m1.add(2, 'Y');		++sz;
	m1.add(4, 'a');		++sz;
	m1.add(5, 'z');		++sz;

	TC_CHECK(m1.size() == sz);

	m1.remove(10);
	TC_CHECK(m1.size() == sz);
	m1.remove(2);		--sz;
	TC_CHECK(m1.size() == sz);

	m1.add(2, 'Z');		++sz;
	TC_CHECK(m1.size() == sz);

	int prevKey = -20;
	char prevValue = 0;
	for(auto cur = e_indexCursor(m1) ; cur.ok() ; ++cur)
	{
		int testKey = cur.key();
		TC_CHECK(prevKey < testKey);
		prevKey = testKey;

		char testValue = *cur;
		TC_CHECK(prevValue < testValue);
		prevValue = testValue;
	}

	auto it = m1.iter_r_min(3);
	TC_CHECK(it.isValid());
	TC_CHECK(it.key() == 4);

	it = m1.iter_r_min(6);
	TC_CHECK(!it.isValid());

	it = m1.iter_r_max(3);
	TC_CHECK(it.isValid());
	TC_CHECK(it.key() == 2);

	it = m1.iter_r_max(0);
	TC_CHECK(!it.isValid());

	m1.clear();
	int k = 0;
	ChunkList<char>		pattern;
	for(int i = 0 ; i <= 256 ; ++i)
	{	// @todo Math.rand()
		char c =  (char)(i);
		pattern.add(c);
		m1.add(k, c);
		++k;
	}

	k = 0;
	auto pcur = e_cursor(pattern);
	for(auto mcur = e_indexCursor(m1) ; mcur.ok() ; ++mcur, ++pcur)
	{
		TC_CHECK(	pcur.ok()			);
		TC_CHECK(	mcur.key() == k		);
		TC_CHECK(	*mcur == *pcur		);
		++k;
	}
}

void	TestSets::testMapList(void)
{
	MapTreeList<int, char, MathClassifier<int>>	m1;
	array_size sz = 0;

	m1.add(1, 'B');		++sz;
	m1.add(1, 'B');		++sz;
	m1.add(2, 'a');		++sz;
	m1.add(2, 'a');		++sz;
	m1.add(5, 'N');		++sz;
	m1.add(5, 'n');		++sz;
	m1.add(5, 'N');		++sz;

	TC_CHECK(m1.size() == sz);

	m1.remove(5);		sz-=3;
	TC_CHECK(m1.size() == sz);

	int i = 1;
	for(auto cur = e_indexCursor(m1) ; cur.ok() ; ++cur, ++i)
	{
		int key = (i / 2);
		if((i % 2) == 1) ++key;

		int testKey = cur.key();
		TC_CHECK(key == testKey);

		char value = (testKey == 1) ? 'B' : 'a';
		char testValue = *cur;
		TC_CHECK(value == testValue);
	}

}

void	TestSets::correctness(void)
{
	this->testSet();
	this->testMap();
	this->testMapList();
}

void	TestSets::speed(void)
{

}
