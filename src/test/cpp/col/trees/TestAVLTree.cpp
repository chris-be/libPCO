/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"col/trees/TestAVLTree.hpp"
#include "col/trees/AVLTree.hpp"
#include "cvs/EmptyClass.hpp"
#include "math/MathClassifier.hpp"
#include "cvs/iter/E_HasIndexIter.hpp"

#include "testUnit/TestMacros.hpp"

namespace NPCO {
	// Force all code generation
	template class AVLTree<float, EmptyClass, MathClassifier<float>>;
}

using namespace NPCO;

TestAVLTree::TestAVLTree(void)
{	}

TestAVLTree::~TestAVLTree(void)
{	}

void	TestAVLTree::correctness(void)
{
	bool tres;
	AVLTree<char, EmptyClass, MathClassifier<char>>	tree;

	// Test add
	tree.add('c');
	tree.add('a');
	tree.add('b');

	// Test has
	tres = tree.hasKey('a');
	TC_CHECK(tres);

	// Test add, remove
	tree.add('d');
	tree.remove('a');

	tres = tree.hasKey('a');
	TC_CHECK(!tres);

	// Test min, max
	char min = *tree.min();
	TC_CHECK(min == 'b');
	char max = *tree.max();
	TC_CHECK(max == 'd');

	// Some values
	tree.add('B');
	tree.add('f');
	tree.add('e');
	tree.add('A');
	tree.add('a');

	tree.add('C');
	tree.add('D');
	tree.add('E');
	tree.add('F');
	tree.add('G');
	tree.add('H');

	tree.add('1');
	tree.add('2');
	tree.add('3');
	tree.add('4');
	tree.add('5');
	tree.add('6');

	tree.add('[');
	tree.add('@');
	tree.add(']');

	// Test copy
	auto	cpy(tree);
	TC_CHECK(cpy.size() == tree.size());
	auto	srcCur = e_indexCursor(tree);
	auto	cpyCur = e_indexCursor(cpy);

	for( ; srcCur.ok() ; ++srcCur, ++cpyCur)
	{
		TC_CHECK(cpyCur.ok());
		ClassifyValue cv = tree.getClassifier().classify(srcCur.key(), cpyCur.key());
		bool sameKey = e_isSame(cv);
		TC_CHECK(sameKey);
		// bool sameValue = srcCur.current() == cpyCur.current();
		// TC_CHECK(sameValue);
	}


//	std::cout << tree << std::endl << std::endl;
/*
	std::cout << "Test cursor" << std::endl;
	std::cout << "Forward: ";
	bool f = true;
	for(auto cur = tree.cursor() ; cur.okFwd() ; ++cur)
	{
		if(f) f = !f;
		else std::cout << ", ";
		std::cout << cur.key() << std::flush;
	}
	std::cout << std::endl;

	std::cout << "Backward: ";
	auto cur = tree.cursor();
	f = true;
	for(cur.toLast() ; cur.okBack() ; --cur)
	{
		if(f) f = !f;
		else std::cout << ", ";
		std::cout << cur.key() << std::flush;
	}
	std::cout << std::endl;

	std::cout << "Forward & Backward: ";
	cur.toFirst();
	f = true;
	for(int i = 4 ; (i > 0) && cur.okFwd() ; --i, ++cur)
	{
		if(f) f = !f;
		else std::cout << ", ";
		std::cout << cur.key() << std::flush;
	}

	std::cout << ", (reverse)";
	for(int i = 4 ; (i > 0) && cur.okBack() ; --i, --cur)
	{
		if(f) f = !f;
		else std::cout << ", ";
		std::cout << cur.key() << std::flush;
	}

	std::cout << ", (reverse)";
	for(int i = 4 ; (i > 0) && cur.okFwd() ; --i, ++cur)
	{
		if(f) f = !f;
		else std::cout << ", ";
		std::cout << cur.key() << std::flush;
	}
	std::cout << std::endl;
*/

	// Bug specific
	decltype(tree)	it;
	it.add(1);
	it.add(2);
	it.add(4);
	it.add(5);
	it.remove(2);
}

void	TestAVLTree::speed(void)
{
	speed(0, 65535);
}

void	TestAVLTree::speed(int lowerBound, int higherBound)
{
	TC_PREDICATE((lowerBound >= 0) && (higherBound >= 0));
	TC_PREDICATE(lowerBound < higherBound);

	int midBound = (lowerBound + higherBound) / 2;

	AVLTree<int, EmptyClass, MathClassifier<int>> at;
	for(int i = lowerBound ; i < higherBound ; ++i)
	{
		at.add(i);
	}

	for(int i = lowerBound ; i < midBound ; ++i)
	{
		at.remove(i);
	}

	for(int i = lowerBound ; i < midBound ; ++i)
	{
		at.add(i);
	}

	for( ; !at.isEmpty() ; )
	{
		const int* min = at.min();
		// std::cout << *min << " ";
		at.remove(*min);
	}

//	std::cout << std::flush;

}
