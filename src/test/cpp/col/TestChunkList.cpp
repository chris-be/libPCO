/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"col/TestChunkList.hpp"
#include "col/ChunkList.hpp"

#include "testUnit/TestMacros.hpp"

namespace NPCO {
	// Force all code generation
	template class ChunkList<float>;
}

using namespace NPCO;

TestChunkList::TestChunkList(void)
{	}

TestChunkList::~TestChunkList(void)
{	}

void	TestChunkList::correctness(void)
{
	ChunkList<int> cl(4);
	for(int i = 0 ; i < 8 ; ++i)
	{
		cl.add(i);
	}

	int t = 0;
	for(auto cur = e_cursor(cl) ; cur.ok() ; ++cur)
	{
		TC_CHECK(*cur == t);
		++t;
	}

	TC_CHECK(cl.tail() == t-1);

	for(auto cur = e_backCursor(cl) ; cur.ok() ; --cur)
	{
		--t;
		TC_CHECK(*cur == t);
	}

	TC_CHECK(cl.head() == t);
}

void	TestChunkList::speed(void)
{
	this->speed(0, 32768);

}

void	TestChunkList::speed(int lowerBound, int higherBound)
{
	TC_PREDICATE((lowerBound >= 0) && (higherBound >= 0));
	TC_PREDICATE(lowerBound < higherBound);

	int midBound = (lowerBound + higherBound) / 2;

	ChunkList<int> cl(32);
	for(int i = lowerBound ; i <= higherBound ; ++i)
	{
		cl.add(i);
	}

	for(int i = midBound ; i <= higherBound ; ++i)
	{
		int v = cl.pullTail();
//		std::cout << v << " ";
	}
//	std::cout << std::endl;

	for(int i = midBound ; i <= higherBound ; ++i)
	{
		cl.add(i);
	}

	for( ; !cl.isEmpty() ; )
	{
		int v = cl.pullTail();
//		std::cout << v << " ";
	}
}
