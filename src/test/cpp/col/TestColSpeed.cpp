/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"col/TestColSpeed.hpp"
#include "testUnit/MemSample.hpp"
#include "time/Chronometer.hpp"

// Col
#include "col/Store.hpp"
#include "col/ChunkList.hpp"
#include "col/MapTree.hpp"
#include "math/MathClassifier.hpp"

// STL
#include <vector>
#include <list>
#include <map>

#include "col/Test_DynBag.hpp"

#include "testUnit/TestMacros.hpp"

#include <iostream>

using namespace NPCO;
using namespace NTestUnit;

//! Default test repeat number
const int	bag_test_nb		= 16384;
const int	map_test_nb		= 4096;

//! Default test data size
const int	bag_sample_size	= 32768;
const int	map_sample_size	= 8192;

//! Configuration for one test
template<int TSize>
class TestConfig {
public:
	using TData	= MemSample<int, TSize>;
protected:
	//! How many time the same test is done (to get an average)
	int			repeatNb;
	//! Test data
	TData		data;
public:
	TestConfig(int repeatNb, bool random = true)
	 : repeatNb(repeatNb), data(random)
	{	}

	int			getRepeatNb(void) const
	{	return this->repeatNb;	}

	const TData&	getData(void) const
	{	return this->data;	}

	TData&			getData(void)
	{	return this->data;	}

};

TestColSpeed::TestColSpeed(void)
{	}

TestColSpeed::~TestColSpeed(void)
{	}

void	TestColSpeed::speed(void)
{
	this->dynBags();
	this->maps();
}

// Fill bag (clear it before), walk through
template<class PDynBag, int TSize>
static void	testDynBag(PDynBag bag, const TestConfig<TSize>& config, const char* name)
{
	PCO_ASSERT(config.getRepeatNb() > 0);

	const int testNb = config.getRepeatNb();
	const int dataNb = config.getData().size();
	Chronometer	chrono;
	Nanotime	delta;

	// Fill
	std::cout << testNb << " fill of " << dataNb << " int for " << name << "\t" << std::flush;
	chrono.start();
	for(int i = testNb ; i > 0 ; --i)
	{
		bag.clear();
		for(auto cur = e_cursor(config.getData()) ; cur.ok() ; ++cur)
			bag.add(*cur);
	}
	chrono.stop();

	delta = chrono.elapsed();
	std::cout << ": " << delta.getSeconds() << "s and " << delta.getNanos() << "ns" << std::endl;

	// Walk through
	std::cout << testNb << " walk of " << dataNb << " int for " << name << "\t" << std::flush;
	chrono.start();
	for(int i = testNb ; i > 0 ; --i)
	{
		using TIterRead = typename PDynBag::TIterRead;
		[[maybe_unused]]	int t;
		const PDynBag& b = bag;
		for(TIterRead it_r = b.first(), l = b.last() ; it_r != l ; ++it_r)
		// for(auto cur = e_cursor(bag) ; cur.ok() ; ++cur)
			t = *it_r;
	}
	chrono.stop();

	delta = chrono.elapsed();
	std::cout << ": " << delta.getSeconds() << "s and " << delta.getNanos() << "ns" << std::endl;

	std::cout << std::flush;
}

// Fill STL bag (clear it before), walk through
template<class PDynBag, int TSize>
static void	testSTLDynBag(PDynBag bag, const TestConfig<TSize>& config, const char* name)
{
	PCO_ASSERT(config.getRepeatNb() > 0);

	const int testNb = config.getRepeatNb();
	const int dataNb = config.getData().size();
	Chronometer	chrono;
	Nanotime	delta;

	// Fill
	std::cout << testNb << " fill of " << dataNb << " int for " << name << "\t" << std::flush;
	chrono.start();
	for(int i = testNb ; i > 0 ; --i)
	{
		bag.clear();
		for(auto cur = e_cursor(config.getData()) ; cur.ok() ; ++cur)
			bag.push_back(*cur);
	}
	chrono.stop();

	delta = chrono.elapsed();
	std::cout << ": " << delta.getSeconds() << "s and " << delta.getNanos() << "ns" << std::endl;

	// Walk through
	std::cout << testNb << " walk of " << dataNb << " int for " << name << "\t" << std::flush;
	chrono.start();
	for(int i = testNb ; i > 0 ; --i)
	{
		using TIterRead = typename PDynBag::const_iterator;
		[[maybe_unused]]	int t;
		for(TIterRead it_r = bag.begin(), e = bag.end() ; it_r != e ; ++it_r)
			t = *it_r;
	}
	chrono.stop();

	delta = chrono.elapsed();
	std::cout << ": " << delta.getSeconds() << "s and " << delta.getNanos() << "ns" << std::endl;

	std::cout << std::flush;
}

void	TestColSpeed::dynBags(void)
{
	TestConfig<bag_sample_size>		config(bag_test_nb, true);

	Store<int>				pcoStore;
	std::vector<int>		stdVector;

	testDynBag(pcoStore			, config, "Store");
	testSTLDynBag(stdVector		, config, "std::vector");

	ChunkList<int>			pcolist;
	std::list<int>			stdList;

	testDynBag(pcolist			, config, "ChunkList");
	testSTLDynBag(stdList		, config, "std::list");
}

// Fill map (clear it before), walk through
template<class PMap, int TSize>
static void	testMap(PMap map, const TestConfig<TSize>& config, const char* name)
{
	PCO_ASSERT(config.getRepeatNb() > 0);

	const int testNb = config.getRepeatNb();
	const int dataNb = config.getData().size();
	Chronometer	chrono;
	Nanotime	delta;

	// Fill
	std::cout << testNb << " fill of " << dataNb << " int for " << name << "\t" << std::flush;
	chrono.start();
	for(int i = testNb ; i > 0 ; --i)
	{
		map.clear();
		for(auto cur = e_cursor(config.getData()) ; cur.ok() ; ++cur)
			map.add(*cur, *cur);
	}
	chrono.stop();

	delta = chrono.elapsed();
	std::cout << ": " << delta.getSeconds() << "s and " << delta.getNanos() << "ns" << std::endl;

	// HasKey
	std::cout << testNb << " hasKey of " << dataNb << " int for " << name << "\t" << std::flush;
	chrono.start();
	for(int i = testNb ; i > 0 ; --i)
	{
		[[maybe_unused]]	bool t;
		for(auto cur = e_cursor(config.getData()) ; cur.ok() ; ++cur)
			t = map.hasKey(*cur);
	}
	chrono.stop();

	delta = chrono.elapsed();
	std::cout << ": " << delta.getSeconds() << "s and " << delta.getNanos() << "ns" << std::endl;

	std::cout << std::flush;
}

// Fill STL map (clear it before), walk through
template<class PMap, int TSize>
static void	testSTLMap(PMap map, const TestConfig<TSize>& config, const char* name)
{
	PCO_ASSERT(config.getRepeatNb() > 0);

	const int testNb = config.getRepeatNb();
	const int dataNb = config.getData().size();
	Chronometer	chrono;
	Nanotime	delta;

	// Fill
	std::cout << testNb << " fill of " << dataNb << " int for " << name << "\t" << std::flush;
	chrono.start();
	for(int i = testNb ; i > 0 ; --i)
	{
		map.clear();
		for(auto cur = e_cursor(config.getData()) ; cur.ok() ; ++cur)
			map[*cur] = *cur;
	}
	chrono.stop();

	delta = chrono.elapsed();
	std::cout << ": " << delta.getSeconds() << "s and " << delta.getNanos() << "ns" << std::endl;

	// HasKey
	std::cout << testNb << " hasKey of " << dataNb << " int for " << name << "\t" << std::flush;
	chrono.start();
	for(int i = testNb ; i > 0 ; --i)
	{
		[[maybe_unused]]	bool t;
		for(auto cur = e_cursor(config.getData()) ; cur.ok() ; ++cur)
			t = map.find(*cur) == map.end();
	}
	chrono.stop();

	delta = chrono.elapsed();
	std::cout << ": " << delta.getSeconds() << "s and " << delta.getNanos() << "ns" << std::endl;

	std::cout << std::flush;
}

void	TestColSpeed::maps(void)
{
/*
	{
		TestConfig<8192>	config(4096, true);
		MapTree<int, int, MathClassifier<int>>	pcoMap;

		testMap(pcoMap, config, "MapTree");
		return;
	}
*/

	TestConfig<map_sample_size>		config(map_test_nb, true);

/*
	int i = 0;
	for(auto r = e_rider(ms) ; r.ok() ; ++r)
		*r = i++;
*/

	MapTree<int, int, MathClassifier<int>>	pcoMap;
	std::map<int, int>						stdMap;

// 8192 fill of MapTree		in : 42s and 450637000ns
// 8192 hasKey of MapTree	in : 9s and 405197000ns

// 8192 fill of MapTree		in : 39s and 36827000ns
// 8192 hasKey of MapTree	in : 7s and 91678000ns

	testMap(pcoMap			, config, "MapTree");
	testSTLMap(stdMap		, config, "std::map");
}
