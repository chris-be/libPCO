/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/TestEncoding.hpp"
#include "string/UTFDecoder.hpp"
#include "string/UTFEncoder.hpp"
#include "string/cvs/CVS_Unicode.hpp"
#include "col/Store.hpp"
#include "mem/cvs/E_MemBlock.hpp"

#include "testUnit/TestMacros.hpp"

#include <iostream>

using namespace NPCO;

// Test data

/*
	Thanks http://hapax.qc.ca/conversion.fr.html

	U+0010 <commande> (échappement transmission)
	Ç U+00C7 LETTRE MAJUSCULE LATINE C CÉDILLE
	a U+0061 LETTRE MINUSCULE LATINE A
	  U+0020 ESPACE
	α U+03B1 LETTRE MINUSCULE GRECQUE ALPHA
	β U+03B2 LETTRE MINUSCULE GRECQUE BÊTA
	豈 U+F900 IDÉOGRAMME DE COMPATIBILITÉ CJC-F900
	℣ U+2123 VERSICULE
	ⴰ U+2D30 LETTRE TIFINAGHE YA
	ⵥ U+2D65 LETTRE TIFINAGHE YAZZ
*/

	const char32_t *pattern = U"Ça αβ豈℣ⴰⵥ";

	// 10 C3 87 61 20 CE B1 CE B2 EF A4 80 E2 84 A3 E2 B4 B0 E2 B5 A5
	const char8_t	s08[] = {
		  0x10, 0xC3, 0x87, 0x61, 0x20, 0xCE, 0xB1, 0xCE, 0xB2, 0xEF, 0xA4
		, 0x80, 0xE2, 0x84, 0xA3, 0xE2, 0xB4, 0xB0, 0xE2, 0xB5, 0xA5
	};

	// 0010 00C7 0061 0020 03B1 03B2 F900 2123 2D30 2D65
	const char8_t	s16_BE[] = {
		  0x00, 0x10, 0x00, 0xC7, 0x00, 0x61, 0x00, 0x20, 0x03, 0xB1
		, 0x03, 0xB2, 0xF9, 0x00, 0x21, 0x23, 0x2D, 0x30, 0x2D, 0x65
	};

	const char8_t	s32_LE[] = {
		  0x10, 0x00, 0x00, 0x00	, 0xC7, 0x00, 0x00, 0x00
		, 0x61, 0x00, 0x00, 0x00	, 0x20, 0x00, 0x00, 0x00
		, 0xB1, 0x03, 0x00, 0x00	, 0xB2, 0x03, 0x00, 0x00
		, 0x00, 0xF9, 0x00, 0x00	, 0x23, 0x21, 0x00, 0x00
		, 0x30, 0x2D, 0x00, 0x00	, 0x65, 0x2D, 0x00, 0x00
	};

	const int s08_size = sizeof(s08)	/ sizeof(char8_t);
	const int s16_size = sizeof(s16_BE)	/ sizeof(char8_t);
	const int s32_size = sizeof(s32_LE)	/ sizeof(char8_t);
	const int nbChars = 10;

	const MemBlock<char8_t, int> pattern_08		= e_memBlock(s08	, s08_size);
	const MemBlock<char8_t, int> pattern_16_BE	= e_memBlock(s16_BE	, s16_size);
	const MemBlock<char8_t, int> pattern_32_LE	= e_memBlock(s32_LE	, s32_size);

TestEncoding::TestEncoding(void)
{	}

TestEncoding::~TestEncoding(void)
{	}

void	TestEncoding::decodeUtf08(void)
{
	auto		referencePattern = e_cursor(pattern_32_LE);
	UTFDecoder	refFlow(referencePattern, EUTF::_32_LE);

	auto		testPattern = e_cursor(pattern_08);
	UTFDecoder	tstFlow(testPattern, EUTF::_8);

	bool ok;
	int read = 0;
	char32_t ref;
	while(refFlow.read(ref))
	{
		ok = !(ref == CVS_Unicode::ErrorCode);
		TC_CHECK(ok);
		++read;

		char32_t t;
		ok = tstFlow.read(t);
		TC_CHECK(ok);
		ok = !(t == CVS_Unicode::ErrorCode);
		TC_CHECK(ok);

		ok = (t == ref);
		TC_CHECK(ok);
	}

	TC_CHECK(read == nbChars);
}

void	TestEncoding::decodeUtf16(void)
{
	auto		referencePattern = e_cursor(pattern_32_LE);
	UTFDecoder	refFlow(referencePattern, EUTF::_32_LE);

	auto		testPattern = e_cursor(pattern_16_BE);
	UTFDecoder	tstFlow(testPattern, EUTF::_16_BE);

	bool ok;
	int read = 0;
	char32_t ref;
	while(refFlow.read(ref))
	{
		ok = !(ref == CVS_Unicode::ErrorCode);
		TC_CHECK(ok);
		++read;

		char32_t t;
		ok = tstFlow.read(t);
		TC_CHECK(ok);
		ok = !(t == CVS_Unicode::ErrorCode);
		TC_CHECK(ok);

		ok = (t == ref);
		TC_CHECK(ok);
	}

	TC_CHECK(read == nbChars);
}

void	TestEncoding::decodeUtf32(void)
{
	auto		referencePattern = e_cursor(pattern_32_LE);
	UTFDecoder	refFlow(referencePattern, EUTF::_32_LE);

	auto		testPattern = e_cursor(pattern_32_LE);
	UTFDecoder	tstFlow(testPattern, EUTF::_32_LE);

	bool ok;
	int read = 0;
	char32_t ref;
	while(refFlow.read(ref))
	{
		ok = !(ref == CVS_Unicode::ErrorCode);
		TC_CHECK(ok);
		++read;

		char32_t t;
		ok = tstFlow.read(t);
		TC_CHECK(ok);
		ok = !(t == CVS_Unicode::ErrorCode);
		TC_CHECK(ok);

		ok = (t == ref);
		TC_CHECK(ok);
	}

	TC_CHECK(read == nbChars);
}

void	TestEncoding::encodeUtf08(void)
{
	auto			referencePattern = e_cursor(pattern_32_LE);

	UTFDecoder		refFlow(referencePattern, EUTF::_32_LE);
	UTFEncoder		tstFlow(refFlow, EUTF::_8);

	Store<uw_08>	testPattern(s08_size);

	bool ok;

	char8_t chr;
	while(tstFlow.read(chr))
	{
		testPattern.add(chr);
	}

	TC_CHECK(testPattern.size() == s08_size);
	ok = ::memcmp(s08, testPattern._da(), s08_size) == 0;
	TC_CHECK(ok);
}

void	TestEncoding::encodeUtf16(void)
{
	auto			referencePattern = e_cursor(pattern_32_LE);

	UTFDecoder		refFlow(referencePattern, EUTF::_32_LE);
	UTFEncoder		tstFlow(refFlow, EUTF::_16_BE);

	Store<uw_08>	testPattern(s16_size);

	bool ok;

	char8_t chr;
	while(tstFlow.read(chr))
	{
		testPattern.add(chr);
	}

	TC_CHECK(testPattern.size() == s16_size);
	ok = ::memcmp(s16_BE, testPattern._da(), s16_size) == 0;
	TC_CHECK(ok);
}

void	TestEncoding::encodeUtf32(void)
{
	auto			referencePattern = e_cursor(pattern_32_LE);

	UTFDecoder		refFlow(referencePattern, EUTF::_32_LE);
	UTFEncoder		tstFlow(refFlow, EUTF::_32_LE);

	Store<uw_08>	testPattern(s32_size);

	bool ok;

	char8_t chr;
	while(tstFlow.read(chr))
	{
		testPattern.add(chr);
	}
/*
	std::cout << std::hex;
	for(auto c = e_cursor(testPattern) ; c.ok() ; ++c)
	{
		std::cout << "0x" << (int)(*c) << " ";
	}
	std::cout << std::endl << std::flush;
	std::cout << std::dec;
*/
	TC_CHECK(testPattern.size() == s32_size);
	ok = ::memcmp(s32_LE, testPattern._da(), s32_size) == 0;
	TC_CHECK(ok);
}

void	TestEncoding::correctness(void)
{
	this->decodeUtf08();
	this->decodeUtf16();
	this->decodeUtf32();

	this->encodeUtf08();
	this->encodeUtf16();
	this->encodeUtf32();
}
