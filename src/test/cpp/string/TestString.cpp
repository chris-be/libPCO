/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/TestString.hpp"
#include "string/StringConstant.hpp"
#include "string/String.hpp"
#include "string/cvs/E_CStyleString.hpp"
#include "string/CIString.hpp"
#include "cvs/cfr/E_Classifier.hpp" // e_isSame

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

// Test data

	#define	T_PAT	"Cèti pô beau ?"
	const char8_t*	pattern_08 = U8_STR(T_PAT);
	const char16_t*	pattern_16 = u"" T_PAT;
	const char32_t*	pattern_32 = U"" T_PAT;
	#undef T_PAT

	const StringC	initState("Force getLocale at init...");

TestString::TestString(void)
{	}

TestString::~TestString(void)
{	}

void	TestString::testStringConstant(void)
{
	bool ok;

	ok = initState.getEncoding() != EStringEncoding::unknown;
	TC_CHECK(ok);

	StringC	test08(pattern_08);
	ok = (test08.size() == e_strlen(pattern_08));
	TC_CHECK(ok);

	StringC	test16(pattern_16);

	StringC	test32(pattern_32);
}

void	TestString::testString(void)
{
	bool ok;

	String	test08(pattern_08);

	String	test16(pattern_16);
	ok = e_isSame(test08, test16);
	TC_CHECK(ok);

	String	test32(pattern_32);
	ok = e_isSame(test32, test32);
	TC_CHECK(ok);

	const char8_t*	name = U8_STR("name");
	String	strName	= name;
	ok = e_isSame( strName.classify(name) );
	TC_CHECK(ok);

	CIString	test_8(pattern_08);

	CIString	test_sc(test32);
}

void	TestString::correctness(void)
{
	this->testStringConstant();
	this->testString();
}
