/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"logic/TestCVS_Find.hpp"
#include "logic/cvs/CVS_Find.hpp"
#include "math/MathClassifier.hpp"

#include "testUnit/TestMacros.hpp"

using namespace NPCO;

TestCVS_Find::TestCVS_Find(void)
{	}

TestCVS_Find::~TestCVS_Find(void)
{	}

void	TestCVS_Find::correctness(void)
{
	this->testFindArray();
}

void	TestCVS_Find::testFindArray(void)
{
	char buffer[10] = { char(0), char(1), char(2), char(3), char(4), 'a', 'b', 'c', 'd', 'e' };
	array_size size = sizeof(buffer) / sizeof(char);
	char wanted = char(2);

	FindResult<array_size> findResult;

	CVS_Find::SelectorFunc<char> selector = [] (const char& t) -> bool { return t == char(2); };

	CVS_Find::find(findResult, buffer, size, selector);

	TC_CHECK(findResult.found() && (findResult.index() == 2));

	MathClassifier<char> comparer;

	wanted = 'a';
	CVS_Find::find(findResult, buffer, size, comparer, wanted);
	TC_CHECK(findResult.found() && (findResult.index() == 5));
}
