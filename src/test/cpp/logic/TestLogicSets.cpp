/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"logic/TestLogicSets.hpp"
#include "logic/set/Bijection.hpp"
namespace NPCO {
	// Force all code generation
	template class Bijection<int, double>;
}

#include "logic/set/Injection.hpp"
namespace NPCO {
	// Force all code generation
	template class Injection<int, double>;
}

#include "logic/expr/test/TestNotOperator.hpp"
#include "logic/expr/test/TestBooleanOr.hpp"
namespace NPCO {

	TestNotOperator	no;
	TestBooleanOr	bor;
}


#include "testUnit/TestMacros.hpp"

// TODO
#include "logic/LogicTree.hpp"

namespace NPCO {
	// Force all code generation
	template class LogicTree<int, double>;
	LogicTree<int, double> youhou;
}

using namespace NPCO;
using namespace NTestUnit;

void	TestLogicSets::testBijection(void)
{
	Bijection<int, double> bijou;
}

void	TestLogicSets::testInjection(void)
{
	Injection<int, double> injou;
}

TestLogicSets::TestLogicSets(void)
{	}

TestLogicSets::~TestLogicSets(void)
{	}

void	TestLogicSets::correctness(void)
{
	this->testBijection();
	this->testInjection();
}
