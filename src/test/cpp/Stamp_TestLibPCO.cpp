/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"Stamp_TestLibPCO.hpp"
#include "dbg/CompilerOptions.hpp"

#define		TEST_LIB_PCO_CODE			"UTs - LibPCO"
#define		TEST_LIB_PCO_VERSION		"0.00a"
#define		TEST_LIB_PCO_NAME			"Unit Tests for 'Petite Caisse à Outils'"

Stamp_TestLibPCO::Stamp_TestLibPCO(void)
 : CompileStamp(TEST_LIB_PCO_CODE, TEST_LIB_PCO_VERSION, TEST_LIB_PCO_NAME)
{
	this->debug			= LIB_PCO_DEBUG_COMPILED;
	this->predicate		= LIB_PCO_PREDICATE_COMPILED;
}
