/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_IO_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_IO_HPP_INCLUDED

#include "testUnit/TestClass.hpp"
#include "file/I_File.hpp"
#include "testUnit/MemSample.hpp"

/**
	Tests of File
 */
class TestFile : public NTestUnit::TestClass {

private:
	// Pattern to write and compare with read back
	static const int pattern_size = 256;
	NTestUnit::MemSample<NPCO::uw_08, pattern_size>	rwPattern;

	bool	testPattern(NPCO::IFile& file) const;

protected:
	void	testMemFile(void);
	void	testBufferedFile(void);

public:
	TestFile(void);
	virtual ~TestFile(void) override;

	virtual void correctness(void) override;

};

#endif
