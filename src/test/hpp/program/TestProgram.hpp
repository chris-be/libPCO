/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_PROGRAM_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_PROGRAM_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of Program
 */
class TestProgram : public NTestUnit::TestClass {

protected:
	void	testExecCommand(void);

public:
	TestProgram(void);
	virtual ~TestProgram(void) override = default;

	virtual void	correctness(void) override;

};

#endif
