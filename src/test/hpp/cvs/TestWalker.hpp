/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_WALKER_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_WALKER_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of Walker
 */
class TestWalker : public NTestUnit::TestClass {

protected:
	void	testFind(void);

public:
	TestWalker(void);
	virtual ~TestWalker(void) override;

	virtual void correctness(void) override;

};

#endif
