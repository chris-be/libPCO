/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_E_BAG_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_E_BAG_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of E_???Bag
 */
class TestE_Bag : public NTestUnit::TestClass {

protected:
	void	testE_Bag(void);
	void	testE_DynBag(void);
	void	testE_RowBag(void);

public:
	TestE_Bag(void);
	virtual ~TestE_Bag(void) override;

	virtual void correctness(void) override;

};

#endif
