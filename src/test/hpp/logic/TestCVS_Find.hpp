/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef TEST_LIB_PCO_TEST_CVS_FIND_HPP_INCLUDED
#define TEST_LIB_PCO_TEST_CVS_FIND_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of CVS_Find
 */
class TestCVS_Find : public NTestUnit::TestClass {

protected:
	void	testFindArray(void);

public:
	TestCVS_Find(void);
	virtual ~TestCVS_Find(void) override;

	virtual void	correctness(void) override;

};

#endif
