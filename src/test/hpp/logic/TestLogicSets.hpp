/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_LOGIC_SETS_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_LOGIC_SETS_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of Bijection / Injection
 */
class TestLogicSets : public NTestUnit::TestClass {

protected:
	void	testBijection(void);
	void	testInjection(void);

public:
	TestLogicSets(void);
	virtual ~TestLogicSets(void) override;

	virtual void	correctness(void) override;

};

#endif
