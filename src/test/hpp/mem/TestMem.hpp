/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_MEM_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_MEM_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of Mem : basics
 */
class TestMem : public NTestUnit::TestClass {

public:
	void	testReference(void);
	void	testResource(void);
	void	testIdea(void);

public:
	TestMem(void);
	virtual ~TestMem(void) override;

	virtual void correctness(void) override;

};

#endif
