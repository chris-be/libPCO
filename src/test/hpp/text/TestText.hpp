/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef TEST_LIB_PCO_TEST_TEXT_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_TEXT_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of text:
 */
class TestText : public NTestUnit::TestClass {

protected:
	void	regex(void);

public:
	TestText(void);
	virtual ~TestText(void) override;

	virtual void correctness(void) override;

};

#endif
