/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef TEST_LIB_PCO_TEST_ENCODING_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_ENCODING_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of string encoding conversions
 */
class TestEncoding : public NTestUnit::TestClass {

protected:
	// Test decodeInflow
	void	decodeUtf08(void);
	void	decodeUtf16(void);
	void	decodeUtf32(void);

	// Test encodeInflow
	void	encodeUtf08(void);
	void	encodeUtf16(void);
	void	encodeUtf32(void);

public:
	TestEncoding(void);
	virtual ~TestEncoding(void) override;

	virtual void correctness(void) override;

};

#endif
