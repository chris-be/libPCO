/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef TEST_LIB_PCO_TEST_STRING_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_STRING_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of string: ...
 */
class TestString : public NTestUnit::TestClass {

protected:
	void	testStringConstant(void);
	void	testString(void);

public:
	TestString(void);
	virtual ~TestString(void) override;

	virtual void correctness(void) override;

};

#endif
