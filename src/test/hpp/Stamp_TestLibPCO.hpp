/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_STAMP_TEST_LIB_PCO_HPP_INCLUDED
#define	TEST_LIB_PCO_STAMP_TEST_LIB_PCO_HPP_INCLUDED

#include "dbg/CompileStamp.hpp"

/*!
	TestLibPCO Compile info
*/
class Stamp_TestLibPCO
	: public NPCO::CompileStamp {

public:
	Stamp_TestLibPCO(void);

};

#endif
