/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef TEST_LIB_PCO_TEST_DYN_BAG_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_DYN_BAG_HPP_INCLUDED

#include "./Test_Bag.hpp"

/**
	Tests for class constrained by C_Bag
 */
template<class PDynBag>
class Test_DynBag : public NTestUnit::TestClass {
// Test_Bag<PDynBag> ??

protected:
	PDynBag&	bag;

public:
	Test_DynBag(PDynBag& bag);

	template<size_t nb>
	void	writeRead(NTestUnit::MemSample<typename PDynBag::TType, nb> buffer);

};

#include "imp/Test_DynBag.cppi"

#endif
