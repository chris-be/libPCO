/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef TEST_LIB_PCO_TEST_BAG_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_BAG_HPP_INCLUDED

#include "testUnit/TestClass.hpp"
#include "testUnit/MemSample.hpp"

/**
	Tests for class constrained by C_Bag
 */
template<class PBag>
class Test_Bag : public NTestUnit::TestClass {

protected:
	PBag&	bag;

public:
	Test_Bag(PBag& bag);

	template<size_t nb>
	void	writeRead(NTestUnit::MemSample<typename PBag::TType, nb> buffer);

};

#include "imp/Test_Bag.cppi"

#endif
