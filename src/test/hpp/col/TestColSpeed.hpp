/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_COL_SPEED_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_COL_SPEED_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Speed tests of collections (and STL)
 */
class TestColSpeed : public NTestUnit::TestClass {

protected:
	void	dynBags(void);
	void	maps(void);

public:
	TestColSpeed(void);
	virtual ~TestColSpeed(void) override;

	virtual void	speed(void) override;

};

#endif
