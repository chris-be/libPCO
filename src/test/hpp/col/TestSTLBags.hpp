/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_STL_BAGS_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_STL_BAGS_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of STL bags
 */
class TestSTLBags : public NTestUnit::TestClass {

public:
	TestSTLBags(void);
	virtual ~TestSTLBags(void) override;

	virtual void	correctness(void) override;

};

#endif
