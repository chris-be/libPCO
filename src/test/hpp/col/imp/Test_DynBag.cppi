/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

//#include	"col/Test_DynBag.hpp"
#include "cvs/bag/E_Bag.hpp"

#include "testUnit/TestMacros.hpp"

using namespace NTestUnit;
using namespace NPCO;

template<class PDynBag>
Test_DynBag<PDynBag>::Test_DynBag(PDynBag& bag)
 : bag(bag)
{
	NPCO::C_DynBag<PDynBag, typename PDynBag::TType, typename PDynBag::TSize>::ensure();
}

template<class PDynBag>
template<size_t nb>
void	Test_DynBag<PDynBag>::writeRead(MemSample<typename PDynBag::TType, nb> buffer)
{
	auto cur = e_cursor(buffer);

	// Fill dynamic bag
	for( ; cur.ok() ; ++cur)
	{
		this->bag.add(*cur);
	}

	bool ok = this->bag.size() == buffer.size();
	TC_CHECK(ok);

	// Check content
	auto test = e_cursor(this->bag);
	ok = buffer.checkContent(test);
	TC_CHECK(ok);

	// Launch sub tests
	Test_Bag<PDynBag>	bagTest(this->bag);
	bagTest.writeRead(buffer);
}
