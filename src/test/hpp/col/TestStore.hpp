/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_STORE_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_STORE_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of Store
 */
class TestStore : public NTestUnit::TestClass {

public:
	TestStore(void);
	virtual ~TestStore(void) override;

	// Insert, delete, ...
	virtual void	correctness(void) override;
	virtual void	speed(void) override;

};

#endif
