/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_SETS_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_SETS_HPP_INCLUDED

#include "testUnit/TestClass.hpp"

/**
	Tests of Set collections
 */
class TestSets : public NTestUnit::TestClass {

protected:
	void	testSet(void);
	void	testMap(void);
	void	testMapList(void);

public:
	TestSets(void);
	virtual ~TestSets(void) override;

	// Insert, delete, ...
	virtual void	correctness(void) override;

	virtual void	speed(void) override;

};

#endif
