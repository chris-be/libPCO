/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_LOGIC_TREE_HPP_INCLUDED
#define	LIB_PCO_LOGIC_TREE_HPP_INCLUDED

#include "col/MapTree.hpp"
#include "set/Injection.hpp"
#include "math/MathClassifier.hpp"
#include "crt/C_LogicId.hpp"

namespace NPCO {

/**
	Used to manage "trees":
		- identified nodes
		- father -> son(s) hierarchy
			- sons may be ordered

	@param PId @see C_LogicId
*/
template<class PId, class PNode>
class LogicTree {

protected:
	using TIdNode		= PId;
	using TNodes		= MapTree<PId, PNode, MathClassifier<PId>>;
	using THierarchy	= Injection<PId, PId>; // , MathClassifier<PId>>;

public:
	using TNodeSize		= typename TNodes::TSize;
	using TSonIdList	= typename THierarchy::TList;

protected:
	TNodes		nodes;
	//! @cache Last max id used
	PId			maxId;

	THierarchy	hierarchy;

	void	updateMaxId(const PId id);

public:
	LogicTree(void) = default;

	// Copy
	LogicTree(const LogicTree& ) = default;
	LogicTree& operator=(const LogicTree& ) = default;

	// Move
	LogicTree(LogicTree&& ) = default;
	LogicTree& operator=(LogicTree&& ) = default;

	bool	isEmpty(void) const;

	void	clear(void);

	//! Get node count
	TNodeSize	getNodeCount(void) const;

	void	addNode(const PId id, const PNode& node);
	void	moveInNode(const PId id, PNode&& node);

	//! @return Generated id
	PId		addNode(const PNode& node);
	PId		moveInNode(PNode&& node);

	const PNode&	getNode(const PId id) const;
	PNode&			getNode(const PId id);

	void	removeNode(const PId id);

	//! Link a son to a father
	void	link(const PId idFather, const PId idSon);

	bool	hasSon(const PId idFather) const;
	bool	hasFather(const PId idSon) const;

	const TSonIdList*	getSonIds(const PId idFather) const;
	PId		getFather(const PId idSon) const;

	//! Remove son from father
	void	unlinkSon(const PId idSon);
	//! Remove all sons from father
	void	unlinkFather(const PId idFather);

	//! Use with care
	const TNodes&	getNodes(void) const;
	TNodes&			getNodes(void);

};

} // N..

#include "imp/LogicTree.cppi"

#endif
