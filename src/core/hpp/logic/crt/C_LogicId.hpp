/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_LOGIC_ID_HPP_INCLUDED
#define	LIB_PCO_C_LOGIC_ID_HPP_INCLUDED

#include "pattern/math/C_Number.hpp"
#include "C_Counter.hpp"

namespace NPCO {

/**
	Contract for "logical" id.
*/
template<class PCd> class C_LogicId
	: public C_Number<PCd>, public C_Counter<PCd> {

private:
/*
	static void constraints(PCd* ptr)
	{
	}

	static void	_ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}
*/

public:
	static void	ensure(void)
	{
		// C_LogicId::_ensure();
		C_Number<PCd>::ensure();
		C_Counter<PCd>::ensure();
	}

	C_LogicId(void)
	{
		// C_LogicId::_ensure();
	}

};

} // N..

#endif
