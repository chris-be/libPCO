/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_COUNTER_HPP_INCLUDED
#define	LIB_PCO_C_COUNTER_HPP_INCLUDED

namespace NPCO {

// Default raz
template<class PType>
void	toZero(PType& counter)
{
	counter = 0;
}

// Default toSingle
template<class PType>
void	toSingle(PType& counter)
{
	counter = 1;
}

// Default isZero
template<class PType>
bool	isZero(const PType& counter)
{
	return (counter == 0);
}

// Default isSingle
template<class PType>
bool	isSingle(const PType& counter)
{
	return (counter == 1);
}

// Default isMultiple
template<class PType>
bool	isMultiple(const PType& counter)
{
	return (counter > 1);
}

/**
	Contract for "counter"
*/
template<class PCd> class C_Counter {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd = *ptr;
		      PCd&	      ctd = *ptr;

		// Compare
		[[maybe_unused]]	bool test = (const_ctd == const_ctd);

		test = isZero(const_ctd);
		test = isSingle(const_ctd);
		test = isMultiple(const_ctd);

		// Reset
		toZero(ctd);

		// Increment
		++ctd;

		// Decrement
		--ctd;
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_Counter(void)
	{
		C_Counter::ensure();
	}

};

} // N..

#endif
