/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FLAG_HPP_INCLUDED
#define	LIB_PCO_FLAG_HPP_INCLUDED

#include "low/Bit.hpp"

namespace NPCO {

/**
	Flag: handle flag with bits.

*/
template<class PType = unsigned int>
class Flag {

protected:
	PType	value;

public:
	Flag(void)
	 : value(0)
	{	}

	Flag(PType v)
	 : value(v)
	{
		PCO_INV_ARG(Bit::isFlag<PType>(v), "Not a flag");
	}

	Flag(const Flag& ) = default;
	Flag&	operator=(const Flag& ) = default;

	Flag(Flag&& ) = default;
	Flag&	operator=(Flag&& ) = default;

	PCO_INLINE
	operator PType() const
	{
		return this->value;
	}

/*
	static Flag	make(PType bitIndex)
	{
		return Flag( Bit::makeFlag<PType>(bitIndex) );
	}
*/

	static constexpr PType	make(PType bitIndex)
	{
//		static_assert(bitIndex < (sizeof(unsigned int)), "Wrong bit index");
		return 1 << bitIndex;
	}

};

} // N..

#endif
