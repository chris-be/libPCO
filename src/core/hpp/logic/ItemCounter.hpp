/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_ITEM_COUNTER_HPP_INCLUDED
#define	LIB_PCO_ITEM_COUNTER_HPP_INCLUDED

#include "col/MapTree.hpp"
#include "set/Cardinality.hpp"

namespace NPCO {

/**
	@convenience
	Used to count instances.
*/
template<class PType, class PCounter, class PClassifier = DefaultClassifier<PType>> class ItemCounter {

protected:
	using TMap		= MapTree<PType, PCounter, PClassifier>;

public:
	using TCounter	= PCounter;
	using TIterRead	= typename TMap::TIterRead;

protected:
	TMap		map;

	static PCounter	Zero(void)
	{
		PCounter c;	toZero(c);
		return c;
	}

public:
	ItemCounter(void) = default;

	void	clear(void)
	{
		this->map.clear();
	}

	//! Get counter
	PCounter	getCount(const PType& item) const
	{
		auto it = this->map.iter_r(item);
		return it.isValid() ? (*it) : Zero();
	}

	//! Increment counter
	void	inc(const PType& item)
	{
		auto it = this->map.iter_w(item);
		if(!it.isValid())
		{
			it = this->map.add(item);
			toZero( *it );
		}

		PCO_ASSERT(it.isValid());
		++( it.cell() );
	}

	//! Decrement counter - @return false if not present or already at 0
	bool	dec(const PType& item)
	{
		auto it = this->map.iter_w(item);
		if(!it.isValid())
		{
			return false;
		}

		PCounter& c = (*it);
		if(isZero(c))
		{
			return false;
		}

		--c;
	}

	bool	check(const PType& item, const Cardinality<PCounter>& card) const
	{
		PCounter count = this->getCount(item);
		return card.check(count);
	}

	TIterRead	first(void) const
	{
		return this->map.first();
	}

	TIterRead	last(void) const
	{
		return this->map.last();
	}

};

} // N..

#endif
