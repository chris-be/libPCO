/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_DUAL_FUNCTION_BASE_HPP_INCLUDED
#define	LIB_PCO_DUAL_FUNCTION_BASE_HPP_INCLUDED

#include "I_DualFunction.hpp"
#include "MonoFunctionBase.hpp"

namespace NPCO {

/**
	IDualFunction base implementation.
	@see IDualFunction
*/
template<class PResult, class PParam1, class PParam2> class DualFunctionBase
	: virtual public IDualFunction<PResult, PParam1, PParam2>
	, public MonoFunctionBase<PResult, PParam1> {

protected:
	PParam2	param2;

public:
	virtual PParam2 getP2(void) const override
	{
		return this->param2;
	}

	virtual void setP2(const PParam2 p2) override
	{
		this->param2 = p2;
	}

	/** @Convenience
		@param p1
		@param p2
	 */
	void set(const PParam1 p1, const PParam2 p2)
	{
		this->setP1(p1);
		this->setP2(p2);
	}

};

} // N..

#endif
