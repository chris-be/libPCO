/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_EXPRESSION_HPP_INCLUDED
#define	LIB_PCO_I_EXPRESSION_HPP_INCLUDED

namespace NPCO {

/**
	Contract for expression
	@param PResult Result type
 */
template<class PResult> class IExpression {

public:
	virtual ~IExpression(void)	{ }

	/** Evaluate expression - parameters must be set before !
		@return Result of eval
	*/
	virtual PResult eval(void) const = 0;

};

} // N..

#endif
