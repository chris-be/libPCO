/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MONO_FUNCTION_BASE_HPP_INCLUDED
#define	LIB_PCO_MONO_FUNCTION_BASE_HPP_INCLUDED

#include "I_MonoFunction.hpp"

namespace NPCO {

/**
	IMonoFunction base implementation.
	@see IMonoFunction
*/
template<class PResult, class PParam1> class MonoFunctionBase
	: virtual public IMonoFunction<PResult, PParam1> {

protected:
	PParam1	param1;

public:

	virtual PParam1 getP1(void) const override
	{
		return this->param1;
	}

	virtual void setP1(const PParam1 p1) override
	{
		this->param1 = p1;
	}

};

} // N..

#endif
