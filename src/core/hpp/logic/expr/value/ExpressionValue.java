/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.value;

import fr.bcm.lib.sil.math.expr.crt.EExpressionType;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Expression returning a value
 */
public class ExpressionValue<Type> implements IExpression<Type> {

	Type value;

	public void set(Type v)
	{
		this.value = v;
	}

	public EExpressionType getExpressionType()
	{
		return EExpressionType.VALUE;
	}

	public ExpressionValue()
	{
		this(null);
	}

	public ExpressionValue(Type v)
	{
		this.set(v);
	}

	@Override
	public Type eval()
	{
		return this.value;
	}

	@Override
	public String createRepresentation()
	{
		return (this.value == null) ? IExpression.NULL_VALUE_REPRESENTATION : this.value.toString();
	}

}