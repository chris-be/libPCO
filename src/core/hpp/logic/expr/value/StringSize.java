/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.value;

import fr.bcm.lib.sil.math.expr.crt.EExpressionType;
import fr.bcm.lib.sil.math.expr.crt.EOperatorName;
import fr.bcm.lib.sil.math.expr.crt.IExpression;
import fr.bcm.lib.sil.math.expr.crt.MonoFunctionBase;

/**
 * Expression returning size of string
 */
public class StringSize extends MonoFunctionBase<Integer, IExpression<String>> {

	public EExpressionType getExpressionType()
	{
		return EExpressionType.VALUE;
	}

	public StringSize()
	{
		this(null);
	}

	public StringSize(IExpression<String> p1)
	{
		this.setP1(p1);
	}

	@Override
	public Integer eval()
	{
		String size = this.getStringValue();
		return (size == null) ? 0 : size.length();
	}

	@Override
	public String createRepresentation()
	{
		String size = this.getStringValue();
		size = (size == null) ? IExpression.NULL_VALUE_REPRESENTATION : Integer.toString(size.length());

		String op = EOperatorName.SIZE.getRepresentation();
		return String.format(IExpression.MONO_FUNCTION_REPRESENTATION, op, size);
	}

	/**
	 * @return param1.eval() if not null, null else
	 * @Convenience
	 */
	protected String getStringValue()
	{
		return (this.param1 == null) ? null : this.param1.eval();
	}

}