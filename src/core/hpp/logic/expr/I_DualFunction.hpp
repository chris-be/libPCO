/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_DUAL_FUNCTION_HPP_INCLUDED
#define	LIB_PCO_I_DUAL_FUNCTION_HPP_INCLUDED

#include "I_MonoFunction.hpp"

namespace NPCO {

/**
	Contract for expression taking two parameters
	@param PResult @see IExpression
	@param PParam1 @see IMonoFunction
	@param PParam2 Type of 2th parameter
 */
template<class PResult, class PParam1, class PParam2> class IDualFunction
	: virtual public IMonoFunction<PResult, PParam1> {

public:

	//! @return param2 value
	virtual PParam2 getP2(void) const = 0;

	//! Set param2 value
	virtual void setP2(const PParam2 p2) = 0;

};

} // N..

#endif
