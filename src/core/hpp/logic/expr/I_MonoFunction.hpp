/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_MONO_FUNCTION_HPP_INCLUDED
#define	LIB_PCO_I_MONO_FUNCTION_HPP_INCLUDED

#include "I_Expression.hpp"

namespace NPCO {

/**
	Contract for expression taking one parameter.
	@param PResult @see IExpression
	@param PParam1 Type of parameter
*/
template<class PResult, class PParam1> class IMonoFunction : public IExpression<PResult> {

public:
	//! @return param1 value
	virtual PParam1 getP1(void) const = 0;

	//! Set param1 value
	virtual void setP1(const PParam1 p1) = 0;

};

} // N..

#endif
