/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_BOOLEAN_OR_HPP_INCLUDED
#define	LIB_PCO_TEST_BOOLEAN_OR_HPP_INCLUDED

#include "DualTestBase.hpp"

namespace NPCO {

/**
	Boolean test: 'left' or 'right'
*/
class TestBooleanOr : public DualTestBase<bool> {

public:
	TestBooleanOr(void) : TestBooleanOr(nullptr, nullptr)
	{	}

	TestBooleanOr(const IExpression<bool>* p1, IExpression<bool>* p2)
	{
		this->set(p1, p2);
	}

	virtual bool	eval(void) const override
	{
		bool l = this->param1->eval();
		bool r = this->param2->eval();
		return l || r;
	}

};

} // N..

#endif
