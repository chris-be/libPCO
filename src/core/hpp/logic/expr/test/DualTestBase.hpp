/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_DUAL_TEST_BASE_HPP_INCLUDED
#define	LIB_PCO_DUAL_TEST_BASE_HPP_INCLUDED

#include "I_DualTest.hpp"
#include "logic/expr/DualFunctionBase.hpp"

namespace NPCO {

/**
	IDualTest base implementation - Binary Test
*/
template<class PType>
class DualTestBase : virtual public IDualTest<PType>
	, public DualFunctionBase<bool, const IExpression<PType>*, const IExpression<PType>*> {

public:

};

} // N..

#endif
