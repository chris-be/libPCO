package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.math.expr.crt.EOperatorName;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Test: 'left' == 'right', using compareTo method !
 */
public class TestEqualForComparable<Type extends Comparable<Type>> extends DualTestBase<Type> {

	@Override
	public EOperatorName getOperatorName()
	{
		return EOperatorName.EQUAL;
	}

	public TestEqualForComparable()
	{	}

	public TestEqualForComparable(IExpression<Type> p1, IExpression<Type> p2)
	{
		super.set(p1, p2);
	}

	@Override
	public Boolean eval()
	{
		Type l = this.param1.eval();
		Type r = this.param2.eval();
		return l.compareTo(r) == 0;
	}

}