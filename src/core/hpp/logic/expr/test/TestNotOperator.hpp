/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_NOT_OPERATOR_HPP_INCLUDED
#define	LIB_PCO_TEST_NOT_OPERATOR_HPP_INCLUDED

#include "MonoTestBase.hpp"

namespace NPCO {

/**
	Bool operator: not
*/
class TestNotOperator : public MonoTestBase<bool> {

public:
	TestNotOperator(void) : TestNotOperator(nullptr)
	{	}

	TestNotOperator(const IExpression<bool>* p1)
	{
		this->setP1(p1);
	}

	virtual bool	eval(void) const
	{
		return !this->param1->eval();
	}

};

} // N..

#endif
