/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_EXPRESSION_TEST_HPP_INCLUDED
#define	LIB_PCO_I_EXPRESSION_TEST_HPP_INCLUDED

#include "logic/expr/I_Expression.hpp"

namespace NPCO {

typedef IExpression<bool>	BoolExpression;

/**
	Contract for tests - bool expressions
*/
class IExpressionTest : virtual public IExpression<bool> {

	// @return Operator name
	// ETestOperatorName getOperatorName(void) const;

};

} // N..

#endif
