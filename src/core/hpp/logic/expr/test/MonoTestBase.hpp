/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MONO_TEST_BASE_HPP_INCLUDED
#define	LIB_PCO_MONO_TEST_BASE_HPP_INCLUDED

#include "I_MonoTest.hpp"
#include "logic/expr/MonoFunctionBase.hpp"

namespace NPCO {

/**
	IMonoTest base implementation - Unary Test
*/
template<class PType>
class MonoTestBase : virtual public IMonoTest<PType>
	, public MonoFunctionBase<bool, const IExpression<PType>*> {

public:

};

} // N..

#endif
