/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.math.expr.crt.EOperatorName;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * UnaryTest: null
 */
public class TestIsNull<Type> extends MonoTestBase<Type> {

	@Override
	public EOperatorName getOperatorName()
	{
		return EOperatorName.IS_NULL;
	}

	public TestIsNull()
	{
		this(null);
	}

	public TestIsNull(IExpression<Type> p1)
	{
		this.setP1(p1);
	}

	@Override
	public Boolean eval()
	{
		DebugBox.predicateNotNull(this.param1);
		return this.param1.eval() == null;
	}

}