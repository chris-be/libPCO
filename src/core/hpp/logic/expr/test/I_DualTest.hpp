/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_DUAL_TEST_HPP_INCLUDED
#define	LIB_PCO_I_DUAL_TEST_HPP_INCLUDED

#include "I_ExpressionTest.hpp"
#include "logic/expr/I_DualFunction.hpp"

namespace NPCO {

/**
	Contract for unary tests: operator 'value'
	@param PType Type of tested value
*/
template<class PType>
class IDualTest : public IExpressionTest
	, virtual public IDualFunction<bool, const IExpression<PType>*, const IExpression<PType>*> {

};

} // N..

#endif
