/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_MONO_TEST_HPP_INCLUDED
#define	LIB_PCO_I_MONO_TEST_HPP_INCLUDED

#include "I_ExpressionTest.hpp"
#include "logic/expr/I_MonoFunction.hpp"

namespace NPCO {

/**
	Contract for unary tests: operator 'value'
	@param PType Type of tested value
*/
template<class PType>
class IMonoTest : public IExpressionTest, virtual public IMonoFunction<bool, const IExpression<PType>*> {

};

} // N..

#endif
