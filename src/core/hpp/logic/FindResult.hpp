/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_FIND_RESULT_HPP_INCLUDED
#define LIB_PCO_FIND_RESULT_HPP_INCLUDED

#include "ActionResult.hpp"

namespace NPCO {

/**
	Hold result of a "find"
	@param PIndex index or key
*/
template<typename PIndex>
class FindResult : protected ActionResult<bool, PIndex> {

protected:
	using TBase		= ActionResult<bool, PIndex>;

public:
	using typename TBase::ActionResult;

	FindResult(void) : TBase(false)
	{	}

	PCO_INLINE
	void	setFound(bool f)
	{
		TBase::setState(f);
	}

	PCO_INLINE
	bool	found(void) const
	{
		return TBase::getState();
	}

	PCO_INLINE
	void	setIndex(PIndex index)
	{
		TBase::setResult(index);
	}

	PCO_INLINE
	PIndex	index(void) const
	{
		return TBase::getResult();
	}

	//! Shortcut - set true
	inline
	void	setFound(PIndex index)
	{
		this->state 	= true;
		this->result	= index;
	}

	//! Shortcut
	inline
	void	set(bool found, PIndex index)
	{
		TBase::set(found, index);
	}

};

} // N..

#endif
