/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CAPSULE_HPP_INCLUDED
#define	LIB_PCO_CAPSULE_HPP_INCLUDED

namespace NPCO {

/**
	Value container: optionally holds a value (handle null as value)
*/
template<class PValue>
class Capsule {

protected:
	bool		isSet;
	PValue		value;

public:
	Capsule(void);
	Capsule(const PValue& value);

	// Copy
	Capsule(const Capsule<PValue>& toCopy) = default;
	Capsule& operator=(const Capsule<PValue>& toCopy) = default;

	// Move
	Capsule(Capsule<PValue>&& toMove) = default;
	Capsule& operator=(Capsule<PValue>&& toMove) = default;

	//! A value was set
	bool	hasValue(void) const;

	//! Mark "no value"
	void	unset(void);

	//! Get value
	const PValue&	getValue(void) const;

	//! Get value
	PValue&			getValue(void);

	//! Set value
	void			setValue(const PValue& value);

	// @convenience
	explicit		operator bool(void) const
	{
		return this->hasValue();
	}

	// @convenience
	bool			operator!(void) const
	{
		return !this->hasValue();	// return !(bool)(*this) ?
	}

	// @convenience
	inline
	const PValue&	operator*(void) const
	{
		return this->getValue();
	}

	// @convenience
	inline
	PValue&		operator*(void)
	{
		return this->getValue();
	}

	// @convenience
	const Capsule&	operator=(const PValue& value)
	{
		this->setValue(value);
		return *this;
	}

};

} // N..

#include "imp/Capsule.cppi"

#endif
