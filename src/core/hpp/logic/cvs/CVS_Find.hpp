/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CVS_FIND_HPP_INCLUDED
#define	LIB_PCO_CVS_FIND_HPP_INCLUDED

#include "pattern/walk/I_Walker.hpp"
#include "pattern/logic//I_Selector.hpp"
#include "logic/FindResult.hpp"
#include "mem/MemSettings.hpp"

namespace NPCO {

/*!

*/
class CVS_Find {
public:
	template<typename PSlctd>
		using SelectorFunc		= typename ISelector<PSlctd>::SelFunc;

public:

	/*! Finds out index of an element if exists
		@param[out] findResult
		@param ptr Array pointer
		@param size Size of array
		@param selector Selector to use
	*/
	template<typename PType>
	static void	find(FindResult<array_size>& findResult, const PType* ptr, array_size size, SelectorFunc<PType> selector);

	/*! Finds out index of an element if exists
		@param[out] findResult
		@param ptr Array pointer
		@param size Size of array
		@param classifier Classifier to use
		@param toFind Value wanted
	*/
	template<typename PType, class PClassifier>
	static void	find(FindResult<array_size>& findResult, const PType* ptr, array_size size, const PClassifier& classifier, const PType& toFind);

	/*!	Finds out index of an element
		Array must be classed before by "classifier" !! dichotomy..
		@param[out] findResult / if !found : index is set where it should be placed
		@param ptr Array pointer
		@param size Size of array
		@param classifier Classifier to use
		@param toFind Value wanted
	*/
	template<typename PType, class PClassifier>
	static void	logFind(FindResult<array_size>& findResult, const PType* ptr, array_size size, const PClassifier& classifier, const PType& toFind);

};

} // N..

#include "imp/CVS_Find.cppi"

#endif
