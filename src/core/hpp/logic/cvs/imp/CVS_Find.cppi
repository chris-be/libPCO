/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

// #include "cvs/CVS_Find.hpp"
#include "PCO_Predicate.hpp"
#include "cvs/cfr/E_Classifier.hpp" // e_isBefore..

namespace NPCO {

template<typename PType>
void	CVS_Find::find(FindResult<array_size>& findResult, const PType* ptr, array_size size, SelectorFunc<PType> selector)
{
	PCO_INV_PTR(ptr != nullptr);
	PCO_INV_PTR(selector != nullptr);

	for(array_size left = 0 ; left < size ; ++left, ++ptr)
	{
		if(selector(*ptr))
		{
			findResult.setFound(left);
			return;
		}
	}

	findResult.setFound(false);
}

template<typename PType, class PClassifier>
void	CVS_Find::find(FindResult<array_size>& findResult, const PType* ptr, array_size size, const PClassifier& cfr, const PType& toFind)
{
	PCO_INV_PTR(ptr != nullptr);

	for(array_size left = 0 ; left < size ; ++left, ++ptr)
	{
		ClassifyValue cv = cfr.classify(*ptr, toFind);
		if(e_isSame(cv))
		{
			findResult.setFound(left);
			return;
		}
	}

	findResult.setFound(false);
}

template<typename PType, class PClassifier>
void	CVS_Find::logFind(FindResult<array_size>& findResult, const PType* ptr, array_size size, const PClassifier& cfr, const PType& toFind)
{
	PCO_INV_PTR(ptr != nullptr);

	if(size == 0)
	{	// Empty : first place
		findResult.set(false, 0);
		return;
	}

	array_size min = 0;
	array_size max = size;
	// Keep track of last "isBefore" test
	bool isBefore;

	while(min < max)
	{
		array_size		mid = (min + max) / 2;
		const PType&	toTest = ptr[mid];

		ClassifyValue cv = cfr.classify(toFind, toTest);
		isBefore = e_isBefore(cv);
		if(isBefore)
		{
			max	= mid;
		}
		else if(e_isAfter(cv))
		{
			min	= mid + 1;
		}
		else
		{
			PCO_ASSERT( e_isSame(cv) );
			findResult.set(true, mid);
			return;
		}
	}

	// When min > max :
	//		previous step : min+1 == max => test on "min" (or "max-1")
	findResult.set(false, isBefore ? min : max);
}

} // N..
