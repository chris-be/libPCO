/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FLAGS_HPP_INCLUDED
#define	LIB_PCO_FLAGS_HPP_INCLUDED

#include "Flag.hpp"

namespace NPCO {

/**
	Flags: handle flag with bits.

// @todo test enum ... : Flag { f1, f2, ... };

*/
template<class PType = unsigned int>
class Flags {

public:
	using TFlag		= Flag<PType>;

protected:
	PType	state;

public:
	Flags(void)
	 : state(0)
	{	}

	Flags(PType state)
	 : state(state)
	{	}

	PCO_INLINE
	operator PType() const
	{
		return this->state;
	}

	void	set(const TFlag flag, bool on)
	{
		if(on)
			this->setOn(flag);
		else
			this->setOff(flag);
	}

	PCO_INLINE
	void	setOn(const TFlag flag)
	{
		Bit::setOn<PType>(this->state, flag);
	}

	PCO_INLINE
	void	setOff(const TFlag flag)
	{
		Bit::setOff<PType>(this->state, flag);
	}

	PCO_INLINE
	bool	isOn(const TFlag flag) const
	{
		return Bit::isOn<PType>(this->state, flag);
	}

	PCO_INLINE
	bool	isOnAndOnly(const TFlag flag) const
	{
		return Bit::isOnOnly<PType>(this->state, flag);
	}

};

} // N..

#endif
