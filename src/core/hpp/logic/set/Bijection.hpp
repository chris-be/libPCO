/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_LOGIC_BIJECTION_HPP_INCLUDED
#define	LIB_PCO_LOGIC_BIJECTION_HPP_INCLUDED

#include "col/MapTree.hpp"
#include "math/MathClassifier.hpp"

namespace NPCO {

/**
	Used to manage "bijection"

	@param PSource
	@param PImage
*/
template<class PSource, class PImage
			, class PSourceClassifier = MathClassifier<PSource>
			, class PImageClassifier = MathClassifier<PImage>
	> class Bijection {

protected:
	using TSrc2Img	= MapTree<PSource, PImage, PSourceClassifier>;
	using TImg2Src	= MapTree<PImage, PSource, PImageClassifier>;

public:
	using TIterSource	= typename TSrc2Img::TIterRead;
	using TIterImage	= typename TImg2Src::TIterRead;

	typedef std::pair<const PSource, PImage>	TInitPair;

protected:
	TSrc2Img	src2Img;
	TImg2Src	img2Src;

public:
	Bijection(void) = default;
	Bijection(std::initializer_list<TInitPair> list);

	// Copy
	Bijection(const Bijection& ) = default;
	Bijection& operator=(const Bijection& ) = default;

	// Move
	Bijection(Bijection&& ) = default;
	Bijection& operator=(Bijection&& ) = default;

	void	clear(void);

	void	add(const PSource& src, const PImage& img);

	bool	hasSource(const PSource& src) const;
	bool	hasImage(const PImage& img) const;

	void	removeSource(const PSource& src);
	void	removeImage(const PImage& img);

	TIterSource	getImage(const PSource& src) const;
	TIterImage	getSource(const PImage& img) const;

};

} // N..

#include "imp/Bijection.cppi"

#endif
