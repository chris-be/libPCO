/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_LOGIC_INJECTION_HPP_INCLUDED
#define	LIB_PCO_LOGIC_INJECTION_HPP_INCLUDED

#include "col/MapTreeList.hpp"
#include "math/MathClassifier.hpp"
#include "col/Store.hpp"

namespace NPCO {

/**
	Used to manage "injection"

	@param PSource
	@param PImage
*/
template<class PSource, class PImage
			, class PSourceClassifier = MathClassifier<PSource>
			, class PImageClassifier = MathClassifier<PImage>
	> class Injection {

protected:
	using TSrc2Img	= MapTreeList<PSource, PImage, PSourceClassifier, Store<PImage>>;
	using TImg2Src	= MapTree<PImage, PSource, PImageClassifier>;

public:
	using TList			= typename TSrc2Img::TList;
	using TIterImage	= typename TImg2Src::TIterRead;

protected:
	TSrc2Img	src2Img;
	TImg2Src	img2Src;

public:
	Injection(void) = default;

	// Copy
	Injection(const Injection& ) = default;
	Injection& operator=(const Injection& ) = default;

	// Move
	Injection(Injection&& ) = default;
	Injection& operator=(Injection&& ) = default;

	void	clear(void);

	void	add(const PSource& src, const PImage& img);

	bool	hasSource(const PImage& img) const;
	bool	hasImage(const PSource& src) const;

	void	removeSource(const PSource& src);
	void	removeImage(const PImage& img);

	const TList*	getImages(const PSource& src) const;
	TIterImage		getSource(const PImage& img) const;

};

} // N..

#include "imp/Injection.cppi"

#endif
