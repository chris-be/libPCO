/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CARDINALITY_HPP_INCLUDED
#define	LIB_PCO_CARDINALITY_HPP_INCLUDED

#include "logic/crt/C_Counter.hpp"

namespace NPCO {

/**
	Cardinality
*/
template<class PCounter>
class Cardinality : public C_Counter<PCounter> {

public:
	enum class EType {
		  any			= 0		// 0 or more
		, optional				// 0 or max
		, strictly				// Strictly "max"
		, least					// "max" or more
//		, least					// min or "max"
	};

protected:
	EType		type;
	PCounter	max;

public:
	Cardinality(void)
	 : Cardinality(EType::any, 0)
	{	}

	Cardinality(const EType type, const PCounter max)
	 : type(type), max(max)
	{	}

	EType		getType(void) const
	{
		return this->type;
	}

	void		setType(const EType type)
	{
		this->type = type;
	}

	PCounter	getMax(void) const
	{
		return this->max;
	}

	void		setMax(const PCounter max)
	{
		this->max = max;
	}

	bool	check(const PCounter count) const
	{
		switch(this->type)
		{
			case EType::any:
			{
				return true;
			} break;
			case EType::optional:
			{
				return isZero(count) || (this->max == count);
			} break;
			case EType::strictly:
			{
				return this->max == count;
			} break;
			case EType::least:
			{
				return this->max <= count;
			} break;
			default:
			{
				PCO_UNK_CASE("Cardinality");
			};
		}

		return false;
	}

	static Cardinality	makeAny(void)
	{
		return Cardinality(EType::any, 0);
	}

	static Cardinality	makeOptional(const PCounter count)
	{
		return Cardinality(EType::optional, count);
	}

	static Cardinality	makeStrictly(const PCounter count)
	{
		return Cardinality(EType::strictly, count);
	}

	static Cardinality	makeLeast(const PCounter count)
	{
		return Cardinality(EType::least, count);
	}

	static Cardinality	makeNone(void)
	{
		PCounter count;	toZero(count);
		return makeStrictly(count);
	}

	static Cardinality	makeOne(void)
	{
		PCounter count;	toSingle(count);
		return makeStrictly(count);
	}

	static Cardinality	makeLeastOne(void)
	{
		PCounter count;	toSingle(count);
		return makeLeast(count);
	}

};

} // N..

#endif
