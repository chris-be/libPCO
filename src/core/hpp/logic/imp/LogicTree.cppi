/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"logic/LogicTree.hpp"
#include "PCO_Predicate.hpp"

namespace NPCO {

template<class PId, class PNode>
void	LogicTree<PId, PNode>::updateMaxId(const PId id)
{
	if(id > this->maxId)
	{
		this->maxId = id;
	}
}

template<class PId, class PNode>
bool	LogicTree<PId, PNode>::isEmpty(void) const
{
	return this->nodes.isEmpty();
}

template<class PId, class PNode>
void	LogicTree<PId, PNode>::clear(void)
{
	this->nodes.clear();
	this->maxId = 0;
}

template<class PId, class PNode>
auto	LogicTree<PId, PNode>::getNodeCount(void) const -> TNodeSize
{
	return this->nodes.size();
}

template<class PId, class PNode>
void	LogicTree<PId, PNode>::addNode(const PId id, const PNode& node)
{
	this->nodes.add(id, node);
	this->updateMaxId(id);
}

template<class PId, class PNode>
void	LogicTree<PId, PNode>::moveInNode(const PId id, PNode&& node)
{
	this->nodes.moveIn(id, std::move(node));
	this->updateMaxId(id);
}

template<class PId, class PNode>
PId		LogicTree<PId, PNode>::addNode(const PNode& node)
{
	++this->maxId;
	this->nodes.add(this->maxId, node);

	return this->maxId;
}

template<class PId, class PNode>
PId		LogicTree<PId, PNode>::moveInNode(PNode&& node)
{
	++this->maxId;
	this->nodes.moveIn(this->maxId, std::move(node));

	return this->maxId;
}

template<class PId, class PNode>
const PNode&	LogicTree<PId, PNode>::getNode(const PId id) const
{
	PCO_INV_ARG(this->nodes.hasKey(id), "Invalid node");
	return this->nodes[id];
}

template<class PId, class PNode>
PNode&			LogicTree<PId, PNode>::getNode(const PId id)
{
	PCO_INV_ARG(this->nodes.hasKey(id), "Invalid node");
	return this->nodes[id];
}

template<class PId, class PNode>
void	LogicTree<PId, PNode>::removeNode(const PId id)
{
	// Detach sons
	this->unlinkFather(id);
	// Detach from father
	this->unlinkSon(id);
	this->nodes.remove(id);
}

template<class PId, class PNode>
void	LogicTree<PId, PNode>::link(const PId idFather, const PId idSon)
{
	PCO_INV_ARG(this->nodes.hasKey(idFather), "Invalid father node");
	PCO_INV_ARG(this->nodes.hasKey(idSon), "Invalid son node");

	PCO_LOGIC_ERR(!this->hierarchy.hasSource(idSon), "Son already linked");

	this->hierarchy.add(idFather, idSon);
}

template<class PId, class PNode>
bool	LogicTree<PId, PNode>::hasSon(const PId idFather) const
{
	PCO_INV_ARG(this->nodes.hasKey(idFather), "Invalid node");
	return this->hierarchy.hasImage(idFather);
}

template<class PId, class PNode>
bool	LogicTree<PId, PNode>::hasFather(const PId idSon) const
{
	PCO_INV_ARG(this->nodes.hasKey(idSon), "Invalid node");
	return this->hierarchy.hasSource(idSon);
}

template<class PId, class PNode>
auto	LogicTree<PId, PNode>::getSonIds(const PId idFather) const -> const TSonIdList*
{
	PCO_INV_ARG(this->nodes.hasKey(idFather), "Invalid node");
	return this->hierarchy.getImages(idFather);
}

template<class PId, class PNode>
PId		LogicTree<PId, PNode>::getFather(const PId idSon) const
{
	PCO_INV_ARG(this->nodes.hasKey(idSon), "Invalid node");
	return *this->hierarchy.getSource(idSon);
}

template<class PId, class PNode>
void	LogicTree<PId, PNode>::unlinkSon(const PId idSon)
{
	PCO_INV_ARG(this->nodes.hasKey(idSon), "Invalid node");
	this->hierarchy.removeImage(idSon);
}

template<class PId, class PNode>
void	LogicTree<PId, PNode>::unlinkFather(const PId idFather)
{
	PCO_INV_ARG(this->nodes.hasKey(idFather), "Invalid node");
	this->hierarchy.removeSource(idFather);
}

template<class PId, class PNode>
auto	LogicTree<PId, PNode>::getNodes(void) const -> const TNodes&
{
	return this->nodes;
}

template<class PId, class PNode>
auto	LogicTree<PId, PNode>::getNodes(void) -> TNodes&
{
	return this->nodes;
}

} // N..
