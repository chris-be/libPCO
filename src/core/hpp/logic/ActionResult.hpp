/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_ACTION_RESULT_HPP_INCLUDED
#define LIB_PCO_ACTION_RESULT_HPP_INCLUDED

#include "PCO_ForceInline.hpp"

#include <utility> // std::move

namespace NPCO {

/**
	Hold state of an action and its associated result.

	@param PState State of action
	@param PResult Result of action
*/
template<class PState, class PResult>
class ActionResult {

public:
	using TState		= PState;
	using TResult		= PResult;

protected:
	PState		state;
	PResult		result;

public:
	ActionResult(void)
	{	}

	ActionResult(const PState& state)
	 : state(state)
	{	}

	ActionResult(const PState& state, const PResult& result)
	 : state(state), result(result)
	{	}

	ActionResult(const PState& state, PResult&& result)
	 : state(state), result(std::move(result))
	{	}

	PCO_INLINE
	void			setState(const PState& state)
	{
		this->state = state;
	}

	PCO_INLINE
	const PState&	getState(void) const
	{
		return this->state;
	}

	PCO_INLINE
	void			setResult(const PResult& result)
	{
		this->result = result;
	}

	PCO_INLINE
	const PResult&	getResult(void) const
	{
		return this->result;
	}

	//! Shortcut
	inline
	void	set(const PState& state, const PResult& result)
	{
		this->state	= state;
		this->result= result;
	}

	//! Shortcut
	inline
	void	set(const PState& state, PResult&& result)
	{
		this->state	= state;
		this->result= std::move(result);
	}

};

} // N..

#endif
