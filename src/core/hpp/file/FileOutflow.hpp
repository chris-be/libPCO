/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FILE_OUTFLOW_HPP_INCLUDED
#define	LIB_PCO_FILE_OUTFLOW_HPP_INCLUDED

#include "pattern/flow/I_Outflow.hpp"
#include "file/I_File.hpp"

namespace NPCO {

/*!
	FileOutflow: encapsulates a file to use as outflow
*/
class FileOutflow : public IOutflow<uw_08> {

protected:
	IFile&		file;

public:
	FileOutflow(IFile& file);

	//! @see IOutflow
	virtual void	write(const uw_08& element) override;

};

} // N..

#endif