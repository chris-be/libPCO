/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CRC_32_HPP_INCLUDED
#define LIB_PCO_CRC_32_HPP_INCLUDED

#include "mem/Array.hpp"

namespace NPCO {

/*!
	Handle CRC32.

*/
class CRC32 {

public:
	typedef uw_32		crc_32;

private:
	CRC32(void);

protected:
	//! Table used to compute CRC
	Array<crc_32>	table;

public:
	/*! Calculate CRC 32
		@param pBuffer Buffer to read
		@param size Size of buffer
	 */
	crc_32	compute(const uw_08 *pBuffer, int size) const;

// Singleton
protected:
	static CRC32*	singletonInstance;

public:
	static CRC32&	singleton(void)
	{
		if(singletonInstance == nullptr)
		{
			singletonInstance = new CRC32();
		}

		return *singletonInstance;
	}

};

} // N..

#endif
