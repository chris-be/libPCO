/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FILE_HPP_INCLUDED
#define	LIB_PCO_FILE_HPP_INCLUDED

#include "FileLow.hpp"
#include "FileBuffered.hpp"

namespace NPCO {

/*!
	File: FileBuffered on FileLow.
	@convenience
*/
class File : public AFile {

public:
	using TBufferSize			= typename FileBuffered::TBufferSize;

protected:
	FileLow			fileLow;
	FileBuffered	file;

public:
	File(void);
	virtual ~File(void) override;

	//! Open given file
	void	open(const char* filename, EStateFlags how, TBufferSize bufferSize = FileBuffered::default_buffer_size);

	//! To use for an already opened resource (pipe, ..)
	void	open(int handle, EStateFlags how, TBufferSize bufferSize = FileBuffered::default_buffer_size);

// General
	//! @see IFile
	virtual io_open_mode	getState(void) const override;

	//! @see IFile
	virtual	io_offset	seek(io_offset offset, ESeek origin) override;

	//! @see IFile
	virtual io_offset	where(void) const override;

	//! @see IFile
	virtual bool		isEnd(void) const override;

	//! @see IFile
	virtual io_offset	size(void) const override;

	//! @see IResource
	virtual bool	isOpened(void) const noexcept override;

	//! @see IResource
	virtual void	close(void) noexcept override;

// Read functions

	//! @see IFile
	virtual io_size	read(uw_08* pBuffer, io_size size) override;

// Write functions

	//! @see IFile
	virtual	io_size	write(const uw_08* pBuffer, io_size size) override;

	//! @see IFile (for derived class)
	virtual void	flush(void) override;

};

} // N..

#endif
