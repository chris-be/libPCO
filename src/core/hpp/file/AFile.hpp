/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AFILE_HPP_INCLUDED
#define	LIB_PCO_AFILE_HPP_INCLUDED

#include "I_File.hpp"

namespace NPCO {

/*!
	AFile: base implementing IFile

	WARNING:
		If seek can't be used, it will return an exception !!
		Open and close depend on what is used, so they must be implemented
		by derived class. In fact, opening and closing must be controlled by calling functions.

		Called functions should only read/write...
		Derived class should call "verify members" (open must set state !!)
		And adjust "length" and "currentOffset"
*/
class AFile : public IFile {

protected:
	//! State of object
	io_open_mode	state;

	//! Reset attributes
	void	reset(void);
	//! Set state
	void	setState(io_open_mode how);

	//! Check some state
	bool	_verifyUnopened(void) const;
	bool	_verifyOpened(void) const;
	bool	_verifyRead(void) const;
	bool	_verifyWrite(void) const;

	//! Check some state and throw runtime if not verified
	void	_checkUnopened(void) const;
	void	_checkOpened(void) const;
	void	_checkRead(void) const;
	void	_checkWrite(void) const;

public:
	//! State must be changed by open and reset by close !!
	AFile(void);

	//! @see IResource
	virtual ~AFile(void) override;

	// Copy - Using a resource in parallel may generate annoying errors
	AFile(const AFile& toCopy) = delete;
	AFile& operator=(const AFile& toCopy) = delete;

	// Move
	AFile(AFile&& toMove) = default;
	AFile& operator=(AFile&& toMove) = default;

// General
	//! @see IFile
	virtual io_open_mode	getState(void) const override;

	//! @see IResource
	virtual bool		isOpened(void) const noexcept override;

	//! @see IFile
	virtual	io_offset	seek(io_offset offset, ESeek origin) override = 0;

	//! @see IFile
	virtual io_offset	where(void) const override = 0;

	//! @see IFile
	virtual bool		isEnd(void) const override = 0;

	//! @see IFile
	virtual io_offset	size(void) const override = 0;

	//! @see IResource
	virtual void		close(void) noexcept override = 0;

// Read functions

	//! @see IFile
	virtual io_size	read(uw_08* pBuffer, io_size size) override = 0;

	//! @see IFile
	virtual io_size	read(TBuffer& buffer) override;

// Write functions

	//! @see IFile
	virtual	io_size	write(const uw_08* pBuffer, io_size size) override = 0;

	//! @see IFile
	virtual io_size	write(const TBuffer& buffer) override;

	//! @see IFile
	virtual void	flush(void) override = 0;

};

} // N..

#endif
