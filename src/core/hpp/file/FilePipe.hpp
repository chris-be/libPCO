/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_IO_FILE_PIPE_HPP_INCLUDED
#define	LIB_PCO_IO_FILE_PIPE_HPP_INCLUDED

#include "FileLow.hpp"

namespace NPCO {

/*!
	To handle pipe
*/
class FilePipe {

protected:
	int		pipeFd[2];
	FileLow	write;
	FileLow	read;

	//! Route handle to write
	void	routeWrite(int handle);

public:
	FilePipe(void);

	void	open(void);

	FileLow&	getWrite(void);
	FileLow&	getRead(void);

	void	closeWrite(void);
	void	closeRead(void);

	//! Route stdout to write
	void	routeStdOut(void);

	//! Route stderr to write
	void	routeStdErr(void);

};

} // N..

#endif
