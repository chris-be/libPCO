/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FILE_TYPES_HPP_INCLUDED
#define	LIB_PCO_FILE_TYPES_HPP_INCLUDED

#include "logic/Flags.hpp"
#include "PCO_Types.hpp"

/*!
	Some typedef for "io classes"

*/
namespace NPCO {

/*!
	Open set general state, read/write... modify current state
	@remarks Flags
*/
typedef enum {
	// General state
	  eRead			=	  1	// Opened for read
	, eWrite		=	  2	// Opened for write
	, eBuffered		=	  4	// Implements buffer
} EStateFlags;

//! To seek
enum class ESeek {
	  fromStart			// From start of file
	, fromEnd			// From end of file (rerverse)
	, fromCurrent		// From current position
};

//! Type of offset used for seek (signed to permits backward)
typedef int32_t			io_offset;

//! Type of size used to read/write
typedef uint16_t		io_size;

//! Opening file flags
typedef Flags<>			io_open_mode;

} // N..

#endif
