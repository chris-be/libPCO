/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FILE_BUFFERED_HPP_INCLUDED
#define	LIB_PCO_FILE_BUFFERED_HPP_INCLUDED

#include "AFile.hpp"
#include "col/FillBuffer.hpp"

namespace NPCO {

/*!
	FileBuffered: buffer for IFile
*/
class FileBuffered : public AFile {

public:
	typedef uw_16				TBufferSize;

	//! Default buffers size and sector reading
	static const TBufferSize	default_buffer_size;

protected:
//	//! Buffer used
//	typedef TFillBuffer		FillBuffer<uw_08>;

	enum class ELastAction {
		// General state
		  none			=  1	// Nothing done
		, read			=  2	// Read
		, write			=  4	// Write
	};

	//! Pointer to the "buffered" IFile
	IFile*		file;

	//! Buffer used - For reading or writing
	FillBuffer<uw_08>	buffer;

	/** Last action performed (to handle cache properly, flush, ...)
		- eRead - eWrite : buffer may be dirty
	*/
	ELastAction	lastAction;

	//! Flush without checking if needed
	void	flushNoCheck(void);

public:
	//! Default : no buffer allocated, 0 size
	FileBuffered(void);
	virtual ~FileBuffered(void);

	// Copy
	FileBuffered(const FileBuffered& toCopy) = delete;
	FileBuffered& operator=(const FileBuffered& toCopy) = delete;

	// Move
	FileBuffered(FileBuffered&& toMove) = default;
	FileBuffered& operator=(FileBuffered&& toMove) = default;

	/** Open
		@param pFile (use read|write flags of pFile given)
		@param bufferSize Buffer size to use
	*/
	void	open(IFile* pFile, TBufferSize bufferSize = default_buffer_size);

	/*!
		@see IResource
		Flush, release buffers, doesn't call close of pFile !!
	*/
	virtual void	close(void) noexcept override;

// General
	//! @see IFile
	virtual io_offset	seek(io_offset offset, ESeek origin) override;

	//! @see IFile
	virtual void		flush(void) override;

	//! @see IFile
	virtual io_offset	where(void) const override;

	//! @see IFile
	virtual bool		isEnd(void) const override;

	//! @see IFile
	virtual io_offset	size(void) const override;

// Read functions

	//! @see IFile
	virtual io_size	read(uw_08* pBuffer, io_size size) override;

// Write functions

	//! @see IFile
	virtual io_size	write(const uw_08* pBuffer, io_size size) override;

};

} // N..

#endif
