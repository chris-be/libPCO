/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FILE_INFLOW_HPP_INCLUDED
#define	LIB_PCO_FILE_INFLOW_HPP_INCLUDED

#include "./I_File.hpp"
#include "pattern/flow/I_Inflow.hpp"

namespace NPCO {

/*!
	FileInflow: encapsulates a file to use as inflow
*/
class FileInflow : public IInflow<uw_08> {

protected:
	IFile&		file;

public:
	FileInflow(IFile& file);

	//! @see IInflow
	virtual bool	read(uw_08& element) override;

};

} // N..

#endif
