#ifndef	__LIBIO_DIR_UTIL_H_INCLUDED__
#define	__LIBIO_DIR_UTIL_H_INCLUDED__

/**
	Class to handle directories listing
*/

#include "LibSys.h"	// CString, CSDArray

using namespace NPCO;

namespace NIO {

class CDirUtil {
public:
		static	int		get(CString &name);
		// Get current directory name with full path
		// If name is too small or any other error, return -1

		static	int		set(char *name);
		// Change current directory

		static	int		list(CSDArray<CString> *pDirs, CSDArray<CString> *pFiles);
		// Get list of directories and files in current directory

};

}	// namespace NIO

#endif	// __LIBIO_DIR_UTIL_H_INCLUDED__
