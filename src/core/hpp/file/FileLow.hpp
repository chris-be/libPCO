/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FILE_LOW_HPP_INCLUDED
#define	LIB_PCO_FILE_LOW_HPP_INCLUDED

#include "AFile.hpp"

namespace NPCO {

/*!
	Wrapper on C open/read/write/close functions.

	Note: no buffer ! Use File if you want to use a buffer.
*/
class FileLow : public AFile {

protected:
	//! Handle of file
	int			handle;
	//! Flag: reached end of file while reading
	bool		atEOF;

public:
	//! Default: no handle in use
	FileLow(void);
	virtual ~FileLow(void) override;

	//! Open given file
	void	open(const char* filename, EStateFlags how);

	//! To use for an already opened resource (pipe, ..)
	void	open(int handle, EStateFlags how);

	//! @see IResource
	virtual void	close(void) noexcept override;

// General

	//! @see IFile
	virtual	io_offset	seek(io_offset offset, ESeek origin) override;

	//! @see IFile
	virtual io_offset	where(void) const override;

	//! @see IFile
	virtual bool		isEnd(void) const override;

	//! @see IFile
	virtual io_offset	size(void) const override;

// Read functions

	//! @see IFile
	virtual io_size	read(uw_08* pBuffer, io_size size) override;

// Write functions

	//! @see IFile
	virtual	io_size	write(const uw_08* pBuffer, io_size size) override;

	//! @see IFile
	virtual	void	flush(void) override;

};

} // N..

#endif
