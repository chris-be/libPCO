/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FILE_MEMORY_HPP_INCLUDED
#define	LIB_PCO_FILE_MEMORY_HPP_INCLUDED

#include "AFile.hpp"
#include "mem/mgr/I_MemoryManager.hpp"

namespace NPCO {

/*!
	FileMemory: to use memory as a IFile

*/
class FileMemory : public AFile {

protected:
	//! MemoryManager to use
	IMemoryManager*	memManager;
	//! Memory allocated with memManager
	uw_08*			pArray;

	//! Size
	io_offset	length;
	//! Current offset in "file"
	io_offset	currentOffset;

	//! Find out new position for a seek
	io_offset	evalSeekPosition(io_offset offset, ESeek origin);

public:
	// Default : no memory allocated, 0 size
	FileMemory(void);
	virtual ~FileMemory(void) override;

	// Copy
	FileMemory(const FileMemory& toCopy) = delete;
	FileMemory& operator=(const FileMemory& toCopy) = delete;

	// Move
	FileMemory(FileMemory&& toMove) = default;
	FileMemory& operator=(FileMemory&& toMove) = default;

	/**
		@param memManager : deleted by destructor !!
	*/
	FileMemory(IMemoryManager* memManager);

	/*!
		Open
		If pBuffer is null => allocation
		Else : pBuffer assumed to have size bytes !!
	*/
	void	open(uw_08* pBuffer, io_offset size, io_open_mode how);

	/** Specific case "read only"
		@convenience
	*/
	void	open(const uw_08* pBuffer, io_offset size);

	//! @see IResource (Free memory)
	virtual void	close(void) noexcept override;

	//! Get pointer and size back, like close but memory isn't freed
	void	release(uw_08* &pBuffer, io_offset &size);

// General

	//! @see IFile
	virtual io_offset	seek(io_offset offset, ESeek origin) override;

	//! @see IFile
	virtual io_offset	where(void) const override;

	//! @see IFile
	virtual bool		isEnd(void) const override;

	//! @see IFile
	virtual io_offset	size(void) const override;

// Read functions

	//! @see IFile
	virtual io_size	read(uw_08* pBuffer, io_size size) override;

// Write functions

	//! @see IFile
	virtual io_size	write(const uw_08* pBuffer, io_size size) override;

	//! @see IFile
	virtual void	flush(void) override;

};

} // N..

#endif
