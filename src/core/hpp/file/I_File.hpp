/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_FILE_HPP_INCLUDED
#define	LIB_PCO_I_FILE_HPP_INCLUDED

#include "file/FileTypes.hpp"
#include "pattern/res/I_Resource.hpp"
#include "mem/Array.hpp"

namespace NPCO {

/*!
	Contract for file tasks (like read/write/seek)

	Design:
		- There's no buffer by default even if flush is present
		- For buffering: use FileBuffered
*/
class IFile : public IResource {

public:
	using TBuffer		=	Array<uw_08, io_size>;

public:
	//! State must be changed by open and reset by close !!
	// IFile(void);
	virtual ~IFile(void) override	{ }

	//! Get current state (@see EStateFlags)
	virtual io_open_mode	getState(void) const = 0;

// General

	/** Seek pointer
		@param offset Offset to add
		@param origin From where to add offset
	*/
	virtual io_offset	seek(io_offset offset, ESeek origin) = 0;

	//! To know where we are
	virtual io_offset	where(void) const = 0;

	//! To know if we are at END OF FILE
	virtual bool		isEnd(void) const = 0;

	/** Get current size of file
		Flush if written before !! Also it won't be sure it's the real size !! Cache...
	*/
	virtual io_offset	size(void) const = 0;

// Read functions

	/** Read
		@param pBuffer Buffer to fill
		@param size Size of given buffer
		@return Effective bytes read
	*/
	virtual io_size	read(uw_08* pBuffer, io_size size) = 0;

	/** Read
		@param buffer Buffer to fill
		@return Effective bytes read
	*/
	virtual io_size	read(TBuffer& buffer) = 0;

// Write functions

	/** Write
		@param pBuffer Buffer to use
		@param size Size of given buffer
		@return Effective bytes written
	*/
	virtual io_size	write(const uw_08* pBuffer, io_size size) = 0;

	/** Write
		@param buffer Buffer to use
		@return Effective bytes read
	*/
	virtual io_size	write(const TBuffer& buffer) = 0;

	//! Flush write buffer
	virtual void		flush(void) = 0;

};

} // N..

#endif
