/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_IO_FILE_INFO_HPP_INCLUDED
#define	LIB_PCO_IO_FILE_INFO_HPP_INCLUDED

#include "file/FileTypes.hpp"
#include <time.h>			// time_t

namespace NPCO {

/*!
	Class to get file informations : size, access time...

*/
class FileInfo {

// See struct stat at end of file to add more options if needed
protected:
		size_t		length;			// File size in bytes
		time_t		accessTime;		// Time of last access
		time_t		modifTime;		// Time of last data modification
		time_t		changeTime;		// Time of last file status change
									// Times measured in seconds since
									// 00:00:00 UTC, Jan. 1, 1970

public:
	FileInfo(void);

	/*!
		Fill infos with file info

		@return Success or not
	*/
	bool	getFileInfos(const char *pName);

	/*!
		Fill infos with handle of file

		@return Success or not
	*/
	bool	getFileInfos(int handle);

	//! Get size of file
	size_t	size(void) const;

};

} // N..

/*
struct stat {
          mode_t   st_mode;		// File mode (type, perms)
          ino_t    st_ino;		// Inode number
          dev_t    st_dev;		// ID of device containing
								// a directory entry for this file
          dev_t    st_rdev;		// ID of device
								// This entry is defined only for
								// char special or block special files
          nlink_t  st_nlink;	// Number of links
          uid_t    st_uid;		// User ID of the file's owner
          gid_t    st_gid;		// Group ID of the file's group
          off_t    st_size;		// File size in bytes
          time_t   st_atime;	// Time of last access
          time_t   st_mtime;	// Time of last data modification
          time_t   st_ctime;	// Time of last file status change
								// Times measured in seconds since
								// 00:00:00 UTC, Jan. 1, 1970
          long     st_blksize;	// Preferred I/O block size
          blkcnt_t st_blocks;	// Number of 512 byte blocks allocated
}
*/

#endif
