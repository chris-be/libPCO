/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_REGEX_SPLIT_RESULT_HPP_INCLUDED
#define	LIB_PCO_REGEX_SPLIT_RESULT_HPP_INCLUDED

#include "logic/ActionResult.hpp"
#include "col/Store.hpp"

#include <regex.h> // regoff_t

namespace NPCO {

//! Offset of pattern extracted
class MatchOffset {

public:
	using TOffset		= regoff_t;
	static_assert(std::is_signed<TOffset>::value, "Something is wrong");

protected:
	TOffset		first;
	TOffset		last;

public:
	MatchOffset(void);
	MatchOffset(int first, int last);

	bool	isValid(void) const;
	//! @return Offset of first char
	TOffset	getFirst(void) const;
	//! @return Offset of last char (included)
	TOffset	getLast(void) const;
};

/*!
	Regex split results
		- found true -> matches are adjusted
*/
class RegexSplitResult : protected ActionResult<bool, Store<MatchOffset>> {

protected:
	using TBase		= ActionResult<bool, Store<MatchOffset>>;

public:
	using TMatches	= typename TBase::TResult;

public:
	RegexSplitResult(void) = default;
	~RegexSplitResult(void) = default;

	void	clear();

	//! Add "no match"
	void	addEmpty(void);
	//! Add match with offset
	void	add(int first, int last);

	inline
	bool	found(void) const
	{
		return TBase::getState();
	}

	inline
	void	setFound(bool f)
	{
		TBase::setState(f);
	}

	inline
	const TMatches&	matches(void) const
	{
		return TBase::getResult();
	}

};

} // N..

#endif
