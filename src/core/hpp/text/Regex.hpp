/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_REGEX_HPP_INCLUDED
#define	LIB_PCO_REGEX_HPP_INCLUDED

#include "pattern/res/I_Resource.hpp"
#include "string/crt/I_StringCarrier.hpp"
#include "RegexSplitResult.hpp"

// #include <regex.h>

namespace NPCO {

/*!
	Regex: use C POSIX
		- no utf-8 complient !

*/
class Regex : public IResource {

protected:
	regex_t	regex;
	//! Flag
	bool	prepared;

	/*! Throw runtime_exception with regex error
		@param ec Error code
		@param msg Additional info
	*/
	void	throwError(int ec, const char* msg) const;

public:
	Regex(void);
	Regex(const IStringCarrier& expr, bool forSplit, bool ignoreCase);
	virtual ~Regex(void) override = default;

	//! @see I_Resource
	virtual bool isOpened(void) const noexcept override;

	//! @see I_Resource
	virtual void close(void) noexcept override;

	/*!	Compile
		@param expr regex to compile
		@param forSplit true: find/split patterns, match else
		@param ignoreCase
	*/
	void	prepare(const IStringCarrier& expr, bool forSplit, bool ignoreCase);

	//!  Match test
	bool	match(const IStringCarrier& toParse) const;

	//! Split (find patterns)
	void	split(RegexSplitResult& mr, const IStringCarrier& toParse) const;

};

} // N..

#endif
