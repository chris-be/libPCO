/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEXT_POSITION_HPP_INCLUDED
#define	LIB_PCO_TEXT_POSITION_HPP_INCLUDED

namespace NPCO {

/**
	Keep track of "cursor"

	Note: position start at (0, 0) but editors usually start at line 1.
*/
class TextPosition {

public:
	typedef unsigned int		TAsciiCompatible;
	typedef TAsciiCompatible	TChar;
	static_assert(sizeof(TChar) >= sizeof(char32_t), "int is too short");

	typedef unsigned int		TSize;

protected:
	TSize	line;
	TSize	column;

	//! @see move
	void	handleControlCode(char control);

	// @convenience
	void	newLine(void);

public:
	TextPosition(void);
	TextPosition(TSize line, TSize column);

	// Copy
	TextPosition(const TextPosition& toCopy) = default;
	TextPosition&	operator=(const TextPosition& toCopy) = default;

	// Move
	TextPosition(TextPosition&& toMove) = default;
	TextPosition&	operator=(TextPosition&& toMove) = default;

	//! Back to origin
	void	reset(void);

	//! Set position
	void	set(TSize line, TSize column);

	TSize	getLine(void) const;
	void	setLine(TSize line);

	TSize	getColumn(void) const;
	void	setColumn(TSize column);

	//! Adjust position depending on character (ignore control code except carriage return)
	void	add(TChar c);

/*
	//! Adjust position depending on character (respect control code)
	void	move(TChar c);
*/

	bool	operator==(const TextPosition& toCompare) const;

	bool	operator!=(const TextPosition& toCompare) const
	{
		return !this->operator==(toCompare);
	}

};

} // N..

#endif
