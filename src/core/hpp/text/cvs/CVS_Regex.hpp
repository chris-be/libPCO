/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CVS_REGEX_HPP_INCLUDED
#define	LIB_PCO_CVS_REGEX_HPP_INCLUDED

#include "text/Regex.hpp"
#include "string/String.hpp"
#include "cvs/bag/E_Bag.hpp" // e_cursor

namespace NPCO {

/*!


*/
class CVS_Regex {


private:
	// Service only
	CVS_Regex(void) = delete;

public:
	bool	match(const IStringCarrier& expr, const IStringCarrier& toParse, bool ignoreCase);

/*
	template<class PString>
	PString		extract(const PString& toParse, const MatchOffset& mo) const
	{
		PString rc;
		if(mo.isValid())
		{
			auto cur = e_cursor(toParse, mo.getStart(), mo.getStop());
			e_addAll(rc, cur);
		}

		return rc;
	}
*/

	/**
		@return true if matches and bag adjusted
	*/
	template<class PDynBag, class PString = typename PDynBag::TType>
	static
	bool	split(PDynBag& bag, const String& toParse, const Regex& regex)
	{
		RegexSplitResult sr;
		regex.split(sr, toParse);

		if(!sr.found())
			return false;

		// Iterate, no direct access
		auto cur = e_cursor(toParse);
		int	idx = 0;
		for(auto mc = e_cursor(sr.matches()) ; mc.ok() ; ++mc)
		{
			PString match;

			const MatchOffset& mo = *mc;
			if(mo.isValid())
			{
				int	delta = mo.getFirst() - idx;
				if(delta > 0)
				{	// forward
					cur+= delta;
				}
				else if(delta < 0)
				{	// backward
					cur-= -delta;
				}
				idx = mo.getFirst();

				int last	= mo.getLast();
				while(idx <= last)
				{
					match.add(*cur);
					++cur;	++idx;
				}
			}

			bag.add(match);
		}

		return true;
	}

};

} // N..

#endif
