/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PCO_TYPES_HPP_INCLUDED
#define	LIB_PCO_PCO_TYPES_HPP_INCLUDED

/**
	Some typedef
	 "Assembler like" (was used to, sorry).

	int_least8_t, int_least64_t
	uint_least8_t, uint_least64_t
*/
#include <cstdint>
// #include <limits>

#include <type_traits>	// is_integral

namespace NPCO {

typedef		int8_t		sw_08;
typedef		int16_t		sw_16;
typedef		int32_t		sw_32;
typedef		int64_t		sw_64;

typedef		uint8_t		uw_08;
typedef		uint16_t	uw_16;
typedef		uint32_t	uw_32;
typedef		uint64_t	uw_64;

/** "raw convert" : change type keeping all bits
	example: keep sign bit from signed to unsigned
	Use with care. Here to work with char8_t, char16_t, char32_t, ...
*/
template<typename PDst, typename PSrc>
PDst	rawConvert(const PSrc& src)
{
	static_assert(std::is_integral<PSrc>::value, "Only for primitives");
	static_assert(std::is_integral<PDst>::value, "Only for primitives");

	static_assert(sizeof(PSrc) == sizeof(PDst), "Only for objects of same size");

	return reinterpret_cast<const PDst&>(src);
}

} // N..

#endif
