/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_REFERENCE_HPP_INCLUDED
#define	LIB_PCO_REFERENCE_HPP_INCLUDED

#include "use/MetaHolder.hpp"
#include "use/ReferenceCounter.hpp"
#include "pattern/math/C_MathClassified.hpp"
#include "PCO_ForceInline.hpp"

namespace NPCO {

/**
	Reference

	MathClassified: use data pointer to class
*/
template<class PType>
class Reference
	: protected MetaHolder<ReferenceCounter, PType>
	, public C_MathClassified<Reference<PType>> {

protected:
	using TBase = MetaHolder<ReferenceCounter, PType>;

	// Destructor / operator=
	bool		decRefNb(void);
	// Copy constructor / operator=
	void		copy(const Reference& toCopy);
	// Move constructor / operator=
	void		move(Reference& toMove);

	// Create metadata - constructor / create / operator=(PType ...)
	template<class PChild, typename... Args>
	void		init(Args&&... args);

public:
	//! Create a null reference
	Reference(void);

	//! Forward args to constructor - expect if first argument is Reference<PType>
	template<typename FArg, typename... Args
		, std::enable_if_t<!std::is_same<std::decay_t<FArg>, Reference>::value, bool> = false>
	Reference(FArg&& fa, Args... args);

	//! Cleanup
	virtual ~Reference(void) override;

	// Copy
	Reference(const Reference<PType>& toCopy);
	Reference&	operator=(const Reference<PType>& toCopy);

	// Move
	Reference(Reference<PType>&& toMove);
	Reference&	operator=(Reference<PType>&& toMove);

	//! Create reference with child class
	template<class PChild, typename... Args>
	static Reference	create(Args&&... args);

	//!
	void	unset(void);

	//! Get reference
	PCO_INLINE
	const PType&	operator*(void)	const;
	//! Get reference
	PCO_INLINE
	PType&			operator*(void);

	//! Get pointer
	PCO_INLINE
	const PType*	operator->(void) const;
	//! Get pointer
	PCO_INLINE
	PType*			operator->(void);

	template<class PChild>
	PChild		try_cast(void) const;

	template<class PChild>
	PChild		try_cast(void);

	//! @see C_SelfClassified
	bool	isBefore(const Reference<PType>& toCompare) const;

	//! @see C_SelfClassified
	bool	isSame(const Reference<PType>& toCompare) const;

	//! @see MetaHolder
	PCO_INLINE
	bool	isNull(void) const;

	//! Check if something hold and valid - @see ReferenceCounter
	PCO_INLINE
	bool	isValid(void) const;

	// @convenience
	PCO_INLINE
	Reference&		operator=(const PType& toCopy);
	// @convenience
	PCO_INLINE
	Reference&		operator=(PType&& toMove);

/*
	// @convenience
	explicit		operator bool(void) const
	{
		return !this->isNull();
	}

	// @convenience
	bool			operator!(void) const
	{
		return this->isNull();	// return !(bool)(*this) ?
	}
*/

	inline
	bool	operator==(const Reference<PType>& toCompare) const
	{
		return this->isSame(toCompare);
	}

	inline
	bool	operator!=(const Reference<PType>& toCompare) const
	{
		return !this->isSame(toCompare);
	}

	inline
	bool	operator<(const Reference<PType>& toCompare) const
	{
		return this->isBefore(toCompare);
	}

	inline
	bool	operator>(const Reference<PType>& toCompare) const
	{
		return toCompare.isBefore(*this);
	}

	inline
	bool	operator<=(const Reference<PType>& toCompare) const
	{
		return !toCompare.isBefore(*this);
	}

	inline
	bool	operator>=(const Reference<PType>& toCompare) const
	{
		return !this->isBefore(toCompare);
	}

	//! Create copy
	template<class PChild>
	static Reference	from(const PChild& res);

	//! Create copy by move
	template<class PChild>
	static Reference	from(PChild&& res);

};

} // N..

#include "imp/Reference.cppi"

#endif
