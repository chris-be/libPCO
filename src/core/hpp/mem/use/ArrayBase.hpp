/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_ARRAY_BASE_HPP_INCLUDED
#define	LIB_PCO_ARRAY_BASE_HPP_INCLUDED

#include "pattern/C_HasSize.hpp" // e_isInBounds
#include "mem/mgr/SimpleMemoryManager.hpp"
#include "cvs/use/UseMemoryManager.hpp"

namespace NPCO {

/*!
	This template provides array manipulation.

	@param PType Type of value
	@param PSize Type of size
	@param PMemMgr Memory manager to use
*/
template<class PType, class PSize, class PMemMgr = SimpleMemoryManager>
class ArrayBase : public UseMemoryManager<PMemMgr> {
	// private C_HasSize<ArrayBase<PType, PSize, PMemMgr>, PSize>

public:
	using TType		= PType;
	using TSize		= PSize;

protected:
	//! Pointer to array
	PType*		pArray;
	//! Number of elements in the array
	PSize		nbItems;

	//! Handle signed/unsigned PSize
	PCO_INLINE
	static mem_size	castSize(const PSize size);

public:
	//! Default : empty array
	ArrayBase(void);
	ArrayBase(const PSize size);
	~ArrayBase(void);

	// Copy
	ArrayBase(const ArrayBase& toCopy);
    ArrayBase& operator=(const ArrayBase& toCopy);

	// Move
	ArrayBase(ArrayBase&& toMove);
    ArrayBase& operator=(ArrayBase&& toMove);

	//! @see C_HasSize
	PCO_INLINE
	PSize	size(void) const;

	//! @see C_HasSize
	PCO_INLINE
	bool	isEmpty(void) const;

	//! Returns if index is valid
	PCO_INLINE
	bool	isInBounds(const PSize i) const;

	//! Fill array with value
	void	fill(const PType& value);

protected:
	//! Empty array (free memory)
	void	i_reset(void);

	//! Resize array with specified size
	void	i_resize(const PSize newSize);

	/** Resize array to prepare "space" - handles memory moves
		Use when needing to insert "items"
		@param chunkIndex Index where to add
		@param chunkSize Number of items to add
	*/
	void	i_insertAt(const PSize chunkIndex, const PSize chunkSize);

	/** Resize array to free "space" - handles memory moves
		Use when needing to remove "items"
		@param chunkIndex Index where to add
		@param chunkSize Number of items to add
	*/
	void	i_removeAt(const PSize chunkIndex, const PSize chunkSize);

	PCO_INLINE
	const PType*	i_da(const PSize i) const;

	PCO_INLINE
	PType*			i_da(const PSize i);

};

} // N..

#include "imp/ArrayBase.cppi"

#endif
