/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_REFERENCE_COUNTER_HPP_INCLUDED
#define	LIB_PCO_REFERENCE_COUNTER_HPP_INCLUDED

namespace NPCO {

class ReferenceCounter {
public:
	typedef signed int		TCounter;

private:
	TCounter	refNb;

public:
	ReferenceCounter(void) : refNb(0)
	{	}

	PCO_INLINE
	TCounter	getRefNb(void) const
	{
		return this->refNb;
	}

	PCO_INLINE
	TCounter&	getRefNb(void)
	{
		return this->refNb;
	}

	PCO_INLINE
	bool	isValid(void) const
	{
		return this->refNb > 0;
	}

};

} // N..

#endif
