/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_META_HOLDER_HPP_INCLUDED
#define	LIB_PCO_META_HOLDER_HPP_INCLUDED

#include "mem/MemSettings.hpp"
#include "PCO_ForceInline.hpp"

namespace NPCO {

/*!
	MetaHolder: create meta data and object in a chunk of memory - cache proximity

	Tech:
		- pointer aligned for meta and data
		- in memory: (alignment garbage) meta | data
					the holder pointer is initialized on "data"

	@param PMeta Meta data associated
	@param PType Data
*/
template<class PMeta, class PType>
class MetaHolder {

public:
	//! Find out alignof to use
	static constexpr size_t	bestAlignof(void);
	//! Compute left size: delta+sizeof(meta)
	static           size_t	leftSize(void);

protected:
	uw_08*			holder;

	struct InnerMeta : public PMeta {
		//! Left size depends on PType and can be a child class - it must be kept
		size_t	__leftSize;
	};

	template<class PChild, typename... Args>
	void	alloc(Args&&... args);

	void	free(void);

	//! Direct access - no nullptr predicate
	PCO_INLINE
	const InnerMeta*	_da_meta(void) const;
	//! Direct access - no nullptr predicate
	PCO_INLINE
	InnerMeta*			_da_meta(void);

	//! Direct access - no nullptr predicate
	PCO_INLINE
	const PType*		_da_data(void) const;

	//! Direct access - no nullptr predicate
	PCO_INLINE
	PType*				_da_data(void);

public:
	//! Nothing hold
	MetaHolder(void);
	//! Ensure cleanup
	virtual ~MetaHolder(void);

	// Copy
	MetaHolder(const MetaHolder& toCopy) = delete;
	MetaHolder&	operator=(const MetaHolder& toCopy) = delete;

	// Move
	MetaHolder(MetaHolder&& toMove) = delete;
	MetaHolder&	operator=(MetaHolder&& toMove) = delete;

	template<typename... Args>
	void	set(Args&&... args);

	//! Use carefully
	template<class PChild, typename... Args>
	void	setWithChild(Args&&... args);

	//! Check if nothing hold
	PCO_INLINE
	bool	isNull(void) const;

	PCO_INLINE
	const PMeta*	meta(void) const;
	PCO_INLINE
	PMeta*			meta(void);

	PCO_INLINE
	const PType*	data(void) const;
	PCO_INLINE
	PType*			data(void);

};

} // N..

#include "imp/MetaHolder.cppi"

#endif
