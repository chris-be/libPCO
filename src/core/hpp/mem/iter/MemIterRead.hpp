/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MEM_ITER_READ_HPP_INCLUDED
#define	LIB_PCO_MEM_ITER_READ_HPP_INCLUDED

#include "MemIter.hpp"
#include "pattern/iter/C_IterRead.hpp"

namespace NPCO {

/*!
	MemIterRead: C_IterRead for memory blocks
*/
template<class PCell>
class MemIterRead
		: private C_IterRead<MemIterRead<PCell>, PCell>
		, public MemIter<const PCell> {

public:
	MemIterRead(void) = default;
	MemIterRead(const PCell* ptr) noexcept : MemIter<const PCell>(ptr)
	{ }

	//! @see C_IterRead
	PCO_INLINE
	MemIterRead&	operator++(void) noexcept
	{
		this->forward();	return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	MemIterRead&	operator--(void) noexcept
	{
		this->back();		return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator==(const MemIterRead& toCompare) const noexcept
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator!=(const MemIterRead& toCompare) const noexcept
	{
		return !this->sameAs(toCompare);
	}

};

} // N..

#endif
