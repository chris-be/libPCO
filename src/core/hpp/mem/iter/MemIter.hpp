/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MEM_ITER_HPP_INCLUDED
#define	LIB_PCO_MEM_ITER_HPP_INCLUDED

#include "pattern/iter/C_Iter.hpp"

namespace NPCO {

/*!
	MemIter: Iter for memory
*/
template<class PCell>
class MemIter : private C_Iter<MemIter<PCell>, PCell> {

public:
	using TCell		= typename std::remove_const<PCell>::type;

protected:
	//! Pointer to current object
	PCell*	ptr;

	PCO_INLINE
	bool	sameAs(const MemIter& toCompare) const noexcept;

public:
	MemIter(void) noexcept;
	MemIter(PCell* ptr) noexcept;

	//! Set
	void	set(PCell *ptr) noexcept;

	//! @see C_Iter
	void	unset(void) noexcept;

	//! @see C_Iter
	PCO_INLINE
	bool	isValid(void) const noexcept;

	//! @see C_Iter
	PCO_INLINE
	void	forward(void) noexcept;

	//! @see C_Iter
	PCO_INLINE
	void	back(void) noexcept;

	//! @see C_Iter
	PCO_INLINE
	PCell&	operator*(void) const;

	//! @see C_Iter
	PCO_INLINE
	PCell*	operator->(void) const;

	//! @see C_Iter
	PCO_INLINE
	PCell&	cell(void) const;

};

} // N..

#include "imp/MemIter.cppi"

#endif
