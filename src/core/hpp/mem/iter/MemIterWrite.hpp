/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MEM_ITER_WRITE_HPP_INCLUDED
#define	LIB_PCO_MEM_ITER_WRITE_HPP_INCLUDED

#include "MemIter.hpp"
#include "pattern/iter/C_IterWrite.hpp"

namespace NPCO {

/*!
	MemIterWrite: C_IterWrite for memory blocks
*/
template<class PCell>
class MemIterWrite
		: private C_IterWrite<MemIterWrite<PCell>, PCell>
		, public MemIter<PCell> {

public:
	MemIterWrite(void) = default;
	MemIterWrite(PCell* ptr) noexcept : MemIter<PCell>(ptr)
	{ }

	//! @see C_IterWrite
	PCO_INLINE
	MemIterWrite&	operator++(void) noexcept
	{
		this->forward();	return *this;
	}

	//! @see C_IterWrite
	PCO_INLINE
	MemIterWrite&	operator--(void) noexcept
	{
		this->back();		return *this;
	}

	//! @see C_IterWrite
	PCO_INLINE
	bool	operator==(const MemIterWrite& toCompare) const noexcept
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IterWrite
	PCO_INLINE
	bool	operator!=(const MemIterWrite& toCompare) const noexcept
	{
		return !this->sameAs(toCompare);
	}

/*
	//! @see IIterWrite
	virtual MemIterRead<PCell>	_ro(void) const override
	{
		return MemIterRead<PCell>(this->ptr);
	}
*/

};

} // N..

#endif
