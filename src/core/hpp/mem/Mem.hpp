/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MEM_HPP_INCLUDED
#define	LIB_PCO_MEM_HPP_INCLUDED

#include "MemSettings.hpp" // mem_size

#include "PCO_ForceInline.hpp"
#include "PCO_Predicate.hpp"

#include <new>			// placement new
#include <cstring>		// memset, memcpy, ...
#include <cstdlib>		// malloc

namespace NPCO {

class Mem {

public:
	//! Delete and set pointer to 0
	template<typename PType>
	PCO_INLINE
	static void		_delete(PType* &toDelete) noexcept
	{
		delete toDelete;		toDelete = nullptr;
	}

	/// Delete[] and set pointer to 0
	template<typename PType>
	PCO_INLINE
	static void		_deleteE(PType* &toDelete) noexcept
	{
		delete [] toDelete;		toDelete = nullptr;
	}

	/** Constructors on memory block - void* operator new[] (std::size_t size, void* ptr) throw();
		@param[in] dst Typed pointer - can be null if size is 0
		@param size Number of items
	*/
	template<typename PType>
	PCO_INLINE
	static void	construct(PType* dst, mem_size size)
	{
		PCO_INV_ARG((dst != nullptr) || (size == 0), "invalid ptr for construct");
		for( ; size > 0 ; --size, ++dst)
		{	::new(dst) PType();	}
	}

	/** Destructors on memory block - void operator delete[] (void* ptr, std::size_t size) noexcept
		@param[in] dst Typed pointer - can be null if size is 0
		@param size Number of items
	*/
	template<typename PType>
	PCO_INLINE
	static void	destruct(PType* dst, mem_size size) noexcept
	{
		PCO_ASSERT_MSG((dst != nullptr) || (size == 0), "invalid ptr for destruct");
		for( ; size > 0 ; --size, ++dst)
		{	dst->~PType();		}
	}

	//! c memset
	template<typename PType>
	PCO_INLINE
	static void	memset(PType* dst, mem_size size, int value)
	{
		PCO_INV_PTR(dst != nullptr);
		PCO_INV_SIZE(size >= 0);
		mem_size realSize = size*sizeof(PType);
		PCO_ASSERT(realSize >= 0);
		::memset(static_cast<void*>(dst), value, realSize);
	}

	//! c malloc
	template<typename PType>
	PCO_INLINE
	static void	malloc(PType* &dst, mem_size size)
	{
		PCO_INV_PTR(dst != nullptr);
		PCO_INV_SIZE(size >= 0);
		mem_size realSize = size*sizeof(PType);
		PCO_ASSERT(realSize >= 0);
		void* ptr = ::malloc(realSize);
		dst = reinterpret_cast<PType*>(ptr);
	}

	//! c memcpy
	template<typename PType>
	PCO_INLINE
	static void	memcpy(PType* dst, const PType* src, mem_size size)
	{
		PCO_INV_PTR(dst != nullptr);
		PCO_INV_PTR(src != nullptr);
		PCO_INV_SIZE(size >= 0);
		mem_size realSize = size*sizeof(PType);
		PCO_ASSERT(realSize >= 0);
		::memcpy(static_cast<void*>(dst), static_cast<const void*>(src), realSize);
	}

	//! c memmove
	template<typename PType>
	PCO_INLINE
	static void	memmove(PType* dst, const PType* src, mem_size size)
	{
		PCO_INV_PTR(dst != nullptr);
		PCO_INV_PTR(src != nullptr);
		PCO_INV_SIZE(size >= 0);
		mem_size realSize = size*sizeof(PType);
		PCO_ASSERT(realSize >= 0);
		::memmove(static_cast<void*>(dst), static_cast<const void*>(src), realSize);
	}

	//! c free
	template<typename PType>
	PCO_INLINE
	static void	free(PType* &dst)
	{
		::free(static_cast<void*>(dst));		dst = nullptr;
	}

	//! Use memset to init struct content to 0
	template<typename PType>
	PCO_INLINE
	static void	structZero(PType& dst)
	{
		::memset(static_cast<void*>(&dst), 0, sizeof(PType));
	}

	//! Use memcpy to copy struct content
	template<typename PType>
	PCO_INLINE
	static void	structCpy(PType& dst, const PType& src)
	{
		::memcpy(static_cast<void*>(&dst), static_cast<const void*>(&src), sizeof(PType));
	}

};

} // N..

#endif
