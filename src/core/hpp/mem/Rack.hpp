/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_RACK_HPP_INCLUDED
#define	LIB_PCO_RACK_HPP_INCLUDED

#include "./use/ArrayBase.hpp"
#include "iter/MemIterRead.hpp"
#include "iter/MemIterWrite.hpp"
#include "MemSettings.hpp"

namespace NPCO {

/*!
	This template provides a bidimensional array which can be resized at demand.
	Use it when you need continuous data and dynamic size.

	Warning:
		- changing width costs a lot
*/
template<class PType, class PSize = array_size, class PMemMgr = SimpleMemoryManager> class Rack
	: public ArrayBase<PType, PSize, PMemMgr> {

public:
	using TIterRead		= MemIterRead<PType>;
	using TIterWrite	= MemIterWrite<PType>;

protected:
	using TBase			= ArrayBase<PType, PSize, PMemMgr>;

	class ReallocAction : public C_ReallocAction<ReallocAction, PType> {
	protected:
		const Rack&	toResize;
		PSize		atCell;
		PSize		nb;
		PSize		newSize;

		ReallocAction(const Rack& toResize, PSize atCell, PSize nb);
	public:
		static ReallocAction forInsert(const Rack& toResize, PSize atCell, PSize nb);
		static ReallocAction forRemove(const Rack& toResize, PSize atCell, PSize nb);

		mem_size	oldNb(void) const;
		mem_size	newNb(void) const;

		void	extend(PType* src);
		void	extend(PType* dst, PType* src);
		void	reduce(PType* src);
		void	reduce(PType* dst, PType* src);
	};

	//! Number of cells by shelf
	PSize		nbCells;

	//! Number of shelves
	PSize		nbShelves;

	//! Returns if cell position is valid
	PCO_INLINE
	bool	isCellValid(const PSize cell) const;

	//! Returns if shelf position is valid
	PCO_INLINE
	bool	isShelfValid(const PSize shelf) const;

	//! Returns if position is valid
	PCO_INLINE
	bool	isValid(const PSize cell, const PSize shelf) const;

	// Returns array position calculated from cell, shelf
	PCO_INLINE
	PSize	apos(const PSize cell, const PSize shelf) const;

	//! Used by insertCells, setWidth
	void	shiftCellRight(const PSize cell, const PSize nb);

public:
	Rack(void);

	/*!
		@param reserveSize Number of items to reserve space for
	*/
	Rack(const PSize width, const PSize height);

	// Copy
	Rack(const Rack& toCopy) = default;
    Rack& operator=(const Rack& toCopy) = default;

	// Move
	Rack(Rack&& toMove) = default;
    Rack& operator=(Rack&& toMove) = default;

	//! Get width
	PSize	width(void) const;
	//! Get height
	PSize	height(void) const;

	//! Set height
	void	setHeight(const PSize height);
	void	insertShelves(const PSize shelf, const PSize nb);
	void	removeShelves(const PSize shelf, const PSize nb);

	//! Set width
	void	setWidth(const PSize& width);
	void	insertCells(const PSize cell, const PSize nb);
	void	removeCells(const PSize cell, const PSize nb);

	//!
	const PType&	cell(const PSize cell, const PSize shelf) const;

	//!
	PType&	cell(const PSize cell, const PSize shelf);

	TIterRead	iter_r(const PSize cell, const PSize shelf) const;

	TIterWrite	iter_w(const PSize cell, const PSize shelf);

/*
	//! Get pointer - Do not free !!
	const PType*	_da(void) const;

	//! Get pointer - Do not free !!
	PType*			_da(void);

	//! Get pointer - Do not free !!
	const PType*	_da(array_size i) const;

	//! Get pointer - Do not free !!
	PType*			_da(array_size i);
*/

};

} // N..

#include "imp/Rack.cppi"

#endif
