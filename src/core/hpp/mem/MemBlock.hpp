/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MEM_BLOCK_HPP_INCLUDED
#define	LIB_PCO_MEM_BLOCK_HPP_INCLUDED

#include "pattern/bag/C_Bag.hpp"
#include "iter/MemIterRead.hpp"
#include "iter/MemIterWrite.hpp"
//#include "pattern/cfr/C_Classifier.hpp"
#include "MemSettings.hpp" // mem_size

namespace NPCO {

/*!
	Simplify use of "(const) PType*" block of fixed size

	@param PType Type of value
	@param PSize Type to use for size
*/
template<class PType, class PSize = mem_size> class MemBlock
	: private C_Bag<MemBlock<PType, PSize>, PType, PSize> {

public:
	using TType			= PType;
	using TSize			= PSize;
	using TIterRead		= MemIterRead<PType>;
	using TIterWrite	= MemIterWrite<PType>;

protected:
	//! Pointer to memory block
	PType*		pBlock;
	//! Size of block
	PSize		_size;

	//! Returns if index is valid
	PCO_INLINE
	bool	isInBounds(PSize i) const;

public:
	MemBlock(void);
	MemBlock(PType* pBlock, const PSize size);
	MemBlock(PType* pMin, PType* pMax);
	~MemBlock(void) = default;

	// Copy
	MemBlock(const MemBlock& toCopy) = default;
    MemBlock& operator=(const MemBlock& toCopy) = default;

	// Move
	MemBlock(MemBlock&& toMove) = default;
    MemBlock& operator=(MemBlock&& toMove) = default;

	//! @see C_HasSize
	PSize	size(void) const;

	//! @see C_HasSize
	bool	isEmpty(void) const;

	//! Mirror content - swap(left, right)
	void	mirror(void);

	//TODO
	/*	Quick sort
		@param ptr Array pointer
		@param size Size of array
		@param classifier Classifier to use
	*/
	//void	quickSort(IClassifier<PType> classifier);

	//! Access to the ith element of the array
	PCO_INLINE
	const PType&	operator[](const PSize i) const;

	//! Access to the ith element of the array
	PCO_INLINE
	PType&			operator[](const PSize i);

	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead		first(void) const;
	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead		last(void) const;

	//! @see C_HasIterWrite
	PCO_INLINE
	TIterWrite		first(void);
	//! @see C_HasIterWrite
	PCO_INLINE
	TIterWrite		last(void);

	//! Get element
	PCO_INLINE
	TIterRead		iter_r(const PSize i) const;
	//! Get element
	PCO_INLINE
	TIterWrite		iter_w(const PSize i);

	//! Get pointer - Do not free !!
	PCO_INLINE
	PType*			_da(void);

	//! Get pointer - Do not free !!
	PCO_INLINE
	const PType*	_da(void) const;

};

} // N..

#include "imp/MemBlock.cppi"

#endif
