/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_ARRAY_HPP_INCLUDED
#define	LIB_PCO_ARRAY_HPP_INCLUDED

#include "pattern/bag/C_Bag.hpp"
#include "./use/ArrayBase.hpp"
#include "mem/iter/MemIterRead.hpp"
#include "mem/iter/MemIterWrite.hpp"
#include "MemSettings.hpp"

namespace NPCO {

/*!
	This template provides simple array features
	Use it when your don't need to realloc often

	@param PType Type of value
	@param PSize Type to use for size
	@param PMemMgr Memory manager to use
*/
template<class PType, class PSize = array_size, class PMemMgr = SimpleMemoryManager>
class Array
		: public C_Bag<Array<PType, PSize, PMemMgr>, PType, PSize>
		, public ArrayBase<PType, PSize, PMemMgr> {

protected:
	using TBase			= ArrayBase<PType, PSize, PMemMgr>;

public:
//	using TType			= PType;
//	using TSize			= PSize;
	using TIterRead		= MemIterRead<PType>;
	using TIterWrite	= MemIterWrite<PType>;

public:
	//! Default : empty array
	Array(void) = default;
	Array(const PSize size);
	~Array(void) = default;

	//! Empty array (free memory)
	void	reset(void);

	// Copy
	Array(const Array& toCopy) = default;
    Array& operator=(const Array& toCopy) = default;

	// Move
	Array(Array&& toMove) = default;
    Array& operator=(Array&& toMove) = default;

	//! Resize array with specified size
	void	resize(const PSize newSize);

	/** Resize array to prepare "space" - handles memory moves
		Use when needing to insert "items"
		@param chunkIndex Index where to add
		@param chunkSize Number of items to add
	*/
	void	insertAt(const PSize chunkIndex, const PSize chunkSize);

	/** Resize array to free "space" - handles memory moves
		Use when needing to remove "items"
		@param chunkIndex Index where to add
		@param chunkSize Number of items to add
	*/
	void	removeAt(const PSize chunkIndex, const PSize chunkSize);

	//! Access to the ith element of the array
	PCO_INLINE
	const PType&	operator[](const PSize i) const;

	//! Access to the ith element of the array
	PCO_INLINE
	PType&			operator[](const PSize i);

	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead		first(void) const;
	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead		last(void) const;

	//! @see C_HasIterWrite
	PCO_INLINE
	TIterWrite		first(void);
	//! @see C_HasIterWrite
	PCO_INLINE
	TIterWrite		last(void);

	//! Get element
	PCO_INLINE
	TIterRead		iter_r(const PSize i) const;
	//! Get element
	PCO_INLINE
	TIterWrite		iter_w(const PSize i);

	//! Get back index for iterator
	PSize			_indexOf(const TIterRead& it_r) const;
	//! Get back index for iterator
	PSize			_indexOf(const TIterWrite& it_w) const;

	//! Get pointer - Do not free !!
	PCO_INLINE
	PType*			_da(void);

	//! Get pointer - Do not free !!
	PCO_INLINE
	const PType*	_da(void) const;

	//! Get pointer - Do not free !!
	PCO_INLINE
	PType*			_da(const PSize i);

	//! Get pointer - Do not free !!
	PCO_INLINE
	const PType*	_da(const PSize i) const;

};

} // N..

#include "imp/Array.cppi"

#endif
