/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_MEM_SETTINGS_HPP_INCLUDED
#define	LIB_PCO_MEM_SETTINGS_HPP_INCLUDED

// Here to set some useful types and consts used by "arrays"
#include "PCO_Types.hpp"
#include <cstddef>	// size_t

namespace NPCO {

typedef		size_t			mem_size;

//! @todo use std::limits ?
const mem_size		MEM_SIZE_MAX = SIZE_MAX;

//! Type for "size of array", "index"/
typedef		mem_size		array_size;

typedef		uw_32			chunk_size;

// std::limits<tarray_size>

//! Maximum length of an "array"

/*
	// Limit of array size
	typedef		uw_32			tarray_size_max_test;
	// Used to test if two tarray_size added are less or equal than MAX_ARRAY_SIZE
	#define		TEST_ARRAY_OUTBOUND(SIZE1, SIZE2)									\
				(((tarray_size_max_test)(SIZE1) + (tarray_size_max_test)(SIZE2))	\
				 <= MAX_ARRAY_SIZE)

	#define		DEFAULT_ARRAY_BLOCK_SIZE		8
	//  default number of elements per block
	#define		DEFAULT_ARRAY_FACTOR			1
	// default factor
*/

} // N..

#endif
