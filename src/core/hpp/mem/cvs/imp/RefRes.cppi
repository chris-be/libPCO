/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

// #include	"mem/RefRes.hpp"

namespace NPCO {

template<class PType>
RefRes<PType>::RefRes(void) noexcept
{
	this->ref = TRef::template create<TOptClean>();
}

template<class PType>
RefRes<PType>::RefRes(PType* res, bool clean) noexcept
{
	if(clean)
	{
		this->ref = TRef::template create<TClean>(res);
	}
	else
	{
		this->ref = TRef::template create<TNoClean>(res);
	}
}

template<class PType>
RefRes<PType>::RefRes(PType* res) noexcept
 : RefRes(res, true)
{	}

template<class PType>
RefRes<PType>::RefRes(PType& res) noexcept
 : RefRes(&res, false)
{	}

template<class PType>
template<class PChild, typename... Args>
RefRes<PType>	RefRes<PType>::create(Args&&... args)
{
	static_assert(std::is_base_of<PType, PChild>::value);

	PType* ptr = new PChild(std::forward<Args>(args)...);
	AutoDelete<PType> cleaner(ptr);

	RefRes<PType> res = RefRes<PType>(ptr, true);
	cleaner.popOut();
	return res;
}

template<class PType>
bool	RefRes<PType>::isNull(void) const
{
	return this->ref.isNull() || (*this->ref).isNull();
}

template<class PType>
bool	RefRes<PType>::isValid(void) const
{
	return this->ref.isValid() && (*this->ref).isValid();
}

template<class PType>
const PType&	RefRes<PType>::operator*(void) const
{
	PCO_LOGIC_ERR(!this->isNull(), "bad use");
	return *(this->_da());
}

template<class PType>
PType&			RefRes<PType>::operator*(void)
{
	PCO_LOGIC_ERR(!this->isNull(), "bad use");
	return *(this->_da());
}

template<class PType>
const PType*	RefRes<PType>::operator->(void) const
{
	PCO_LOGIC_ERR(!this->isNull(), "bad use");
	return this->_da();
}

template<class PType>
PType*			RefRes<PType>::operator->(void)
{
	PCO_LOGIC_ERR(!this->isNull(), "bad use");
	return this->_da();
}

template<class PType>
const PType*	RefRes<PType>::_da(void) const
{
	return (*this->ref)._da();
}

template<class PType>
PType*			RefRes<PType>::_da(void)
{
	return (*this->ref)._da();
}

template<class PType>
template<class PChild>
RefRes<PType>	RefRes<PType>::from(const PChild& res)
{
	return RefRes<PType>::create<PChild>(res);
}

template<class PType>
template<class PChild>
RefRes<PType>	RefRes<PType>::from(PChild&& res)
{
	return RefRes<PType>::create<PChild>(std::move(res));
}

} // N..
