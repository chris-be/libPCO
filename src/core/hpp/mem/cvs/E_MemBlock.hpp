/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CVS_MEM_BLOCK_HPP_INCLUDED
#define	LIB_PCO_CVS_MEM_BLOCK_HPP_INCLUDED

#include "mem/MemBlock.hpp"

namespace NPCO {

template<class PType, class PSize>
const MemBlock<PType, PSize>	e_memBlock(const PType* pBlock, const PSize size)
{
	return MemBlock<PType, PSize>(const_cast<PType*>(pBlock), size);
}

template<class PType>
const MemBlock<PType, mem_size>	e_memBlock(const PType* pMin, const PType* pMax)
{
	return MemBlock<PType, mem_size>(const_cast<PType*>(pMin), const_cast<PType*>(pMax));
}

//! Reverse content
template<class PType>
PCO_INLINE
void	e_mirror(PType* dst, mem_size size)
{
	MemBlock<PType, mem_size> mb(dst, size);
	mb.mirror();
}

} // N..

#endif
