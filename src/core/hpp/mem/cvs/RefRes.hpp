/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_REFERENCE_RESOURCE_HPP_INCLUDED
#define	LIB_PCO_REFERENCE_RESOURCE_HPP_INCLUDED

#include "mem/Reference.hpp"
#include "cvs/res/AutoDelete.hpp"

namespace NPCO {

/**
	Reference Resource:
		- enable to give an "object" that will be automatically deleted (or not)
*/
template<class PType>
class RefRes {

protected:
	typedef ResourceHolder<PType>		TOptClean;
	typedef Reference<TOptClean>		TRef;

	using TNoClean		= TOptClean;
	using TClean		= AutoDelete<PType>;

protected:
	TRef	ref;

public:
	RefRes(void) noexcept;
	//! @see PointerHolder
	RefRes(PType* res, bool clean) noexcept;
	//! clean
	RefRes(PType* res) noexcept;
	//! Keep address - no clean
	RefRes(PType& res) noexcept;

	~RefRes(void) = default;

	// Copy
	RefRes(const RefRes<PType>& toCopy) = default;
	RefRes&	operator=(const RefRes<PType>& toCopy) = default;

	// Move
	RefRes(RefRes<PType>&& toMove) = default;
	RefRes&	operator=(RefRes<PType>&& toMove) = default;

	template<class PChild, typename... Args>
	static RefRes	create(Args&&... args);

	//! @see Reference
	PCO_INLINE
	bool	isNull(void) const;

	//! @see Reference
	PCO_INLINE
	bool	isValid(void) const;

	//! @see Reference
	PCO_INLINE
	const PType&	operator*(void)	const;
	//! @see Reference
	PCO_INLINE
	PType&			operator*(void);

	//! @see Reference
	PCO_INLINE
	const PType*	operator->(void) const;
	//! @see Reference
	PCO_INLINE
	PType*			operator->(void);

	//! Direct access
	PCO_INLINE
	const PType*	_da(void) const;

	//! Direct access
	PCO_INLINE
	PType*			_da(void);

	//! Create copy
	template<class PChild>
	static RefRes	from(const PChild& res);

	//! Create copy by move
	template<class PChild>
	static RefRes	from(PChild&& res);

};

} // N..

#include "imp/RefRes.cppi"

#endif
