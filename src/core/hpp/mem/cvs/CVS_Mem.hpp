/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CVS_MEM_HPP_INCLUDED
#define	LIB_PCO_CVS_MEM_HPP_INCLUDED

#include "mem/iter/MemIterRead.hpp"
#include "mem/iter/MemIterWrite.hpp"

#include "cvs/walk/CursorIter.hpp"
#include "cvs/walk/RiderIter.hpp"

#include "mem/MemSettings.hpp" // mem_size

namespace NPCO {

/*!
	CVS Mem
*/
class CVS_Mem {

private:
	CVS_Mem(void) = delete;

public:

// Read

	template<class PCell>
	static CursorIter<MemIterRead<PCell>>	_cursor(const PCell *pMin, const PCell *pMax);

	template<class PCell>
	static CursorIter<MemIterRead<PCell>>	cursor(const PCell *pMin, const PCell *pMax);

	template<class PCell>
	static CursorIter<MemIterRead<PCell>>	back_cursor(const PCell *pMin, const PCell *pMax);

	template<class PCell>
	static CursorIter<MemIterRead<PCell>>	_cursor(const PCell *pMin, mem_size size);

	template<class PCell>
	static CursorIter<MemIterRead<PCell>>	cursor(const PCell *pMin, mem_size size);

	template<class PCell>
	static CursorIter<MemIterRead<PCell>>	back_cursor(const PCell *pMin, mem_size size);

// Write

	template<class PCell>
	static RiderIter<MemIterWrite<PCell>>	_rider(PCell *pMin, PCell *pMax);

	template<class PCell>
	static RiderIter<MemIterWrite<PCell>>	rider(PCell *pMin, PCell *pMax);

	template<class PCell>
	static RiderIter<MemIterWrite<PCell>>	back_rider(PCell *pMin, PCell *pMax);

	template<class PCell>
	static RiderIter<MemIterWrite<PCell>>	_rider(PCell *pMin, mem_size size);

	template<class PCell>
	static RiderIter<MemIterWrite<PCell>>	rider(PCell *pMin, mem_size size);

	template<class PCell>
	static RiderIter<MemIterWrite<PCell>>	back_rider(PCell *pMin, mem_size size);

};

} // N..

#include "imp/CVS_Mem.cppi"

#endif
