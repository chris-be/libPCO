/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_REALLOC_ACTION_HPP_INCLUDED
#define	LIB_PCO_C_REALLOC_ACTION_HPP_INCLUDED

#include "mem/MemSettings.hpp"

namespace NPCO {

/*!
	Constraint "realloc action"
	Reallocating memory simply enlarges the buffer when possible or allocate a new block and move content.
	The idea here is to take the right action depending on what is happening.

	Example:
		Suppose you want a bigger block to insert data in the middle. If realloc moves block,
		there will be 2 moves for some data (to create the hole).
		Same for reducing, even if this might not happen.

	@see ArrayMiddleReallocAction

	@param PCd Constrained class
*/
template<class PCd, class PType>
class C_ReallocAction {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		[[maybe_unused]]	PType* src = nullptr;
		[[maybe_unused]]	PType* dst = nullptr;
		[[maybe_unused]]	mem_size nb;

		// Get previous number of elements
		nb = const_ctd.oldNb();
		// Get future number of elements
		nb = const_ctd.newNb();

		// Memory extent with same address (no move by realloc)
		//	src is already reallocated
		//		-> move data in src as needed
		ctd.extend(src);
		// Memory extent with another address
		//	dst is fresh allocated, src will be freed after
		//		-> move/copy from src to dst as needed
		ctd.extend(dst, src);

		// Memory reduce with same address (no move by realloc)
		//	src will be reallocated after
		//		-> move data in src as needed
		ctd.reduce(src);
		// Memory reduce with another address
		// dst is fresh allocated, src will be freed after
		//		-> move/copy from src to dst as needed
		ctd.reduce(dst, src);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_ReallocAction(void)
	{
		C_ReallocAction::ensure();
	}

};

} // N..

#endif
