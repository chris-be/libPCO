/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_ARRAY_MIDDLE_REALLOC_ACTION_HPP_INCLUDED
#define	LIB_PCO_ARRAY_MIDDLE_REALLOC_ACTION_HPP_INCLUDED

#include "./C_ReallocAction.hpp"
#include "PCO_Predicate.hpp"

namespace NPCO {

/*!
	Used by Array to manage insertAt/removeAt
*/
template<class PType> class ArrayMiddleReallocAction
	: public C_ReallocAction<ArrayMiddleReallocAction<PType>, PType> {

protected:
	//! Current size of array
	mem_size currentSize;
	//! Size of chunk added/removed
	mem_size chunkSize;
	//! Index where to insert/cut
	mem_size chunkIndex;

	//! New size expected
	mem_size newSize;

	/**
		@param currentSize size of currently reserved block
		@param chunkSize size to add
		@param chunkIndex where to add
	*/
	ArrayMiddleReallocAction(mem_size currentSize, mem_size chunkSize, mem_size chunkIndex)
	 : currentSize(currentSize), chunkSize(chunkSize), chunkIndex(chunkIndex)
	{
		PCO_INV_SIZE(currentSize > 0);
		PCO_INV_SIZE(chunkSize > 0);
	}

public:
	//! Init for "insertAt" logic
	static ArrayMiddleReallocAction	forInsert(mem_size currentSize, mem_size chunkSize, mem_size chunkIndex)
	{
		PCO_LOGIC_ERR(chunkIndex < currentSize-1, "logic");
		ArrayMiddleReallocAction ra(currentSize, chunkSize, chunkIndex);
		ra.newSize = currentSize + chunkSize;
		return ra;
	}

	//! Init for "removeAt" logic
	static ArrayMiddleReallocAction	forRemove(mem_size currentSize, mem_size chunkSize, mem_size chunkIndex)
	{
		PCO_LOGIC_ERR(chunkIndex < currentSize, "logic");
		PCO_RANGE_ERR((chunkIndex + chunkSize) <= currentSize, "invalid range");
		ArrayMiddleReallocAction ra(currentSize, chunkSize, chunkIndex);
		ra.newSize = currentSize - chunkSize;
		PCO_ASSERT(ra.newSize >= 0);
		return ra;
	}

	mem_size	oldNb(void) const
	{
		return this->currentSize;
	}

	mem_size	newNb(void) const
	{
		return this->newSize;
	}

	void	extend(PType* src)
	{
		PCO_INV_PTR(src != nullptr);
		// Move "right part"
		PType* pHole = src + chunkIndex;
		Mem::memmove(pHole + chunkSize, pHole, currentSize - chunkIndex);
		// Construct objects in "hole"
		Mem::construct(pHole, chunkSize);
	}

	void	extend(PType* dst, PType* src)
	{
		PCO_INV_PTR(dst != nullptr);
		PCO_INV_PTR(src != nullptr);
		// Copy "splitted content"
		if(chunkIndex > 0)
		{	// Copy "left part"
			Mem::memcpy(dst, src, chunkIndex);
		}

		// Copy "right part"
		PType* pHole = dst + chunkIndex;
		Mem::memcpy(pHole + chunkSize, src + chunkIndex, currentSize - chunkIndex);
		// Construct objects in "hole"
		Mem::construct(pHole, chunkSize);
	}

	void	reduce(PType* src)
	{
		PCO_INV_PTR(src != nullptr);
		// Destruct "hole"
		PType* pHole = src + chunkIndex;
		Mem::destruct(pHole, chunkSize);
		// Move "right part"
		Mem::memmove(pHole, pHole + chunkSize, newSize - chunkIndex);
	}

	void	reduce(PType* dst, PType* src)
	{
		PCO_INV_PTR(dst != nullptr);
		PCO_INV_PTR(src != nullptr);
		// Copy "splitted content"
		if(chunkIndex > 0)
		{	// Copy "left part"
			Mem::memcpy(dst, src, chunkIndex);
		}

		// Destruct "hole"
		PType* pHole = src + chunkIndex;
		Mem::destruct(pHole, chunkSize);
		// Copy "right part"
		Mem::memcpy(dst + chunkIndex, pHole + chunkSize, newSize - chunkIndex);
	}

};

} // N..

#endif
