/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_DEFAULT_MEMORY_MANAGER_HPP_INCLUDED
#define	LIB_PCO_DEFAULT_MEMORY_MANAGER_HPP_INCLUDED

#include "./I_MemoryManager.hpp"

namespace NPCO {

/*!
	Simple Memory Manager
	Uses C malloc, realloc, free
*/
class SimpleMemoryManager : public IMemoryManager {

public:

	//! @see IMemoryManager
	virtual ~SimpleMemoryManager(void);

	//! @see IMemoryManager
	virtual void*	basic_alloc(mem_size size) override;

	//! @see IMemoryManager
	virtual void*	basic_realloc(void* src, mem_size currentSize, mem_size newSize) override;

	//! @see IMemoryManager
    virtual void	basic_free(void* toFree) override;

	//! @see IMemoryManager
	virtual bool	basic_hasToMove(const void* src, mem_size currentSize, mem_size newSize) override;

};

} // N..

#endif
