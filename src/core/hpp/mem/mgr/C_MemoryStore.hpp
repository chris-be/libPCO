/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_MEMORY_STORE_HPP_INCLUDED
#define	LIB_PCO_C_MEMORY_STORE_HPP_INCLUDED

#include "mem/Mem.hpp" // mem_size

namespace NPCO {

typedef void (realloc_move*)(void* src, void* dst);

/*!
	Constraint for "memory allocation/release"

*/
template<class PCd>
class C_MemoryStore {

private:

	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		[[maybe_unused]]	void* memPtr = nullptr;
		[[maybe_unused]]	mem_size size = 1;
		[[maybe_unused]]	realloc_move = nullptr;

		/* Low level memory allocation
			@param size 0 forbidden ! @see adv_resize
			@return reserved block address
			@note C malloc : implentation specific for size(0) => forbidden !
		*/
		memPtr = ctd.alloc(size);

		/*	Low level mem reallocation
			@param src block address to resize
			@param newSize 0 forbidden !
			@return resized block address
		*/
		memPtr = ctd.realloc(memPtr, size, realloc_move);

		/* Low level memory release
			@param toFree block address
		*/
		ctd.free(memPtr);

};

} // N..

#endif
