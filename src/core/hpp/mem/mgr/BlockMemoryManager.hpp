/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_BLOCK_MEMORY_MANAGER_HPP_INCLUDED
#define	LIB_PCO_BLOCK_MEMORY_MANAGER_HPP_INCLUDED

#include "SimpleMemoryManager.hpp"

namespace NPCO {

/*!
	Block Memory Manager
		Allocates memory by blocks in order not to realloc each time.

	Dedicated usage:
		If multiple realloc for 1 element : there will only be n calls / "block size" realloc.

*/
class BlockMemoryManager : public SimpleMemoryManager {
public:
	static const mem_size	DefaultBlockSize;

protected:
	//! Block size
	mem_size	blockSize;

	//! Returns size to alloc regarding to asked
	mem_size	computeSize(mem_size size);

public:

	BlockMemoryManager(mem_size blockSize = DefaultBlockSize);

	mem_size getBlockSize(void) const;

	//! Set "block size" (only used when next call to realloc)
	void setBlockSize(mem_size size);

	//! @see IMemoryManager
	virtual ~BlockMemoryManager(void) override;

	//! @see IMemoryManager
	virtual void*	basic_alloc(mem_size size) override;

	//! @see IMemoryManager
	virtual void*	basic_realloc(void* src, mem_size currentSize, mem_size newSize) override;

	//! @see IMemoryManager
	virtual bool	basic_hasToMove(const void* src, mem_size currentSize, mem_size newSize) override;

};

} // N..

#endif
