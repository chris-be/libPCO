/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_MEMORY_MANAGER_HPP_INCLUDED
#define	LIB_PCO_I_MEMORY_MANAGER_HPP_INCLUDED

#include "mem/Mem.hpp"
#include "C_ReallocAction.hpp"

#include "PCO_Predicate.hpp"

namespace NPCO {

/*!
	Contract for Memory management

*/
class IMemoryManager {

public:
	virtual ~IMemoryManager(void);

	/** Low level memory allocation
		@param size 0 forbidden ! @see adv_resize
		@return reserved block address
		@note C malloc : implentation specific for size(0) => forbidden !
	*/
    virtual void*	basic_alloc(mem_size size) = 0;

	/**	Low level mem reallocation
		@param src block address to resize
		@param currentSize Current size of memory block
		@param newSize 0 forbidden ! @see adv_resize
		@return resized block address
	*/
    virtual void*	basic_realloc(void* src, mem_size currentSize, mem_size newSize) = 0;

	/** Low level memory release
		@param toFree block address
	*/
	virtual void	basic_free(void* toFree) = 0;

	/** Check if current allocated block will be moved when reallocating
		@param src block address to resize
		@param currentSize Current size of memory block
		@param newSize New size wanted
	*/
	virtual bool	basic_hasToMove(const void* src, mem_size currentSize, mem_size newSize) = 0;

	/*! Shortcut - memory allocation
		@param[out] dst Typed pointer
		@param size Number of items
	*/
	template<typename PType>
	inline void	reserve(PType* &dst, mem_size size)
	{
		PCO_INV_SIZE(size > 0);
		mem_size byteSize = size*sizeof(PType);
		PCO_ASSERT(byteSize > 0);

		dst = static_cast<PType*>(this->basic_alloc(byteSize));
		PCO_ASSERT(dst != nullptr);
	}

	/*! Shortcut - memory reallocation
		@param[out] src Typed pointer
		@param currentSize number of currently reserved items
		@param newSize Number of items
	*/
	template<typename PType>
	inline void	resize(PType* &src, mem_size currentSize, mem_size newSize)
	{
		PCO_INV_PTR(src != nullptr);
		PCO_INV_SIZE(currentSize > 0);
		PCO_INV_SIZE(newSize > 0);
		size_t byteCurrentSize	= currentSize*sizeof(PType);
		size_t byteNewSize		= newSize*sizeof(PType);
		PCO_ASSERT(byteCurrentSize > 0);
		PCO_ASSERT(byteNewSize > 0);

		PType	*dst = static_cast<PType*>(this->basic_realloc(src, byteCurrentSize, byteNewSize));
		PCO_ASSERT(dst != nullptr);
		if(dst == nullptr)
		{	// src isn't free
			throw std::bad_alloc();
		}
		src = dst;
	}

	/*! Shortcut - memory free
		@param[out] toFree Typed pointer
	*/
	template<typename PType>
	inline void	release(PType* &toFree)
	{
		this->basic_free(static_cast<void*>(toFree));
		toFree = nullptr;
	}

	/*! Shortcut
		@param src Block memory pointer to adjust
		@param currentSize number of currently reserved items
		@param newSize Number of items
	*/
	template<typename PType>
	inline bool	hasToMove(const PType* src, mem_size currentSize, mem_size newSize)
	{
		size_t byteCurrentSize	= currentSize*sizeof(PType);
		size_t byteNewSize		= newSize*sizeof(PType);
		return this->basic_hasToMove(static_cast<const void*>(src), byteCurrentSize, byteNewSize);
	}

	/*! Shortcut - alloc / realloc / free depending on src state and size asked
		@param[out] src Memory block to adjust
		@param currentSize number of currently reserved items
		@param newSize Number of items wanted

		src == nullptr and newSize > 0 : reserve
		src != nullptr and newSize > 0 : resize
		src != nullptr and newSize   0 : free
	*/
	template<typename PType>
	inline void	adv_resize(PType* &src, mem_size currentSize, mem_size newSize)
	{
		if(newSize == 0)
		{
			if(src != nullptr)
			{	// Free reserved memory
				this->release(src);
			}
			return;
		}

		if(src == nullptr)
		{
			this->reserve(src, newSize);
			return;
		}

		this->resize(src, currentSize, newSize);
	}

	/*! Shortcut - extend memory
		Use when need to insert data in a memory block (to limit memmove)
		@param[out] src Memory block to adjust
		@param reallocAction ReallocAction to use - @see ReallocAction
	*/
	template<typename PType, class PReallocAction>
	void	extend(PType* &src, PReallocAction& reallocAction)
	{
		C_ReallocAction<PReallocAction, PType>::ensure();
		PCO_INV_PTR(src != nullptr);
		mem_size currentSize = reallocAction.oldNb();
		mem_size newSize = reallocAction.newNb();

		PCO_INV_SIZE(currentSize > 0);
		PCO_INV_SIZE(newSize > 0);

		if(!this->hasToMove(src, currentSize, newSize))
		{	// Resize
			this->resize(src, currentSize, newSize);
			reallocAction.extend(src);
			return;
		}

		// Need to realloc and copy contents
		PType* dst = nullptr;
		this->reserve(dst, newSize);
		reallocAction.extend(dst, src);
		this->release(src);
		src = dst;
	}

	/*! Shortcut - reduce memory
		Use when need to remove data in a memory block (to limit memmove)
		@param[out] src Memory block to adjust
		@param reallocAction ReallocAction to use - @see ReallocAction
	*/
	template<typename PType, class PReallocAction>
	void	reduce(PType* &src, PReallocAction& reallocAction)
	{
		C_ReallocAction<PReallocAction, PType>::ensure();
		PCO_INV_PTR(src != nullptr);
		mem_size currentSize = reallocAction.oldNb();
		mem_size newSize = reallocAction.newNb();

		PCO_INV_SIZE(currentSize > 0);
		PCO_ASSERT(newSize >= 0);

		if(newSize == 0)
		{	// Free all
			reallocAction.reduce(src);
			this->release(src);
			return;
		}

		if(!this->hasToMove(src, currentSize, newSize))
		{	// Resize
			reallocAction.reduce(src);
			this->resize(src, currentSize, newSize);
			return;
		}

		// Need to realloc and copy contents
		PType* dst = nullptr;
		this->reserve(dst, newSize);
		reallocAction.reduce(dst, src);
		this->release(src);
		src = dst;
	}

};

} // N..

#endif
