/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_INFLOW_FILTER_INFLOW_CARRIER_HPP_INCLUDED
#define	LIB_PCO_INFLOW_FILTER_INFLOW_CARRIER_HPP_INCLUDED

#include "flow/crt/I_InflowFilter.hpp"

namespace NPCO {

/*!
	InflowFilter wrapper containing inflow.

	@param PFilter Filter to wrap
	@param PInflow Inflow to hold
*/
template<class PFilter, class PInflow> class FlowFilterInflowCarrier
	: public PFilter {

protected:
	PInflow		includedInflow;

public:
	template<typename... Args>
	FlowFilterInflowCarrier(PInflow&& src, Args&&... args)
	 : PFilter(this->includedInflow, std::forward<Args>(args)...), includedInflow(std::move(src))
	{	}

	// Copy
	FlowFilterInflowCarrier(const FlowFilterInflowCarrier& ) = default;
	FlowFilterInflowCarrier&	operator=(const FlowFilterInflowCarrier& ) = default;

	// Move
	FlowFilterInflowCarrier(FlowFilterInflowCarrier&& ) = default;
	FlowFilterInflowCarrier&	operator=(FlowFilterInflowCarrier&& ) = default;

	const PInflow&	getIncludedInflow(void) const
	{
		return this->includedInflow;
	}

	PInflow&	getIncludedInflow(void)
	{
		return this->includedInflow;
	}

};

} // N..

#endif
