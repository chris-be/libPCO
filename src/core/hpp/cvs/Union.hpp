/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_UNION_HPP_INCLUDED
#define	LIB_PCO_UNION_HPP_INCLUDED

#include "PCO_Types.hpp"
#include "cvs/tpl_fct/V_MaxOf.hpp"
#include "PCO_ForceInline.hpp"

namespace NPCO {

namespace NUnion {

	template<class...> struct UnionMgr;

	template<class PType> struct UnionMgr<PType> {

		static void	construct(const size_t id, uw_08* ptr);
		static void	destruct(const size_t id, uw_08* ptr);

		static void	copy(const size_t id, uw_08* dst, const uw_08* src);
		// static void	move(const size_t id, uw_08* dst, uw_08* src);
	};

	template<class PType, class... PNext> struct UnionMgr<PType, PNext...> {

		static void	construct(const size_t id, uw_08* ptr);
		static void	destruct(const size_t id, uw_08* ptr);

		static void	copy(const size_t id, uw_08* dst, const uw_08* src);
		// static void	move(const size_t id, uw_08* dst, uw_08* src);
	};

} // NUnion

/**
	Union: union like, optionally holds a value

	Note: C++17 variant not available yet

	Note:
		Given:
			typedef TMyUnion = Union<bool, int>;
			TMyUnion	u;
		Something like:
			if(u.hasValueOf<bool>())	..
			else (u.hasValueOf<int>())	..
			else						..
		Can be handled like this:
			enum EMyUnion : int {
				  eUndefined	= TMyUnion::undefined_id
				, eBool			= TMyUnion::makeId<bool>()
				, eInt			= TMyUnion::makeId<int>()
			};
			switch(u.getId())
			{
				case eUndefined:	.. break;
				case eBool:			.. break;
				case eInt:			.. break;
				default:			..
			}

*/
template<class... PTypes> class Union {
public:
	static_assert(sizeof...(PTypes) < std::numeric_limits<int>::max(), "Too many types");

	//! "id" meaning "no value"
	static constexpr int undefined_id	= -1;

	//! Get "id" used for type
	template<class PValue>
	static constexpr int	makeId(void);

protected:
	static constexpr int type_number	= sizeof...(PTypes);

	static constexpr size_t max_sizeof	= v_maxOf(sizeof(PTypes)...);
	static constexpr size_t max_align	= v_maxOf(alignof(PTypes)...);

	//! Current id: -1: no value, > 0 index in template parameters of type stored
	int			id;
	//! Value space
	alignas(max_align) uw_08	data[max_sizeof];

	PCO_INLINE
	bool	inRange(const int id) const
	{
		return (id < type_number) && (id >= 0);
	}

	//! Change for "newId" - delete previous (if needed) and construct new (if needed)
	void	changeFor(const int newId);

public:
	Union(void);
	~Union(void);

	template<class PValue>
	Union(const PValue& value);

	// Copy
	Union(const Union<PTypes...>& toCopy);
	Union& operator=(const Union<PTypes...>& toCopy);

	// Move
	Union(Union<PTypes...>&& toMove);
	Union& operator=(Union<PTypes...>&& toMove);

	//! Get "id"
	int	getId(void) const;

	//! Delete current value and mark as "no value"
	void	clear(void);

	//! A value was set
	bool	hasValue(void) const;

	//! Test type of stored value
	template<class PValue>
	bool	hasValueOf(void) const;

	//! Get value of given type
	template<class PValue>
	const PValue&	get(void) const;

	//! Get value of given type
	template<class PValue>
	PValue&			get(void);

	//! Set value
	template<class PValue>
	void	set(const PValue& value);

	//! Move value
	template<class PValue>
	void	moveIn(PValue&& toMove);

};

} // N..

#include "imp/Union.cppi"

#endif
