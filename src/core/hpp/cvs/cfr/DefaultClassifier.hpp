/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_DEFAULT_CLASSIFIER_HPP_INCLUDED
#define	LIB_PCO_DEFAULT_CLASSIFIER_HPP_INCLUDED

#include "pattern/cfr/C_Classifier.hpp"
#include "pattern/cfr/C_SelfClassified.hpp"

namespace NPCO {

/**
	Default implementation for classifier: for C_SelfClassified objects

	@param PSelf SelfClassified object
*/
template<class PSelf>
class DefaultClassifier
		: private C_Classifier<DefaultClassifier<PSelf>, PSelf> {

public:
	using TClassified				= PSelf;

public:
	DefaultClassifier(void)
	{
		C_SelfClassified<PSelf>::ensure();
	}

	//! @see C_Classifier
	PCO_INLINE
	ClassifyValue	classify(const PSelf& o1, const PSelf& o2) const
	{
		return o1.classify(o2);
	}

};

} // N..

#endif
