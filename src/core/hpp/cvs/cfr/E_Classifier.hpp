/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_CLASSIFIER_HPP_INCLUDED
#define	LIB_PCO_E_CLASSIFIER_HPP_INCLUDED

#include "pattern/cfr/C_Classifier.hpp"
//#include "pattern/cfr/C_SelfClassified.hpp"
//#include "PCO_ForceInline.hpp"

namespace NPCO {

/** Extension for objects constrained by C_Classifier / C_SelfClassified
	@see C_Bag
*/

//! Returns if o1 "is same as" o2
template<class PSelf>
PCO_INLINE
static bool	e_isSame(const PSelf& o1, const PSelf& o2) //noexcept
{
	return o1.classify(o2) == 0;
}

//! Returns if o1 "is before" o2
template<class PSelf>
PCO_INLINE
static bool	e_isBefore(const PSelf& o1, const PSelf& o2) //noexcept
{
	return o1.classify(o2) < 0;
}

//! Returns if o1 "is after" o2
template<class PSelf>
PCO_INLINE
static bool	e_isAfter(const PSelf& o1, const PSelf& o2) //noexcept
{
	return o1.classify(o2) > 0;
}

} // N..

#endif