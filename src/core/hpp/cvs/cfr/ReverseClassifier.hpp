/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_REVERSE_CLASSIFIER_HPP_INCLUDED
#define	LIB_PCO_REVERSE_CLASSIFIER_HPP_INCLUDED

#include "pattern/cfr/C_Classifier.hpp"

namespace NPCO {

/**
	Reverse Classifier: reverse given IClassifier results

	@param PClassifier
*/
template<class PClassifier>
class ReverseClassifier
	: private C_Classifier<PClassifier, typename PClassifier::TClassified> {

public:
	using TClassified		= typename PClassifier::TClassified;

protected:
	PClassifier		classifier;

public:

	//! @see C_Classifier
	PCO_INLINE
	int		classify(const TClassified& o1, const TClassified& o2) const
	{
		return this->classifier.classify(o2, o1);
	}

};

} // N..

#endif
