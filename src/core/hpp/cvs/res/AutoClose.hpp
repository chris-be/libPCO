/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AUTO_CLOSE_HPP_INCLUDED
#define	LIB_PCO_AUTO_CLOSE_HPP_INCLUDED

#include "pattern/res/I_Resource.hpp"

namespace NPCO {

/*!
	AutoClose is designed to close automatically resources.
	Idea : ensure "close" automatically if any exception occurs.

	Example :
	try
	{
		AutoClose	ac(file);
		// Do whatever...
	}
	// Here "file" is closed
	catch(...)
	{	// No worries about file
	}
 */
class AutoClose
{
private:
	IResource&	resource;

public:
	//! Get resource
	PCO_INLINE
	AutoClose(IResource& resource)
	 : resource(resource)
	{	}

	//! Close resource
 	PCO_INLINE
	~AutoClose(void)
	{
		if(this->resource.isOpened())
		{
			this->resource.close();
		}
	}

};

} // N..

#endif
