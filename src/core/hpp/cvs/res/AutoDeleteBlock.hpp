/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AUTO_DELETE_BLOCK_HPP_INCLUDED
#define	LIB_PCO_AUTO_DELETE_BLOCK_HPP_INCLUDED

#include "AutoClean.hpp"
#include "mem/Mem.hpp"

namespace NPCO {

/*!
	AutoDeleteBlock follow Auto logic for "memory block"
	"Block" is for "allocated blocks" and simply calls delete[]

	@param PType Pointed class
*/
template<class PType>
class AutoDeleteBlock
	: public AutoClean<PType, Mem::_deleteE<PType>> {

protected:
	using TBase	= AutoClean<PType, Mem::_deleteE<PType>>;

public:
//	//! Import constructors
//	using TBase::AutoClean;

	AutoDeleteBlock(void) noexcept : TBase()
	{	}

	AutoDeleteBlock(PType* resource) noexcept : TBase(resource)
	{	}

	~AutoDeleteBlock(void) = default;

	// Copy
	// AutoDeleteBlock(const AutoDeleteBlock& ) = delete;
	// AutoDeleteBlock& operator=(const AutoDeleteBlock& ) = delete;

	// Move
	AutoDeleteBlock(AutoDeleteBlock&& toMove) = default;
	AutoDeleteBlock& operator=(AutoDeleteBlock&& toMove) = default;

/*
	PCO_INLINE
	AutoDeleteBlock& operator=(PType* ptr)
	{
		this->_setPtr(ptr);
		return *this;
	}

	//! Access to ith element (Can't do verification on "bound")
	PCO_INLINE
	const PType&	operator[](const int i) const
	{
		PCO_INV_PTR(this->isValid());
		PCO_INV_SIZE(i);
		return this->ptr[i];
	}

	//! Access to ith element (Can't do verification on "bound")
	PCO_INLINE
	PType&			operator[](const int i)
	{
		PCO_INV_PTR(this->isValid());
		PCO_INV_SIZE(i);
		return this->ptr[i];
	}
*/

};

} // N..

#endif
