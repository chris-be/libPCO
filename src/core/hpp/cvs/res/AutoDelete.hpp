/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AUTO_DELETE_HPP_INCLUDED
#define	LIB_PCO_AUTO_DELETE_HPP_INCLUDED

#include "AutoClean.hpp"
#include "mem/Mem.hpp"

namespace NPCO {

/*!
	AutoDeleteBlock follow Auto logic for "memory object"

	@param PType Pointed class
*/
template<class PType>
class AutoDelete
	: public AutoClean<PType, Mem::_delete<PType>> {

protected:
	using TBase	= AutoClean<PType, Mem::_delete<PType>>;

public:
	// //! Import constructors
	// using TBase::AutoClean;
	AutoDelete(void) noexcept : TBase()
	{	}

	AutoDelete(PType* resource) noexcept : TBase(resource)
	{	}

	~AutoDelete(void) = default;

	// Copy
	// AutoDelete(const AutoDelete& ) = default;
	// AutoDelete& operator=(const AutoDelete& ) = default;

	// Move
	AutoDelete(AutoDelete&& toMove) = default;
	AutoDelete& operator=(AutoDelete&& toMove) = default;

	PCO_INLINE
	AutoDelete& operator=(PType* ptr)
	{
		this->_setPtr(ptr);
		return *this;
	}

/*
	PCO_INLINE
	bool	operator==(const PType* ptr) const
	{
		return this->ptr == ptr;
	}

	PCO_INLINE
	bool	operator!=(const PType* ptr) const
	{
		return this->ptr == ptr;
	}
*/

};

} // N..

#endif
