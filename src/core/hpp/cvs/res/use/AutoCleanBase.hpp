/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AUTO_CLEAN_BASE_HPP_INCLUDED
#define	LIB_PCO_AUTO_CLEAN_BASE_HPP_INCLUDED

#include "ResourceHolder.hpp"

namespace NPCO {

//! Prerequisite for AutoClean
template<typename PType>
using AutoCleanFunc_t = void (*)(PType* &res);

/*!
	AutoCleanBase provides everything for AutoClean capable class.

	@see AutoClean
 */
template<class PType, AutoCleanFunc_t<PType> cleanFunc>
class AutoCleanBase : public ResourceHolder<PType> {

protected:
	using TBase	= ResourceHolder<PType>;

protected:
	static_assert(cleanFunc != nullptr);

	//! Called when cleanup needed (destructor, ...)
	void	_clean(void);

	//! Set resource and clean old one
	void	_setPtr(PType* ptr);

public:
	AutoCleanBase(void) noexcept;
	AutoCleanBase(PType* resource) noexcept;
	//! Clean resource
	virtual ~AutoCleanBase(void) override;

	// Copy
	AutoCleanBase(const AutoCleanBase& ) = delete;
	AutoCleanBase& operator=(const AutoCleanBase& ) = delete;

	// Move
	AutoCleanBase(AutoCleanBase&& toMove) = default;
	AutoCleanBase& operator=(AutoCleanBase&& toMove);

};

} // N..

#include "imp/AutoCleanBase.cppi"

#endif
