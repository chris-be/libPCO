/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

//#include	"cvs/res/ResourceHolder.hpp"

namespace NPCO {

template<class PType>
ResourceHolder<PType>::ResourceHolder(void) noexcept
 : ResourceHolder(nullptr)
{	}

template<class PType>
ResourceHolder<PType>::ResourceHolder(PType* res) noexcept
 : res(res)
{	}

template<class PType>
ResourceHolder<PType>::ResourceHolder(ResourceHolder&& toMove)
{
	// Copy pointer and prevent "cleaning" toMove
	this->res = toMove.res;
	toMove.res = nullptr;
}

template<class PType>
void	ResourceHolder<PType>::_setRes(PType* res)
{
	this->res = res;
}

template<class PType>
PType*	ResourceHolder<PType>::_popOut(void)
{
	PType*	tmp = this->res;
	this->res = nullptr;

	return tmp;
}

template<class PType>
bool	ResourceHolder<PType>::isNull(void) const
{
	return this->res == nullptr;
}

template<class PType>
bool	ResourceHolder<PType>::isValid(void) const
{
	return this->res != nullptr;
}

template<class PType>
const PType*	ResourceHolder<PType>::_da(void) const
{
	return this->res;
}

template<class PType>
PType*			ResourceHolder<PType>::_da(void)
{
	return this->res;
}

} // N..
