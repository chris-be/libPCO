/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AUTO_CLEAN_MEM_HPP_INCLUDED
#define	LIB_PCO_AUTO_CLEAN_MEM_HPP_INCLUDED

#include "AutoCleanBase.hpp"

namespace NPCO {

/*!
	AutoCleanMem: AutoCleanBase with operators *, ->

	@convenience
	@param PType Pointed class
*/
template<class PType, AutoCleanFunc_t<PType> cleanFunc>
class AutoCleanMem
	: public AutoCleanBase<PType, cleanFunc> {

protected:
	using TBase		= AutoCleanBase<PType, cleanFunc>;

public:
	using TBase::AutoCleanBase;
	~AutoCleanMem(void) = default;

	// Copy
	// AutoCleanMem(const AutoCleanMem& toCopy) = delete;
	// AutoCleanMem& operator=(const AutoCleanMem& toCopy) = delete;

	// Move
	AutoCleanMem(AutoCleanMem&& toMove) = default;
	AutoCleanMem& operator=(AutoCleanMem&& toMove) = default;

	//! Get reference
	PCO_INLINE
	const PType&	operator*(void) const
	{
		PCO_LOGIC_ERR(this->isValid(), "bad use");
		return *(this->_da());
	}

	//! Get reference
	PCO_INLINE
	PType&			operator*(void)
	{
		PCO_LOGIC_ERR(this->isValid(), "bad use");
		return *(this->_da());
	}

	//! Get pointer
	PCO_INLINE
	const PType*	operator->(void) const
	{
		PCO_LOGIC_ERR(this->isValid(), "bad use");
		return this->_da();
	}

	//! Get pointer
	PCO_INLINE
	PType*			operator->(void)
	{
		PCO_LOGIC_ERR(this->isValid(), "bad use");
		return this->_da();
	}

};

} // N..

#endif
