/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_RESOURCE_HOLDER_HPP_INCLUDED
#define	LIB_PCO_RESOURCE_HOLDER_HPP_INCLUDED

#include "PCO_ForceInline.hpp"

namespace NPCO {

/*!
	ResourceHolder simply keeps track of resource.

 */
template<class PType>
class ResourceHolder {

protected:
	// Pointer to resource
	PType*	res;

protected:
	// Copy
	ResourceHolder(const ResourceHolder& ) = delete;
	ResourceHolder& operator=(const ResourceHolder& ) = delete;

	// Move
	ResourceHolder(ResourceHolder&& toMove);
	// Child class must handle
	ResourceHolder& operator=(ResourceHolder&& toMove) = delete;

	//! Set resource
	void	_setRes(PType* res);

	//! Return resource and set it to nullptr (without calling cleaner)
	PType*	_popOut(void);

public:
	ResourceHolder(void) noexcept;
	ResourceHolder(PType* res) noexcept;
	//! Do nothing
	virtual ~ResourceHolder(void) = default;

	//! Verify if pointer == nullptr
	PCO_INLINE
	bool	isNull(void) const;

	//! Verify if pointer != nullptr
	PCO_INLINE
	bool	isValid(void) const;

	//! Get pointer - use with care
	PCO_INLINE
	const PType*	_da(void) const;
	//! Get pointer - use with care
	PCO_INLINE
	PType*			_da(void);

};

} // N..

#include "imp/ResourceHolder.cppi"

#endif
