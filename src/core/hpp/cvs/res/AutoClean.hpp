/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AUTO_CLEAN_HPP_INCLUDED
#define	LIB_PCO_AUTO_CLEAN_HPP_INCLUDED

#include "use/AutoCleanBase.hpp"

namespace NPCO {

/*!
	AutoClean is designed to "free" automatically resources.
	Idea : ensure "free" automatically if any exception occurs.

	Example :
	try
	{
		AutoClean	ac(resource);
		// Do whatever...
	}
	// Here "resource" is "freed"
	catch(...)
	{	// No worries about resource
	}

 */
template<class PType, AutoCleanFunc_t<PType> cleanFunc>
class AutoClean : public AutoCleanBase<PType, cleanFunc> {

protected:
	using TBase	= AutoCleanBase<PType, cleanFunc>;

public:
	using TBase::AutoCleanBase;

	// Copy
	AutoClean(const AutoClean& ) = delete;
	AutoClean& operator=(const AutoClean& ) = delete;

	// Move
	AutoClean(AutoClean&& toMove) = default;
	AutoClean& operator=(AutoClean&& toMove) = default;

	//! @see _popOut
	PType* popOut(void)
	{
		return this->_popOut();
	}

};

} // N..

#endif
