/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AUTO_LOCK_HPP_INCLUDED
#define	LIB_PCO_AUTO_LOCK_HPP_INCLUDED

#include "crt/res/I_ResourceLock.hpp"

namespace NPCO {

/*!
	AutoLock is designed to lock/unlock automatically resources.
	Idea : ensure "unlock" automatically if any exception occurs.

	Example :
	try
	{
		AutoLock	al(mutex);
		// Do whatever...
	}
	// Here "mutex" is unlocked
	catch(...)
	{	// No worries about mutex
	}
 */
class AutoLock
{
private:
	IResourceLock&	resource;

public:
	//! Lock resource
	PCO_INLINE
	AutoLock(IResourceLock& resource)
	 : resource(resource)
	{
		this->resource.lock();
	}

	//! Unlock resource
 	PCO_INLINE
	~AutoLock(void)
	{
		this->resource.unlock();
	}

};

} // N..

#endif
