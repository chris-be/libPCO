/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_USE_CLASSIFIER_HPP_INCLUDED
#define	LIB_PCO_USE_CLASSIFIER_HPP_INCLUDED

#include "pattern/cfr/I_Classifier.hpp"

namespace NPCO {

/**
	Base class for convenience

	@param PClassifier Classifier to use
*/
template<class PClassifier>
class UseClassifier {

protected:
    //! Classifier to use
    PClassifier		classifier;

public:
	// Ensure proper cleanup
	// virtual ~UseClassifier(void) = default;

	//! Classifier used
	const PClassifier	getClassifier(void) const
	{
		return this->classifier;
	}

};

} // N..

#endif
