/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_USE_MEMORY_MANAGER_HPP_INCLUDED
#define	LIB_PCO_USE_MEMORY_MANAGER_HPP_INCLUDED

namespace NPCO {

/**
	Base class for convenience
*/
template<class PMemMgr>
class UseMemoryManager {

public:
	using TMemMgr		= PMemMgr;

protected:
	//! MemoryManager to use
	PMemMgr			memManager;

public:
	// Ensure proper cleanup
	// virtual ~UseMemoryManager(void) = default;

	//! Memory Manager configuration access
	const PMemMgr&	getManager(void) const
	{
		return memManager;
	}

	//! Memory Manager configuration access
	PMemMgr&		getManager(void)
	{
		return memManager;
	}

};

} // N..

#endif
