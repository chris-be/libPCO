/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_EMPTY_CLASS_HPP_INCLUDED
#define	LIB_PCO_EMPTY_CLASS_HPP_INCLUDED

/**
	Empty class, yes :D
	@see AVLTree
*/
class EmptyClass	{};

#endif
