/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_T_PARAM_HPP_INCLUDED
#define	LIB_PCO_T_PARAM_HPP_INCLUDED

#include <type_traits>

namespace NPCO {

template<typename PClass, bool byValue>
struct value_reference_selector;

template<typename PClass>
struct value_reference_selector<PClass, false>
{
	typedef const PClass&	type;
};

template<typename PClass>
struct value_reference_selector<PClass, true>
{
	typedef const PClass	type;
};

/**
	Trait to use value or reference for parameter.
*/
template<typename PClass>
struct param_transit : value_reference_selector<PClass, std::is_fundamental_v<PClass>>
{	};

template<typename PClass>
using param_transit_t = typename param_transit<PClass>::type;


} // N..

// #define	PCO_PARAM(Type)		typename NPCO::value_reference_selector<Type>::param

#endif
