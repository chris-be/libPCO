/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_INDEX_RIDER_ITER_HPP_INCLUDED
#define	LIB_PCO_INDEX_RIDER_ITER_HPP_INCLUDED

#include "./IndexWalkerIter.hpp"
#include "pattern/walk/I_IndexRider.hpp"

namespace NPCO {

/*!
	IndexRider using IndexIterWrite
	@see IIndexRider
	@see IndexWalkerIter
	@param PIter IIterWrite
*/
template<class PIter, class PKey = typename PIter::TKey, class PValue = typename PIter::TValue>
class IndexRiderIter
	: public IIndexRider<PKey, PValue>
	, virtual public IndexWalkerIter<PIter, PKey, PValue> {

public:
	IndexRiderIter(void) = default;
	IndexRiderIter(const PIter& first, const PIter& last) : IndexWalkerIter<PIter, PKey, PValue>(first, last)
	{	}

	// Copy
	IndexRiderIter(const IndexRiderIter<PIter, PKey, PValue>& ) = default;
	IndexRiderIter& operator=(const IndexRiderIter<PIter, PKey, PValue>& ) = default;

	// Move
	IndexRiderIter(IndexRiderIter<PIter, PKey, PValue>&& ) = default;
	IndexRiderIter& operator=(IndexRiderIter<PIter, PKey, PValue>&& ) = default;

	PCO_INLINE
	const IndexRiderIter&	operator++(void)
	{
		this->forward();	return *this;
	}

	PCO_INLINE
	const IndexRiderIter&	operator--(void)
	{
		this->back();		return *this;
	}

};

} // N..

#endif
