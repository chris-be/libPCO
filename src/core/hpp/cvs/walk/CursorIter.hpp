/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CURSOR_ITER_HPP_INCLUDED
#define	LIB_PCO_CURSOR_ITER_HPP_INCLUDED

#include "WalkerIter.hpp"
#include "pattern/walk/C_Cursor.hpp"
#include "pattern/flow/I_Inflow.hpp"

namespace NPCO {

/*!
	Cursor using IterRead - @see C_Cursor
	@convenience can be used as inflow - @see IInflow

	@see WalkerIter
	@param PIter IIterRead
*/
template<class PIter, class PCell = typename PIter::TCell>
class CursorIter	: private C_Cursor<CursorIter<PIter, PCell>, PCell>
					, public WalkerIter<PIter, const PCell>
					, public IInflow<PCell> {

protected:
	using TBase		= WalkerIter<PIter, const PCell>;

public:
	using TCell		= typename TBase::TCell;

public:
	CursorIter(void) = default;
	CursorIter(const PIter& first, const PIter& last) : TBase(first, last)
	{	}

	~CursorIter(void) = default;

	// Copy
	CursorIter(const CursorIter<PIter, PCell>& ) = default;
	CursorIter& operator=(const CursorIter<PIter, PCell>& ) = default;

	// Move
	CursorIter(CursorIter<PIter, PCell>&& ) = default;
	CursorIter& operator=(CursorIter<PIter, PCell>&& ) = default;

	PCO_INLINE
	const CursorIter&	operator++(void)
	{
		this->forward();	return *this;
	}

	PCO_INLINE
	const CursorIter&	operator--(void)
	{
		this->back();		return *this;
	}

	//! @see IInflow
	virtual bool	read(PCell& element) override
	{
		bool ret = this->ok();
		if(ret)
		{
			element = this->cell();
			this->forward();
		}

		return ret;
	}

};

} // N..

#endif
