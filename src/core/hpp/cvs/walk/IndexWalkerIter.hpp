/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_INDEX_WALKER_ITER_HPP_INCLUDED
#define	LIB_PCO_INDEX_WALKER_ITER_HPP_INCLUDED

#include "pattern/walk/I_IndexWalker.hpp"

namespace NPCO {

/*!
	IndexWalker using IIndexIter

	@param PKey @see IIndexWalker
	@param PValue @see IIndexWalker
	@param PIter For inheritance
*/
template<class PIter, class PKey, class PValue> class IndexWalkerIter
	: virtual public ::NPCO::IIndexWalker<PKey, PValue> {

public:
	using TCell			= typename std::remove_const<PValue>::type;
	using TKey			= PKey;
	using TValue		= PValue;

protected:
	PIter		first;
	PIter		last;
	PIter		current;

public:
	IndexWalkerIter(void);
	IndexWalkerIter(const PIter& first, const PIter& last);

	// Copy
	IndexWalkerIter(const IndexWalkerIter<PIter, PKey, PValue>& ) = delete;
	IndexWalkerIter& operator=(const IndexWalkerIter<PIter, PKey, PValue>& ) = delete;

	// Move
	IndexWalkerIter(IndexWalkerIter<PIter, PKey, PValue>&& ) = default;
	IndexWalkerIter& operator=(IndexWalkerIter<PIter, PKey, PValue>&& ) = default;

	void	set(const PIter& first, const PIter& last);

	const PIter&	getFirst(void) const;
	const PIter&	getLast(void) const;
	const PIter&	getCurrent(void) const;

	//! @see IWalker
	virtual bool	checkRange(void) const override;

	//! @see IWalker
	virtual void	toFirst(void) override;

	//! @see IWalker
	virtual void	toLast(void) override;

	//! @see IWalker
	virtual void	forward(void) override;

	//! @see IWalker
	virtual void	back(void) override;

	//! @see IWalker
	virtual void	operator+=(size_t step) override;

	//! @see IWalker
	virtual void	operator-=(size_t step) override;

	//! @see IWalker
	virtual bool	ok(void) const override;

	//! @see IWalker
	virtual PValue&	operator*(void) const override;

	//! @see IWalker
	virtual PValue*	operator->(void) const override;

	//! @see IIndexWalker
	virtual const PKey&	key(void) const override;

	//! @see IWalker
	virtual void	limitRangeFirst(void) override;
	//! @see IWalker
	virtual void	limitRangeLast(void) override;

};

} // N..

#include "imp/IndexWalkerIter.cppi"

#endif
