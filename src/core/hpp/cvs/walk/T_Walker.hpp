/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_T_WALKER_HPP_INCLUDED
#define	LIB_PCO_T_WALKER_HPP_INCLUDED

// #include "pattern/walk/C_Walker.hpp"
#include <type_traits>

namespace NPCO {

// Test if PClass is C_Walker
template<typename PClass, typename = void>
struct is_walker : public std::false_type
{	};

template<typename PClass>
struct is_walker<PClass, std::void_t<typename PClass::TCell>>
 : public std::true_type
{	};



} // N..

#endif
