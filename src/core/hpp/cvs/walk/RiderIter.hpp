/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_RIDER_ITER_HPP_INCLUDED
#define	LIB_PCO_RIDER_ITER_HPP_INCLUDED

#include "WalkerIter.hpp"
#include "pattern/walk/C_Rider.hpp"

namespace NPCO {

/*!
	Rider using IterWrite - @see C_Rider

	@see WalkerIter
	@param PIter C_IterWrite
*/
template<class PIter, class PCell = typename PIter::TCell>
class RiderIter : private C_Rider<RiderIter<PIter, PCell>, PCell>
				, public WalkerIter<PIter, PCell> {

protected:
	using TBase		= WalkerIter<PIter, PCell>;

public:
	RiderIter(void) = default;
	RiderIter(const PIter& first, const PIter& last) : TBase(first, last)
	{ }

	// Copy
	RiderIter(const RiderIter<PIter, PCell>& ) = default;
	RiderIter& operator=(const RiderIter<PIter, PCell>& ) = default;

	// Move
	RiderIter(RiderIter<PIter, PCell>&& toMove) = default;
	RiderIter& operator=(RiderIter<PIter, PCell>&& toMove) = default;

	PCO_INLINE
	const RiderIter&	operator++(void)
	{
		this->forward();	return *this;
	}

	PCO_INLINE
	const RiderIter&	operator--(void)
	{
		this->back();		return *this;
	}

};

} // N..

#endif
