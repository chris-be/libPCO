/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CURSOR_ITER_HPP_INCLUDED
#define	LIB_PCO_CURSOR_ITER_HPP_INCLUDED

#include "WalkerIter.hpp"
#include "pattern/walk/I_Cursor.hpp"
#include "pattern/flow/I_Inflow.hpp"

namespace NPCO {

/*!
	ICursor using IterRead
	@convenience can be used as inflow - @see IInflow

	@see ICursor
	@see WalkerIter
	@param PIter IIterRead
*/
template<class PIter, class PCell = typename PIter::TCell> class CursorIter
	: public ICursor<PCell>
	, virtual public WalkerIter<PIter, const PCell>
	, public IInflow<PCell> {

protected:
	//using TCellType		= typename WalkerIter<PIter, const PCell>::TCellType;

public:
	CursorIter(void) = default;
	CursorIter(const PIter& first, const PIter& last) : WalkerIter<PIter, const PCell>(first, last)
	{	}

	virtual ~CursorIter(void) override = default;

	// Copy
	CursorIter(const CursorIter<PIter, PCell>& ) = default;
	CursorIter& operator=(const CursorIter<PIter, PCell>& ) = default;

	// Move
	CursorIter(CursorIter<PIter, PCell>&& ) = default;
	CursorIter& operator=(CursorIter<PIter, PCell>&& ) = default;

	//! @see IInflow
	virtual bool	read(PCell& element) override
	{
		bool ret = this->ok();
		if(ret)
		{
			element = this->cell();
			this->forward();
		}

		return ret;
	}

};

} // N..

#endif
