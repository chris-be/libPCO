/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_RIDER_ITER_HPP_INCLUDED
#define	LIB_PCO_RIDER_ITER_HPP_INCLUDED

#include "./WalkerIter.hpp"
#include "pattern/walk/I_Rider.hpp"
#include "pattern/flow/I_Outflow.hpp"

namespace NPCO {

/*!
	IRider using IIterWrite
	@see IRider
	@see WalkerIter
	@param PIter IIterWrite
*/
template<class PIter, class PCell = typename PIter::TCell> class RiderIter
	: public IRider<PCell>
	, virtual public WalkerIter<PIter, PCell>
	, public IOutflow<PCell> {

public:
	RiderIter(void) = default;
	RiderIter(const PIter& first, const PIter& last) : WalkerIter<PIter, PCell>(first, last)
	{ }

	// Copy
	RiderIter(const RiderIter<PIter, PCell>& ) = delete;
	RiderIter& operator=(const RiderIter<PIter, PCell>& ) = delete;

	// Move
	RiderIter(RiderIter<PIter, PCell>&& toMove) = default;
	RiderIter& operator=(RiderIter<PIter, PCell>&& toMove) = default;

	//! @see IOutflow
	virtual void	write(const PCell& element) override
	{
		if(!this->ok())
		{
			PCO_RANGE_ERR(false, "More write than possible");
		}

		this->cell() = element;
		this->forward();
	}

};

} // N..

#endif
