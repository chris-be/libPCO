/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_WALKER_HPP_INCLUDED
#define	LIB_PCO_E_WALKER_HPP_INCLUDED

#include "T_Walker.hpp"
#include "pattern/logic/I_Selector.hpp"

namespace NPCO {

/** Extension for "walker" objects (constrained by C_IWalker ? )
	@see IWalker
*/

/*! Finds out an element if exists - use operator==
	@return true if found / "walker" at right position
*/
template<class PWalker>
bool	e_find(PWalker& walker, const typename PWalker::TCell& toFind);

/*! Finds out in reverse order an element if exists - use operator==
	@return true if found / "walker" at right position
*/
template<class PWalker>
bool	e_rvsFind(PWalker& walker, const typename PWalker::TCell& toFind);

/*! Finds out an element if exists
	@param classifier Classifier to use
	@return true if found / "walker" at right position
*/
template<class PWalker, class PClassifier>
bool	e_find(PWalker& walker, const typename PWalker::TCell& toFind, const PClassifier& classifier);

/*! Finds out an element if exists
	@param classifier Classifier to use
	@return true if found / "walker" at right position
*/
template<class PWalker, class PClassifier>
bool	e_rvsFind(PWalker& walker, const typename PWalker::TCell& toFind, const PClassifier& classifier);

/*! Finds out an element if exists
	@param selector Selector to use
	@return true if found / "walker" at right position
*/
template<class PWalker>
bool	e_find(PWalker& walker, typename ISelector<typename PWalker::TCell>::SelFunc selector);

/*! Finds out an element if exists
	@param selector Selector to use
	@return true if found / "walker" at right position
*/
template<class PWalker>
bool	e_rvsFind(PWalker& walker, typename ISelector<typename PWalker::TCell>::SelFunc selector);

/*! Finds out an element if exists
	@param selector Selector to use
	@return true if found / "walker" at right position
*/
template<class PWalker>
bool	e_find(PWalker& walker, const ISelector<typename PWalker::TCell>& selector);

/*! Finds out an element if exists
	@param selector Selector to use
	@return true if found / "walker" at right position
*/
template<class PWalker>
bool	e_rvsFind(PWalker& walker, const ISelector<typename PWalker::TCell>& selector);


/** Forward walker as long as it points to any given pattern
	@return True if something found, walker.current() pointing to last found item
*/
template<class PWalker, class PC_KeyBag>
bool	e_scanFromStart(PWalker& walker, const PC_KeyBag& patterns);

/** Backward walker as long as it points to any given pattern
	@return True if something found, walker.current() pointing to last found item
*/
template<class PWalker, class PC_KeyBag>
bool	e_scanFromEnd(PWalker& walker, const PC_KeyBag& patterns);

} // N..

#include "imp/E_Walker.cppi"

#endif
