/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_WALKER_ITER_HPP_INCLUDED
#define	LIB_PCO_WALKER_ITER_HPP_INCLUDED

#include "pattern/walk/C_Walker.hpp"

namespace NPCO {

/*!
	Walker using Iter - @see C_Walker

	@param PIter For inheritance
	@param PCell Cell type
*/
template<class PIter, class PCell>
class WalkerIter : private C_Walker<WalkerIter<PIter, PCell>, PCell> {

public:
	using TCell		= typename std::remove_const<PCell>::type;

protected:
	PIter		first;
	PIter		last;
	PIter		current;

public:
	WalkerIter(void);
	WalkerIter(const PIter& first, const PIter& last);
	~WalkerIter(void) = default;

	// Copy
	WalkerIter(const WalkerIter<PIter, PCell>& ) = default;
	WalkerIter& operator=(const WalkerIter<PIter, PCell>& ) = default;

	// Move
	WalkerIter(WalkerIter<PIter, PCell>&& ) = default;
	WalkerIter& operator=(WalkerIter<PIter, PCell>&& ) = default;

	void	set(const PIter& first, const PIter& last);

	const PIter&	getFirst(void) const;
	const PIter&	getLast(void) const;
	const PIter&	getCurrent(void) const;

	//! @see C_Walker
	bool	checkRange(void) const;

	//! @see C_Walker
	void	toFirst(void);

	//! @see C_Walker
	void	toLast(void);

	//! @see C_Walker
	PCO_INLINE
	void	forward(void);

	//! @see C_Walker
	PCO_INLINE
	void	back(void);

	//! @see C_Walker
	void	operator+=(size_t step);

	//! @see C_Walker
	void	operator-=(size_t step);

	//! @see C_Walker
	PCO_INLINE
	bool	ok(void) const;

	//! @see C_Walker
	PCO_INLINE
	PCell&	operator*(void) const;

	//! @see C_Walker
	PCO_INLINE
	PCell*	operator->(void) const;

	//! @see C_Walker
	void	limitRangeFirst(void);
	//! @see C_Walker
	void	limitRangeLast(void);

	//! @see C_Walker
	PCO_INLINE
	PCell&	cell(void) const
	{
		return this->operator*();
	}

};

} // N..

#include "imp/WalkerIter.cppi"

#endif
