/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_INDEX_CURSOR_ITER_HPP_INCLUDED
#define	LIB_PCO_INDEX_CURSOR_ITER_HPP_INCLUDED

#include "./IndexWalkerIter.hpp"
#include "pattern/walk/I_IndexCursor.hpp"

namespace NPCO {

/*!
	IndexCursor using IndexIterRead
	@see IIndexCursor
	@see IndexWalkerIter
	@param PIter IIterRead
*/
template<class PIter, class PKey = typename PIter::TKey, class PValue = typename PIter::TValue>
class IndexCursorIter
	: public IIndexCursor<PKey, PValue>
	, virtual public IndexWalkerIter<PIter, PKey, const PValue> {

public:
	IndexCursorIter(void) = default;
	IndexCursorIter(const PIter& first, const PIter& last) : IndexWalkerIter<PIter, PKey, const PValue>(first, last)
	{	}

	// Copy
	IndexCursorIter(const IndexCursorIter<PIter, PKey, PValue>& ) = default;
	IndexCursorIter& operator=(const IndexCursorIter<PIter, PKey, PValue>& ) = default;

	// Move
	IndexCursorIter(IndexCursorIter<PIter, PKey, PValue>&& ) = default;
	IndexCursorIter& operator=(IndexCursorIter<PIter, PKey, PValue>&& ) = default;

	PCO_INLINE
	const IndexCursorIter&	operator++(void)
	{
		this->forward();	return *this;
	}

	PCO_INLINE
	const IndexCursorIter&	operator--(void)
	{
		this->back();		return *this;
	}

};

} // N..

#endif
