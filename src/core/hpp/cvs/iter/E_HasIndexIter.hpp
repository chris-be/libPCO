/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_HAS_INDEX_ITER_HPP_INCLUDED
#define	LIB_PCO_E_HAS_INDEX_ITER_HPP_INCLUDED

#include "pattern/iter/C_HasIndexIterRead.hpp"
#include "cvs/walk/IndexCursorIter.hpp"

#include "pattern/iter/C_HasIndexIterRead.hpp"
#include "cvs/walk/IndexRiderIter.hpp"

namespace NPCO {

/** Extension for objects constrained by C_HasIndexIterRead
	@see C_HasIndexIterRead
*/

//! Create an uninitialized index cursor
template<class PC_HasIndexIterRead>
auto	e_createIndexCursor(const PC_HasIndexIterRead& iterable)
{
	auto	f = iterable.first();
	auto	l = iterable.last();

	return IndexCursorIter<decltype(f)>(f, l);
}

//! Create a "forward index cursor"
template<class PC_HasIndexIterRead>
auto	e_indexCursor(const PC_HasIndexIterRead& iterable)
{
	auto	c = e_createIndexCursor(iterable);
	c.toFirst();
	return c;
}

//! Create a "backward index cursor"
template<class PC_HasIndexIterRead>
auto	e_backIndexCursor(const PC_HasIndexIterRead& iterable)
{
	auto	c = e_createIndexCursor(iterable);
	c.toLast();
	return c;
}


//! Create an uninitialized index cursor
template<class PC_HasIndexIterRead, class PSize>
auto	e_createIndexCursor(const PC_HasIndexIterRead& iterable, const PSize first, const PSize last)
{
	auto	f = iterable.iter_r(first);
	auto	l = iterable.iter_r(last);

	return IndexCursorIter<decltype(f)>(f, l);
}

//! Create a "forward index cursor"
template<class PC_HasIndexIterRead, class PSize>
auto	e_indexCursor(const PC_HasIndexIterRead& iterable, const PSize first, const PSize last)
{
	auto	c = e_createIndexCursor(iterable, first, last);
	c.toFirst();
	return c;
}

//! Create a "backward index cursor"
template<class PC_HasIndexIterRead, class PSize>
auto	e_backIndexCursor(const PC_HasIndexIterRead& iterable, const PSize first, const PSize last)
{
	auto	c = e_createIndexCursor(iterable, first, last);
	c.toLast();
	return c;
}


/** Extension for objects constrained by C_HasIndexIterWrite
	@see C_HasIndexIterWrite
*/

//! Create an uninitialized index rider
template<class PC_HasIndexIterWrite>
auto	e_createIndexRider(PC_HasIndexIterWrite& iterable)
{
	auto	f = iterable.first();
	auto	l = iterable.last();

	return IndexRiderIter<decltype(f)>(f, l);
}

//! Create a "forward index rider"
template<class PC_HasIndexIterWrite>
auto	e_indexRider(PC_HasIndexIterWrite& iterable)
{
	auto	c = e_createIndexRider(iterable);
	c.toFirst();
	return c;
}

//! Create a "backward index rider"
template<class PC_HasIndexIterWrite>
auto	e_backIndexRider(PC_HasIndexIterWrite& iterable)
{
	auto	c = e_createIndexRider(iterable);
	c.toLast();
	return c;
}


//! Create an uninitialized rider
template<class PC_HasIndexIterWrite, class PSize>
auto	e_createIndexRider(PC_HasIndexIterWrite& iterable, const PSize first, const PSize last)
{
	auto	f = iterable.iter_r(first);
	auto	l = iterable.iter_r(last);

	return IndexRiderIter<decltype(f)>(f, l);
}

//! Create a "forward index rider"
template<class PC_HasIndexIterWrite, class PSize>
auto	e_indexRider(PC_HasIndexIterWrite& iterable, const PSize first, const PSize last)
{
	auto	c = e_createIndexRider(iterable, first, last);
	c.toFirst();
	return c;
}

//! Create a "backward rider"
template<class PC_HasIndexIterWrite, class PSize>
auto	e_backIndexRider(PC_HasIndexIterWrite& iterable, const PSize first, const PSize last)
{
	auto	c = e_createIndexRider(iterable, first, last);
	c.toLast();
	return c;
}

} // N..

#endif
