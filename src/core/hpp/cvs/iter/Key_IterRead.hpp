/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_KEY_ITER_READ_HPP_INCLUDED
#define	LIB_PCO_KEY_ITER_READ_HPP_INCLUDED

#include "Key_Iter.hpp"
#include "pattern/iter/C_IndexIterRead.hpp"
#include "pattern/iter/C_IterRead.hpp"

namespace NPCO {

/**
	Key_IterRead: takes IndexIter.key() as value
	@see C_IterRead
	@param PIter C_IndexIterRead
*/
template<class PKey, class PValue, class PIter> class Key_IterRead
	: public C_IterRead<Key_IterRead<PKey, PValue, PIter>, const PKey>
	, public Key_Iter<const PKey, PValue, PIter> {

public:
	using TCell			= typename std::remove_const<PKey>::type;

protected:

	PCO_INLINE
	bool	isSame(const Key_IterRead<PKey, PValue, PIter>& toCompare) const
	{
		if(this->res == nullptr)
		{
			return (toCompare.res == nullptr);
		}

		if(toCompare.res == nullptr)
		{
			return false;
		}

		return this->res->operator==(*toCompare.res);
	}

public:
	Key_IterRead(void) = default;
	Key_IterRead(const PIter& iter) : Key_Iter<const PKey, PValue, PIter>(iter)
	{ }

	Key_IterRead(PIter* iter) : Key_Iter<const PKey, PValue, PIter>(iter)
	{ }

	// Copy
	Key_IterRead(const Key_IterRead<PKey, PValue, PIter>& toCopy) = default;
	Key_IterRead& operator=(const Key_IterRead<PKey, PValue, PIter>& toCopy) = default;

	// Move
	Key_IterRead(Key_IterRead<PKey, PValue, PIter>&& toMove) = default;
	Key_IterRead& operator=(Key_IterRead<PKey, PValue, PIter>&& toMove) = default;

	//! @see C_IterRead
	PCO_INLINE
	Key_IterRead&	operator++(void)
	{
		this->forward();	return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	Key_IterRead&	operator--(void)
	{
		this->back();	return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator==(const Key_IterRead& toCompare) const
	{
		return this->isSame(toCompare);
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator!=(const Key_IterRead& toCompare) const
	{
		return !this->isSame(toCompare);
	}

};

} // N..

#endif
