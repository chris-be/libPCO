/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_INDEX_ITER_TO_ITER_HPP_INCLUDED
#define	LIB_PCO_INDEX_ITER_TO_ITER_HPP_INCLUDED

//#include "pattern/iter/C_IndexIter.hpp"
#include "pattern/iter/C_Iter.hpp"
#include "cvs/res/AutoDelete.hpp"

namespace NPCO {

/**
	Key_Iter: takes IndexIter.key() as value
	@see C_Iter
	@param PIter C_Iter
*/
template<class PKey, class PValue, class PIter>
class Key_Iter
	: protected AutoDelete<PIter>
	, public C_Iter<Key_Iter<PKey, PValue, PIter>, const PKey> {

public:
	using TCell			= typename std::remove_const<PKey>::type;

	// C_IndexIter<PIter, PKey, PValue>::ensure();

protected:
	using TBase 		= AutoDelete<PIter>;

public:
	Key_Iter(void);
	Key_Iter(const PIter& iter);
	//! @param iter Will be deleted
	Key_Iter(PIter* iter);

	// Copy
	Key_Iter(const Key_Iter<PKey, PValue, PIter>& toCopy);
	Key_Iter& operator=(const Key_Iter<PKey, PValue, PIter>& toCopy);

	// Move
	Key_Iter(Key_Iter<PKey, PValue, PIter>&& toMove) = default;
	Key_Iter& operator=(Key_Iter<PKey, PValue, PIter>&& toMove) = default;

	//! @see C_Iter
	void	unset(void);

	//! @see C_Iter
	PCO_INLINE
	bool	isValid(void) const;

	//! @see C_Iter
	PCO_INLINE
	void	forward(void);

	//! @see C_Iter
	PCO_INLINE
	void	back(void);

	//! @see C_Iter
	PCO_INLINE
	PKey&	operator*(void) const;

	//! @see C_Iter
	PCO_INLINE
	PKey*	operator->(void) const;

	//! @see C_Iter
	PCO_INLINE
	PKey&	cell(void) const
	{
		return this->operator*();
	}

};

} // N..

#include "imp/Key_Iter.cppi"

#endif
