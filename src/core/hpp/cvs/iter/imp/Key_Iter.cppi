/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

// #include	"cvs/iter/Key_Iter.hpp"

namespace NPCO {

template<class PKey, class PValue, class PIter>
Key_Iter<PKey, PValue, PIter>::Key_Iter(void)
 : TBase(nullptr)
{	}

template<class PKey, class PValue, class PIter>
Key_Iter<PKey, PValue, PIter>::Key_Iter(const PIter& iter)
{
	this->res = new PIter();
	*this->res = iter;
}

template<class PKey, class PValue, class PIter>
Key_Iter<PKey, PValue, PIter>::Key_Iter(PIter* iter)
 : TBase(iter)
{	}

template<class PKey, class PValue, class PIter>
Key_Iter<PKey, PValue, PIter>::Key_Iter(const Key_Iter<PKey, PValue, PIter>& toCopy)
 : Key_Iter()
{
	this->operator=(toCopy);
}

template<class PKey, class PValue, class PIter>
Key_Iter<PKey, PValue, PIter>& Key_Iter<PKey, PValue, PIter>
	::operator=(const Key_Iter<PKey, PValue, PIter>& toCopy)
{
	PIter*	cpy = nullptr;
	if(toCopy.res != nullptr)
	{
		cpy = new PIter();
		cpy->operator=(*toCopy.res);
	}
	this->_setPtr(cpy);

	return *this;
}

template<class PKey, class PValue, class PIter>
void	Key_Iter<PKey, PValue, PIter>::unset(void)
{
	this->_popOut();
}

template<class PKey, class PValue, class PIter>
bool	Key_Iter<PKey, PValue, PIter>::isValid(void) const
{
	return (TBase::isValid() && this->res->isValid());
}

template<class PKey, class PValue, class PIter>
void	Key_Iter<PKey, PValue, PIter>::forward(void)
{
	PCO_LOGIC_ERR(this->isValid(), "bad use");
	this->res->forward();
}

template<class PKey, class PValue, class PIter>
void	Key_Iter<PKey, PValue, PIter>::back(void)
{
	PCO_LOGIC_ERR(this->isValid(), "bad use");
	this->res->back();
}

template<class PKey, class PValue, class PIter>
PKey&	Key_Iter<PKey, PValue, PIter>::operator*(void) const
{
	PCO_LOGIC_ERR(this->isValid(), "bad use");
	return this->res->key();
}

template<class PKey, class PValue, class PIter>
PKey*	Key_Iter<PKey, PValue, PIter>::operator->(void) const
{
	PCO_LOGIC_ERR(this->isValid(), "bad use");
	return &this->res->key();
}

} // N..
