/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_T_ITER_HPP_INCLUDED
#define	LIB_PCO_T_ITER_HPP_INCLUDED

#include <type_traits>

namespace NPCO {

// Test if PClass has "::const_iterator"
template<typename PClass, typename = void>
struct has_STL_const_iterator : public std::false_type
{	};

template<typename PClass>
struct has_STL_const_iterator<PClass, std::void_t<typename PClass::const_iterator>>
 : public std::true_type
{	};


// Test if PClass has "::iterator"
template<typename PClass, typename = void>
struct has_STL_iterator : public std::false_type
{	};

template<typename PClass>
struct has_STL_iterator<PClass, std::void_t<typename PClass::iterator>>
 : public std::true_type
{	};

// Test if PClass is C_ITer
template<typename PClass, typename = void>
struct is_iter : public std::false_type
{	};

template<typename PClass>
struct is_iter<PClass, std::void_t<typename PClass::TCell>>
 : public std::true_type
{	};


} // N..

#endif
