/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_HAS_ITER_HPP_INCLUDED
#define	LIB_PCO_E_HAS_ITER_HPP_INCLUDED

//#include "pattern/iter/C_HasIterRead.hpp"
//#include "pattern/iter/C_HasIterWrite.hpp"
#include "T_Iter.hpp"

#include "cvs/walk/CursorIter.hpp"
#include "cvs/walk/RiderIter.hpp"

#include "cvs/stl/walk/STLCursor.hpp"
#include "cvs/stl/walk/STLRider.hpp"

namespace NPCO {

/** Cursors - @see C_HasIterRead
*/

//! Create an uninitialized cursor
template<class PC_Iterable, class PIter = typename PC_Iterable::TIterRead
		, std::enable_if_t<!has_STL_const_iterator<PC_Iterable>::value, bool> = true>
auto	e_createCursor(const PC_Iterable& iterable)
{
	PIter	f = iterable.first();
	PIter	l = iterable.last();

	return CursorIter<decltype(f)>(f, l);
}

//! Create an uninitialized cursor
template<class PC_Iterable
		, std::enable_if_t<has_STL_const_iterator<PC_Iterable>::value, bool> = true>
auto	e_createCursor(const PC_Iterable& iterable)
{
	auto	f = iterable.begin();
	auto	l = iterable.end();

	return STLCursor<decltype(f)>(f, l);
}

//! Create a "forward cursor"
template<class PC_Iterable>
auto	e_cursor(const PC_Iterable& iterable)
{
	auto	c = e_createCursor(iterable);
	c.toFirst();
	return c;
}

//! Create a "backward cursor"
template<class PC_Iterable>
auto	e_backCursor(const PC_Iterable& iterable)
{
	auto	c = e_createCursor(iterable);
	c.toLast();
	return c;
}

/** Riders @see C_HasIterWrite
*/

//! Create an uninitialized rider
template<class PC_Iterable	, class PIter = typename PC_Iterable::TIterWrite
		, std::enable_if_t<!has_STL_iterator<PC_Iterable>::value, bool> = true>
auto	e_createRider(PC_Iterable& iterable)
{
	PIter	f = iterable.first();
	PIter	l = iterable.last();

	return RiderIter<decltype(f)>(f, l);
}

template<class PC_Iterable
		, std::enable_if_t<has_STL_iterator<PC_Iterable>::value, bool> = true>
auto	e_createRider(PC_Iterable& iterable)
{
	auto	f = iterable.begin();
	auto	l = iterable.end();

	return STLRider<decltype(f)>(f, l);
}

//! Create a "forward rider"
template<class PC_Iterable>
auto	e_rider(PC_Iterable& iterable)
{
	auto	r = e_createRider(iterable);
	r.toFirst();
	return r;
}

//! Create a "backward rider"
template<class PC_Iterable>
auto	e_backRider(PC_Iterable& iterable)
{
	auto	r = e_createRider(iterable);
	r.toLast();
	return r;
}

} // N..

#endif
