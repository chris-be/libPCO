/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_HAS_SIZE_HPP_INCLUDED
#define	LIB_PCO_E_HAS_SIZE_HPP_INCLUDED

#include "pattern/C_HasSize.hpp"

namespace NPCO {

/**
	Extension for objects constrained by C_HasSize
*/

//! Returns if index is valid
template<class PC_HasSize, class PSize = typename PC_HasSize::TSize>
PCO_INLINE
static bool	e_isInBounds(const PC_HasSize& sized, const PSize i) noexcept
{
	return e_isInBounds(i, sized.size());
}

} // N..

#endif
