/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_V_INDEX_OF_HPP_INCLUDED
#define	LIB_PCO_V_INDEX_OF_HPP_INCLUDED

namespace NPCO {

/**
	Find out index of type in a variadic template.
*/
template<class...> struct v_indexOf;

template <class PType, class... PNext>
struct v_indexOf<PType, PType, PNext...> : std::integral_constant<std::size_t, 0>
{
};

template <class PTest, class PType, class... PNext>
struct v_indexOf<PTest, PType, PNext...>
	: std::integral_constant<std::size_t, 1 + v_indexOf<PTest, PNext...>::value>
{
};

} // N..

#endif
