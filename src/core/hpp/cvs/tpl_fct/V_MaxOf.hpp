/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_V_MAX_OF_HPP_INCLUDED
#define	LIB_PCO_V_MAX_OF_HPP_INCLUDED

#include <algorithm>	// max

namespace NPCO {

/**
	Find out max in a variadic template of size_t.
*/
template<class... Ps>
constexpr size_t v_maxOf(Ps... sizes)
{
	size_t max = 0;
	for(size_t t : {sizes...})
		max = std::max(max, t);

	return max;
};

} // N..

#endif
