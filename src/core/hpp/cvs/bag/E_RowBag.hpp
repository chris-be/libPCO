/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_ROW_BAG_HPP_INCLUDED
#define	LIB_PCO_E_ROW_BAG_HPP_INCLUDED

#include "E_Bag.hpp"

namespace NPCO {

/** Extension for objects constrained by C_RowBag
	@see C_RowBag
*/

// Cursors

//! Create an uninitialized cursor
template<class PC_DynBag, class PSize = typename PC_DynBag::TSize
		, class PIter = typename PC_DynBag::TIterRead
		, std::enable_if_t<!has_STL_const_iterator<PC_DynBag>::value, bool> = true>
auto	e_createCursor(const PC_DynBag& iterable, const PSize first, const PSize last)
{
	PCO_INV_ARG(first <= last, "");

	auto	f = iterable.iter_r(first);
	auto	l = iterable.iter_r(last);

	return CursorIter<decltype(f)>(f, l);
}

//! Create an uninitialized cursor
template<class PC_DynBag, class PSize = typename PC_DynBag::size_type
		, std::enable_if_t<has_STL_const_iterator<PC_DynBag>::value, bool> = true>
auto	e_createCursor(const PC_DynBag& iterable, const PSize first, const PSize last)
{
	PCO_INV_ARG(first <= last, "");

	PSize	s = iterable.size();
	auto	f = iterable.begin();
	f = (first < s) ? f + first : iterable.end();
	auto	l = iterable.begin();
	l = (last < s) ? l + last : iterable.end();

	return STLCursor<decltype(f)>(f, l);
}

//! Create a "forward cursor"
template<class PC_DynBag, class PSize = typename PC_DynBag::TSize>
auto	e_cursor(const PC_DynBag& iterable, const PSize first, const PSize last)
{
	auto	c = e_createCursor(iterable, first, last);
	c.toFirst();
	return c;
}

//! Create a "backward cursor"
template<class PC_DynBag, class PSize = typename PC_DynBag::TSize>
auto	e_backCursor(const PC_DynBag& iterable, const PSize first, const PSize last)
{
	auto	c = e_createCursor(iterable, first, last);
	c.toLast();
	return c;
}

// Riders

//! Create an uninitialized rider
template<class PC_DynBag, class PSize = typename PC_DynBag::TSize
		, class PIter = typename PC_DynBag::TIterWrite
		, std::enable_if_t<!has_STL_const_iterator<PC_DynBag>::value, bool> = true>
auto	e_createRider(PC_DynBag& iterable, const PSize first, const PSize last)
{
	PCO_INV_ARG(first <= last, "");

	auto	f = iterable.iter_w(first);
	auto	l = iterable.iter_w(last);

	return RiderIter<decltype(f)>(f, l);
}

//! Create an uninitialized rider
template<class PBag, class PSize = typename PBag::size_type
		, std::enable_if_t<has_STL_iterator<PBag>::value, bool> = true>
auto	e_createRider(PBag& iterable, const PSize first, const PSize last)
{
	PCO_INV_ARG(first <= last, "");

	PSize	s = iterable.size();
	auto	f = iterable.begin();
	f = (first < s) ? f + first : iterable.end();
	auto	l = iterable.begin();
	l = (last < s) ? l + last : iterable.end();

	return STLRider<decltype(f)>(f, l);
}

//! Create a "forward cursor"
template<class PC_DynBag, class PSize = typename PC_DynBag::TSize>
auto	e_rider(PC_DynBag& iterable, const PSize first, const PSize last)
{
	auto	r = e_createRider(iterable, first, last);
	r.toFirst();
	return r;
}

//! Create a "backward cursor"
template<class PC_DynBag, class PSize = typename PC_DynBag::TSize>
auto	e_backRider(PC_DynBag& iterable, const PSize first, const PSize last)
{
	auto	r = r_createRider(iterable, first, last);
	r.toLast();
	return r;
}

} // N..

#endif
