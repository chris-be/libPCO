/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_DYN_BAG_HPP_INCLUDED
#define	LIB_PCO_E_DYN_BAG_HPP_INCLUDED

#include "pattern/bag/C_DynBag.hpp"
#include "cvs/iter/E_HasIter.hpp"
#include "pattern/flow/I_Inflow.hpp"

#include "cvs/walk/E_Walker.hpp"		// e_scan
// #include "cvs/walk/CursorIter.hpp"	// scan
// #include "cvs/cur/RiderIter.hpp"

namespace NPCO {

/** Extension for objects constrained by C_DynBag
	@see C_DynBag
*/

// dynbag <- iter
template<class PC_DynBag, class PC_Iter
			, std::enable_if_t<NPCO::is_iter<PC_Iter>::value, bool> = true>
void	e_addAll(PC_DynBag& bag, const PC_Iter& first, const PC_Iter& last)
{
	PC_Iter it = first;
	for( ; it != last ; ++it)
	{
		bag.add(*it);
	}

	bag.add(*last);
}

template<class PC_DynBag, class PC_Iter
			, std::enable_if_t<!NPCO::is_iter<PC_Iter>::value, bool> = true>
void	e_addAll(PC_DynBag& bag, const PC_Iter& begin, const PC_Iter& end)
{
	PC_Iter it = begin;
	for( ; it != end ; ++it)
	{
		bag.add(*it);
	}
}

// dynBag <- cursor

//! Add all elements
template<class PC_DynBag, class PC_Walker
			, std::enable_if_t<NPCO::is_walker<PC_Walker>::value, bool> = true>
void	e_addAll(PC_DynBag& bag, PC_Walker& cur)
{
	for( ; cur.ok() ; ++cur)
	{
		bag.add(*cur);
	}
}

//! Replace all elements
template<class PC_DynBag, class PC_Walker
			, std::enable_if_t<NPCO::is_walker<PC_Walker>::value, bool> = true>
void	e_replaceAll(PC_DynBag& bag, PC_Walker& cur)
{
	bag.clear();
	e_addAll(bag, cur);
}

// dynBag <- bag

//! Add all elements
template<class PC_DynBag, class PC_Bag>
void	e_addAll(PC_DynBag& bag, const PC_Bag& iterable)
{
	auto	cur = e_cursor(iterable);
	e_addAll(bag, cur);
}

//! Replace all elements
template<class PC_DynBag, class PC_Bag>
void	e_replaceAll(PC_DynBag& bag, const PC_Bag& iterable)
{
	bag.clear();
	e_addAll(bag, iterable);
}

// dynBag <- inflow

//! Add all elements
template<class PC_DynBag>
void	e_addFlow(PC_DynBag& bag, NPCO::IInflow<typename PC_DynBag::TType>& inflow)
{
	typename PC_DynBag::TType el;
	while(inflow.read(el))
	{
		bag.add(el);
	}
}

//! Replace all elements
template<class PC_DynBag>
void	e_replaceWithFlow(PC_DynBag& bag, NPCO::IInflow<typename PC_DynBag::TType>& inflow)
{
	bag.clear();
	e_addFlow(bag, inflow);
}


//! Trim start elements
template<class PC_RowBag, class PC_KeyBag>
void	e_trimStart(PC_RowBag& iterable, const PC_KeyBag& patterns)
{
	auto	first = iterable.first();
	if(!first.isValid())
	{	// Empty
		return;
	}

	// Scan
	auto	last = iterable.last();
	auto c = CursorIter<decltype(first)>(first, last);
	if(!e_scanFromStart(c, patterns))
	{
		return;
	}

	if(c.ok())
	{	// Range
		iterable.remove(first, c.getCurrent());
	}
	else
	{	// All elements to remove
		iterable.clear();
		return;
	}
}

//! Trim end elements
template<class PC_RowBag, class PC_KeyBag>
void	e_trimEnd(PC_RowBag& iterable, const PC_KeyBag& patterns)
{
	auto	last = iterable.last();
	if(!last.isValid())
	{	// Empty
		return;
	}

	// Scan
	auto	first = iterable.first();
	auto c = RiderIter<decltype(first)>(first, last);
	if(!e_scanFromEnd(c, patterns))
	{
		return;
	}

	if(c.ok())
	{	// Range
		iterable.remove(c.getCurrent(), last);
	}
	else
	{	// All elements to remove
		iterable.clear();
		return;
	}
}

//! Trim start and end elements
template<class PC_RowBag, class PC_KeyBag>
void	e_trim(PC_RowBag& iterable, const PC_KeyBag& patterns)
{
	// "end" before (should be more efficient)
	e_trimEnd(iterable, patterns);
	e_trimStart(iterable, patterns);
}



/** Trim start elements in given range
	@param low range (included)
	@param high range (included)
*/
template<class PC_RowBag, class PC_KeyBag>
void	e_trimStart(PC_RowBag& iterable, const PC_KeyBag& patterns, const typename PC_RowBag::TIterWrite& low, const typename PC_RowBag::TIterWrite& high)
{
	PCO_INV_ARG(low.isValid(), "invalid low bound");
	PCO_INV_ARG(high.isValid(), "invalid high bound");

	using PIter = typename PC_RowBag::TIterWrite;
	// Scan
	auto c = CursorIter<PIter>(low, high);
	if(!e_scanFromStart(c, patterns))
	{
		return;
	}

	if(c.ok())
	{	// Range
		iterable.remove(low, c.getCurrent());
	}
	else
	{	// All
		iterable.remove(low, high);
	}
}

/** Trim end elements in given range
	@param low range (included)
	@param high range (included)
*/
template<class PC_RowBag, class PC_KeyBag>
void	e_trimEnd(PC_RowBag& iterable, const PC_KeyBag& patterns, const typename PC_RowBag::TIterWrite& low, const typename PC_RowBag::TIterWrite& high)
{
	PCO_INV_ARG(low.isValid(), "invalid low bound");
	PCO_INV_ARG(high.isValid(), "invalid high bound");

	using PIter = typename PC_RowBag::TIterWrite;
	// Scan
	auto c = CursorIter<PIter>(low, high);
	if(!e_scanFromEnd(c, patterns))
	{
		return;
	}

	if(c.ok())
	{	// Range
		iterable.remove(c.getCurrent(), high);
	}
	else
	{	// All
		iterable.remove(low, high);
	}
}


} // N..

#endif
