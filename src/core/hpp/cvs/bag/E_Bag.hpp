/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_BAG_HPP_INCLUDED
#define	LIB_PCO_E_BAG_HPP_INCLUDED

#include "cvs/iter/E_HasIter.hpp"
#include "pattern/cfr/I_Classifier.hpp"

namespace NPCO {

/** Extension for objects constrained by C_Bag
	@see C_Bag
*/

/*! Check if two "bags" have same content - contains the same elements
	- use of operator==
*/
template<class PC_Bag1, class PC_Bag2>
bool	e_equalContent(const PC_Bag1& bag1, const PC_Bag2& bag2)
{
	if(bag1.size() != bag2.size())
	{
		return false;
	}

	auto c1 = e_cursor(bag1);
	auto c2 = e_cursor(bag2);

	for( ; c1.ok() ; ++c1, ++c2)
	{
		if(! (*c1 == *c2))
		{
			return false;
		}
	}

	return true;
}

/*! Compare two "bags" lexicographically
	- use of operator== and operator<
*/
template<class PC_Bag1, class PC_Bag2>
int	e_lexiMathCompare(const PC_Bag1& bag1, const PC_Bag2& bag2)
{
	auto c1 = e_cursor(bag1);
	auto c2 = e_cursor(bag2);

	for( ; c1.ok() && c2.ok() ; ++c1, ++c2)
	{
		if(! (*c1 == *c2))
		{
			return (*c1 < *c2) ? -1 : +1;
		}
	}

	if(c1.ok())
	{	// !c2.ok()
		return +1;
	}

	return c2.ok() ? -1 : 0;
}



/*! Check if two "bags" are equals - contains the same elements
	- use of classify
*/
template<class PC_Bag1, class PC_Bag2>
bool	e_sameContent(const PC_Bag1& bag1, const PC_Bag2& bag2)
{
	if(bag1.size() != bag2.size())
	{
		return false;
	}

	auto c1 = e_cursor(bag1);
	auto c2 = e_cursor(bag2);

	for( ; c1.ok() ; ++c1, ++c2)
	{
		ClassifyValue cv = c1->classify(*c2);
		if(cv != 0)
			return false;
	}

	return true;
}

/*! Compare two "bags" lexicographically
	- use of classify
*/
template<class PC_Bag1, class PC_Bag2>
int	e_lexiSameCompare(const PC_Bag1& bag1, const PC_Bag2& bag2)
{
	auto c1 = e_cursor(bag1);
	auto c2 = e_cursor(bag2);

	for( ; c1.ok() && c2.ok() ; ++c1, ++c2)
	{
		ClassifyValue cv = c1->classify(*c2);
		if(cv != 0)
			return cv;
	}

	if(c1.ok())
	{	// !c2.ok()
		return +1;
	}

	return c2.ok() ? -1 : 0;
}


/*! Check if two "bags" are equals - contains the same elements
	- use of classify
*/
template<class PC_Bag1, class PC_Bag2, class PClassifier>
bool	e_sameContent(const PC_Bag1& bag1, const PC_Bag2& bag2, const PClassifier& cfr)
{
	if(bag1.size() != bag2.size())
	{
		return false;
	}

	auto c1 = e_cursor(bag1);
	auto c2 = e_cursor(bag2);

	for( ; c1.ok() ; ++c1, ++c2)
	{
		ClassifyValue cv = cfr.classify(*c1, *c2);
		if(!e_isSame(cv))
			return false;
	}

	return true;
}

/*! Compare two "bags" lexicographically
	- use of classify
*/
template<class PC_Bag1, class PC_Bag2, class PClassifier>
int		e_lexiSameCompare(const PC_Bag1& bag1, const PC_Bag2& bag2, const PClassifier& cfr)
{
	auto c1 = e_cursor(bag1);
	auto c2 = e_cursor(bag2);

	for( ; c1.ok() && c2.ok() ; ++c1, ++c2)
	{
		ClassifyValue cv = cfr.classify(*c1, *c2);
		if(cv != 0)
			return cv;
	}

	if(c1.ok())
	{	// !c2.ok()
		return +1;
	}

	return c2.ok() ? -1 : 0;
}

} // N..

#endif
