/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_RIDER_HPP_INCLUDED
#define	LIB_PCO_STL_RIDER_HPP_INCLUDED

#include "STLWalker.hpp"
#include "pattern/walk/C_Rider.hpp"

namespace NPCO {

/*!
	Rider using STL iterator - @see C_Rider

	@see STLRider
	@param PIter iterator
	@param PCell
*/
template<class PIter, class PCell = typename PIter::value_type>
class STLRider	: private C_Rider<STLRider<PIter, PCell>, PCell>
				, public STLWalker<PIter, PCell> {

protected:
	using TBase		= STLWalker<PIter, PCell>;

public:
	STLRider(void) = default;
	STLRider(const PIter& begin, const PIter& end) : TBase(begin, end)
	{ }

	// Copy
	STLRider(const STLRider<PIter, PCell>& ) = default;
	STLRider& operator=(const STLRider<PIter, PCell>& ) = default;

	// Move
	STLRider(STLRider<PIter, PCell>&& ) = default;
	STLRider& operator=(STLRider<PIter, PCell>&& ) = default;

	PCO_INLINE
	const STLRider&		operator++(void)
	{
		this->forward();	return *this;
	}

	PCO_INLINE
	const STLRider&		operator--(void)
	{
		this->back();		return *this;
	}

};

} // N..

#endif
