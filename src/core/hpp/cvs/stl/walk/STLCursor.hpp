/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_CURSOR_HPP_INCLUDED
#define	LIB_PCO_STL_CURSOR_HPP_INCLUDED

#include "STLWalker.hpp"
#include "pattern/walk/C_Cursor.hpp"
#include "pattern/flow/I_Inflow.hpp"

namespace NPCO {

/*!
	Cursor using STL iterator - @see C_Cursor
	@convenience can be used as inflow - @see IInflow

	@see STLWalker
	@param PIter const_iterator
	@param PCell
*/
template<class PIter, class PCell = typename PIter::value_type>
class STLCursor : private C_Cursor<STLCursor<PIter, PCell>, PCell>
				, public STLWalker<PIter, const PCell>
				, public IInflow<PCell> {

protected:
	using TBase		= STLWalker<PIter, const PCell>;

public:
	using TCell		= typename TBase::TCell;

public:
	STLCursor(void) = default;
	STLCursor(const PIter& begin, const PIter& end) : TBase(begin, end)
	{ }

	~STLCursor(void) = default;

	// Copy
	STLCursor(const STLCursor<PIter, PCell>& ) = default;
	STLCursor& operator=(const STLCursor<PIter, PCell>& ) = default;

	// Move
	STLCursor(STLCursor<PIter, PCell>&& ) = default;
	STLCursor& operator=(STLCursor<PIter, PCell>&& ) = default;

	PCO_INLINE
	const STLCursor&	operator++(void)
	{
		this->forward();	return *this;
	}

	PCO_INLINE
	const STLCursor&	operator--(void)
	{
		this->back();		return *this;
	}

	//! @see IInflow
	virtual bool	read(PCell& element) override
	{
		bool ret = this->ok();
		if(ret)
		{
			element = this->cell();
			this->forward();
		}

		return ret;
	}

};

} // N..

#endif
