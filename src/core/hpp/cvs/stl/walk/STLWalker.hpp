/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_WALKER_HPP_INCLUDED
#define	LIB_PCO_STL_WALKER_HPP_INCLUDED

#include "pattern/walk/C_Walker.hpp"

namespace NPCO {

/*!
	Walker using STL iterator - @see C_Walker

	@param PIter For inheritance
	@param PCell Cell type
*/
template<class PIter, class PCell>
class STLWalker	: private C_Walker<STLWalker<PIter, PCell>, PCell> {

public:
	using TCell		= typename std::remove_const<PCell>::type;

protected:
	PIter		begin;
	PIter		end;
	PIter		current;

public:
	STLWalker(void);
	STLWalker(const PIter& begin, const PIter& end);
	~STLWalker(void) = default;

	// Copy
	STLWalker(const STLWalker<PIter, PCell>& ) = default;
	STLWalker& operator=(const STLWalker<PIter, PCell>& ) = default;

	// Move
	STLWalker(STLWalker<PIter, PCell>&& ) = default;
	STLWalker& operator=(STLWalker<PIter, PCell>&& ) = default;

	void	set(const PIter& begin, const PIter& end);

	const PIter&	getBegin(void) const;
	const PIter&	getEnd(void) const;
	const PIter&	getCurrent(void) const;

	//! @see C_Walker
	bool	checkRange(void) const;

	//! @see C_Walker
	void	toFirst(void);

	//! @see C_Walker
	void	toLast(void);

	//! @see C_Walker
	PCO_INLINE
	void	forward(void);

	//! @see C_Walker
	PCO_INLINE
	void	back(void);

	//! @see C_Walker
	void	operator+=(size_t step);

	//! @see C_Walker
	void	operator-=(size_t step);

	//! @see C_Walker
	PCO_INLINE
	bool	ok(void) const;

	//! @see C_Walker
	PCO_INLINE
	PCell&	operator*(void) const;

	//! @see C_Walker
	PCO_INLINE
	PCell*	operator->(void) const;

	//! @see C_Walker
	void	limitRangeFirst(void);
	//! @see C_Walker
	void	limitRangeLast(void);

	//! @see C_Walker
	PCO_INLINE
	PCell&	cell(void) const
	{
		return this->operator*();
	}

};

} // N..

#include "imp/STLWalker.cppi"

#endif
