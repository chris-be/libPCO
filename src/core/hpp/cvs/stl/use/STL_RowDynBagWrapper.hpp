/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_ROW_DYN_BAG_WRAPPER_HPP_INCLUDED
#define	LIB_PCO_STL_ROW_DYN_BAG_WRAPPER_HPP_INCLUDED

#include "pattern/bag/C_RowDynBag.hpp"
#include "STL_DynBagWrapper.hpp"

namespace NPCO {

/*!
	Wrap STL container as C_RowDynBag
*/
template<class PContainer>
class STL_RowDynBagWrapper : public STL_DynBagWrapper<PContainer>
	, private C_RowDynBag<STL_RowDynBagWrapper<PContainer>, typename PContainer::value_type, typename PContainer::size_type> {

protected:
	using TBase			= STL_DynBagWrapper<PContainer>;
	using TConstIter	= typename TBase::TConstIter;
	using TIter			= typename TBase::TIter;

public:
	using TBag			= TBase;	// For child class
	using TSize			= typename TBase::TSize;
	using TType			= typename TBase::TType;
	using TIterRead		= typename TBase::TIterRead;
	using TIterWrite	= typename TBase::TIterWrite;

protected:
	//! Returns if index is valid
	PCO_INLINE
	bool	isInBounds(const TSize i) const
	{
		PCO_INV_SIZE(e_isSizeValid(i));
		return e_isInBounds(i, this->size());
	}

public:
	STL_RowDynBagWrapper(void) = default;

	STL_RowDynBagWrapper(const STL_RowDynBagWrapper& ) = default;
	STL_RowDynBagWrapper&	operator=(const STL_RowDynBagWrapper& ) = default;

	STL_RowDynBagWrapper(STL_RowDynBagWrapper&& ) = default;
	STL_RowDynBagWrapper&	operator=(STL_RowDynBagWrapper&& ) = default;

	//! @see C_RowBag
	const TType&	operator[](TSize i) const
	{
		PCO_INV_ARG(this->isInBounds(i), "Out of bounds");
		return this->cont.operator[](i);
	}

	//! @see C_RowBag
	TType&		operator[](TSize i)
	{
		PCO_INV_ARG(this->isInBounds(i), "Out of bounds");
		return this->cont.operator[](i);
	}

	//! @see C_RowBag
	TIterRead	iter_r(TSize i) const
	{
		PCO_INV_ARG(i >= 0, "Negative index ?");
		TConstIter	ith = this->cont.begin();
		bool valid = (i < this->cont.size());
		if(valid)
		{
			ith = ith + i;
		}

		return TIterRead(ith, valid);
	}

	//! @see C_RowBag
	TIterWrite	iter_w(TSize i)
	{
		PCO_INV_ARG(i >= 0, "Negative index ?");
		TIter		ith = this->cont.begin();
		bool valid = (i < this->cont.size());
		if(valid)
		{
			ith = ith + i;
		}

		return TIterWrite(ith, valid);
	}

};

} // N..

#endif
