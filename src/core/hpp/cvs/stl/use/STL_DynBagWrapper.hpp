/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_DYN_BAG_WRAPPER_HPP_INCLUDED
#define	LIB_PCO_STL_DYN_BAG_WRAPPER_HPP_INCLUDED

#include "pattern/bag/C_DynBag.hpp"
#include "STL_BagWrapper.hpp"

namespace NPCO {

/*!
	Wrap STL container as C_DynBag
*/
template<class PContainer>
class STL_DynBagWrapper : public STL_BagWrapper<PContainer>
	, private C_DynBag<STL_DynBagWrapper<PContainer>, typename PContainer::value_type, typename PContainer::size_type> {

private:
	using TBase			= STL_BagWrapper<PContainer>;

public:
	using TBag			= TBase;	// For child class
	using TSize			= typename TBase::TSize;
	using TType			= typename TBase::TType;
	using TIterRead		= typename TBase::TIterRead;
	using TIterWrite	= typename TBase::TIterWrite;

public:
	STL_DynBagWrapper(void) = default;

	STL_DynBagWrapper(const STL_DynBagWrapper&) = default;
	STL_DynBagWrapper&	operator=(const STL_DynBagWrapper&) = default;

	STL_DynBagWrapper(STL_DynBagWrapper&&) = default;
	STL_DynBagWrapper&	operator=(STL_DynBagWrapper&&) = default;

	//! @see C_DynBag

	void	clear(void)
	{
		this->cont.clear();
	}

	//! @see C_DynBag
	void	add(const TType& toAdd)
	{
		this->cont.push_back(toAdd);
	}

	//! @see C_DynBag
	void	moveIn(TType&& toAdd)
	{
		this->cont.push_back(std::move(toAdd));
	}

	//! @see C_DynBag
	void	remove(const TIterWrite& iw)
	{
		if(iw.isValid())
		{
			this->cont.erase(iw.getIter());
		}
	}

};

} // N..

#endif
