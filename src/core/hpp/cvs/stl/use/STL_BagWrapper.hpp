/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_BAG_WRAPPER_HPP_INCLUDED
#define	LIB_PCO_STL_BAG_WRAPPER_HPP_INCLUDED

#include "pattern/bag/C_Bag.hpp"
#include "pattern/C_Wrapper.hpp"
#include "cvs/stl/iter/STL_IterRead.hpp"
#include "cvs/stl/iter/STL_IterWrite.hpp"

namespace NPCO {

/*!
	Wrap STL container as C_Bag
*/
template<class PContainer>
class STL_BagWrapper
	: private C_Bag<STL_BagWrapper<PContainer>, typename PContainer::value_type, typename PContainer::size_type>
	, private C_Wrapper<STL_BagWrapper<PContainer>, PContainer> {

protected:
	using TConstIter	= typename PContainer::const_iterator;
	using TIter			= typename PContainer::iterator;

public:
	using TWrapped		= PContainer;
	using TSize			= typename PContainer::size_type;
	using TType			= typename PContainer::value_type;
	using TIterRead		= STL_IterRead<TConstIter>;
	using TIterWrite	= STL_IterWrite<TIter>;

protected:
	PContainer		cont;

public:
	STL_BagWrapper(void) = default;

	STL_BagWrapper(const STL_BagWrapper&) = default;
	STL_BagWrapper&	operator=(const STL_BagWrapper&) = default;

	STL_BagWrapper(STL_BagWrapper&&) = default;
	STL_BagWrapper&	operator=(STL_BagWrapper&&) = default;

	//! @see C_Wrapper
	const PContainer&		getWrapped(void) const
	{
		return this->cont;
	}
	//! @see C_Wrapper
	PContainer&				getWrapped(void)
	{
		return this->cont;
	}

	TSize	size(void) const
	{
		return this->cont.size();
	}

	bool	isEmpty(void) const
	{
		return this->cont.empty();
	}

	TIterRead	first(void) const
	{
		TConstIter first = this->cont.begin();
		return TIterRead(first, first != this->cont.end());
	}

	TIterRead	last(void) const
	{
		TConstIter last = this->cont.end();
		bool valid = !this->cont.empty();
		if(valid)
			--last;

		return TIterRead(last, valid);
	}

	TIterWrite	first(void)
	{
		TIter first = this->cont.begin();
		return TIterWrite(first, first != this->cont.end());
	}

	TIterWrite	last(void)
	{
		TIter last = this->cont.end();
		bool valid = !this->cont.empty();
		if(valid)
			--last;

		return TIterWrite(last, valid);
	}

};

} // N..

#endif
