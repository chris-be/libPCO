/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_VECTOR_HPP_INCLUDED
#define	LIB_PCO_STL_VECTOR_HPP_INCLUDED

#include "use/STL_RowDynBagWrapper.hpp"
#include <vector>

namespace NPCO {

/*!
	Use STL Vector as C_DynBag
*/
template<class PType>
class Vector : public STL_RowDynBagWrapper<std::vector<PType>> {

private:
	using TBase			= STL_RowDynBagWrapper<std::vector<PType>>;

public:
	using TDynBag		= TBase;
	using TBag			= typename TDynBag::TBag;
	//using TIterRead	= typename TBase::TIterRead;
	//using TIterWrite	= typename TBase::TIterWrite;

public:
	Vector(void) = default;

	Vector(const Vector&) = default;
	Vector&	operator=(const Vector&) = default;

	Vector(Vector&&) = default;
	Vector&	operator=(Vector&&) = default;

};

/*
//! Create an uninitialized cursor
template<class PType>
auto	e_createCursor(const Vector<PType>& iterable)
{
	const typename Vector<PType>::TBag& bag = iterable;
	return e_createCursor(bag);
}

//! Create a "forward cursor"
template<class PType>
auto	e_cursor(const Vector<PType>& iterable)
{
	const typename Vector<PType>::TBag& bag = iterable;
	return e_cursor(bag);
}

//! Create a "backward cursor"
template<class PType>
auto	e_backCursor(const Vector<PType>& iterable)
{
	const typename Vector<PType>::TBag& bag = iterable;
	return e_backCursor(bag);
}

//! Create an uninitialized rider
template<class PType>
auto	e_createRider(Vector<PType>& iterable)
{
	typename Vector<PType>::TBag& bag = iterable;
	return e_createRider(bag);
}

//! Create a "forward rider"
template<class PType>
auto	e_rider(Vector<PType>& iterable)
{
	typename Vector<PType>::TBag& bag = iterable;
	return e_rider(bag);
}

//! Create a "backward rider"
template<class PType>
auto	e_backRider(Vector<PType>& iterable)
{
	typename Vector<PType>::TBag& bag = iterable;
	return e_backRider(bag);
}
*/

} // N..

#endif
