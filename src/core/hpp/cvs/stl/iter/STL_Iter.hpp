/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_ITER_HPP_INCLUDED
#define	LIB_PCO_STL_ITER_HPP_INCLUDED

#include "pattern/iter/C_Iter.hpp"

namespace NPCO {

/*!
	STL_Iter: C_Iter for STL iterator
*/
template<class PIterator, class PCell>
class STL_Iter : private C_Iter<STL_Iter<PIterator, PCell>, PCell> {

public:
	using TCell		= typename std::remove_const<PCell>::type;

protected:
	//! STL iterator
	PIterator	it;
	//! Valid state - only set at ctr !!
	bool		valid;

	PCO_INLINE
	bool	sameAs(const STL_Iter& toCompare) const;

public:
	STL_Iter(void);
	STL_Iter(const PIterator& it, bool valid);

	//! Get underlying iterator
	const PIterator&	getIter(void) const;

	//! Set
	void	set(const PIterator& it, bool valid);

	//! @see C_Iter
	void	unset(void);

	//! @see C_Iter
	PCO_INLINE
	bool	isValid(void) const;

	//! @see C_Iter
	PCO_INLINE
	void	forward(void);

	//! @see C_Iter
	PCO_INLINE
	void	back(void);

	//! @see C_Iter
	PCO_INLINE
	PCell&	operator*(void) const;

	//! @see C_Iter
	PCO_INLINE
	PCell*	operator->(void) const;

	//! @see C_Iter
	PCO_INLINE
	PCell&	cell(void) const
	{
		return this->operator*();
	}

};

} // N..

#include "imp/STL_Iter.cppi"

#endif
