/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_ITER_READ_HPP_INCLUDED
#define	LIB_PCO_STL_ITER_READ_HPP_INCLUDED

#include "STL_Iter.hpp"
#include "pattern/iter/C_IterRead.hpp"

namespace NPCO {

/*!
	Use STL Iterator as C_IterRead
*/
template<class PIterator, class PCell = typename PIterator::value_type>
class STL_IterRead
	: private C_IterRead<STL_IterRead<PIterator>, PCell>
	, public STL_Iter<PIterator, const PCell> {

protected:
	using TBase			= STL_Iter<PIterator, const PCell>;

public:
	STL_IterRead(void) = default;
	STL_IterRead(const PIterator& it, bool valid)
		: TBase(it, valid)
	{	}

	STL_IterRead(const STL_IterRead&) = default;
	STL_IterRead&	operator=(const STL_IterRead&) = default;

	STL_IterRead(STL_IterRead&&) = default;
	STL_IterRead&	operator=(STL_IterRead&&) = default;

	//! @see C_IterRead
	PCO_INLINE
	STL_IterRead&	operator++(void)
	{
		this->forward();	return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	STL_IterRead&	operator--(void)
	{
		this->back();	return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator==(const STL_IterRead& toCompare) const
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator!=(const STL_IterRead& toCompare) const
	{
		return !this->sameAs(toCompare);
	}

};

} // N..

#endif
