/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STL_ITER_WRITE_HPP_INCLUDED
#define	LIB_PCO_STL_ITER_WRITE_HPP_INCLUDED

#include "STL_Iter.hpp"
#include "pattern/iter/C_IterWrite.hpp"

namespace NPCO {

/*!
	Use STL Iterator as C_IterWrite
*/
template<class PIterator, class PCell = typename PIterator::value_type>
class STL_IterWrite
	: private C_IterWrite<STL_IterWrite<PIterator>, PCell>
	, public STL_Iter<PIterator, PCell> {

protected:
	using TBase			= STL_Iter<PIterator, PCell>;

public:
	STL_IterWrite(void) = default;
	STL_IterWrite(const PIterator& it, bool valid)
		: TBase(it, valid)
	{	}

	STL_IterWrite(const STL_IterWrite&) = default;
	STL_IterWrite&	operator=(const STL_IterWrite&) = default;

	STL_IterWrite(STL_IterWrite&&) = default;
	STL_IterWrite&	operator=(STL_IterWrite&&) = default;

	//! @see C_IterWrite
	PCO_INLINE
	STL_IterWrite&	operator++(void)
	{
		this->forward();	return *this;
	}

	//! @see C_IterWrite
	PCO_INLINE
	STL_IterWrite&	operator--(void)
	{
		this->back();	return *this;
	}

	//! @see C_IterWrite
	PCO_INLINE
	bool	operator==(const STL_IterWrite& toCompare) const
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IterWrite
	PCO_INLINE
	bool	operator!=(const STL_IterWrite& toCompare) const
	{
		return !this->sameAs(toCompare);
	}

};

} // N..

#endif
