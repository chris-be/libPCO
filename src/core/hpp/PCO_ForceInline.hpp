/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FORCE_INLINE_HPP_INCLUDED
#define	LIB_PCO_FORCE_INLINE_HPP_INCLUDED

/*!
	Try to "force" inline even if no optimization flag is used at compile time
*/

#ifdef _MSC_VER
	#define PCO_INLINE		__forceinline

#elif defined(__GNUC__)
	#define PCO_INLINE		__attribute__((__always_inline__))

#elif defined(__CLANG__)
	#if __has_attribute(__always_inline__)
		#define PCO_INLINE	__attribute__((__always_inline__))
	#else
		#define PCO_INLINE	inline
		#warning Falling back to "inline"
    #endif
#else
	#define PCO_INLINE			inline
	#warning Falling back to "inline"
#endif



#endif // lib
