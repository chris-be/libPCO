/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MACROS_HPP_INCLUDED
#define	LIB_PCO_MACROS_HPP_INCLUDED

// Some (usefull ?) macros

#define PCO_INNER_QUOTE(X)	#X

// Create string (const char*) from a DEFINE
#define PCO_MAKE_STRING(X)	PCO_INNER_QUOTE(X)


#endif
