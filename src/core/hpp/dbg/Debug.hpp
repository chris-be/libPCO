/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_DEBUG_HPP_INCLUDED
#define	LIB_PCO_DEBUG_HPP_INCLUDED

namespace NPCO {

class Debug {

private:
	//! Only service
	Debug(void) = delete;

public:
	/** Echo an error in cerr and abort program
		@param msg Message to print
		@param file Source file incriminated
		@param line Line in source file
	*/
	[[noreturn]] static void do_assert(const char* msg, const char* file, const int line);
	// Note: do_assert since "assert" is defined as macro
};

} // N..

#define		PCO_STRING(NS)			#NS

#ifndef NDEBUG

	#define	PCO_ASSERT_MSG(X, MSG)		if(!(X)) { NPCO::Debug::do_assert(MSG, __FILE__, __LINE__); }
	#define PCO_VERIFY_MSG(X, MSG)		PCO_ASSERT_MSG(X, MSG);

	#define	PCO_ASSERT(X)				PCO_ASSERT_MSG(X, "");
	#define PCO_VERIFY(X)				PCO_VERIFY_MSG(X, "");

	#define	PCO_ERR_MSG(MSG)			PCO_STRING({ __FILE__ }[ PCO_STRING(__LINE__) ] error: MSG)

#else

	#define PCO_ASSERT_MSG(X, MSG)		;
	#define	PCO_VERIFY_MSG(X, MSG)		X;

	#define PCO_ASSERT(X)				;
	#define	PCO_VERIFY(X)				X;

	#define	PCO_ERR_MSG(MSG)			MSG

#endif

// Error: always throw exception
#define PCO_ERROR(MSG, EXC)									\
		PCO_ASSERT_MSG(false, "PCO ERROR : '" MSG "'");		\
		throw EXC;


#endif // LIB_...
