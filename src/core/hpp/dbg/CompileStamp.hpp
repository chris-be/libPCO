/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_COMPILE_STAMP_HPP_INCLUDED
#define	LIB_PCO_COMPILE_STAMP_HPP_INCLUDED

namespace NPCO {

/**
	Compilation info.
*/
class CompileStamp {

protected:
	const char* codeName;
	const char* version;
	const char* name;

	const char* compileDate;
	const char* compileTime;

	//! Compiled in debug mode
	bool debug;

	//! Compiled with predicate mode
	bool predicate;

public:
	CompileStamp(const char* codeName, const char* version, const char* name);

	const char*	getCodeName(void) const;
	const char*	getVersion(void) const;
	const char*	getName(void) const;

	const char*	getCompilationDate(void) const;
	const char*	getCompilationTime(void) const;

	//! Compiled in debug mode
	bool	hasDebugInfo(void) const;

	//! Compiled with predicate mode
	bool	hasPredicate(void) const;

	/** Print on "cout" infos
	*/
	void	echoInfo(bool fullName = true) const;

};

} // N..

#endif
