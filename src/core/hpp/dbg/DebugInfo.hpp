/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_DEBUG_INFO_HPP_INCLUDED
#define	LIB_PCO_DEBUG_INFO_HPP_INCLUDED

namespace NPCO {

/**
	Used when debugging
*/
class DebugInfo {
protected:
	char*			info;

	char*			file;
	unsigned int	line;

	DebugInfo(void);

	void	clear(void);

public:
	DebugInfo(const char* info, const char* file, unsigned int line);
	virtual ~DebugInfo(void);

	// Copy
	DebugInfo(const DebugInfo& toCopy);
	DebugInfo& operator=(const DebugInfo& toCopy);

	// Move not allowed
	DebugInfo(DebugInfo&& ) = delete;
	DebugInfo& operator=(DebugInfo&& ) = delete;

	const char* 	getInfo(void) const noexcept;
	const char* 	getFile(void) const noexcept;
	unsigned int	getLine(void) const noexcept;

};

} // N..

// #define	PCO_DEBUG_INFO(MSG)		DebugInfo(MSG, __FILE__, __LINE__)

#endif
