/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_DEBUG_EXCEPTION_HPP_INCLUDED
#define	LIB_PCO_DEBUG_EXCEPTION_HPP_INCLUDED

#include "DebugInfo.hpp"
#include <exception>

namespace NPCO {

/**
	Used when debugging
*/
class DebugException : public DebugInfo, public std::exception {

public:
	DebugException(const char* info, const char* file, unsigned int line);
	DebugException(const DebugInfo& info);

	virtual ~DebugException(void) override;

	// Copy
	DebugException(const DebugException& ) = default;
	DebugException& operator=(const DebugException& ) = default;

	// Move does copy
	DebugException(DebugException&& );
	DebugException& operator=(DebugException&& ) = delete;

	virtual const char* what(void) const noexcept override;

};

} // N..

#define	PCO_DEBUG_EXCEPTION(MSG)	DebugException(MSG, __FILE__, __LINE__)

#endif
