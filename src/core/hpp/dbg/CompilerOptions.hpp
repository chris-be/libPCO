/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_COMPILER_OPTIONS_HPP_INCLUDED
#define	LIB_PCO_COMPILER_OPTIONS_HPP_INCLUDED

// Check some compiler MACROS
#ifndef NDEBUG
	#define LIB_PCO_DEBUG_COMPILED			true
#else
	#define LIB_PCO_DEBUG_COMPILED			false
#endif

#ifndef NPREDICATE
	#define LIB_PCO_PREDICATE_COMPILED		true
#else
	#define LIB_PCO_PREDICATE_COMPILED		false
#endif

#endif
