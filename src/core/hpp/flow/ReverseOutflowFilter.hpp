/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_REVERSE_OUTFLOW_FILTER_HPP_INCLUDED
#define	LIB_PCO_REVERSE_OUTFLOW_FILTER_HPP_INCLUDED

#include "flow/InflowFilterBase.hpp"
#include "flow/crt/I_OutflowFilter.hpp"
#include "cvs/flow/DynBagOutflow.hpp"
#include "col/ChunkList.hpp"

namespace NPCO {

/*!
	Invert "outflow filter".

	@param POutflowFilter must have a constructor taking (IOutflow<TOut>& , ...)
*/
template<class POutflowFilter>
class ReverseOutflowFilter
	: public InflowFilterBase<typename POutflowFilter::TOut, typename POutflowFilter::TIn> {

protected:
	typedef typename POutflowFilter::TOut		TOut;
	typedef typename POutflowFilter::TIn		TIn;
	using TBase		= InflowFilterBase<TOut, TIn>;
	using TFifo		= ChunkList<TOut>;

	typedef typename TBase::TSource		TSource;

	static_assert(std::is_base_of<IOutflowFilter<TOut, TIn>, POutflowFilter>::value, "Not a IOutflowFilter");

protected:
	TFifo					fifo;
	DynBagOutflow<TFifo>	fifoOutflow;

	POutflowFilter			filter;

public:

	template<typename... Args>
	ReverseOutflowFilter(TSource* src, bool clean, Args&&... args);

	template<typename... Args>
	ReverseOutflowFilter(TSource& src, Args&&... args);

	//! @see IInflow
	virtual bool	read(TOut& element) override;

};

} // N..

#include "imp/ReverseOutflowFilter.cppi"

#endif
