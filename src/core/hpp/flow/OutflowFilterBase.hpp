/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_OUTFLOW_FILTER_BASE_HPP_INCLUDED
#define	LIB_PCO_OUTFLOW_FILTER_BASE_HPP_INCLUDED

#include "flow/crt/I_OutflowFilter.hpp"
#include "mem/PointerHolder.hpp"

#include "PCO_Predicate.hpp"

namespace NPCO {

/*!
	Default implementation of IOutflowFilter.

	@param POut Type of cell written
	@param PIn Type of cell given
	@param PDstOutflow IOutflow<POut> implementation
*/
template<class POut, class PIn, class PDstOutflow = IOutflow<POut>>
class OutflowFilterBase : public IOutflowFilter<POut, PIn> {

public:
	typedef POut			TOut;
	typedef PIn				TIn;
	typedef PDstOutflow		TDestination;

protected:
	PointerHolder<TDestination>	dst;

public:
	OutflowFilterBase(TDestination* dst, bool clean) : dst(dst, clean)
	{
		PCO_INV_PTR(dst);
	}

	//! Reference -> no clean
	OutflowFilterBase(TDestination& dst) : dst(dst, false)
	{	}

	virtual ~OutflowFilterBase(void) = default;

	// Copy
	OutflowFilterBase(const OutflowFilterBase& ) = default;
	OutflowFilterBase&	operator=(const OutflowFilterBase& ) = default;

	// Move
	OutflowFilterBase(OutflowFilterBase&& ) = default;
	OutflowFilterBase&	operator=(OutflowFilterBase&& ) = default;

	//! @see IOutflow
	virtual void	write(const PIn& element) override = 0;
};

} // N..

#endif
