/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_INFLOW_FILTER_BASE_HPP_INCLUDED
#define	LIB_PCO_INFLOW_FILTER_BASE_HPP_INCLUDED

#include "pattern/flow/I_InflowFilter.hpp"
#include "mem/cvs/RefRes.hpp"

#include "PCO_Predicate.hpp"

namespace NPCO {

/*!
	Default implementation of IInflowFilter.

	@param POut Type of cell returned
	@param PIn Type of cell read
	@param PSrcInflow IInflow<PIn> implementation
*/
template<class POut, class PIn, class PSrcInflow = IInflow<PIn>>
class InflowFilterBase : public IInflowFilter<POut, PIn> {

public:
	typedef POut			TOut;
	typedef PIn				TIn;
	typedef PSrcInflow		TSource;
	typedef RefRes<PSrcInflow>	TResource;

protected:
	TResource	src;

public:
	InflowFilterBase(TResource src) : src(src)
	{
		PCO_INV_ARG(!src.isNull(), "Valid resource required");
	}

	virtual ~InflowFilterBase(void) = default;

	// Copy
	InflowFilterBase(const InflowFilterBase& ) = default;
	InflowFilterBase&	operator=(const InflowFilterBase& ) = default;

	// Move
	InflowFilterBase(InflowFilterBase&& ) = default;
	InflowFilterBase&	operator=(InflowFilterBase&& ) = default;

	//! @see IInflow
	virtual bool	read(POut& element) override = 0;

};

} // N..

#endif
