/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_DYN_BAG_OUTFLOW_HPP_INCLUDED
#define	LIB_PCO_DYN_BAG_OUTFLOW_HPP_INCLUDED

#include "flow/crt/I_Outflow.hpp"

namespace NPCO {

/*!
	DynBagOutflow: encapsulates a "dynamic bag" to use as outflow
*/
template<class PDynBag> class DynBagOutflow
	: public IOutflow<typename PDynBag::TType> {

public:
	typedef typename PDynBag::TType		TOut;

protected:
	PDynBag&	bag;

public:
	//! Default constructor
	DynBagOutflow(PDynBag& bag);

	//! @see IOutflow
	virtual void	write(const TOut& element) override;

};

} // N..

#include "imp/DynBagOutflow.cppi"

#endif
