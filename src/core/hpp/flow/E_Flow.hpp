/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_FLOW_HPP_INCLUDED
#define	LIB_PCO_E_FLOW_HPP_INCLUDED

#include "pattern/cfr/I_Classifier.hpp"

namespace NPCO {

/** Extension for flows
	@see IInflow / IOutflow
*/

/*! Check if two "flows" have same content - contains the same elements
	- use of operator==
*/
template<class PCell>
bool	e_inflow_equalContent(IInflow<PCell>& flow1, IInflow<PCell>& flow2)
{
	PCell	c1, c2;
	while(flow1.read(c1))
	{
		if(!flow2.read(c2))
		{
			return false;
		}

		if(! (c1 == c2))
		{
			return false;
		}
	}

	// Same if flow2 contains no more element
	return !flow2.read(c2);
}

/*! Compare two "flows" lexicographically
	- use of operator== and operator<
*/
template<class PCell>
int	e_inflow_lexiEqualCompare(IInflow<PCell>& flow1, IInflow<PCell>& flow2)
{
	PCell	c1, c2;
	while(flow1.read(c1))
	{
		if(!flow2.read(c2))
		{
			return +1;
		}

		if(! (c1 == c2))
		{
			return (c1 < c2) ? -1 : +1;
		}
	}

	// Same if flow2 contains no more element
	return flow2.read(c2) ? -1 : 0;
}


/*! Check if two "flows" are equals - contains the same elements
	- use of isSame
*/
template<class PCell>
bool	e_inflow_sameContent(IInflow<PCell>& flow1, IInflow<PCell>& flow2)
{
	PCell	c1, c2;
	while(flow1.read(c1))
	{
		if(!flow2.read(c2))
		{
			return false;
		}

		if(!c1.isSame(c2))
		{
			return false;
		}
	}

	// Same if flow2 contains no more element
	return !flow2.read(c2);
}

/*! Compare two "bags" lexicographically
	- use of isSame and isBefore
*/
template<class PCell>
int	e_inflow_lexiSameCompare(IInflow<PCell>& flow1, IInflow<PCell>& flow2)
{
	PCell	c1, c2;
	while(flow1.read(c1))
	{
		if(!flow2.read(c2))
		{
			return +1;
		}

		if(!c1.isSame(c2))
		{
			return c1.isBefore(c2) ? -1 : +1;
		}
	}

	// Same if flow2 contains no more element
	return flow2.read(c2) ? -1 : 0;
}



} // N..

#endif
