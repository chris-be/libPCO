/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TRANSFORM_OUTFLOW_HPP_INCLUDED
#define	LIB_PCO_TRANSFORM_OUTFLOW_HPP_INCLUDED

#include "OutflowFilterBase.hpp"

namespace NPCO {

/*!
	TransformOutflow: encapsulates an outflow and a "transform function"
*/
template<class POut, class PIn>
class TransformOutflow : public OutflowFilterBase<POut, PIn> {

public:
	typedef POut (*TransformFunc)(const PIn& in);

protected:
	using TBase		= OutflowFilterBase<POut, PIn>;

protected:
	TransformFunc		func;

public:
	TransformOutflow(IOutflow<POut>* dst, bool clean, TransformFunc func);
	TransformOutflow(IOutflow<POut>& dst, TransformFunc func);

	//! @see IOutflow
	virtual void	write(const PIn& element) override;

};

} // N..

#include "imp/TransformOutflow.cppi"

#endif
