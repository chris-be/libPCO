/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TRANSFORM_INFLOW_HPP_INCLUDED
#define	LIB_PCO_TRANSFORM_INFLOW_HPP_INCLUDED

#include "InflowFilterBase.hpp"

namespace NPCO {

/*!
	TransformInflow: encapsulates an inflow and a "transform function"
*/
template<class POut, class PIn>
class TransformInflow : public InflowFilterBase<POut, PIn> {

protected:
	using TBase		= InflowFilterBase<POut, PIn>;

public:
	using TResource	= typename TBase::TResource;

	typedef POut (*TransformFunc)(const PIn& in);

protected:
	TransformFunc	func;

public:
	TransformInflow(TResource src, TransformFunc func);

	//! @see IInflow
	virtual bool	read(POut& element) override;

};

} // N..

#include "imp/TransformInflow.cppi"

#endif
