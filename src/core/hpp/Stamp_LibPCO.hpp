/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STAMP_LIB_PCO_HPP_INCLUDED
#define	LIB_PCO_STAMP_LIB_PCO_HPP_INCLUDED

#include "dbg/CompileStamp.hpp"

/*!
	LibPCO Compile info
*/
class Stamp_LibPCO
	: public NPCO::CompileStamp {

public:
	Stamp_LibPCO(void);

};

#endif
