/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_IRI_PATH_HPP_INCLUDED
#define	LIB_PCO_IRI_PATH_HPP_INCLUDED

#include "string/String.hpp"
#include "pattern/cfr/C_SelfClassified.hpp"
#include "cvs/stl/Vector.hpp"
// #include "col/ChunkList.hpp"

namespace NPCO {

/*!
	Path for IRI/filenames/...
*/
class Path
		: private C_SelfClassified<Path>
		, private C_DynBag<Path, String, array_size>
		, private C_HasTail<Path, String> {

public:
	static const char8_t	separator;

public:
	//!
	using TType			= String;
	using TPartVector	= Vector<TType>;
	using TSize			= typename TPartVector::TSize;
	using TIterRead		= typename TPartVector::TIterRead;
	using TIterWrite	= typename TPartVector::TIterWrite;

protected:
	TPartVector	parts;
	//! /node must be differentiated from node (or */node")
	bool		relative;

	//! Use "isRelative"
	bool	getRelative(void) const;

	/** Parse and add
		@return If "absolute"
	*/
	bool	parseAdd(const IStringCarrier& sc);

public:
	Path(void) = default;
	Path(bool relative);
	Path(const IStringCarrier& path);

	// Copy
	Path(const Path& ) = default;
	Path& operator=(const Path& ) = default;

	// Move
	Path(Path&& ) = default;
	Path& operator=(Path&& ) = default;

	void	setRelative(bool value);
	bool	isRelative(void) const;

	//! @see C_HasSize
	TSize	size(void) const;

	//! @see C_HasSize
	bool	isEmpty(void) const;

	//! @see C_DynBag
	void	clear(void);

	//! @see C_DynBag
	void	add(const TType& toAdd);

	//! @see C_DynBag
	void	moveIn(TType&& toMove);

	//! Set
	void	set(const IStringCarrier& path);

	//! Add "sub path"
	void	add(const Path& toAdd);

	//! Add "sub path"
	void	add(const IStringCarrier& toAdd);

	String	createString(void) const;

	//! @see C_HasIterRead
	TIterRead		first(void) const;
	//! @see C_HasIterRead
	TIterRead		last(void) const;

	//! @see C_HasIterWrite
	TIterWrite		first(void);
	//! @see C_HasIterWrite
	TIterWrite		last(void);

	//! @see C_DynBag
	const TType&	operator[](const TSize i) const;
	//! @see C_DynBag
	TType&			operator[](const TSize i);

	//! @see C_DynBag
	TIterRead		iter_r(const TSize i) const;
	//! @see C_DynBag
	TIterWrite		iter_w(const TSize i);

	//! @see C_HasHead
	const TType&	head(void) const;
	TType&			head(void);
	void			popHead(void);
	TType			pullHead(void);

	//! @see C_HasTail
	const TType&	tail(void) const;
	TType&			tail(void);
	void			popTail(void);
	TType			pullTail(void);

	//! @see C_SelfClassified
	ClassifyValue	classify(const Path& toCompare) const;

};

} // N..

#endif
