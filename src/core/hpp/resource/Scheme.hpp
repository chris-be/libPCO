/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_IRI_SCHEME_HPP_INCLUDED
#define	LIB_PCO_IRI_SCHEME_HPP_INCLUDED

#include "string/String.hpp"
// #include "pattern/cfr/C_SelfClassified.hpp"

namespace NPCO {

/*!
	Scheme for IRI
*/
class Scheme : private C_SelfClassified<Scheme> {
		// beware of lower/upper case and %hexa

public:
	//!
	static const Scheme		mem_scheme;
	static const Scheme		file_scheme;
	static const Scheme		ftp_scheme;
	static const Scheme		ssh_scheme;
	static const Scheme		http_scheme;
	static const Scheme		https_scheme;

	enum EType {
		  unknown
		, mem
		, file
		, ftp
		, ssh
		, http	, https
	};

protected:
	String	scheme;

protected:
	//! Normalize scheme
	void	normalize(void);

	Scheme(const char8_t* name);

public:
	Scheme(void) = default;
	Scheme(const IStringCarrier& scheme);

	// Copy
	Scheme(const Scheme& ) = default;
	Scheme& operator=(const Scheme& ) = default;

	// Move
	Scheme(Scheme&& ) = default;
	Scheme& operator=(Scheme&& ) = default;

	void	clear(void);

	//! Get
	const String&	get(void) const;

	//! Set
	void	set(const IStringCarrier& scheme);

	bool	isValid(void) const;

	EType	findType(void) const;

	//! @see C_SelfClassified
	int		classify(const Scheme& toCompare) const;

};

} // N..

#endif
