/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_RESOURCE_IDENTIFIER_HPP_INCLUDED
#define	LIB_PCO_C_RESOURCE_IDENTIFIER_HPP_INCLUDED

namespace NPCO {

/*!
	Constraint "resource identifier" URI / IRI ...

	@param PChar Type of char used for scheme, protocol ...
*/
template<class PCd, class PChar>
class C_ResourceIdentifier {

public:
	typedef PChar			TChar;

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd = *ptr;

		// Has size
		// [[maybe_unused]]	PSize s = const_ctd.size();

		// Has isEmpty
		// [[maybe_unused]]	bool test = const_ctd.isEmpty();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_ResourceIdentifier(void)
	{
		C_ResourceIdentifier::ensure();
	}

};

} // N..

#endif
