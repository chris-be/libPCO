/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FILENAME_HPP_INCLUDED
#define	LIB_PCO_FILENAME_HPP_INCLUDED

#include "Path.hpp"
// #include "pattern/cfr/C_SelfClassified.hpp"
#include "string/CStringHolder.hpp"

namespace NPCO {

/*!
	Filename
*/
class Filename
	: public C_SelfClassified<Filename> {

public:
	using TType		= typename Path::TType;

	static const char8_t	extension_separator;

protected:
	Path		path;
	//! name (with extension)
	TType		name;

public:
	Filename(void) = default;
	Filename(const Path& path);
	Filename(const Path& path, const TType& name);
	Filename(const IStringCarrier& path, const IStringCarrier& name);
	Filename(const IStringCarrier& name);

	// Copy
	Filename(const Filename& ) = default;
	Filename& operator=(const Filename& ) = default;

	// Move
	Filename(Filename&& ) = default;
	Filename& operator=(Filename&& ) = default;

	//! Get path
	const Path&		getPath(void) const;

	//! Get path
	Path&			getPath(void);

	//! Get name
	const TType&	getName(void) const;

	//! Get name
	TType&			getName(void);

	void	clear(void);

	//! Set
	void	set(const IStringCarrier& name);

	//! @see C_SelfClassified
	ClassifyValue	classify(const Filename& toCompare) const;

	//!
	String		createString(void) const;

	//! Get "C style" compatible string -  @convenience
	CStringHolder	CFilename(void) const;

	//! Get name without extension
	static TType	extractName(const TType& name);
	//! Get extension only
	static TType	extractExtension(const TType& name);

	static TType	createFullname(const TType& name, const TType& extension);

};

} // N..

#endif
