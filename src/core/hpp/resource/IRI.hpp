/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_INTERNATIONALIZED_RESOURCE_IDENTIFIER_HPP_INCLUDED
#define	LIB_PCO_INTERNATIONALIZED_RESOURCE_IDENTIFIER_HPP_INCLUDED

// #include "C_ResourceIdentifier.hpp"
#include "Scheme.hpp"
// #include "Path.hpp"
#include "Filename.hpp"
// #include "string/String.hpp"
// #include "crt/cfr/C_SelfClassified.hpp"

namespace NPCO {

/*!
	Internationalized Resource Identifier - RFC 3987
	(URI : RFC 3986)

*/
class IRI	: private C_SelfClassified<IRI> {
			//, private C_ResourceIdentifier<IRI, String>
				// beware of lower/upper case and %hexa

public:
	using TString		= NPCO::String;

protected:
	Scheme		scheme;
	TString		authority;
	Path		path;
	TString		query;
	TString		fragment;

public:
	IRI(void) = default;
	IRI(const String& uri);
	IRI(const Scheme& scheme, const IStringCarrier& authority, const Path& path, const IStringCarrier& query, const IStringCarrier& fragment);
	IRI(const IStringCarrier& scheme, const IStringCarrier& authority, const IStringCarrier& path, const IStringCarrier& query, const IStringCarrier& fragment);
	IRI(const Scheme& scheme, const IStringCarrier& authority, const Filename& filename);

	// Copy
	IRI(const IRI& ) = default;
	IRI& operator=(const IRI& ) = default;

	// Move
	IRI(IRI&& ) = default;
	IRI& operator=(IRI&& ) = default;

	const Scheme&	getScheme(void) const;
	void			setScheme(const Scheme& v);

	const TString&	getAuthority(void) const;
	void			setAuthority(const IStringCarrier& v);

	const Path&		getPath(void) const;
	void			setPath(const Path& v);

	const TString&	getQuery(void) const;
	void			setQuery(const IStringCarrier& v);

	const TString&	getFragment(void) const;
	void			setFragment(const IStringCarrier& v);

	void	clear(void);

	bool	isValid(void) const;

	//! Decode string and set IRI with
	void	decode(const String& uri);

	//! @return Encoded IRI in UTF 8
	String	encode(void) const;

	// Path in fact
	Filename	filename(void) const;

	//! @see C_SelfClassified
	ClassifyValue	classify(const IRI& toCompare) const;

	static IRI	forFile(const Filename& filename);
	static IRI	forMem(const Filename& name);

};

} // N..

#endif
