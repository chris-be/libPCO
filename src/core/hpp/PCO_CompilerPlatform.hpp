/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_COMPILER_PLATFORM_HPP_INCLUDED
#define	LIB_PCO_COMPILER_PLATFORM_HPP_INCLUDED

/*!
	Used for cross platform compilation
*/

// predef.h code is obscure and not clean !
// Premake simplify declaring MACRO, so go for MACRO :/

#ifdef PREMAKE_POSIX
	#define FOR_POSIX		1
#elif defined PREMAKE_LINUX
	#define FOR_LINUX		2
#elif defined PREMAKE_WINDOWS
	#define FOR_WINDOWS		3
#else
	#error Unspecified OS
#endif

/*
#warning boost 1.55 non dispo sur Squeeze
// #include <boost/predef.h>
#define BOOST_OS_LINUX	1


#if (defined BOOST_OS_LINUX || defined BOOST_OS_MACOS)
	#define FOR_POSIX	1
#endif

#ifdef BOOST_OS_WINDOWS
	#define FOR_WIN32	2
#endif

#ifdef BOOST_OS_LINUX
	#define FOR_X11		1
#endif
*/

#endif
