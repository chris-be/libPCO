/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

// #include	"math/Matrix.hpp"
// #include "mem/Mem.hpp"

// #include "PCO_Predicate.hpp"

namespace NPCO {

template<class PType, TMatrixSize n, TMatrixSize m>
bool	Matrix<PType, n, m>::isInBounds(TMatrixSize col, TMatrixSize line) const
{
	PCO_INV_SIZE(e_isSizeValid(col));
	PCO_INV_SIZE(e_isSizeValid(line));
	return e_isInBounds(col, n) && e_isInBounds(line, m);
}

template<class PType, TMatrixSize n, TMatrixSize m>
TMatrixSize		Matrix<PType, n, m>::apos(TMatrixSize col, TMatrixSize line) const
{
	return col + n * line;
}

template<class PType, TMatrixSize n, TMatrixSize m>
const PType*	Matrix<PType, n, m>::_da(TMatrixSize col, TMatrixSize line) const
{
	return this->matrix._da() + this->apos(col, line);
}

template<class PType, TMatrixSize n, TMatrixSize m>
PType*			Matrix<PType, n, m>::_da(TMatrixSize col, TMatrixSize line)
{
	return this->matrix._da() + this->apos(col, line);
}

template<class PType, TMatrixSize n, TMatrixSize m>
template<class PCell>
PCell*	Matrix<PType, n, m>::nextCol(PCell* c) const
{
	return c + 1;
}

template<class PType, TMatrixSize n, TMatrixSize m>
template<class PCell>
PCell*	Matrix<PType, n, m>::nextLine(PCell* c) const
{
	return c + n;
}

template<class PType, TMatrixSize n, TMatrixSize m>
PType	Matrix<PType, n, m>::mul(const PType* line, const Matrix<PType, m, n>& right, TMatrixSize col)
{
	PType rv = 0;

	const PType* r = right._da(col, 0);
	for(const PType* e = line + n ; line < e ; ++line)
	{
		rv+= *line * *r;
		r = right.nextLine(r);
	}

	return rv;
}

template<class PType, TMatrixSize n, TMatrixSize m>
PType	Matrix<PType, n, m>::mul(const Matrix<PType, n, m>& left, TMatrixSize line, const Matrix<PType, m, n>& right, TMatrixSize col)
{
	const PType* l = left._da(0, line);
	return mul(l, right, col);
}

template<class PType, TMatrixSize n, TMatrixSize m>
Matrix<PType, n, m>::Matrix()
 : matrix(n * m)
{	}

template<class PType, TMatrixSize n, TMatrixSize m>
void	Matrix<PType, n, m>::fill(TParam v)
{
	this->matrix.fill(v);
}

template<class PType, TMatrixSize n, TMatrixSize m>
const PType&	Matrix<PType, n, m>::operator()(TMatrixSize col, TMatrixSize line) const
{
	PCO_RANGE_ERR(this->isInBounds(col, line), "invalid coordinate");
	const PType* p = this->_da(col, line);
	return *p;
}

template<class PType, TMatrixSize n, TMatrixSize m>
PType&			Matrix<PType, n, m>::operator()(TMatrixSize col, TMatrixSize line)
{
	PCO_RANGE_ERR(this->isInBounds(col, line), "invalid coordinate");
	PType* p = this->_da(col, line);
	return *p;
}

template<class PType, TMatrixSize n, TMatrixSize m>
void	Matrix<PType, n, m>::mulScalar(Matrix& res, const Matrix& mtx, TParam coef)
{
	const PType* src = mtx.matrix._da();

	PType* r = res.matrix._da();
	TMatrixSize limit = res.apos(n-1, m-1) + 1;
	for(PType* e = r + limit ; r != e ; ++r, ++src)
	{
		(*r) = (*src) * coef;
	}
}

template<class PType, TMatrixSize n, TMatrixSize m>
const Matrix<PType, n, m>&	Matrix<PType, n, m>::operator*=(TParam coef)
{
	mulScalar(*this, *this, coef);
	return *this;
}

template<class PType, TMatrixSize n, TMatrixSize m>
Matrix<PType, n, m>	Matrix<PType, n, m>::operator*(TParam coef)
{
	Matrix rv;	mulScalar(rv, *this, coef);
	return rv;
}

template<class PType, TMatrixSize n, TMatrixSize m>
void	Matrix<PType, n, m>::divScalar(Matrix& res, const Matrix& mtx, TParam coef)
{
	const PType* src = mtx.matrix._da();

	PType* r = res.matrix._da();
	TMatrixSize limit = res.apos(n-1, m-1) + 1;
	for(PType* e = r + limit ; r != e ; ++r, ++src)
	{
		(*r) = (*src) / coef;
	}
}

template<class PType, TMatrixSize n, TMatrixSize m>
const Matrix<PType, n, m>&	Matrix<PType, n, m>::operator/=(TParam coef)
{
	divScalar(*this, *this, coef);
	return *this;
}

template<class PType, TMatrixSize n, TMatrixSize m>
Matrix<PType, n, m>	Matrix<PType, n, m>::operator/(TParam coef)
{
	Matrix rv;	divScalar(rv, *this, coef);
	return rv;
}

template<class PType, TMatrixSize n, TMatrixSize m>
const Matrix<PType, n, m>&	Matrix<PType, n, m>::operator+=(const Matrix<PType, n, m>& v)
{
	const PType* src = v.matrix._da();

	PType* r = this->matrix._da();
	TMatrixSize limit = this->apos(n-1, m-1) + 1;
	for(PType* e = r + limit ; r != e ; ++r, ++src)
	{
		(*r) += (*src);
	}

	return *this;
}

template<class PType, TMatrixSize n, TMatrixSize m>
Matrix<PType, n, m>	Matrix<PType, n, m>::operator+(const Matrix<PType, n, m>& v)
{
	const PType* src = this->matrix._da();
	const PType* src2 = v.matrix._da();

	Matrix res;
	PType* r = res.matrix._da();
	TMatrixSize limit = res.apos(n-1, m-1) + 1;
	for(PType* e = r + limit ; r != e ; ++r, ++src, ++src2)
	{
		(*r) = (*src) + (*src2);
	}

	return res;
}

template<class PType, TMatrixSize n, TMatrixSize m>
const Matrix<PType, n, m>&	Matrix<PType, n, m>::operator-=(const Matrix<PType, n, m>& v)
{
	const PType* src = v.matrix._da();

	PType* r = this->matrix._da();
	TMatrixSize limit = this->apos(n-1, m-1) + 1;
	for(PType* e = r + limit ; r != e ; ++r, ++src)
	{
		(*r) -= (*src);
	}

	return *this;
}

template<class PType, TMatrixSize n, TMatrixSize m>
Matrix<PType, n, m>	Matrix<PType, n, m>::operator-(const Matrix<PType, n, m>& v)
{
	const PType* src = this->matrix._da();
	const PType* src2 = v.matrix._da();

	Matrix res;
	PType* r = res.matrix._da();
	TMatrixSize limit = res.apos(n-1, m-1) + 1;
	for(PType* e = r + limit ; r != e ; ++r, ++src, ++src2)
	{
		(*r) = (*src) - (*src2);
	}

	return res;
}

/*
// Only square matrix
template<class PType, TMatrixSize n, TMatrixSize m>
const Matrix<PType, n, m>&	Matrix<PType, n, m>::operator*=(const Matrix<PType, m, n>& v)
{
	return *this;
}
*/

template<class PType, TMatrixSize n, TMatrixSize m>
Matrix<PType, m, m>	Matrix<PType, n, m>::operator*(const Matrix<PType, m, n>& v)
{
	Matrix<PType, m, m> rm;

	PType* rv = rm.matrix._da();
	for(TMatrixSize i = 0 ; i < m ; ++i)
	{	// Sweep each line
		for(TMatrixSize j = m ; j > 0 ; --j)
		{	// Compute columns values
			*rv = mul(*this, i, v, i);
			++rv;
		}
	}

	return rm;
}

} // N..
