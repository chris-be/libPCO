/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_NUMBER_HPP_INCLUDED
#define	LIB_PCO_NUMBER_HPP_INCLUDED

#include "pattern/math/C_Number.hpp"

// #include "PCO_ForceInline.hpp"
#include "math/MathMembers.hpp"

namespace NPCO {

/**
	TypeNumber: encapsulate primitive type.

	@param PType encapsulated type
	@param PDerived for child class
*/
template<class PType>
class TypeNumber : private C_Number<TypeNumber<PType>> {

public:
	using TType		= PType;

protected:
	//! Signed to simplify computations
	PType	value;

public:
	TypeNumber(void) = default;
	TypeNumber(PType v) : value(v)
	{	}

	// Copy
	TypeNumber(const TypeNumber& ) = default;
	TypeNumber& operator=(const TypeNumber& ) = default;

	// Move
	TypeNumber(TypeNumber&& ) = default;
	TypeNumber& operator=(TypeNumber&& ) = default;

	PCO_INLINE
	TypeNumber&	operator=(const PType v)
	{
		this->value = v;
		return *this;
	}

	// Cast
	PCO_INLINE
	explicit operator PType() const
	{
		return this->value;
	}

	ADD_ADDITION_MEMBERS(TypeNumber, value);
	ADD_MULTIPLICATION_MEMBERS(TypeNumber, value, TType);
	ADD_COMPARISON_MEMBERS(TypeNumber, value);

};

} // N..

#endif