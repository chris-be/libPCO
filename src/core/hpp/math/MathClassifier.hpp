/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MATH_CLASSIFIER_HPP_INCLUDED
#define	LIB_PCO_MATH_CLASSIFIER_HPP_INCLUDED

#include "pattern/cfr/C_Classifier.hpp"

namespace NPCO {

/**
	C_Classifier for math objects : use operators <, ==
	@todo : for primitives: o1 - o2

	Note: could only use "operator<" like STL but MathClassifier is designed for primitives.
*/
template<class PClfd>
class MathClassifier
		: private C_Classifier<MathClassifier<PClfd>, PClfd> {

public:
	using TClassified				= PClfd;

public:
	//! @see C_Classifier
	PCO_INLINE
	ClassifyValue	classify(const PClfd& o1, const PClfd& o2) const
	{
		if(o1 < o2)		return -1;
		if(o1 == o2)	return 0;
		return 1;
	}

};

} // N..

#endif
