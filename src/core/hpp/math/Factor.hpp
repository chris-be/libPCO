/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FACTOR_HPP_INCLUDED
#define	LIB_PCO_FACTOR_HPP_INCLUDED

#include "pattern/math/C_Number.hpp"
#include "pattern/cfr/C_SelfClassified.hpp"
#include "cvs/T_Param.hpp"

#include "PCO_ForceInline.hpp"
#include "dbg/Debug.hpp"

namespace NPCO {

/**
	Factor: nominator, denominator.

	@param PType encapsulated type
*/
template<class PType>
class Factor	: private C_Number<PType>
				, private C_Number<Factor<PType>>
				, private C_SelfClassified<Factor<PType>> {

public:
	using TType		= PType;
	using TParam	= typename NPCO::param_transit_t<PType>;

protected:
	PType	nominator;
	PType	denominator;

public:
	Factor(void) : Factor(1, 1)
	{	}

	Factor(TParam n, TParam d) : nominator(n), denominator(d)
	{	}

	Factor(TParam n)
	{
		this->operator=(n);
	}

	// Copy
	Factor(const Factor& ) = default;
	Factor&	operator=(const Factor& ) = default;

	// Move
	Factor(Factor&& ) = default;
	Factor&	operator=(Factor&& ) = default;

	PCO_INLINE
	void	set(TParam n, TParam d)
	{
		this->nominator = n;
		this->denominator = d;
	}

	//! Set n/1
	Factor&	operator=(TParam n)
	{
		this->set(n, 1);
		return *this;
	}

	//! Set 1/1
	PCO_INLINE
	void	setNeutral(void)
	{
		this->set(1, 1);
	}

	PCO_INLINE
	bool	isValid(void) const
	{
		return this->denominator != 0;
	}

	PCO_INLINE
	TParam	getNominator(void) const
	{
		return this->nominator;
	}

	PCO_INLINE
	TParam	getDenominator(void) const
	{
		return this->denominator;
	}

	// negate

	Factor	operator-(void) const
	{
		return Factor(-this->nominator, this->denominator);
	}

	// add / sub

	const Factor&	operator+=(const Factor& f)
	{
		this->add(f.nominator, f.denominator);
		return *this;
	}

	const Factor&	operator-=(const Factor& f)
	{
		this->add(-f.nominator, f.denominator);
		return *this;
	}

	Factor	operator+(const Factor& f) const
	{
		Factor rv(*this);	rv+= f;
		return rv;
	}

	Factor	operator-(const Factor& f) const
	{
		Factor rv(*this);	rv-= f;
		return rv;
	}

	// inc / dec

	const Factor&	operator++(void)
	{
		this->nominator += this->denominator;
		return *this;
	}

	Factor	operator++(int ignored)
    {
        Factor rv(*this);	this->operator++();
        return rv;
    }

	const Factor&	operator--(void)
	{
		this->nominator -= this->denominator;
		return *this;
	}

	Factor	operator--(int ignored)
    {
        Factor rv(*this);	this->operator--();
        return rv;
    }

	// mul / div

	const Factor&	operator*=(const Factor& f)
	{
		this->mul(f.nominator, f.denominator);
		return *this;
	}

	const Factor&	operator/=(const Factor& f)
	{
		this->mul(f.denominator, f.nominator);
		return *this;
	}

	Factor	operator*(const Factor& f) const
	{
		Factor rv(*this);	rv*= f;
		return rv;
	}

	Factor	operator/(const Factor& f) const
	{
		Factor rv(*this);	rv/= f;
		return rv;
	}

	const Factor&	operator*=(TParam v)
	{
		this->nominator*= v;
		return *this;
	}

	const Factor&	operator/=(TParam v)
	{
		this->denominator*= v;
		return *this;
	}

	Factor	operator*(TParam v)
	{
		Factor rv(*this);	rv*= v;
		return rv;
	}

	Factor	operator/(TParam v)
	{
		Factor rv(*this);	rv/= v;
		return rv;
	}

	PCO_INLINE
	PType	mul(TParam v) const
	{
		PCO_ASSERT(this->isValid());
		// mul before div to limit loss
		return v * this->nominator / this->denominator;
	}

	PCO_INLINE
	PType	div(TParam v) const
	{
		PCO_ASSERT(this->isValid());
		// mul before div to limit loss
		return v * this->denominator / this->nominator;
	}

	PCO_INLINE
	PType	eval(void) const
	{
		PCO_ASSERT(this->isValid());
		return this->nominator / this->denominator;
	}

	PCO_INLINE
	explicit operator PType(void) const
	{
		return this->eval();
	}

	// comparison

	//! @see C_SelfClassified
	int		classify(const Factor& toCompare) const
	{
		PType left = this->nominator;
		PType right= toCompare.nominator;

		if(this->denominator != toCompare.denominator)
		{
			left *= toCompare.denominator;
			right *= this->denominator;
		}

		// TODO: return (left <=> right); in x20
		if(left == right)	return 0;
		else return (left < right) ? -1 : +1;
	}

	PCO_INLINE
	bool		operator==(const Factor& toCompare) const
	{
		return this->classify(toCompare) == 0;
	}

	PCO_INLINE
	bool		operator!=(const Factor& toCompare) const
	{
		return this->classify(toCompare) != 0;
	}

	PCO_INLINE
	bool		operator<(const Factor& toCompare) const
	{
		return this->classify(toCompare) < 0;
	}

	PCO_INLINE
	bool		operator>(const Factor& toCompare) const
	{
		return this->classify(toCompare) > 0;
	}

	PCO_INLINE
	bool		operator<=(const Factor& toCompare) const
	{
		return this->classify(toCompare) <= 0;
	}

	PCO_INLINE
	bool		operator>=(const Factor& toCompare) const
	{
		return this->classify(toCompare) >= 0;
	}

protected:
	// greatest common factor ??
	void	mul(TParam nom, TParam denom)
	{
		this->nominator *= nom;
		this->denominator *= denom;
	}

	// greatest common factor ?
	void	add(TParam nom, TParam denom)
	{
		// Ensure copy !
		PType toAdd = nom;
		if(this->denominator != denom)
		{	// Same denominator
			toAdd *= this->denominator;
			this->nominator *= denom;
			this->denominator*= denom;
		}

		this->nominator+= toAdd;
	}

};

} // N..

#endif
