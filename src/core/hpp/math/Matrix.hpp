/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MATRIX_HPP_INCLUDED
#define	LIB_PCO_MATRIX_HPP_INCLUDED

#include "pattern/math/C_Number.hpp"
#include "mem/Array.hpp"
#include "cvs/T_Param.hpp"

#include "PCO_ForceInline.hpp"

namespace NPCO {

typedef unsigned int 		TMatrixSize;

/**
	Matrix:
		- columns are contiguous

	@param PType encapsulated type
	@param n Column number
	@param m Line number
*/
template<class PType, TMatrixSize n, TMatrixSize m>
class Matrix : private C_Number<PType> {

public:
	static_assert(n > 0, "No column ? Nonsense");
	static_assert(m > 0, "No line ? Nonsense");

	using TType		= PType;
	using TParam	= typename NPCO::param_transit_t<PType>;

protected:
	using TMatrix	= Array<PType>;

	template<class AnyType, TMatrixSize anyN, TMatrixSize anyM>
	friend class Matrix;

protected:
	TMatrix	matrix;

	PCO_INLINE
	bool		isInBounds(TMatrixSize col, TMatrixSize line) const;

	PCO_INLINE
	// Returns array position calculated from column, line
	TMatrixSize	apos(TMatrixSize col, TMatrixSize line) const;

	PCO_INLINE
	const PType*	_da(TMatrixSize col, TMatrixSize line) const;

	PCO_INLINE
	PType*			_da(TMatrixSize col, TMatrixSize line);

	template<class PCell>
	PCO_INLINE
	PCell*		nextCol(PCell* c) const;

	template<class PCell>
	PCO_INLINE
	PCell*		nextLine(PCell* c) const;

	static void	mulScalar(Matrix& res, const Matrix& mtx, TParam coef);
	static void	divScalar(Matrix& res, const Matrix& mtx, TParam coef);

	/** Compute multiplication of line with right.column
		@param line PType[n]
	*/
	static PType	mul(const PType* line, const Matrix<PType, m, n>& right, TMatrixSize col);

	//! Compute multiplication of left.line with right.column
	static PType	mul(const Matrix<PType, n, m>& left, TMatrixSize line, const Matrix<PType, m, n>& right, TMatrixSize col);

public:
	Matrix(void);

	// Copy
	Matrix(const Matrix& ) = default;
	Matrix&	operator=(const Matrix& ) = default;

	// Move
	Matrix(Matrix&& ) = default;
	Matrix&	operator=(Matrix&& ) = default;

	void	fill(TParam v);

	const PType&	operator()(TMatrixSize col, TMatrixSize line) const;
	PType&			operator()(TMatrixSize col, TMatrixSize line);

	// Scalar
	const Matrix&	operator*=(TParam v);
	Matrix	operator*(TParam coef);
	const Matrix&	operator/=(TParam v);
	Matrix	operator/(TParam coef);

	// Addition
	const Matrix&	operator+=(const Matrix<PType, n, m>& v);
	Matrix	operator+(const Matrix<PType, n, m>& v);
	const Matrix&	operator-=(const Matrix<PType, n, m>& v);
	Matrix	operator-(const Matrix<PType, n, m>& v);

	// Multiplication
	Matrix<PType, m, m>	operator*(const Matrix<PType, m, n>& v);

};

} // N..

#include "imp/Matrix.cppi"

#endif
