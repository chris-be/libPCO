/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MATH_MEMBERS_HPP_INCLUDED
#define	LIB_PCO_MATH_MEMBERS_HPP_INCLUDED

#include "PCO_ForceInline.hpp"

namespace NPCO {

// Add addition members to class
#define	ADD_ADDITION_MEMBERS(ClassName, ValueMember)		\
	ClassName	operator-(void) const						\
	{														\
		return ClassName(-this->ValueMember);				\
	}														\
\
	ClassName&	operator++(void)							\
	{														\
		++this->ValueMember;								\
		return *this;										\
	}														\
\
	ClassName&	operator--(void)							\
	{														\
		--this->ValueMember;								\
		return *this;										\
	}														\
\
	ClassName&	operator+=(const ClassName& delta)			\
	{														\
		this->ValueMember+= delta.ValueMember;				\
		return *this;										\
	}														\
\
	ClassName&	operator-=(const ClassName& delta)			\
	{														\
		this->ValueMember-= delta.ValueMember;				\
		return *this;										\
	}														\
\
	ClassName	operator+(const ClassName& delta) const		\
	{																\
		return ClassName(this->ValueMember + delta.ValueMember);	\
	}														\
\
	ClassName	operator-(const ClassName& delta) const		\
	{																\
		return ClassName(this->ValueMember - delta.ValueMember);	\
	}

// Add multiplication members to class
#define	ADD_MULTIPLICATION_MEMBERS(ClassName, ValueMember, ValueType)	\
	ClassName&	operator*=(const ClassName& coef)			\
	{														\
		this->ValueMember*= coef.ValueMember;				\
		return *this;										\
	}														\
\
	ClassName&	operator/=(const ClassName& coef)			\
	{														\
		this->ValueMember/= coef.ValueMember;				\
		return *this;										\
	}														\
\
	ClassName&	operator*=(const ValueType factor)			\
	{														\
		this->ValueMember*= factor;							\
		return *this;										\
	}														\
\
	ClassName&	operator/=(const ValueType factor)			\
	{														\
		this->ValueMember/= factor;							\
		return *this;										\
	}														\
\
	ClassName	operator*(const ClassName& coef) const		\
	{														\
		return ClassName(this->ValueMember * coef.ValueMember);	\
	}														\
\
	ClassName	operator/(const ClassName& coef) const		\
	{														\
		return ClassName(this->ValueMember / coef.ValueMember);	\
	}

// Add comparison members to class
#define	ADD_COMPARISON_MEMBERS(ClassName, ValueMember)			\
	bool	operator==(const ClassName& toCompare) const		\
	{	return this->ValueMember == toCompare.ValueMember;	}	\
\
	bool	operator!=(const ClassName& toCompare) const		\
	{	return this->ValueMember != toCompare.ValueMember;	}	\
\
	bool	operator<(const ClassName& toCompare) const			\
	{	return this->ValueMember < toCompare.ValueMember;	}	\
\
	bool	operator>(const ClassName& toCompare) const			\
	{	return this->ValueMember > toCompare.ValueMember;	}	\
\
	bool	operator<=(const ClassName& toCompare) const		\
	{	return this->ValueMember <= toCompare.ValueMember;	}	\
\
	bool	operator>=(const ClassName& toCompare) const		\
	{	return this->ValueMember >= toCompare.ValueMember;	}	\

} // N..

#endif