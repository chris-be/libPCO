/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_SQUARE_MATRIX_HPP_INCLUDED
#define	LIB_PCO_SQUARE_MATRIX_HPP_INCLUDED

#include "Matrix.hpp"

namespace NPCO {

/**
	SquareMatrix

	@param PType encapsulated type
	@param n Size number
*/
template<class PType, TMatrixSize n>
class SquareMatrix : public Matrix<PType, n, n> {

public:
	using TBase		= Matrix<PType, n, n>;
//	using TType		= PType;
//	using TParam	= typename NPCO::param_transit_t<PType>;

	static const TMatrixSize	dimension;

protected:

public:
	SquareMatrix(void) = default;

	// Copy
	SquareMatrix(const SquareMatrix& ) = default;
	SquareMatrix&	operator=(const SquareMatrix& ) = default;

	// Move
	SquareMatrix(SquareMatrix&& ) = default;
	SquareMatrix&	operator=(SquareMatrix&& ) = default;

	void	setIdentity(void);

	// Multiplication
	SquareMatrix&	operator*=(const SquareMatrix<PType, n>& v);

};

} // N..

#include "imp/SquareMatrix.cppi"

#endif
