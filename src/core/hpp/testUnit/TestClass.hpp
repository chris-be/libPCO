/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_TEST_CLASS_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT_TEST_CLASS_HPP_INCLUDED

#include "TestTypes.hpp"

namespace NTestUnit {

/**
	Base class to inherit to write tests
 */
class TestClass {
public:

	/*!
		FunctionPointer to create an instance of a TestClass
		@return Instance of a test
	*/
	typedef TestClass* (*FacFunc)(void);

protected:

	/** Use to enforce test writing - will run in release mode too
		@param condition Result of predicate condition
		@param msg Additional message (can be nullptr)
		@param file In which file the test is done
		@param line At which line this method is called
	*/
	void	predicate(bool condition, const char* msg, const char* file, unsigned int line) const;

	/** Call it to validate test results
		@param condition Result of test
		@param msg Additional message (can be nullptr)
		@param file In which file the test is done
		@param line At which line this method is called
	*/
	void	check(bool condition, const char* msg, const char* file, unsigned int line) const;

public:
	//! Ensure proper cleanup
	virtual ~TestClass(void);

	//! Test for reliability/stability/...
	virtual void correctness(void);

	//! Test for speed (more benchmark than anything)
	virtual void speed(void);

	//! Test for volumetry (more resources usage benchmark than anything)
	virtual void volumetry(void);

	// ! Run test @param mode
	//virtual void run(ETestMode mode);

};

} // N..

#endif
