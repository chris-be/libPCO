/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_TEST_RUNNER_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT_TEST_RUNNER_HPP_INCLUDED

#include "ITestLogger.hpp"
#include "TestContainer.hpp"

namespace NTestUnit {

/**

 */
class TestRunner {
protected:
	static const TestName ErrorMessage_AddingTestClass;
	static const TestName ErrorMessage_RunningTest;

protected:
	ITestLogger*	logger;
	TestContainer	container;

	int	runTest(const TestName& name, const ETestMode mode);

public:
	/**
		@param testLogger Will be deleted
	*/
	TestRunner(ITestLogger*	testLogger = nullptr);

	~TestRunner(void);

	/** Launch all registered tests
		@param mode
		@return 0 if ok
	*/
	int	run(ETestMode mode);

	/**
		Add test
		@param PTest TestClass child
		@param name If empty => use RTTI name
		@return name given
	*/
	template<class PTest>
	TestName	addTestClass(const TestName& name)
	{
		TestClass* (*fac)(void) = [] (void) -> TestClass* { return new PTest(); };
		this->container.addTestClass(name, fac);

		return name;
	}

	/**
		Add test as dependant of another
		@convenience
		@param PTest TestClass child
		@return name given
	*/
	template<class PTest>
	TestName	addTestClass(const TestName& previous, const TestName& name)
	{
		this->addTestClass<PTest>(name);
		this->container.setDependancy(previous, name);

		return name;
	}

	/**
		Add a test as dependant of another
		@param previous Test name to be executed before
		@param name Dependant test name
	*/
	void	addDependancy(const TestName& previous, const TestName& name)
	{
		this->container.setDependancy(previous, name);
	}

};

} // N..

#endif
