/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	TEST_LIB_PCO_TEST_UNIT_MEM_SAMPLE_HPP_INCLUDED
#define	TEST_LIB_PCO_TEST_UNIT_MEM_SAMPLE_HPP_INCLUDED

#include "pattern/bag/C_Bag.hpp"
#include "pattern/walk/I_Walker.hpp"
#include "mem/iter/MemIterRead.hpp"
#include "mem/iter/MemIterWrite.hpp"

namespace NTestUnit {

using namespace NPCO;

/**
	Create "samples" to fill bags, buffers, ... and test content back.
	@param nb Size of buffer
 */
template<class PType, size_t nb>
class MemSample
	: private C_Bag<MemSample<PType, nb>, PType, size_t> {

public:
	using TType			= PType;
	using TSize			= size_t;
	using TIterRead		= MemIterRead<PType>;
	using TIterWrite	= MemIterWrite<PType>;

protected:
	PType		buffer[nb];

public:
	//!
	MemSample(void);
	MemSample(bool randomFill);

	//! @see C_Bag
	PCO_INLINE
	size_t	size(void) const
	{
		return nb;
	}

	//! @see C_Bag
	inline
	bool	isEmpty(void) const
	{
		return nb == 0;
	}

	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead	first(void) const;
	PCO_INLINE
	TIterRead	last(void) const;

	//! @see C_HasIterWrite
	PCO_INLINE
	TIterWrite	first(void);
	PCO_INLINE
	TIterWrite	last(void);

	//! Fill sample randomly
	void	randomFill(void);

	bool checkContent(const PType *toTest, const size_t testSize = nb) const;

	template<class PWalker>
	bool checkContent(PWalker& toTest) const;

	template<class PWalker>
	bool checkContent(PWalker& toTest, const size_t testSize) const;

	PCO_INLINE
	const PType*	_da(void) const
	{
		return this->buffer;
	}

	PCO_INLINE
	PType*			_da(void)
	{
		return this->buffer;
	}

};

} // N..

#include "imp/MemSample.cppi"

#endif
