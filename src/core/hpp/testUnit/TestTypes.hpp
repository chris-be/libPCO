/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_TEST_TYPES_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT TEST_TYPES_HPP_INCLUDED

#include "logic/Flag.hpp"

/**
	Types for TestUnit
 */
namespace NTestUnit {

// Flag for a "test mode"
typedef NPCO::Flag<>	TestMode;

/*!
	Test mode
	@remarks Flags
*/
enum class ETestMode : unsigned int {

	  correctness	= TestMode::make(1)	// Normal
	, speed			= TestMode::make(2)	// Speed tests
	, volumetry		= TestMode::make(3)	// Volumetry tests
};

} // N..

#endif
