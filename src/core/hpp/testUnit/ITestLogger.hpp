/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_ILOGGER_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT_ILOGGER_HPP_INCLUDED

#include "string/StringConstant.hpp"

namespace NTestUnit {

using namespace NPCO;

/**
	Contract for tests results logger
 */
class ITestLogger {
public:
	using LogLevel		= unsigned int;
	using TChannel		= IStringCarrier;
	using TMessage		= IStringCarrier;

	static const StringC	MessageChannel;
	static const StringC	ErrorChannel;
	static const StringC	VolumetryChannel;

public:
	virtual ~ITestLogger(void)	{ }

	/** Set log level
		@param channel Channel to configure
		@param logLevel Max level of log
	*/
	virtual void setLogLevel(const TChannel& channel, LogLevel logLevel) = 0;

	/** Indicates that a sub test will be done. Permits to indent text for example.
	*/
	virtual void enterSubSection(const TChannel& channel, const TMessage& name) = 0;

	/** Indicates that a sub test is done.
	*/
	virtual void leaveSubSection(const TChannel& channel) = 0;

	virtual void logMessage(const TChannel& channel, LogLevel level, const TMessage& message) = 0;

	//! Shortcut
	void logMessage(LogLevel level, const TMessage& message)
	{
		this->logMessage(MessageChannel, level, message);
	}

	//! Shortcut
	void logError(LogLevel level, const TMessage& message)
	{
		this->logMessage(ErrorChannel, level, message);
	}

	//! Shortcut
	void logVolumetry(LogLevel level, const TMessage& message)
	{
		this->logMessage(VolumetryChannel, level, message);
	}

};

} // N..

#endif
