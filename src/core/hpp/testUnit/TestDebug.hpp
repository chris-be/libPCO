/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_TEST_DEBUG_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT_TEST_DEBUG_HPP_INCLUDED

#include "dbg/Debug.hpp"

// #define TESTUNIT_PREDICATE(X)		PCO_ASSERT_MSG(X, "TEST PREDICATE")

#endif
