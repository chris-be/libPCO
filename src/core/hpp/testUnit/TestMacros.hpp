/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_TEST_MACROS_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT_TEST_MACROS_HPP_INCLUDED

// TestRunner macros

// Add test class
#define ADD_TESTC(RUNNER, CLASS)			RUNNER.addTestClass<CLASS>(#CLASS);

// Add dependent test class
#define ADD_DEP_TESTC(RUNNER, PREC, CLASS)	RUNNER.addTestClass<CLASS>(PREC, #CLASS);



// TestClass macros

#define TC_PREDICATE_MSG(C, MSG)	this->predicate(C, MSG, __FILE__, __LINE__);
#define TC_PREDICATE(C)				TC_PREDICATE_MSG(C, nullptr);

#define TC_CHECK_MSG(T, MSG)		this->check(T, MSG, __FILE__, __LINE__);
#define TC_CHECK(T)					TC_CHECK_MSG(T, nullptr);

#endif
