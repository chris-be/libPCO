/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_TEST_NAME_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT_TEST_NAME_HPP_INCLUDED

#include "string/String.hpp"
#include "cvs/cfr/DefaultClassifier.hpp" // StringClassifier ??

/**
	Type to use for names
 */
namespace NTestUnit {

using TestName			= NPCO::String;
using TestNameCfr		= NPCO::DefaultClassifier<TestName>;

} // N..

#endif
