/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_TEST_CONTAINER_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT_TEST_CONTAINER_HPP_INCLUDED

#include "col/ChunkList.hpp"
#include "col/MapTree.hpp"
#include "TestName.hpp"
#include "TestClass.hpp"

namespace NTestUnit {

using namespace NPCO;

/**
	Container of tests to run
 */
class TestContainer {

public:
	static const chunk_size BlockSize;

	using FacFunc		= typename TestClass::FacFunc;

	using TFactory		= MapTree<TestName, FacFunc, TestNameCfr>;
	using TNameList		= ChunkList<TestName>;
	using THierarchy	= MapTree<TestName, TNameList, TestNameCfr>;

protected:
	TFactory		factory;
	THierarchy		hierarchy;

public:
	TestContainer(void);
	~TestContainer(void);

	//! Clear
	void	clear(void);

	/** Add test
		@param name Name of test
		@param func Factory function
	*/
	void	addTestClass(const TestName& name, FacFunc func);

	/** Check if a dependancy exists (any leaf of root)
		@param root Root name
		@param leaf Leaf name
	*/
	bool	isDependancy(const TestName& root, const TestName& leaf) const;

	/**
		Set a test as dependant of another
	*/
	void	setDependancy(const TestName& previous, const TestName& name);

	/** Create a TestClass instance
		@return Instance : must be deleted !
	*/
	TestClass*	newTest(const TestName& name) const;

	/**
		@return Names of test roots (which are not dependant)
	*/
	TNameList	findRoots(void) const;

};

} // N..

#endif
