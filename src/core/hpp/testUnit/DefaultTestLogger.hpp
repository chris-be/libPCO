/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_DEFAULT_TEST_LOGGER_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT_DEFAULT_TEST_LOGGER_HPP_INCLUDED

#include "ITestLogger.hpp"

namespace NTestUnit {

/**
	Default test logger:
		- infos: cout
		- errors: cerr
 */
class DefaultTestLogger : public ITestLogger {

protected:
	LogLevel	level;

public:
	//! Ensure proper cleanup
	virtual ~DefaultTestLogger(void) override;

	//! @see ITestLogger
	virtual void setLogLevel(const TChannel& channel, LogLevel level) override;

	//! @see ITestLogger
	virtual void enterSubSection(const TChannel& channel, const TMessage& name) override;

	//! @see ITestLogger
	virtual void leaveSubSection(const TChannel& channel) override;

	//! @see ITestLogger
	virtual void logMessage(const TChannel& channel, LogLevel level, const TMessage& message) override;

};

} // N..

#endif
