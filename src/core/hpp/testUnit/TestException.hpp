/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEST_UNIT_TEST_EXCEPTION_HPP_INCLUDED
#define	LIB_PCO_TEST_UNIT_TEST_EXCEPTION_HPP_INCLUDED

#include "dbg/DebugInfo.hpp"
#include <exception>

namespace NTestUnit {

using NPCO::DebugInfo;

/**
	TestException : used to signal "validation errors" during tests

	Design:
		- carry same info as DebugException
		- no inheritance : catch(DebugException) would get it !!
*/
class TestException : public DebugInfo, public std::exception {

public:
	TestException(const char* info, const char* file, unsigned int line);
	TestException(const DebugInfo& info);

	virtual ~TestException(void) override;

	// Copy
	TestException(const TestException& );
	TestException& operator=(const TestException& );

	// Move does copy
	TestException(TestException&& );
	TestException& operator=(TestException&& ) = delete;

	virtual const char* what(void) const noexcept override;

};

} // N..

#endif
