/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TEMPORAL_HPP_INCLUDED
#define	LIB_PCO_TEMPORAL_HPP_INCLUDED

namespace NPCO {

/*!
	Temporal: some temporal values
	@todo math : SizeConverter(scale:1000/1024)
					toKb, toMilli...
*/
class Temporal
{
public:

	class Second
	{
	public:
		static const long In_Milliseconds		= 1000L;
		static const long In_Microseconds		= 1000000L;
		static const long In_Nanoseconds		= 1000000000L;
	};

	class MicroSecond
	{
	public:
		static const long In_Nanoseconds		= 1000L;
	};

};

} // N..

#endif
