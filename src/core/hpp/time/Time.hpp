/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TIME_HPP_INCLUDED
#define	LIB_PCO_TIME_HPP_INCLUDED

#include <time.h>
#include <string>

/*!
	Quelques routines de conversions
		N'utilise pas le GMT !!

class Time
{

public:
	/
		@param time Time à convertir (without time zone)
		@param toPut Partie de la date à afficher (voir gmtime_r)
	/
	static std::string	timeToString(time_t time, const char *toPut);

	/
		@param toConvert Chaine à convertir en timestamp (without time zone)
		@param toExtract Partie de la chaine qu'il faut extraire (voir timegm)
	/
	static time_t		stringToTime(const std::string &toConvert, const char *toExtract);

/ Rappels :
		%Y-%m-%d %H:%M:%S <=> %T %F
/

	/
		@param time Time à convertir (with time zone)
		@param toPut Partie de la date à afficher (voir gmtime_r)
	/
	static std::string	timeToStringTZ(time_t time, const char *toPut);

	/
		@param toConvert Chaine à convertir en timestamp (with time zone)
		@param toExtract Partie de la chaine qu'il faut extraire (voir timegm)
	/
	static time_t		stringToTimeTZ(const std::string &toConvert, const char *toExtract);

};
*/

#endif
