/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_TIMER_HPP_INCLUDED
#define	LIB_PCO_TIMER_HPP_INCLUDED

#include "time/Nanotime.hpp"

namespace NPCO {

/**
	Timer
*/
class Timer {

protected:
	//! Start time
	Nanotime		start;
	//! Delay
	Nanotime		delay;
	//! start + delay
	Nanotime		stop;

public:
	Timer(void);
	/**
		@param secDelay Delay in seconds
		@param nanoDelay Delay in nanoseconds
	*/
	Timer(time_t secDelay, long nanoDelay);
	Timer(const Nanotime& delay);

	~Timer(void);

	// Copy
	Timer(const Timer& toCopy) = default;
	Timer& operator=(const Timer& toCopy) = default;

	// Move
	Timer(Timer&& toMove) = default;
	Timer& operator=(Timer&& toMove) = default;

	//! Set "start" with current time and compute "stop"
	void	top(void);

	/** Check "stop" is overlapped
		@return
	*/
	bool	isStopOverlapped(void) const;

	/*! Sleep if current time is before "stop"
		@return true if effectively slept
	*/
	bool	sleep(void) const;

	inline
	const Nanotime&	getStart(void) const
	{	return this->start;		}

	inline
	const Nanotime&	getStop(void) const
	{	return this->stop;		}

	inline
	const Nanotime&	getDelay(void) const
	{	return this->delay;		}

	inline
	Nanotime&		getDelay(void)
	{	return this->delay;		}

	//! Set delay
	inline
	void	setDelay(time_t seconds, long nanos)
	{	this->delay.set(seconds, nanos);	}

	//! Set delay
	inline
	void	setDelay(const Nanotime &delay)
	{	this->delay = delay;	}

};

} // N..

#endif
