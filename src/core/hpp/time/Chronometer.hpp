/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHRONOMETER_HPP_INCLUDED
#define	LIB_PCO_CHRONOMETER_HPP_INCLUDED

#include "time/Nanotime.hpp"

namespace NPCO {

/**
	Chronometer
*/
class Chronometer {

protected:
	//! Start time
	Nanotime	begin;
	//! Stop time
	Nanotime	end;

public:
	Chronometer(void) = default;
	~Chronometer(void) = default;

	// Copy
	Chronometer(const Chronometer& ) = default;
	Chronometer& operator=(const Chronometer& ) = default;

	// Move
	Chronometer(Chronometer&& ) = default;
	Chronometer& operator=(Chronometer&& ) = default;

	//! Set "start" with current time
	void		start(void);

	//! Set "stop" with current time
	void		stop(void);

	//! Get delay between begin and end
	Nanotime	elapsed(void) const;

	//! Get delay between begin and "now"
	Nanotime	liveElapsed(void) const;

/*
	inline const Nanotime&	getStart(void) const
	{	return this->start;		}

	inline const Nanotime&	getStop(void) const
	{	return this->stop;		}
*/

};

} // N..

#endif
