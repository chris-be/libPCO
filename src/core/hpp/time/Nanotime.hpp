/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_NANOTIME_HPP_INCLUDED
#define	LIB_PCO_NANOTIME_HPP_INCLUDED

#include "pattern/cfr/C_SelfClassified.hpp"

#include <sys/time.h>	// timeval, timespec
//#include	<time.h>

namespace NPCO {

/*!
	Nanotime : handle time with nanoseconds precision
*/
class Nanotime : public C_SelfClassified<Nanotime> {

protected:
	timespec	time;

public:
	// No time
	Nanotime(void);
	Nanotime(time_t seconds, long nanos);

	// Copy
	Nanotime(const Nanotime& toCopy) = default;
	Nanotime& operator=(const Nanotime& toCopy) = default;

	// Move
	Nanotime(Nanotime&& toMove) = default;
	Nanotime& operator=(Nanotime&& toMove) = default;

	//! Set with actual time
	void	setNow(void);

	void	set(time_t seconds, long nanos);

	Nanotime& operator+=(const Nanotime& toAdd);
	Nanotime& operator-=(const Nanotime& toSub);

	Nanotime& operator*=(unsigned int factor);
	Nanotime& operator/=(unsigned int factor);

	//! @see C_SelfClassified
	int	 classify(const Nanotime& toCompare) const;

	/**

	*/
	inline
	const timespec&	getTime(void) const
	{	return this->time;			}

	inline
	time_t	getSeconds(void) const
	{	return this->time.tv_sec;	}

	inline
	void		setSeconds(time_t seconds)
	{	this->time.tv_sec = seconds;	}

	inline
	long		getNanos(void) const
	{	return this->time.tv_nsec;		}

	inline
	void		setNanos(long nanos)
	{	this->time.tv_nsec = nanos;		}

};

Nanotime	operator+(const Nanotime& left, const Nanotime& right);
Nanotime	operator-(const Nanotime& left, const Nanotime& right);

} // N..

#endif
