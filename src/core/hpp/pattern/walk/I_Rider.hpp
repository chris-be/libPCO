/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_RIDER_HPP_INCLUDED
#define	LIB_PCO_I_RIDER_HPP_INCLUDED

#include "I_Walker.hpp"

namespace NPCO {

/*!
	Contract for Rider: IWalker with "write allowed".
	@see IWalker
*/
template<class PCell> class IRider
	: virtual public ::NPCO::IWalker<PCell> {

};

} // N..

#endif
