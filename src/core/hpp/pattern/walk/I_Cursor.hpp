/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_CURSOR_HPP_INCLUDED
#define	LIB_PCO_I_CURSOR_HPP_INCLUDED

#include "I_Walker.hpp"

namespace NPCO {

/*!
	Contract for Cursor: "read only" Walker
	@see IWalker
*/
template<class PCell> class ICursor
	: virtual public ::NPCO::IWalker<const PCell> {

};

} // N..

#endif
