/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_CURSOR_HPP_INCLUDED
#define	LIB_PCO_C_CURSOR_HPP_INCLUDED

#include "C_Walker.hpp"

namespace NPCO {

/*!
	Contract for Cursor: "read only" Walker

	@see C_Walker
*/
template<class PCd, class PCell>
class C_Cursor : private C_Walker<PCd, const PCell> {

private:
	static void constraints([[maybe_unused]] PCd* ptr)
	{
		//const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// @see forward
		ctd = ctd.operator++();

		// @see back
		ctd = ctd.operator--();
	}

	static void	_ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

public:
	static void	ensure(void)
	{
		C_Walker<C_Cursor<PCd, PCell>, const PCell>::ensure();
		C_Cursor::_ensure();
	}

	C_Cursor(void)
	{
		C_Cursor::_ensure();
	}

};

} // N..

#endif
