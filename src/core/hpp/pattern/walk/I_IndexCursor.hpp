/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_INDEX_CURSOR_HPP_INCLUDED
#define	LIB_PCO_I_INDEX_CURSOR_HPP_INCLUDED

#include "I_IndexWalker.hpp"

namespace NPCO {

/*!
	Contract for IndexCursor: same as ICursor with index accessor (Key)
	@see IIndexWalker
*/
template<class PKey, class PValue> class IIndexCursor
	: virtual public ::NPCO::IIndexWalker<PKey, const PValue> {

};

} // N..

#endif
