/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_INDEX_WALKER_HPP_INCLUDED
#define	LIB_PCO_I_INDEX_WALKER_HPP_INCLUDED

#include "I_Walker.hpp"

namespace NPCO {

/*!
	Contract for IndexWalker: same as IWalker with index accessor (Key)
	@see IWalker
*/
template<class PKey, class PValue> class IIndexWalker
	: virtual public ::NPCO::IWalker<PValue> {

public:
	//! Go to key
	// virtual bool	toKey(const PKey& key) = 0;

	//! Access to current key
	virtual const PKey&	key(void) const = 0;

};

} // N..

#endif
