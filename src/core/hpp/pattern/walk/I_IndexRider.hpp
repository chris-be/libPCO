/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_INDEX_RIDER_HPP_INCLUDED
#define	LIB_PCO_I_INDEX_RIDER_HPP_INCLUDED

#include "I_IndexWalker.hpp"

namespace NPCO {

/*!
	Contract for IndexRider: same as IRider with index accessor (Key)
	@see IIndexWalker
*/
template<class PKey, class PValue> class IIndexRider
	: virtual public ::NPCO::IIndexWalker<PKey, PValue> {

};

} // N..

#endif
