/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_WALKER_HPP_INCLUDED
#define	LIB_PCO_I_WALKER_HPP_INCLUDED

#include "C_Walker.hpp"

namespace NPCO {

/*!
	Contract for Walker: move across a range of objects.
	Base for Cursor (read only walker) and Rider (write walker)

	@param PCell Cell type
*/
template<class PCell>
class IWalker : private C_Walker<IWalker<PCell>, PCell> {

public:
	using TCell		= typename std::remove_const<PCell>::type;

public:
	IWalker(void) = default;
	virtual ~IWalker(void) = default;

	// Copy - "custom" destructor delete default copy constructor
	IWalker(const IWalker& toCopy) = default;
	IWalker& operator=(const IWalker& toCopy) = default;

	// Move
	IWalker(IWalker&& toMove) = default;
	IWalker& operator=(IWalker&& toMove) = default;

	//! Check if range is valid (first and last are valid)
	virtual bool	checkRange(void) const = 0;

	//! Go to first element (if exists)
	virtual void	toFirst(void) = 0;

	//! Go to last element (if exists)
	virtual void	toLast(void) = 0;

	//! Move walker forward
	virtual void	forward(void) = 0;

	//! Move walker back
	virtual void	back(void) = 0;

	//! Equivalent to "step" calls of forward
	virtual void	operator+=(std::size_t step);

	//! Equivalent to "step" calls of back
	virtual void	operator-=(std::size_t step);

	//! Return if current position is valid - can be read/written
	virtual bool	ok(void) const = 0;

	//! Access to current element
	virtual PCell&	operator*(void) const = 0;

	//! Access to current element
	virtual PCell*	operator->(void) const = 0;

	//! Reduce range to [ first -> current ]
	virtual void	limitRangeFirst(void) = 0;
	//! Reduce range to [ current -> last ]
	virtual void	limitRangeLast(void) = 0;

	//! @see operator* @todo rename value ? for IndexWalker.key()
	PCO_INLINE
	PCell&	cell(void) const;

};

} // N..

#include "imp/IWalker.cppi"

#endif
