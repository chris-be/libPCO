/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_WALKER_HPP_INCLUDED
#define	LIB_PCO_C_WALKER_HPP_INCLUDED

// Lot of chance that it will be needed
#include "PCO_ForceInline.hpp"

#include <cstddef>	// size_t
#include <type_traits>

namespace NPCO {

/*!
	Constraint for Walker: move across a range of objects.
	Base for Cursor (read only walker) and Rider (write walker)

	@param PCd Constrained class
	@param PCell Cell type
*/
template<class PCd, class PCell>
class C_Walker {

private:
	static void constraints([[maybe_unused]] PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// TCell defined (without const)
		[[maybe_unused]]	typename PCd::TCell		*has_TCell;
		static_assert(std::is_same<typename PCd::TCell, typename std::remove_const<PCell>::type>::value, "");

		// Check if range is valid (first and last are valid)
		[[maybe_unused]]	bool t;
		t = const_ctd.checkRange();

		// Go to first element (if exists)
		ctd.toFirst();

		// Go to last element (if exists)
		ctd.toLast();

		// Move walker forward
		ctd.forward();

		// Move walker back
		ctd.back();

		std::size_t step = 10;
		// Equivalent to "step" calls of forward
		ctd.operator+=(step);

		// Equivalent to "step" calls of back
		ctd.operator-=(step);

		// Return if current position is valid - can be read/written
		t = const_ctd.ok();

		// Access to current element
		[[maybe_unused]]	PCell&	cell = const_ctd.operator*();

		// Access to current element
		[[maybe_unused]]	PCell*	pCell = const_ctd.operator->();

		// Reduce range to [ first -> current ]
		ctd.limitRangeFirst();
		// Reduce range to [ current -> last ]
		ctd.limitRangeLast();

		// @see operator* @todo rename value ? for IndexWalker.key()
		[[maybe_unused]]	PCell&	cell_alias = const_ctd.cell();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_Walker(void)
	{
		C_Walker::ensure();
	}

};



} // N..

#endif
