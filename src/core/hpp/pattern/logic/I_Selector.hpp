/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_I_SELECTOR_HPP_INCLUDED
#define LIB_PCO_I_SELECTOR_HPP_INCLUDED

namespace NPCO {

/*!
	Give rules for selecting objects
	@param POn Type of object tested/selected
*/
template<class POn>
class ISelector {
public:

	/*!
		FunctionPointer
		@return
	*/
	typedef bool (*SelFunc)(const POn&);

public:
	virtual ~ISelector(void) = default;

	/*!
		Check if object is selected
	*/
	virtual bool select(const POn& o) const = 0;

};

} // N..

#endif
