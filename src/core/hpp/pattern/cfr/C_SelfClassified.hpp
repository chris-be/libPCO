/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_SELF_CLASSIFIED_HPP_INCLUDED
#define	LIB_PCO_C_SELF_CLASSIFIED_HPP_INCLUDED

#include "ClassifyValue.hpp"

namespace NPCO {

/**
	Constraint "self classified"
	@param PCd
*/
template<class PCd> class C_SelfClassified {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd = *ptr;

		[[maybe_unused]]	ClassifyValue cv;
		// Find classification (=0: same, <0: before, >0: after)
		cv = const_ctd.classify(const_ctd);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_SelfClassified(void)
	{
		C_SelfClassified::ensure();
	}

};

} // N..

#endif
