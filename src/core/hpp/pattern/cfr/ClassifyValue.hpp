/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CLASSIFY_VALUE_HPP_INCLUDED
#define	LIB_PCO_CLASSIFY_VALUE_HPP_INCLUDED

#include "PCO_ForceInline.hpp"

namespace NPCO {

/**
	Type classify result.
*/
typedef signed int		ClassifyValue;

//! Returns if "classify returned value" means "same as"
PCO_INLINE
static bool	e_isSame[[maybe_unused]](const ClassifyValue delta) noexcept
{
	return delta == 0;
}

//! Returns if "classify returned value" means "different as"
PCO_INLINE
static bool	e_isDiff[[maybe_unused]](const ClassifyValue delta) noexcept
{
	return delta != 0;
}

//! Returns if "classify returned value" means "is before"
PCO_INLINE
static bool	e_isBefore[[maybe_unused]](const ClassifyValue delta) noexcept
{
	return delta < 0;
}

//! Returns if "classify returned value" means "is after"
PCO_INLINE
static bool	e_isAfter[[maybe_unused]](const ClassifyValue delta) noexcept
{
	return delta > 0;
}

} // N..

#endif
