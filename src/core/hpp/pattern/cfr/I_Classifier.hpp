/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_I_CLASSIFIER_HPP_INCLUDED
#define LIB_PCO_I_CLASSIFIER_HPP_INCLUDED

#include "pattern/cfr/C_Classifier.hpp"

namespace NPCO {

/*!
	Interface Classifier
	Give rules for classifying objects

	@param PClfd Classified object
*/
template<class PClfd>
class IClassifier : private C_Classifier<IClassifier<PClfd>, PClfd> {

public:
	typedef PClfd				TClassified;

public:
	virtual ~IClassifier(void) = default;

	//! @see C_Classifier
	virtual ClassifyValue	classify(const PClfd& o1, const PClfd& o2) const = 0;

};

} // N..

#endif
