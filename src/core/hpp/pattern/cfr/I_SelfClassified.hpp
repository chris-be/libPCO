/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_SELF_CLASSIFIED_HPP_INCLUDED
#define	LIB_PCO_I_SELF_CLASSIFIED_HPP_INCLUDED

#include "ClassifyValue.hpp"

namespace NPCO {

/**
	Class can declare "self classified"
	@param PSelf Class that can be compared to
*/
template<class PSelf>
class ISelfClassified {

public:
	virtual ~ISelfClassified(void) = default;

	virtual ClassifyValue	classify(const PSelf& toCompare) const = 0;

};

} // N..

#endif
