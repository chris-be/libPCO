/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_CLASSIFIER_HPP_INCLUDED
#define	LIB_PCO_C_CLASSIFIER_HPP_INCLUDED

#include "ClassifyValue.hpp"

namespace NPCO {

/**
	Constraint "classifier"

	@param PCd
	@param PClassified Object to class
*/
template<class PCd, class PClassified>
class C_Classifier {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd = *ptr;

		// Has TClassified
		[[maybe_unused]]	typename PCd::TClassified	*has_TClassified;

		// Pointers in case of "no constructor"
		PClassified *const left		= nullptr;
		PClassified *const right	= nullptr;

		[[maybe_unused]]	ClassifyValue cv;
		// Find classification (=0: same, <0: before, >0: after)
		cv = const_ctd.classify(*left, *right);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_Classifier(void)
	{
		C_Classifier::ensure();
	}

};

} // N..

#endif
