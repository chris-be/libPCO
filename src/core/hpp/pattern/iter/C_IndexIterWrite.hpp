/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_INDEX_ITER_WRITE_HPP_INCLUDED
#define	LIB_PCO_C_INDEX_ITER_WRITE_HPP_INCLUDED

#include "C_IndexIter.hpp"

namespace NPCO {

/*!
	Constraints for IndexIterWrite: "write allowed".
	@see IndexIter
*/
template<class PCd, class PKey, class PValue>
class C_IndexIterWrite : private C_IndexIter<PCd, PKey, PValue> {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// Ensure type of key is available
		[[maybe_unused]]	typename PCd::TKey*		pKey;
		// Ensure type of cell is available
		[[maybe_unused]]	typename PCd::TCell*	pCell;

		// Convenience
		ctd = ctd.operator++();
		ctd = ctd.operator--();

		[[maybe_unused]]	bool test;
		// Compare with it's type
		test = const_ctd.operator==(const_ctd);
		// Compare with it's type
		test = const_ctd.operator!=(const_ctd);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_IndexIterWrite(void)
	{
		C_IndexIterWrite::ensure();
	}

};

} // N..

#endif
