/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_HAS_ITER_READ_HPP_INCLUDED
#define	LIB_PCO_C_HAS_ITER_READ_HPP_INCLUDED

#include "C_IterRead.hpp"

namespace NPCO {

/*!
	Contract for IterRead providers (array, collection, ...)
	@see C_IterRead
*/
template<class PCd, class PCell>
class C_HasIterRead {

private:
	static void constraints(PCd *ptr)
	{
		const PCd&	const_ctd	= *ptr;

		// TIterRead defined and C_IterRead
		[[maybe_unused]]	typename PCd::TIterRead		*has_TIterRead;
		C_IterRead<typename PCd::TIterRead, PCell>::ensure();

		// Has first, last
		[[maybe_unused]]	auto it_r = const_ctd.first();
							it_r = const_ctd.last();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_HasIterRead(void)
	{
		C_HasIterRead::ensure();
	}

};

} // N..

#endif
