/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_ITER_WRITE_HPP_INCLUDED
#define	LIB_PCO_C_ITER_WRITE_HPP_INCLUDED

#include "C_Iter.hpp"

namespace NPCO {

/*!
	Constraints for IterWrite: "write allowed".
	@see C_Iter
*/
template<class PCd, class PCell>
class C_IterWrite : private C_Iter<PCd, PCell> {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// Ensure type of cell is available
		[[maybe_unused]]	typename PCd::TCell*	has_TCell;

		// Convenience
		ctd = ctd.operator++();
		ctd = ctd.operator--();

		[[maybe_unused]]	bool test;
		// Compare with it's type
		test = const_ctd.operator==(const_ctd);
		// Compare with it's type
		test = const_ctd.operator!=(const_ctd);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_IterWrite(void)
	{
		C_IterWrite::ensure();
	}

};

} // N..

#endif
