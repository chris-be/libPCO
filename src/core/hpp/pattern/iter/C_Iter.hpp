/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_ITER_HPP_INCLUDED
#define	LIB_PCO_C_ITER_HPP_INCLUDED

// Lot of chance that it will be needed
#include "PCO_ForceInline.hpp"

#include <type_traits>

namespace NPCO {

/*!
	Constraints for Iter: iterate through a range of objects.

	@param PCd Constrained class
	@param PCell Type of "pointed" value
*/
template<class PCd, class PCell>
class C_Iter {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// TCell defined (without const)
		[[maybe_unused]]	typename PCd::TCell*	has_TCell;
		static_assert(std::is_same<typename PCd::TCell, typename std::remove_const<PCell>::type>::value, "");

		// Make Iter invalid
		ctd.unset();

		// Check if current position is valid
		const_ctd.isValid();

		// Move Iter forward
		ctd.forward();

		// Move Iter back
		ctd.back();

		// Access to current element
		[[maybe_unused]]	PCell&	cellRef	= const_ctd.operator*();

		// Access to current element
		[[maybe_unused]]	PCell*	pCell	= const_ctd.operator->();

		// @see operator* @todo rename value ? for IndexIter.key()
		[[maybe_unused]]	PCell&	cellRef2= const_ctd.cell();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_Iter(void)
	{
		C_Iter::ensure();
	}

};

} // N..

#endif
