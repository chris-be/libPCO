/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_HAS_ITER_WRITE_HPP_INCLUDED
#define	LIB_PCO_C_HAS_ITER_WRITE_HPP_INCLUDED

#include "C_IterWrite.hpp"

namespace NPCO {

/*!
	Contract for IterWrite providers (array, collection, ...)
	@see C_IterWrite
*/
template<class PCd, class PCell>
class C_HasIterWrite {

private:
	static void constraints(PCd *ptr)
	{
		// const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// TIterWrite defined and C_IterWrite
		[[maybe_unused]]	typename PCd::TIterWrite		*has_TIterWrite;
		C_IterWrite<typename PCd::TIterWrite, PCell>::ensure();

		// Has first, last
		[[maybe_unused]]	auto it_w = ctd.first();
							it_w = ctd.last();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_HasIterWrite(void)
	{
		C_HasIterWrite::ensure();
	}

};

} // N..

#endif
