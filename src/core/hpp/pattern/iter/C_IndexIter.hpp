/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_INDEX_ITER_HPP_INCLUDED
#define	LIB_PCO_C_INDEX_ITER_HPP_INCLUDED

#include "C_Iter.hpp"

namespace NPCO {

/*!
	Constraints for IndexIter: iterate through a range of objects associated with keys.
	@see C_Iter
*/
template<class PCd, class PKey, class PValue> class C_IndexIter
	: private C_Iter<PCd, PValue> {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		//      PCd&	ctd			= *ptr;

		// Ensure type of key is available
		[[maybe_unused]]	typename PCd::TKey*	pKey;

		// Access to current key
		[[maybe_unused]]	const PKey&	keyRef = const_ctd.key();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_IndexIter(void)
	{
		C_IndexIter::ensure();
	}

};

} // N..

#endif
