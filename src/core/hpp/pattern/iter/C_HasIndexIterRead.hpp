/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_HAS_INDEX_ITER_READ_HPP_INCLUDED
#define	LIB_PCO_C_HAS_INDEX_ITER_READ_HPP_INCLUDED

#include "C_IndexIterRead.hpp"

namespace NPCO {

/*!
	Contract for IterRead providers (array, collection, ...)
	@see C_IndexIterRead
*/
template<class PCd, class PKey, class PValue>
class C_HasIndexIterRead {

private:
	static void constraints(PCd *ptr)
	{
		const PCd&	const_ctd	= *ptr;

		// TIterRead defined and C_IndexIterRead
		[[maybe_unused]]	typename PCd::TIterRead			*has_TIndexIterRead;
		C_IndexIterRead<typename PCd::TIterRead, PKey, PValue>::ensure();

		// Has first, last
		[[maybe_unused]]	auto it_r = const_ctd.first();
							it_r = const_ctd.last();

		// iter_r(key)
		[[maybe_unused]]	const PKey& key = it_r.key();
		it_r = const_ctd.iter_r(key);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_HasIndexIterRead(void)
	{
		C_HasIndexIterRead::ensure();
	}

};

} // N..

#endif
