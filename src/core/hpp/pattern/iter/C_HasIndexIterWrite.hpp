/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_HAS_INDEX_ITER_WRITE_HPP_INCLUDED
#define	LIB_PCO_C_HAS_INDEX_ITER_WRITE_HPP_INCLUDED

#include "C_IndexIterWrite.hpp"

namespace NPCO {

/*!
	Contract for IndexIterWrite providers (array, collection, ...)
	@see C_IndexIterWrite
*/
template<class PCd, class PKey, class PValue>
class C_HasIndexIterWrite {

private:
	static void constraints(PCd *ptr)
	{
		// const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// TIterWrite defined and C_IndexIterWrite
		[[maybe_unused]]	typename PCd::TIterWrite		*has_TIndexIterWrite;
		C_IndexIterWrite<typename PCd::TIterWrite, PKey, PValue>::ensure();

		// Has first, last
		[[maybe_unused]]	auto it_w = ctd.first();
							it_w = ctd.last();

		// Has iter_w(key)
		[[maybe_unused]]	const PKey& key = it_w.key();
		it_w = ctd.iter_w(key);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_HasIndexIterWrite(void)
	{
		C_HasIndexIterWrite::ensure();
	}

};

} // N..

#endif
