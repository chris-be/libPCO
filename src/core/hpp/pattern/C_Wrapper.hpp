/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_WRAPPER_HPP_INCLUDED
#define	LIB_PCO_C_WRAPPER_HPP_INCLUDED

// #include <type_traits>

namespace NPCO {

/*!
	Constraint "wrapper": object that wrap another one and let it know

	@param PCd Constrained class
	@param PWrapped
*/
template<class PCd, class PWrapped>
class C_Wrapper {

private:
	static void constraints(PCd* ptr)
	{
		const	PCd&	const_ctd	= *ptr;
				PCd&	ctd			= *ptr;

		// TWrapped defined
		[[maybe_unused]]	typename PCd::TWrapped	*hasTWrapped;

		// Has "getWrap" const
		[[maybe_unused]]	const PWrapped&	ro_w = const_ctd.getWrapped();

		// Has "getWrap"
		[[maybe_unused]]	PWrapped&		rw_w = ctd.getWrapped();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_Wrapper(void)
	{
		C_Wrapper::ensure();
	}

};

// C_Wrapper specific
template<typename PWrapper, typename PWrapped>
struct is_C_Wrapper;

template<typename PWrapper, typename PWrapped = typename PWrapper::TWrapped>
struct is_C_Wrapper : public std::true_type	{ };

} // N..

#endif
