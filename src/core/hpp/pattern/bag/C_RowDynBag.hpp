/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_ROW_DYNAMIC_BAG_HPP_INCLUDED
#define	LIB_PCO_C_ROW_DYNAMIC_BAG_HPP_INCLUDED

#include "C_DynBag.hpp"
#include "C_RowBag.hpp"

namespace NPCO {

/**
	Constraint for "indexed" collections

	- same as DynBag
	- items have an index
*/
template<class PCd, class PType, class PSize> class C_RowDynBag
	: private C_DynBag<PCd, PType, PSize>
	, private C_RowBag<PCd, PType, PSize> {

private:

public:
	static void	ensure(void)
	{
		C_DynBag<PCd, PType, PSize>::ensure();
		C_RowBag<PCd, PType, PSize>::ensure();
	}

};

} // N..

#endif
