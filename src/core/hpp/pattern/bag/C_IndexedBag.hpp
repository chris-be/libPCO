/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_INDEXED_BAG_HPP_INCLUDED
#define	LIB_PCO_C_INDEXED_BAG_HPP_INCLUDED

#include "pattern/C_HasSize.hpp"
#include "pattern/iter/C_HasIndexIterRead.hpp"
#include "pattern/iter/C_HasIndexIterWrite.hpp"

#include <utility> // std::move

namespace NPCO {

/**
	Constraint for indexed bags: store values with associated keys

	- can get size
	- can walk trough (only for reading)

	@param PCd Constrained class
	@param PKey Type of key
	@param PValue Type of value
	@param PSize Type of size
*/
template<class PCd, class PKey, class PValue, class PSize> class C_IndexedBag
	: private C_HasSize<PCd, PSize>
	, private C_HasIndexIterRead<PCd, PKey, PValue>
	, private C_HasIndexIterWrite<PCd, PKey, PValue> {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// TKey defined
		[[maybe_unused]]	typename PCd::TKey		*has_TKey;
		// TValue defined
		[[maybe_unused]]	typename PCd::TValue	*has_TValue;

		[[maybe_unused]]	PKey *k = nullptr;
		[[maybe_unused]]	const PKey& key = *k;
		[[maybe_unused]]	PValue *v = nullptr;

		// iterators for some functions...
		[[maybe_unused]]	auto it_r = const_ctd.iter_r(key);
		[[maybe_unused]]	auto it_w = ctd.iter_w(key);

		// Clear bag - removes all
		ctd.clear();

		// Add (or replace) a copy
		ctd.add(key, *v);

		// Add an item (move)
		ctd.moveIn(key, std::move(*v) );

		// Add node with undefined value
		it_w = ctd.add(key);

		// Check if key is present
		[[maybe_unused]]	bool test;
		test = const_ctd.hasKey(key);

		//! Remove key and associated value(s)
		ctd.remove(key);

		// todo activate
		// it_r = const_ctd.find(key); nearest..
		// ctd.remove(it_r);
		// ctd.moveOut(it_r, el);

		// ctd.remove(it_r, it_r);
		// it_w = ctd.find(key); nearest...
	}

	static void	_ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

public:
	static void	ensure(void)
	{
		C_HasSize<PCd, PSize>::ensure();
		C_HasIndexIterRead<PCd, PKey, PValue>::ensure();
		C_HasIndexIterWrite<PCd, PKey, PValue>::ensure();
		_ensure();
	}

	C_IndexedBag(void)
	{
		C_IndexedBag::_ensure();
	}

};

} // N..

#endif
