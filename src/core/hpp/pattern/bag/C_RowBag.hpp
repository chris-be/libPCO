/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_ROW_BAG_HPP_INCLUDED
#define	LIB_PCO_C_ROW_BAG_HPP_INCLUDED

#include "C_Bag.hpp"

namespace NPCO {

/**
	Constraint for bags indexing items by an order.

	- same as Bag
	- items have an index
*/
template<class PCd, class PType, class PSize> class C_RowBag
	: private C_Bag<PCd, PType, PSize> {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		[[maybe_unused]]	PSize	i = const_ctd.size()-1;
		// operator[]
		[[maybe_unused]]	const	PType&	i1 = const_ctd.operator[](i);
		[[maybe_unused]]			PType&	i2 = ctd.operator[](i);

		// Has iter_r(i)
		auto it_r	= const_ctd.iter_r(i);
		C_IterRead<decltype(it_r), PType>::ensure();
		// Has iter_w(i)
		auto it_w	= ctd.iter_w(i);
		C_IterWrite<decltype(it_w), PType>::ensure();
	}

	static void _ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

public:
	static void	ensure(void)
	{
		C_Bag<PCd, PType, PSize>::ensure();
		_ensure();
	}

	C_RowBag(void)
	{
		C_RowBag::_ensure();
	}

};

} // N..

#endif
