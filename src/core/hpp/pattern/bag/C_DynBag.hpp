/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_DYNAMIC_BAG_HPP_INCLUDED
#define	LIB_PCO_C_DYNAMIC_BAG_HPP_INCLUDED

#include "./C_Bag.hpp"
#include <utility>	// std::move

namespace NPCO {

/**
	Constraint for "dynamic" collections

	- same as Bag
	- can add/remove items
*/
template<class PCd, class PType, class PSize> class C_DynBag
	: public C_Bag<PCd, PType, PSize> {

private:
	static void constraints(PCd* ptr)
	{
		//const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		//! Clear bag - removes all
		ctd.clear();

		//! Add a copy - always at end for non sorted collection
		[[maybe_unused]]	PType *el = nullptr;	// Wunused-parameter
		ctd.add(*el);

		//! Add an item (move)
		ctd.moveIn( std::move(*el) );

		// todo activate
		[[maybe_unused]]	auto it_w = ctd.last();
		//ctd.remove(it_w);
		// ctd.moveOut(it_w, *el);

		// ctd.remove(it_w, it_w);
	}

	static void	_ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

public:
	static void	ensure(void)
	{
		C_Bag<PCd, PType, PSize>::ensure();
		_ensure();
	}

	C_DynBag(void)
	{
		C_DynBag::_ensure();
	}

};

} // N..

#endif
