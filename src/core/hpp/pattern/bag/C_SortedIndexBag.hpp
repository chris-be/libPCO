/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_SORTED_INDEX_BAG_HPP_INCLUDED
#define	LIB_PCO_C_SORTED_INDEX_BAG_HPP_INCLUDED

#include "pattern/bag/C_IndexedBag.hpp"

namespace NPCO {

/**
	Constraint for indexed bags with sorted keys.

	- can get size
	- can walk trough (only for reading)

	@param PCd Constrained class
	@param PKey Type of key
	@param PValue Type of value
	@param PSize Type of size
*/
template<class PCd, class PKey, class PValue, class PSize>
class C_SortedIndexBag : private C_IndexedBag<PCd, PKey, PValue, PSize> {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		[[maybe_unused]]	PKey *k = nullptr;
		[[maybe_unused]]	const PKey& key = *k;

		// need iterators for some functions...
		[[maybe_unused]]	auto it_r = const_ctd.iter_r(key);
		[[maybe_unused]]	auto it_w = ctd.iter_w(key);

		// Find key or first "greater value"
		it_r = const_ctd.iter_r_min(key);
		// Find key or first "lesser value"
		it_r = const_ctd.iter_r_max(key);

	}

	static void	_ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

public:
	static void	ensure(void)
	{
		C_IndexedBag<PCd, PKey, PValue, PSize>::ensure();
		_ensure();
	}

	C_SortedIndexBag(void)
	{
		C_SortedIndexBag::_ensure();
	}

};

} // N..

#endif
