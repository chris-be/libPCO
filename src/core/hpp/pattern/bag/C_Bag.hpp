/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_BAG_HPP_INCLUDED
#define	LIB_PCO_C_BAG_HPP_INCLUDED

#include "pattern/C_HasSize.hpp"
#include "pattern/iter/C_HasIterRead.hpp"
#include "pattern/iter/C_HasIterWrite.hpp"

namespace NPCO {

/**
	Minimal contract for "static" collections

	- can get size
	- can walk trough
*/
template<class PCd, class PType, class PSize> class C_Bag
	: private C_HasSize<PCd, PSize>
	, private C_HasIterRead<PCd, PType>
	, private C_HasIterWrite<PCd, PType> {

private:
	static void constraints([[maybe_unused]] PCd* ptr)
	{
		// const PCd&	const_ctd	= *ptr;
		//      PCd&	ctd			= *ptr;

		// TType defined
		[[maybe_unused]]	typename PCd::TType		*has_TType;
	}

	static void _ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

public:
	static void	ensure(void)
	{
		_ensure();
		C_HasSize<PCd, PSize>::ensure();
		C_HasIterRead<PCd, PType>::ensure();
		C_HasIterWrite<PCd, PType>::ensure();
	}

	C_Bag(void)
	{
		C_Bag::ensure();
	}

};

} // N..

#endif
