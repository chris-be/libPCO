/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_KEY_BAG_HPP_INCLUDED
#define	LIB_PCO_C_KEY_BAG_HPP_INCLUDED

#include "pattern/C_HasSize.hpp"
#include "pattern/iter/C_HasIterRead.hpp"

#include <utility> // std::move

namespace NPCO {

/**
	Constraint for key bags: store keys

	- can get size
	- can walk trough (only for reading)
*/
template<class PCd, class PKey, class PSize> class C_KeyBag
	: public C_HasSize<PCd, PSize>
	, public C_HasIterRead<PCd, PKey> {

public:
	typedef PKey				TKey;

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		[[maybe_unused]]	PKey *k = nullptr;
		[[maybe_unused]]	const PKey	key = *k;

		// need iterator for some functions...
		[[maybe_unused]]	auto it_r = const_ctd.iter_r(key);

		// Clear bag - removes all
		ctd.clear();

		// Add (or replace) a copy
		ctd.add(key);

		// Add an item (move)
		ctd.moveIn( std::move(*k) );

		// Check if key is present
		[[maybe_unused]]	bool test;
		test = const_ctd.hasKey(key);

		//! Remove
		ctd.remove(key);

		// todo activate
		it_r = const_ctd.last();
		// ctd.remove(it_r);
		// ctd.moveOut(it_r, el);

		// ctd.remove(it_r, it_r);
	}

	static void _ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

public:
	static void	ensure(void)
	{
		C_HasSize<PCd, PSize>::ensure();
		C_HasIterRead<PCd, PKey>::ensure();
		_ensure();
	}

	C_KeyBag(void)
	{
		C_KeyBag::_ensure();
	}

};

} // N..

#endif
