/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_OUTFLOW_HPP_INCLUDED
#define	LIB_PCO_I_OUTFLOW_HPP_INCLUDED

namespace NPCO {

/*!
	Contract for Outflow

	@param PCell Type written one by one
*/
template<class PCell> class IOutflow {

public:
	typedef PCell			TCell;

public:
	virtual ~IOutflow(void)	{ }

	//! Write new element
	virtual void	write(const PCell& element) = 0;

};

} // N..

#endif
