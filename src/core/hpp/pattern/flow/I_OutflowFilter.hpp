/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_OUTFLOW_FILTER_HPP_INCLUDED
#define	LIB_PCO_I_OUTFLOW_FILTER_HPP_INCLUDED

#include "I_Outflow.hpp"

namespace NPCO {

/*!
	Contract for OutflowFilter

	@param POut Type of cell written
	@param PIn Type of cell given
*/
template<class POut, class PIn>
class IOutflowFilter : public IOutflow<PIn> {

public:
	IOutflowFilter(void) = default;
	virtual ~IOutflowFilter(void) = default;

	// Copy
	IOutflowFilter(const IOutflowFilter& ) = default;
	IOutflowFilter&	operator=(const IOutflowFilter& ) = default;

	// Move
	IOutflowFilter(IOutflowFilter&& ) = default;
	IOutflowFilter&	operator=(IOutflowFilter&& ) = default;

	virtual void	write(const PIn& element) override = 0;

};

} // N..

#endif
