/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_INFLOW_HPP_INCLUDED
#define	LIB_PCO_I_INFLOW_HPP_INCLUDED

namespace NPCO {

/*!
	Contract for Inflow

	@param PCell Type read one by one
*/
template<class PCell> class IInflow {

public:
	typedef PCell			TCell;

public:
	//! Ensure destructor
	virtual ~IInflow(void) = default;

	/** Read next element - blocking !
		@return False when nothing was read (end of flow)
	*/
	virtual bool	read(PCell& element) = 0;

};

} // N..

#endif
