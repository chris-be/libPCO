/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_INFLOW_FILTER_HPP_INCLUDED
#define	LIB_PCO_I_INFLOW_FILTER_HPP_INCLUDED

#include "I_Inflow.hpp"

namespace NPCO {

/*!
	Contract for InflowFilter

	@param POut Type of cell returned
	@param PIn Type of cell read
*/
template<class POut, class PIn>
class IInflowFilter	: public IInflow<POut> {

public:
	IInflowFilter(void) = default;
	virtual ~IInflowFilter(void) = default;

	// Copy
	IInflowFilter(const IInflowFilter& ) = default;
	IInflowFilter&	operator=(const IInflowFilter& ) = default;

	// Move
	IInflowFilter(IInflowFilter&& ) = default;
	IInflowFilter&	operator=(IInflowFilter&& ) = default;

	//! @see IInflow
	virtual bool	read(POut& element) override = 0;

};

} // N..

#endif
