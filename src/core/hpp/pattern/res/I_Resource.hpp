/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_RESOURCE_HPP_INCLUDED
#define	LIB_PCO_I_RESOURCE_HPP_INCLUDED

namespace NPCO {

/*!
 	Interface to handle "external resources" in a common manner.
*/
class IResource {

public:
	virtual ~IResource(void);

	//! Check if resource is opened
	virtual bool isOpened(void) const noexcept = 0;

	//! Close/release resource
	virtual void close(void) noexcept = 0;

};

} // N..

#endif
