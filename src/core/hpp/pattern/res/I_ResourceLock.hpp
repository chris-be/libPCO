/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_RESOURCE_LOCK_HPP_INCLUDED
#define	LIB_PCO_I_RESOURCE_LOCK_HPP_INCLUDED

namespace NPCO {

/*!
	Contract for class handling "resource" that can be locked
*/
class IResourceLock {

public:
	//! No need to unlock if deleted (done by child class)
	virtual ~IResourceLock(void) = default;

	//! Check if resource is locked
	virtual bool isLocked() const noexcept = 0;

	//! Lock resource
	virtual void lock(void) = 0;

	/*!
		Unlock resource
		WARNING ! No exception should be thrown if used with AutoLock.
	*/
	virtual void unlock(void) noexcept = 0;

};

} // N..

#endif
