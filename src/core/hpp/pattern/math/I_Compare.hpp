/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_COMPARE_HPP_INCLUDED
#define	LIB_PCO_I_COMPARE_HPP_INCLUDED

namespace NPCO {

/*!
	Contract for comparing objects.
	Give rules for comparing mathematical objects
*/
template<class PNb> class ICompare {

public:
	virtual bool	operator==(const PNb& toCompare) const = 0;

	virtual bool	operator!=(const PNb& toCompare) const = 0;

	virtual bool	operator<(const PNb& toCompare) const = 0;

	virtual bool	operator>(const PNb& toCompare) const = 0;

	virtual bool	operator<=(const PNb& toCompare) const = 0;

	virtual bool	operator>=(const PNb& toCompare) const = 0;

/*
	///	For convenience
	inline	friend bool operator==(const PNb& o1, const PNb& o2)
	{
		return o1 == o2;
	}

	///	For convenience
	inline	friend bool operator!=(const PNb& o1, const PNb& o2)
	{
		return o1 != o2;
	}

	///	For convenience
	inline	friend bool operator<(const PNb& o1, const PNb& o2)
	{
		return o1 < o2;
	}

	///	For convenience
	inline	friend bool operator>(const PNb& o1, const PNb& o2)
	{
		return o1 > o2;
	}

	///	For convenience
	inline	friend bool operator<=(const PNb& o1, const PNb& o2)
	{
		return o1 <= o2;
	}

	///	For convenience
	inline	friend bool operator>=(const PNb& o1, const PNb& o2)
	{
		return o1 >= o2;
	}
*/
};

} // N..

#endif
