/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_NUMBER_HPP_INCLUDED
#define	LIB_PCO_C_NUMBER_HPP_INCLUDED

namespace NPCO {

/**
	Contract for number.
*/
template<class PCd>
class C_Number {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd = *ptr;
		      PCd&	      ctd = *ptr;

		// Set
		ctd = const_ctd;

		// Negate operator-(void)
		ctd = -const_ctd;

		// & operator+=(const &)
		ctd+= const_ctd;
		// & operator-=(const &)
		ctd-= const_ctd;

		// operator+(const &) const
		ctd = const_ctd + const_ctd;
		// operator-(const &) const
		ctd = const_ctd - const_ctd;

		// Increment
		++ctd;
		// Decrement
		--ctd;
		// & operator*=(const &)
		ctd*= const_ctd;
		// & operator/=(const &)
		ctd*= const_ctd;

		// Compare
		[[maybe_unused]]	bool test;
		test = (const_ctd == const_ctd);
		test = (const_ctd != const_ctd);

		test = (const_ctd < const_ctd);
		test = (const_ctd > const_ctd);

		test = (const_ctd <= const_ctd);
		test = (const_ctd >= const_ctd);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_Number(void)
	{
		C_Number::ensure();
	}

};

} // N..

#endif
