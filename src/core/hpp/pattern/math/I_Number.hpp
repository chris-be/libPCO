/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_INUMBER_HPP_INCLUDED
#define	LIB_PCO_INUMBER_HPP_INCLUDED

#include "SysDebug.hpp"

namespace NPCO {

/*!

	Interface to handle numbers

*/
class INumber {

public:
	//! Ensure destructor
	virtual ~INumber(void)	{ }

/*
a + b => c same type ! (double, int, ...)

	ArithmeticVector<T,n>	&operator =(const ArithmeticVector<T,n> &src);		// =
	ArithmeticVector<T,n>	&operator+=(const ArithmeticVector<T,n> &toAdd);	// +
	ArithmeticVector<T,n>	&operator-=(const ArithmeticVector<T,n> &toSub);	// -

	ArithmeticVector<T,n>	operator+(const ArithmeticVector<T,n> &toAdd) const;	// +
	ArithmeticVector<T,n>	operator-(const ArithmeticVector<T,n> &toSub) const;	// -

	ArithmeticVector<T,n>	&operator*=(const T toMul);	// *
	ArithmeticVector<T,n>	&operator/=(const T toDiv);	// /

	bool			operator==(const ArithmeticVector<T,n> &toTest) const;	// ==
	bool			operator!=(const ArithmeticVector<T,n> &toTest) const;	// !=

*/
};

} // N..

#endif
