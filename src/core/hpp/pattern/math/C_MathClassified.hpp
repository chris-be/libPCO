/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_MATH_CLASSIFIED_HPP_INCLUDED
#define	LIB_PCO_C_MATH_CLASSIFIED_HPP_INCLUDED

namespace NPCO {

/**
	Constraint for mathematical comparisons.
	@param PCd Constrained class
*/
template<class PCd> class C_MathClassified {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd = *ptr;

		[[maybe_unused]]	bool	test;
		// operator==
		test = const_ctd.operator==(const_ctd);
		// operator!=
		test = const_ctd.operator!=(const_ctd);

		// operator<
		test = const_ctd.operator<(const_ctd);
		// operator>
		test = const_ctd.operator>(const_ctd);

		// operator<=
		test = const_ctd.operator<=(const_ctd);
		// operator>=
		test = const_ctd.operator>=(const_ctd);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_MathClassified(void)
	{
		C_MathClassified::ensure();
	}

};

} // N..

#endif
