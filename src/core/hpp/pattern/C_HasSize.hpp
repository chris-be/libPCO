/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_HAS_SIZE_HPP_INCLUDED
#define	LIB_PCO_C_HAS_SIZE_HPP_INCLUDED

#include "PCO_ForceInline.hpp"

namespace NPCO {

/*!
	Constraint "has size"
	All objects that contains something are "sized"

	@param PCd Constrained class
	@param PSize Type for size number
*/
template<class PCd, class PSize>
class C_HasSize {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd = *ptr;

		// TSize defined
		[[maybe_unused]]	typename PCd::TSize	*hasTSize;

		// Has size
		[[maybe_unused]]	PSize s = const_ctd.size();

		// Has isEmpty
		[[maybe_unused]]	bool test = const_ctd.isEmpty();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_HasSize(void)
	{
		C_HasSize::ensure();
	}

};

//! Returns if size is valid
template<class PSize>
PCO_INLINE
static bool	e_isSizeValid(const PSize s) noexcept
{
	return s >= 0;
}

//! Returns if index if valid - do not check is validSize !
template<class PSize>
PCO_INLINE
static bool	e_isInBounds(const PSize i, const PSize size) noexcept
{
	return (i < size);
}

} // N..

#endif
