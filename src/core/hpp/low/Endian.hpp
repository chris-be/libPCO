/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_ENDIAN_HPP_INCLUDED
#define	LIB_PCO_ENDIAN_HPP_INCLUDED

#include "low/EndianModes.hpp"
#include "mem/MemSettings.hpp"

namespace NPCO {

/*!
	Endians conversions.

	Class to convert from/to little or big endian format.
*/
class Endian {
public:
	// Conversion function to use
	typedef void (*ConvertFunc)(unsigned char *dst, mem_size size);

protected:
	/// To get little object
	ConvertFunc		fctToLittle;
	/// To get big object
	ConvertFunc		fctToBig;

public:
	/// Swap all
	static void	swap(unsigned char *dst, mem_size size);

	/// Do not swap anything
	static void	noSwap(unsigned char *dst, mem_size size);

	//! Detect computer endian mode
	static EEndianMode	detect(void);

public:
	/// Tests on the fly type of processor and chooses adapted functions
	Endian(void);

	/// Conversion to given type
	template<typename TIn>	void	toLittle(TIn& val) const
	{
		this->fctToLittle(reinterpret_cast<unsigned char*>(&val), sizeof(TIn));
	}

	/// Conversion to given type
	template<typename TIn>	void	toBig(TIn& val) const
	{
		this->fctToBig(reinterpret_cast<unsigned char*>(&val), sizeof(TIn));
	}

	/// Conversion from given type
	template<typename TIn>	void	fromLittle(TIn& val) const
	{
		this->fctToLittle(reinterpret_cast<unsigned char*>(&val), sizeof(TIn));
	}

	/// Conversion from given type
	template<typename TIn>	void	fromBig(TIn& val) const
	{
		this->fctToBig(reinterpret_cast<unsigned char*>(&val), sizeof(TIn));
	}

//
// Read only
//

	template<typename TIn>	TIn	toLittle_ro(const TIn val) const
	{
		TIn		tmp = val;
		this->toLittle(tmp);
		return tmp;
	}

	template<typename TIn>	TIn	toBig_ro(const TIn val) const
	{
		TIn		tmp = val;
		this->toBig(tmp);
		return tmp;
	}

	template<typename TIn>	TIn	fromLittle_ro(const TIn val) const
	{
		TIn		tmp = val;
		this->fromLittle(tmp);
		return tmp;
	}

	/// Conversion from given type
	template<typename TIn>	TIn	fromBig_ro(const TIn val) const
	{
		TIn		tmp = val;
		this->fromBig(tmp);
		return tmp;
	}

};

} // N..

#endif
