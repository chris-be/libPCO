/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_ENDIAN_CONVERTER_HPP_INCLUDED
#define	LIB_PCO_ENDIAN_CONVERTER_HPP_INCLUDED

#include "low/Endian.hpp"

namespace NPCO {

/**
	Endian Converter
		- converts words from/to "outside endian mode" to/from "inside endian mode"
*/
class EndianConverter {

protected:
	using ConvertFunc	= Endian::ConvertFunc;

	ConvertFunc		fctFrom;
	ConvertFunc		fctTo;

protected:
	// "inside mode": default is CPU detected mode
	EEndianMode		insideMode;
	// "outside mode": must be set
	EEndianMode		outsideMode;

public:
	//! insideMode set to CPU detected mode
	EndianConverter(void);
	//! Particular cases: set insideMode
	EndianConverter(const EEndianMode insideMode);

	//! Get defined "inside mode"
	EEndianMode	getInsideMode(void) const;

	//! Get defined "outside mode"
	EEndianMode	getMode(void) const;
	//! Set "outside mode"
	void		setMode(const EEndianMode oMode);

	template<typename TIn>	void	fromOutside(TIn& val) const
	{
		this->fctFrom(reinterpret_cast<unsigned char*>(&val), sizeof(TIn));
	}

	template<typename TIn>	void	toOutside(TIn& val) const
	{
		this->fctTo(reinterpret_cast<unsigned char*>(&val), sizeof(TIn));
	}

};

} // N..

#endif
