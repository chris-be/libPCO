/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_BIT_HPP_INCLUDED
#define	LIB_PCO_BIT_HPP_INCLUDED

#include "PCO_Predicate.hpp"
#include "PCO_ForceInline.hpp"

namespace NPCO {

/**
	Common bit manipulation
*/
class Bit {

private:
	//! Only static methods
	Bit(void) = delete;

public:
	/** Create a flag from a bit index
	*/
	template<typename PType>
	PCO_INLINE
	static PType	makeFlag(unsigned int bitIndex)
	{
		PCO_RANGE_ERR(bitIndex < sizeof(PType)*8, "");
		return 1 << bitIndex;
	}

	/** Test if it's a flag (only one bit set)
	*/
	template<typename PType>
	PCO_INLINE
	static bool		isFlag(unsigned int test)
	{
		return test && !(test & (test-1));
	}

	/** Switch "on" a flag
	*/
	template<typename PType>
	PCO_INLINE
	static void		setOn(PType &v, const PType flag)
	{
		v |= flag;
	}

	/** Switch "off" a flag
	*/
	template<typename PType>
	PCO_INLINE
	static void		setOff(PType &v, const PType flag)
	{
		v &= ~flag;
	}

	/** Test if flag is "on"
	*/
	template<typename PType>
	PCO_INLINE
	static bool		isOn(const PType v, const PType flag)
	{
		return (v & flag) > 0;
	}

	/** Test if flag is the only one "on"
	*/
	template<typename PType>
	PCO_INLINE
	static bool		isOnOnly(const PType v, const PType flag)
	{
		return (v & flag) == v;
	}

};

} // N..

#endif
