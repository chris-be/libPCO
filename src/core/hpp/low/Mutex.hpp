/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MUTEX_HPP_INCLUDED
#define	LIB_PCO_MUTEX_HPP_INCLUDED

#include "PCO_CompilerPlatform.hpp"
#if defined FOR_POSIX || defined FOR_LINUX
	#include <pthread.h>
	#include <errno.h>	// EBUSY
#elif defined FOR_WIN32
	#include <WinUse.h>
#else
	#error Unspecified or Unknown Platform
#endif

#include "pattern/res/I_ResourceLock.hpp"

namespace NPCO {

/*!
	To handle mutex

*/
class Mutex : public IResourceLock {
public:
	static const bool Default_Check_Recursive_Lock;

protected:

#if defined FOR_POSIX || defined FOR_LINUX
	pthread_mutexattr_t		attribute;
	pthread_mutex_t			mutex;
	// To preperly clean
	unsigned int			initState;
	bool					lockedState;
#elif defined FOR_WIN32
	CRITICAL_SECTION		mutex;
#else
	#error Unspecified or Unknown Platform
#endif

public:
	/**
		@param checkRecursiveLock
	*/
	Mutex(bool checkRecursiveLock = Default_Check_Recursive_Lock);

	~Mutex(void);

	// Copy
	Mutex(const Mutex &toCopy) = delete;
	Mutex& operator=(const Mutex &toCopy) = delete;

	// Move
	Mutex(Mutex &&toMove) = delete;
	Mutex& operator=(Mutex &&toMove) = delete;

	/** Try to lock mutex
		@return Success
	*/
	bool	tryLock(void);

	//! @see IResourceLock
	virtual bool	isLocked(void) const noexcept override;

	//! @see IResourceLock
	virtual void	lock(void) override;

	//! @see IResourceLock
	virtual void	unlock(void) noexcept override;

};

} // N..

#endif
