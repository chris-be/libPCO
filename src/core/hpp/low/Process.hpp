/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PROCESS_HPP_INCLUDED
#define	LIB_PCO_PROCESS_HPP_INCLUDED

#include "file/FilePipe.hpp"

#include <sys/types.h>	// pid_t

namespace NPCO {

/**
	Process
*/
class Process {

protected:
	//! Set before execute !
	bool		pipeStdOut;
	bool		pipeStdErr;

	FilePipe	stdOut;
	FilePipe	stdErr;

	//! Process pid
	pid_t		pid;
	int			status;

public:
	Process(void);
	Process(bool pipeStdOut, bool pipeStdErr);

	// Copy
	Process(const Process& ) = delete;
	Process& operator=(const Process& ) = delete;

	// Move
	Process(Process&& ) = default;
	Process& operator=(Process&& ) = default;

	bool	isPipeStdOut(void) const;
	bool	isPipeStdErr(void) const;

	FileLow&	getStdOut(void);
	FileLow&	getStdErr(void);

	bool	isValid(void) const;
	int		getExitStatus(void) const;

	//! Execute command in shell - can't pipe - exitStatus is adjusted
	void	shell(const char* cmd);

	/** const char* env[] ?
		@param exec Executable to use (with path if needed)
		@param usePath Search for exec using PATH (only if exec doesn't define a path)
		@param argv [0] must be the program name (exec without path)
		@return true if "fork" succeeded
	*/
	bool	execute(const char* exec, bool usePath, char* const argv[]);

	//! Wait for process - exitStatus is adjusted
	void	wait(void);

	// void	tryWait(void);

};

} // N..

#endif
