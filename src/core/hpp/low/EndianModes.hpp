/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_ENDIAN_MODES_HPP_INCLUDED
#define	LIB_PCO_ENDIAN_MODES_HPP_INCLUDED

namespace NPCO {

/*!

	Middle-Endian not handled
*/
enum class EEndianMode {
	  eLittle		= 1
	, eBig			= 2
	, eMiddle		= 3
};

} // N..

#endif
