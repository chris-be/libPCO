/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_THREAD_HPP_INCLUDED
#define	LIB_PCO_THREAD_HPP_INCLUDED

#include "PCO_CompilerPlatform.hpp"
#if defined FOR_POSIX || defined FOR_LINUX
	#include	<pthread.h>

	#define		THREAD_RET		void*
	#define		THREAD_FUNC		*
	#define		THREAD_ARG		void*
#elif defined FOR_WIN32
	#include	"WinUse.h"

	#define		THREAD_RET		DWORD
	#define		THREAD_FUNC		WINAPI
	#define		THREAD_ARG		void*
#else
	#error Unspecified or Unknown Platform
#endif

namespace NPCO {

/*!
	To handle thread

*/
class Thread {
/*

private:
#ifdef FOR_WIN32
		HANDLE				handle;	// Handle of thread
		DWORD				status;	// Thread status
#elif defined FOR_POSIX
		pthread_t			thread;
#else
	#error Unspecified or Unknown Platform
#endif

public:
	Thread(void);

	~Thread(void);

	// Copy
	Thread(const Thread &toCopy) = delete;
	Thread& operator=(const Thread &toCopy) = delete;

	// Move
	Thread(Thread &&toMove) = delete;
	Thread& operator=(Thread &&toMove) = delete;

	// Create a thread and launch it
	// Assume no thread was allready launched with same object
	bool		launch(THREAD_RET (THREAD_FUNC pFunction)(THREAD_ARG), THREAD_ARG pDatas);

	// Kill a thread
	void		kill();

	// Wait a thread to end
	void		wait();
*/

};

} // N..

#endif
