/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PREDICATE_HPP_INCLUDED
#define	LIB_PCO_PREDICATE_HPP_INCLUDED

#include "dbg/Debug.hpp"
#include <stdexcept>

#ifndef NPREDICATE

	#define PCO_PREDICATE(COND, MSG, EXC)					\
		PCO_ASSERT_MSG(COND, "PCO PREDICATE : '" MSG "'");	\
		if(!(COND))	throw EXC;

#else

	// Only debug asserts
	#define PCO_PREDICATE(COND, MSG, EXC)					\
		PCO_ASSERT_MSG(COND, "PCO PREDICATE : '" MSG "'");

#endif

// Problems
#define PCO_BAD_ALLOC(COND)				PCO_PREDICATE(COND	, MSG, std::bad_alloc())
#define PCO_RUNTIME_ERR(COND, MSG)		PCO_PREDICATE(COND	, MSG, std::runtime_error(MSG))

// Bad use
#define PCO_LOGIC_ERR(COND, MSG)		PCO_PREDICATE(COND	, MSG, std::logic_error(MSG))

// Bad argument
#define PCO_INV_ARG(COND, MSG)			PCO_PREDICATE(COND	, MSG, std::invalid_argument(MSG))
#define PCO_INV_SIZE(COND)				PCO_PREDICATE(COND	, "invalid size", std::length_error("invalid size"))
#define PCO_RANGE_ERR(COND, MSG)		PCO_PREDICATE(COND	, MSG, std::range_error(MSG))

#define PCO_INV_PTR(COND)				PCO_RANGE_ERR(COND	, "nullptr")

// Errors
#define PCO_UNK_CASE(MSG)				PCO_ERROR(MSG, std::runtime_error(MSG))


#endif
