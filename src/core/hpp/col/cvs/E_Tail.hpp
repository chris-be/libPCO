/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_TAIL_HPP_INCLUDED
#define	LIB_PCO_E_TAIL_HPP_INCLUDED

namespace NPCO {

/** Extension for
	@see C_Bag
	@see C_DynBag
*/

//! Get last value const ref
template<class PC_HasIterRead, class PType = typename PC_HasIterRead::TType>
const PType&	e_tail(const PC_HasIterRead& iterable)
{
	auto	l = iterable.last();
	PCO_ASSERT(l.isValid());
	return *l;
}

//! Get last value ref
template<class PC_HasIterWrite, class PType = typename PC_HasIterWrite::TType>
PType&			e_tail(PC_HasIterWrite& iterable)
{
	auto	l = iterable.last();
	PCO_ASSERT(l.isValid());
	return *l;
}

//! Pop last value
template<class PC_HasIterWrite>
void			e_popTail(PC_HasIterWrite& iterable)
{
	auto	l = iterable.last();
	PCO_ASSERT(l.isValid());
	iterable.remove(l);
}

//! Retrieve last value and pop it
template<class PC_HasIterWrite, class PType = typename PC_HasIterWrite::TType>
PType			e_pullTail(PC_HasIterWrite& iterable)
{
	PType tmp = std::move(e_tail(iterable));
	e_popTail(iterable);
	return tmp;
}

} // N..

#endif
