/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_HEAD_HPP_INCLUDED
#define	LIB_PCO_E_HEAD_HPP_INCLUDED

namespace NPCO {

/** Extension for
	@see C_Bag
	@see C_DynBag
*/

//! Get first value const ref
template<class PC_HasIterRead, class PType = typename PC_HasIterRead::TType>
const PType&	e_head(const PC_HasIterRead& iterable)
{
	auto	f = iterable.first();
	PCO_ASSERT(f.isValid());
	return *f;
}

//! Get first value ref
template<class PC_HasIterWrite, class PType = typename PC_HasIterWrite::TType>
PType&			e_head(PC_HasIterWrite& iterable)
{
	auto	f = iterable.first();
	PCO_ASSERT(f.isValid());
	return *f;
}

//! Pop first value
template<class PC_HasIterWrite>
void			e_popHead(PC_HasIterWrite& iterable)
{
	auto	f = iterable.first();
	PCO_ASSERT(f.isValid());
	iterable.remove(f);
}

//! Retrieve first value and pop it
template<class PC_HasIterWrite, class PType = typename PC_HasIterWrite::TType>
PType			e_pullHead(PC_HasIterWrite& iterable)
{
	PType tmp = std::move(e_head(iterable));
	e_popHead(iterable);
	return tmp;
}

} // N..

#endif
