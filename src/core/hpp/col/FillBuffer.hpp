/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_FILL_BUFFER_HPP_INCLUDED
#define	LIB_PCO_FILL_BUFFER_HPP_INCLUDED

#include "crt/C_FillBuffer.hpp"
#include "mem/Array.hpp"

namespace NPCO {

/*!
	FillBuffer:
		- fixed size
		- fill / read
 */
template<class PType> class FillBuffer
	: public IFillBuffer<PType, array_size> {

protected:
	//! Buffer
	Array<PType>		buffer;

	//! To know where we are in reading
	array_size			readPos;
	//! To know where we are in writing
	array_size			writePos;

public:
	//! Create buffer with given size
	FillBuffer(const array_size size);

	//! @see IFillBuffer
	virtual void		reset(void) override;

	//! @see C_HasSize
	virtual array_size	size(void) const override;

	//! @see IFillBuffer
	virtual void		resize(array_size newSize) override;

// Filling buffer with data

	//! @see IFillBuffer
	virtual array_size	leftToWrite(void) const override;

	//! @see IFillBuffer
	virtual void		add(const PType* pBuffer, const array_size size) override;

// Reading data in buffer

	//! @see IFillBuffer
	virtual void		resetRead(void) override;

	//! @see IFillBuffer
	virtual bool		isDirty(void) override;

	//! @see IFillBuffer
	virtual array_size	leftToRead(void) const override;

	//! @see IFillBuffer
	virtual void		get(PType* pBuffer, const array_size size) override;

// Direct access

	//! To know how many was already read
	array_size		position(void) const;

	//! Advance writePointer of size
	void	emuleWrite(const array_size size);

	//! Advance readPointer of size
	void	emuleRead(const array_size size);

	/** Get address of the next element to directly read into
		WARNING !! You should test leftToRead before reading
					And use emuleRead after that
	*/
	PCO_INLINE
	const PType*	_da_read(void) const;

	/** Get address of the next element to directly write into
		WARNING !! You should test placeLeft before filling
					And use emuleWrite after that
	*/
	PCO_INLINE
	PType*			_da_write(void);

};

} // N..

#include "imp/FillBuffer.cppi"

#endif
