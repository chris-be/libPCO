/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

// #include	"col/iter/ChunkList_Iter.hpp"

namespace NPCO {

template<class PCell, class PChunkList>
bool	ChunkList_Iter<PCell, PChunkList>::sameAs(const ChunkList_Iter& toCompare) const noexcept
{
	return     (this->currentCell == toCompare.currentCell);
}

template<class PCell, class PChunkList>
bool	ChunkList_Iter<PCell, PChunkList>::notSameAs(const ChunkList_Iter& toCompare) const noexcept
{
	return     (this->currentCell != toCompare.currentCell);
}

template<class PCell, class PChunkList>
inline
bool	ChunkList_Iter<PCell, PChunkList>::adjustMinMax(TInnerChunk* chunk) noexcept
{
	if(chunk == nullptr)
	{
		this->minCell = this->maxCell = nullptr;
		return false;
	}

	this->minCell = chunk->data;
	this->maxCell = chunk->data + this->chunkSize-1;
	return true;
}

template<class PCell, class PChunkList>
ChunkList_Iter<PCell, PChunkList>::ChunkList_Iter(void)
 : chunkSize(0), currentChunk(nullptr), minCell(nullptr), maxCell(nullptr), currentCell(nullptr)
{	}

template<class PCell, class PChunkList>
ChunkList_Iter<PCell, PChunkList>::ChunkList_Iter(const chunk_size chunkSize, TInnerChunk* chunk, chunk_size index)
 : chunkSize(chunkSize), currentChunk(chunk)
{
	PCO_INV_ARG(chunkSize > 0, "");
	PCO_INV_ARG((index >= 0) && (index < chunkSize), "");
	bool notNull = this->adjustMinMax(this->currentChunk);
	this->currentCell = notNull ? this->currentChunk->data + index : nullptr;
}

template<class PCell, class PChunkList>
void	ChunkList_Iter<PCell, PChunkList>::unset(void) noexcept
{
	this->currentCell = nullptr;
}

template<class PCell, class PChunkList>
bool	ChunkList_Iter<PCell, PChunkList>::isValid(void) const noexcept
{
	return this->currentCell != nullptr;
}

template<class PCell, class PChunkList>
void	ChunkList_Iter<PCell, PChunkList>::forward(void) noexcept
{
	if(this->currentCell < this->maxCell)
	{	// Wrong when maxCell is null
		++this->currentCell;
		return;
	}

	TInnerChunk*	next = this->currentChunk->next;
	this->adjustMinMax(next);
	if(next != nullptr)
	{
		this->currentChunk = next;
	}
	// Will be nullptr if end
	this->currentCell = this->minCell;
}

template<class PCell, class PChunkList>
void	ChunkList_Iter<PCell, PChunkList>::back(void) noexcept
{
	if(this->currentCell > this->minCell)
	{	// Wrong when minCell is null
		--this->currentCell;
		return;
	}

	TInnerChunk*	prev = this->currentChunk->previous;
	if(this->adjustMinMax(prev))
	{
		this->currentChunk = prev;
	}
	// Will be nullptr if end
	this->currentCell = this->maxCell;
}

template<class PCell, class PChunkList>
PCell&	ChunkList_Iter<PCell, PChunkList>::operator*(void) const
{
	PCO_RUNTIME_ERR(this->isValid(), "bad use");
	return *this->currentCell;
}

template<class PCell, class PChunkList>
PCell*	ChunkList_Iter<PCell, PChunkList>::operator->(void) const
{
	PCO_RUNTIME_ERR(this->isValid(), "bad use");
	return this->currentCell;
}

} // N..
