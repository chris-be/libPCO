/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

// #include	"col/iter/MapTreeList_Iter.hpp"

namespace NPCO {

template<class PKey, class PValue, class PTreeIter, class PListIter>
void	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::setListBounds(void)
{
	if(this->treeIter.isValid())
	{
		this->listFirst	= this->treeIter.cell().first();
		this->listLast	= this->treeIter.cell().last();
	}
	else
	{
		this->listFirst.unset();
		this->listLast.unset();
	}
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
inline
bool	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::sameAs(const MapTreeList_Iter& toCompare) const
{
	return     (this->treeIter == toCompare.treeIter)
			&& (this->listIter == toCompare.listIter);
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
inline
bool	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::notSameAs(const MapTreeList_Iter& toCompare) const
{
	return     (this->treeIter != toCompare.treeIter)
			|| (this->listIter != toCompare.listIter);
}

/*
template<class PKey, class PValue, class PTreeIter, class PListIter>
	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::MapTreeList_Iter(void)
{	}
*/

template<class PKey, class PValue, class PTreeIter, class PListIter>
	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::MapTreeList_Iter(const PTreeIter& mi, bool last)
 : treeIter(mi)
{
	this->setListBounds();
	this->listIter = last ? this->listLast : this->listFirst;
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::MapTreeList_Iter(const PTreeIter& mi, const PListIter& li)
 : treeIter(mi), listIter(li)
{
	this->setListBounds();
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
void	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::unset(void)
{
	this->treeIter.unset();
	// this->listIter.unset();
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
bool	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::isValid(void) const
{
	return this->treeIter.isValid() && this->listIter.isValid();
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
void	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::forward(void)
{
	if(!this->treeIter.isValid())
	{
		return;
	}

	if(this->listIter == this->listLast)
	{
		this->treeIter.forward();
		if(!this->treeIter.isValid())
		{
			return;
		}
		this->setListBounds();
		this->listIter = this->listFirst;
	}
	else
	{
		this->listIter.forward();
	}
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
void	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::back(void)
{
	if(!this->treeIter.isValid())
	{
		return;
	}

	if(this->listIter == this->listFirst)
	{
		this->treeIter.back();
		if(!this->treeIter.isValid())
		{
			return;
		}
		this->setListBounds();
		this->listIter = this->listLast;
	}
	else
	{
		this->listIter.back();
	}
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
PValue&	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::operator*(void) const
{
	PCO_RUNTIME_ERR(this->isValid(), "bad use");
	return this->listIter.operator*();
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
PValue*	MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::operator->(void) const
{
	PCO_RUNTIME_ERR(this->isValid(), "bad use");
	return this->listIter.operator->();
}

template<class PKey, class PValue, class PTreeIter, class PListIter>
const PKey&		MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>::key(void) const
{
	PCO_RUNTIME_ERR(this->isValid(), "bad use");
	return this->treeIter.key();
}

} // N..
