/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHUNKLIST_ITER_WRITE_HPP_INCLUDED
#define	LIB_PCO_CHUNKLIST_ITER_WRITE_HPP_INCLUDED

#include "./ChunkList_Iter.hpp"
#include "pattern/iter/C_IterWrite.hpp"

namespace NPCO {

/**
	ChunkList_IterWrite
*/
template<class PType, class PChunkList>
class ChunkList_IterWrite
		: private C_IterWrite<ChunkList_IterWrite<PType, PChunkList>, PType>
		, public ChunkList_Iter<PType, PChunkList> {

public:
	typedef PType		TCell;

protected:
	using TBase			= ChunkList_Iter<PType, PChunkList>;
	using TInnerChunk	= typename TBase::TInnerChunk;

public:
	ChunkList_IterWrite(void) = default;

	ChunkList_IterWrite(const chunk_size chunkSize, TInnerChunk* chunk, chunk_size index)
		: TBase(chunkSize, chunk, index)
	{	}

	//! @see C_IterWrite
	PCO_INLINE
	ChunkList_IterWrite&	operator++(void)
	{
		this->forward();	return *this;
	}

	//! @see C_IterWrite
	PCO_INLINE
	ChunkList_IterWrite&	operator--(void)
	{
		this->back();	return *this;
	}

	//! @see C_IterWrite
	PCO_INLINE
	bool	operator==(const ChunkList_IterWrite& toCompare) const
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IterWrite
	PCO_INLINE
	bool	operator!=(const ChunkList_IterWrite& toCompare) const
	{
		return this->notSameAs(toCompare);
	}

};

} // N..

#endif
