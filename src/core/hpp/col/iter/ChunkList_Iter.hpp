/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHUNKLIST_ITER_HPP_INCLUDED
#define	LIB_PCO_CHUNKLIST_ITER_HPP_INCLUDED

#include "pattern/iter/C_Iter.hpp"
#include "col/ChunkList.hpp"

namespace NPCO {

/**
	ChunkList_Iter
	@param PChunkList (const) ChunkList<PCell, ..>
*/
template<class PCell, class PChunkList>
class ChunkList_Iter : private C_Iter<ChunkList_Iter<PCell, PChunkList>, PCell> {

public:
	using TCell			= typename std::remove_const<PCell>::type;

protected:
	using TInnerChunk	= typename PChunkList::InnerChunk;

protected:
	chunk_size		chunkSize;
	TInnerChunk		*currentChunk;
	// min and max for currentChunk->data
	PCell			*minCell;
	PCell			*maxCell;
	// Current cell
	PCell			*currentCell;

	PCO_INLINE
	bool	sameAs(const ChunkList_Iter& toCompare) const noexcept;
	PCO_INLINE
	bool	notSameAs(const ChunkList_Iter& toCompare) const noexcept;

	// Adjust minCall/maxCell depending on chunk - @return true if chunk in not null
	PCO_INLINE
	bool	adjustMinMax(TInnerChunk* chunk) noexcept;

public:
	ChunkList_Iter(void);
	ChunkList_Iter(const chunk_size chunkSize, TInnerChunk* chunk, chunk_size index);

	//! @see C_Iter
	void	unset(void) noexcept;

	//! @see C_Iter
	PCO_INLINE
	bool	isValid(void) const noexcept;

	//! @see C_Iter
	void	forward(void) noexcept;

	//! @see C_Iter
	void	back(void) noexcept;

	//! @see C_Iter
	PCO_INLINE
	PCell&	operator*(void) const;

	//! @see C_Iter
	PCO_INLINE
	PCell*	operator->(void) const;

	//! @see C_Iter
	PCO_INLINE
	PCell&	cell(void) const
	{
		return this->operator*();
	}

};

} // N..

#include "imp/ChunkList_Iter.cppi"

#endif
