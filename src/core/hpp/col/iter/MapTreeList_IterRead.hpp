/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MAP_TREE_LIST_ITER_READ_HPP_INCLUDED
#define	LIB_PCO_MAP_TREE_LIST_ITER_READ_HPP_INCLUDED

#include "./MapTreeList_Iter.hpp"
#include "pattern/iter/C_IndexIterRead.hpp"

namespace NPCO {

/**
	MapTreeList_IterRead
*/
template<class PKey, class PValue, class PTree, class PList>
class MapTreeList_IterRead
	: private C_IndexIterRead<MapTreeList_IterRead<PKey, PValue, PTree, PList>, PKey, PValue>
	, public MapTreeList_Iter<PKey, const PValue, typename PTree::TIterRead, typename PList::TIterRead> {

protected:
	using TTreeIter		= typename PTree::TIterRead;
	using TListIter		= typename PList::TIterRead;
	using TBase			= MapTreeList_Iter<PKey, const PValue, TTreeIter, TListIter>;

public:
	MapTreeList_IterRead(void) = default;

	MapTreeList_IterRead(const TTreeIter& pi, bool last)
		: TBase(pi, last)
	{	}

	MapTreeList_IterRead(const TTreeIter& mi, const TListIter& li)
		: TBase(mi, li)
	{	}

	//! @see C_IndexIterRead
	MapTreeList_IterRead&	operator++(void)
	{
		this->forward();	return *this;
	}

	//! @see C_IndexIterRead
	MapTreeList_IterRead&	operator--(void)
	{
		this->back();	return *this;
	}

	//! @see C_IndexIterRead
	bool	operator==(const MapTreeList_IterRead& toCompare) const
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IndexIterRead
	bool	operator!=(const MapTreeList_IterRead& toCompare) const
	{
		return this->notSameAs(toCompare);
	}

};

} // N..

#endif
