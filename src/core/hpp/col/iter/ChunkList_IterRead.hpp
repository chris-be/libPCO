/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHUNKLIST_ITER_READ_HPP_INCLUDED
#define	LIB_PCO_CHUNKLIST_ITER_READ_HPP_INCLUDED

#include "ChunkList_Iter.hpp"
#include "pattern/iter/C_IterRead.hpp"

namespace NPCO {

/**
	ChunkList_IterRead
*/
template<class PType, class PChunkList>
class ChunkList_IterRead
		: private C_IterRead<ChunkList_IterRead<PType, PChunkList>, PType>
		, public ChunkList_Iter<const PType, const PChunkList> {

public:
	using TCell			= PType;

protected:
	using TBase			= ChunkList_Iter<const PType, const PChunkList>;
	using TInnerChunk	= typename TBase::TInnerChunk;

public:
	ChunkList_IterRead(void) = default;

	ChunkList_IterRead(const chunk_size chunkSize, TInnerChunk* chunk, chunk_size index)
		: TBase(chunkSize, chunk, index)
	{	}

	//! @see C_IterRead
	PCO_INLINE
	ChunkList_IterRead&	operator++(void)
	{
		this->forward();	return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	ChunkList_IterRead&	operator--(void)
	{
		this->back();	return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator==(const ChunkList_IterRead& toCompare) const
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator!=(const ChunkList_IterRead& toCompare) const
	{
		return this->notSameAs(toCompare);
	}

};

} // N..

#endif
