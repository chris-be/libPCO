/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MAP_TREE_LIST_ITER_HPP_INCLUDED
#define	LIB_PCO_MAP_TREE_LIST_ITER_HPP_INCLUDED

#include "pattern/iter/C_IndexIter.hpp"
#include "col/MapTreeList.hpp"

namespace NPCO {

/**
	MapTreeList_Iter

	@param PTreeIter
	@param PListIter
*/
template<class PKey, class PValue, class PTreeIter, class PListIter>
class MapTreeList_Iter
	: private C_IndexIter<MapTreeList_Iter<PKey, PValue, PTreeIter, PListIter>, PKey, PValue> {

public:
	using TCell			= typename std::remove_const<PValue>::type;
	using TKey			= PKey;
	using TValue		= PValue;

protected:
	PTreeIter		treeIter;
	PListIter		listIter;
	PListIter		listFirst;
	PListIter		listLast;

	//! Set listFirst and listLast accordingly to treeIter
	PCO_INLINE
	void	setListBounds(void);

	PCO_INLINE
	bool	sameAs(const MapTreeList_Iter& toCompare) const;
	PCO_INLINE
	bool	notSameAs(const MapTreeList_Iter& toCompare) const;

public:
	MapTreeList_Iter(void) = default;
	//! @param last false -> first
	MapTreeList_Iter(const PTreeIter& pi, bool last);
	MapTreeList_Iter(const PTreeIter& pi, const PListIter& li);

	//! @see C_IndexIter
	void	unset(void);

	//! @see C_IndexIter
	PCO_INLINE
	bool	isValid(void) const;

	//! @see C_IndexIter
	void	forward(void);

	//! @see C_IndexIter
	void	back(void);

	//! @see C_IndexIter
	PCO_INLINE
	PValue&	operator*(void) const;

	//! @see C_IndexIter
	PCO_INLINE
	PValue*	operator->(void) const;

	//! @see C_IndexIter
	PCO_INLINE
	PValue&	cell(void) const
	{
		return this->operator*();
	}

	//! @see C_IndexIter
	PCO_INLINE
	const PKey&		key(void) const;

};

} // N..

#include "imp/MapTreeList_Iter.cppi"

#endif
