/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MAP_TREE_LIST_ITER_WRITE_HPP_INCLUDED
#define	LIB_PCO_MAP_TREE_LIST_ITER_WRITE_HPP_INCLUDED

#include "./MapTreeList_Iter.hpp"
#include "pattern/iter/C_IndexIterWrite.hpp"

namespace NPCO {

/**
	MapTreeList_IterWrite
*/
template<class PKey, class PValue, class PTree, class PList>
class MapTreeList_IterWrite
	: private C_IndexIterWrite<MapTreeList_IterWrite<PKey, PValue, PTree, PList>, PKey, PValue>
	, public MapTreeList_Iter<PKey, PValue, typename PTree::TIterWrite, typename PList::TIterWrite> {

protected:
	using TTreeIter		= typename PTree::TIterWrite;
	using TListIter		= typename PList::TIterWrite;
	using TBase			= MapTreeList_Iter<PKey, PValue, TTreeIter, TListIter>;

public:
	MapTreeList_IterWrite(void) = default;

	MapTreeList_IterWrite(const TTreeIter& pi, bool last)
		: TBase(pi, last)
	{	}

	MapTreeList_IterWrite(const TTreeIter& pi, const TListIter& ti)
		: TBase(pi, ti)
	{	}

	//! @see C_IndexIterWrite
	MapTreeList_IterWrite&	operator++(void)
	{
		this->forward();	return *this;
	}

	//! @see C_IndexIterWrite
	MapTreeList_IterWrite&	operator--(void)
	{
		this->back();	return *this;
	}

	//! @see C_IndexIterWrite
	bool	operator==(const MapTreeList_IterWrite& toCompare) const
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IndexIterWrite
	bool	operator!=(const MapTreeList_IterWrite& toCompare) const
	{
		return this->notSameAs(toCompare);
	}

};

} // N..

#endif
