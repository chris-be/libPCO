/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STORE_HPP_INCLUDED
#define	LIB_PCO_STORE_HPP_INCLUDED

#include "crt/C_Collection.hpp"
#include "mem/Array.hpp"
#include "mem/mgr/BlockMemoryManager.hpp"
#include "cvs/use/UseMemoryManager.hpp"

#include <initializer_list>

namespace NPCO {

/*!
	This template provides a dynamic array which grows at demand.
	Use it when you need continuous data and dynamic size.
*/
template<class PType, class PMemMgr = SimpleMemoryManager> class Store
	: public C_Collection<Store<PType, PMemMgr>, PType, array_size>
	, public UseMemoryManager<PMemMgr> {

public:
	using TSize			= typename C_Collection<Store<PType, PMemMgr>, PType, array_size>::TSize;
	using TIterRead		= typename Array<PType, TSize, PMemMgr>::TIterRead;
	using TIterWrite	= typename Array<PType, TSize, PMemMgr>::TIterWrite;

	// 1.25
	static const TSize	factor_numerator	= 5;
	static const TSize	factor_denominator	= 4;

	static_assert((factor_numerator > 0) && (factor_denominator > 0), "Invalid factor");
	static_assert(factor_denominator < factor_numerator);

protected:
	//! Array in memory (its size is defined as "maxSize")
	Array<PType, TSize, PMemMgr>	array;

	//! Currently number of items
	TSize		nbItems;

	//! Returns if index is valid
	PCO_INLINE
	bool	isInBounds(const TSize i) const;

	/** Shift to right values
		@param i Index to start
		@param delta How many shifts to do
	*/
	void	shiftRight(const TSize i, const TSize delta);

	/** Shift to left a block of values
		@param i Index to start
		@param delta How many shifts to do
	*/
	void	shiftLeft(const TSize i, const TSize delta);

	//! Grow if needed - doesn't update nbItems ! Must be done after if needed
	void	grow(const TSize delta);
	//! Shrink if needed - doesn't update nbItems ! Must be done after if needed
	void	shrink(const TSize delta);

	//! Add an empty cell
	TSize	prepareNew(void);

	//! Insert an empty cell
	void	prepareNew(const TSize i);

public:
	Store(void);

	/*!
		@param reserveSize Number of items to reserve space for
	*/
	Store(const TSize reserveSize);

	Store(std::initializer_list<PType> list);

	// Copy
	Store(const Store& toCopy) = default;
    Store& operator=(const Store& toCopy) = default;

	// Move
	Store(Store&& toMove) = default;
    Store& operator=(Store&& toMove) = default;

	//! Real size of reserved memory
	TSize	realSize(void) const;

	//! Resize to free extra reserved space
	void	compact(void);

	//! @see C_HasSize
	TSize	size(void) const;

	//! @see C_HasSize
	bool	isEmpty(void) const;

	//! @see C_DynBag
	void	add(const PType& toCopy);
	//! Add an item (push items farther)
	void	add(const PType& toCopy, const TSize i);

	//!
	void	add(const Store& toAdd);

	//! @see C_DynBag
	void	moveIn(PType&& toMove);
	//! Add an item (push items farther)
	void	moveIn(PType&& toMove, const TSize i);

	//! @see C_DynBag
	void	remove(const TIterWrite& it_w);
	void	remove(const TIterWrite& low, const TIterWrite& high);

	//! Remove item at index (pull remaining items)
	void	removeAt(const TSize i);

	/** Remove item at index (pull remaining items)
		@return Removed item
	*/
	PType	moveOut(const TSize i);

	//! @see C_DynBag
	void	clear(void);

	//! Access to the ith element
	PCO_INLINE
	const PType&	operator[](const TSize i) const;

	//! Access to the ith element
	PCO_INLINE
	PType&			operator[](const TSize i);

	//! @see C_Bag
	PCO_INLINE
	TIterRead	first(void) const;
	//! @see C_Bag
	PCO_INLINE
	TIterRead	last(void) const;

	//! @see C_Bag
	PCO_INLINE
	TIterWrite	first(void);
	//! @see C_Bag
	PCO_INLINE
	TIterWrite	last(void);

	//! @see C_RowBag
	PCO_INLINE
	TIterRead	iter_r(const TSize i) const;
	//! @see C_RowBag
	PCO_INLINE
	TIterWrite	iter_w(const TSize i);

	//! Get pointer - Do not free !!
	PCO_INLINE
	const PType*	_da(void) const;

	//! Get pointer - Do not free !!
	PCO_INLINE
	PType*			_da(void);

};

} // N..

#include "imp/Store.cppi"

#endif
