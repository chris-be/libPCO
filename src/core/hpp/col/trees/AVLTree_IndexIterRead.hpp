/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AVL_TREE_INDEX_ITER_READ_HPP_INCLUDED
#define	LIB_PCO_AVL_TREE_INDEX_ITER_READ_HPP_INCLUDED

#include "AVLTree_IndexIter.hpp"
#include "pattern/iter/C_IterRead.hpp"

namespace NPCO {

/**
	AVLTree_IndexIterRead
*/
template<class PKey, class PValue, class PClassifier, class PMemMgr> class AVLTree_IndexIterRead
	: public C_IndexIterRead<AVLTree_IndexIterRead<PKey, PValue, PClassifier, PMemMgr>, PKey, PValue>
	, public AVLTree_IndexIter<PKey, const PValue, const AVLTree<PKey, PValue, PClassifier, PMemMgr>> {

protected:
	using TTree			= const AVLTree<PKey, PValue, PClassifier, PMemMgr>;
	using TBase			= AVLTree_IndexIter<PKey, const PValue, TTree>;

public:
	AVLTree_IndexIterRead(void) = default;

	AVLTree_IndexIterRead(TTree* tree) : TBase(tree)
	{ }

	AVLTree_IndexIterRead(TTree* tree, const PKey& key) : TBase(tree, key)
	{ }

	//! @see C_IterRead
	PCO_INLINE
	AVLTree_IndexIterRead&	operator++(void)
	{
		this->forward();	return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	AVLTree_IndexIterRead&	operator--(void)
	{
		this->back();	return *this;
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator==(const AVLTree_IndexIterRead& toCompare) const
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IterRead
	PCO_INLINE
	bool	operator!=(const AVLTree_IndexIterRead& toCompare) const
	{
		return this->sameAs(toCompare);
	}

};

} // N..

#endif
