/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AVL_TREE_INDEX_ITER_HPP_INCLUDED
#define	LIB_PCO_AVL_TREE_INDEX_ITER_HPP_INCLUDED

#include "pattern/iter/C_IndexIter.hpp"

namespace NPCO {

/**
	AVLTree_IndexIter

	@param PTree (const) AVLTree<PKey, PValue, ..>
*/
template<class PKey, class PValue, class PTree> class AVLTree_IndexIter
	: public C_IndexIter<AVLTree_IndexIter<PKey, PValue, PTree>, PKey, PValue> {

public:
	using TCell			= typename std::remove_const<PValue>::type;
	using TKey			= PKey;
	using TValue		= PValue;

protected:
	friend class AVLTree<typename PTree::TKey, typename PTree::TValue, typename PTree::TClassifier, typename PTree::TMemMgr>;

	using TInnerNode	= typename PTree::InnerNode;
	using TNodeStack	= typename PTree::template NodeStack<TInnerNode>;

protected:
	PTree*			tree;

	// Fathers of current node + current node
	TNodeStack		stack;

	//! Add "lt_branch" of tail node
	void	addLt_branch(void);
	//! Add "gt_branch" of tail node
	void	addGt_branch(void);

	PCO_INLINE
	bool	sameAs(const AVLTree_IndexIter& toCompare) const;

	void	toMin(void);
	void	toMax(void);

	void	toKey(const PKey& key);

	//! Find key or first "greater value"
	void	toKeyMin(const PKey& key);
	//! Find key or first "lesser value"
	void	toKeyMax(const PKey& key);

public:
	AVLTree_IndexIter(void);
	AVLTree_IndexIter(PTree* tree);
	AVLTree_IndexIter(PTree* tree, const PKey& key);

	// Copy
	AVLTree_IndexIter(const AVLTree_IndexIter& toCopy);
	AVLTree_IndexIter& operator=(const AVLTree_IndexIter& toCopy);

	// Move
	// AVLTree_IndexIter(AVLTree_IndexIter&& toMove) = default;
	// AVLTree_IndexIter& operator=(AVLTree_IndexIter&& toMove) = default;

	//! @see C_Iter
	void	unset(void);

	//! @see C_Iter
	PCO_INLINE
	bool	isValid(void) const;

	//! @see C_Iter
	PCO_INLINE
	void	forward(void);

	//! @see C_Iter
	PCO_INLINE
	void	back(void);

	//! @see C_Iter
	PCO_INLINE
	PValue&	operator*(void) const;

	//! @see C_Iter
	PCO_INLINE
	PValue*	operator->(void) const;

	//! @see C_Iter
	PCO_INLINE
	PValue&	cell(void) const
	{
		return this->operator*();
	}

	//! @see C_IndexIter
	PCO_INLINE
	const PKey&	key(void) const;

};

} // N..

#include "imp/AVLTree_IndexIter.cppi"

#endif
