/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AVL_TREE_INDEX_ITER_WRITE_HPP_INCLUDED
#define	LIB_PCO_AVL_TREE_INDEX_ITER_WRITE_HPP_INCLUDED

#include "./AVLTree_IndexIter.hpp"
#include "pattern/iter/C_IterWrite.hpp"

namespace NPCO {

/**
	AVLTree_IndexIterWrite
*/
template<class PKey, class PValue, class PClassifier, class PMemMgr> class AVLTree_IndexIterWrite
	: public C_IndexIterRead<AVLTree_IndexIterRead<PKey, PValue, PClassifier, PMemMgr>, PKey, PValue>
	, public AVLTree_IndexIter<PKey, PValue, AVLTree<PKey, PValue, PClassifier, PMemMgr>> {

protected:
	using TTree			= AVLTree<PKey, PValue, PClassifier, PMemMgr>;
	using TBase			= AVLTree_IndexIter<PKey, PValue, TTree>;

public:
	AVLTree_IndexIterWrite(void) = default;

	AVLTree_IndexIterWrite(TTree* tree) : TBase(tree)
	{ }

	AVLTree_IndexIterWrite(TTree* tree, const PKey& key) : TBase(tree, key)
	{ }

	//! @see C_IterWrite
	PCO_INLINE
	AVLTree_IndexIterWrite&	operator++(void)
	{
		this->forward();	return *this;
	}

	//! @see C_IterWrite
	PCO_INLINE
	AVLTree_IndexIterWrite&	operator--(void)
	{
		this->back();	return *this;
	}

	//! @see C_IterWrite
	PCO_INLINE
	bool	operator==(const AVLTree_IndexIterWrite& toCompare) const
	{
		return this->sameAs(toCompare);
	}

	//! @see C_IterWrite
	PCO_INLINE
	bool	operator!=(const AVLTree_IndexIterWrite& toCompare) const
	{
		return this->sameAs(toCompare);
	}

};

} // N..

#endif
