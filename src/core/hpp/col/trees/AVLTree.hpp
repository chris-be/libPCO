/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_AVL_TREE_HPP_INCLUDED
#define	LIB_PCO_AVL_TREE_HPP_INCLUDED

#include "pattern/bag/C_SortedIndexBag.hpp"
#include "cvs/cfr/DefaultClassifier.hpp"
#include "col/ChunkList.hpp"
#include "cvs/use/UseClassifier.hpp"
#include "cvs/use/UseMemoryManager.hpp"

#include <ostream>

namespace NPCO {

template<class PKey, class PValue, class PTree> class AVLTree_IndexIter;
template<class PKey, class PValue, class PClassifier, class PMemMgr> class AVLTree_IndexIterRead;
template<class PKey, class PValue, class PClassifier, class PMemMgr> class AVLTree_IndexIterWrite;

/*!
	AVL Tree : sorted key with "optional associated values"

	Usage:
	- lots of finds (in facts a lot more than adds/removes)
	- ordering

	Tech:
	- minimum memory footprint : no father pointer => removing anything invalids cursor !
	-
*/
template<class PKey, class PValue, class PClassifier = DefaultClassifier<PKey>
		, class PMemMgr = SimpleMemoryManager> class AVLTree
	: public C_SortedIndexBag<AVLTree<PKey, PValue, PClassifier, PMemMgr>, PKey, PValue, array_size>
	, public UseClassifier<PClassifier>
	, public UseMemoryManager<PMemMgr> {

public:
	using TKey			= PKey;
	using TValue		= PValue;
	using TSize			= array_size;
	using TClassifier	= PClassifier;

protected:
	// Note: permits 2^255-1 nodes => (2^128 * 2^127)-1 nodes => enough !!
	using Height = uw_08;

	class InnerNode {
	public:
		InnerNode*	father;
		// Can't use : less, greater : <functional> declare them
		InnerNode*	lt_branch;
		InnerNode*	gt_branch;

		PKey		key;
		PValue		value;

		// Last for padding...
		Height		height;
	};

	// Keep track of nodes - @param TNode (const) InnerNode
	template<class TNode>
	using NodeStack			= ChunkList<TNode*, PMemMgr>;

	// Keep track of node addresses (for modification) - @param TNode (const) InnerNode
	// NodeStack would be enough but code is not very understandable
	template<class TNode>
	using NodePtrStack		= NodeStack<TNode*>;

	friend class AVLTree_IndexIter<PKey, const PValue, const AVLTree>;
	friend class AVLTree_IndexIter<PKey, PValue, AVLTree>;

public:
	using TIterRead		= AVLTree_IndexIterRead<PKey, PValue, PClassifier, PMemMgr>;
	using TIterWrite	= AVLTree_IndexIterWrite<PKey, PValue, PClassifier, PMemMgr>;

protected:
	InnerNode*	root;

	//! Number of currently items
	TSize		nbItems;

	//! Get height (convenience : handle nullptr)
	PCO_INLINE
	static Height	getHeight(const InnerNode* pNode);

	//! Eval balance (convenience : handle nullptr)
	PCO_INLINE
	static int		evalBalance(const InnerNode* pNode);

	//! Update height depending on children
	PCO_INLINE
	static void		updateHeight(InnerNode* pNode);

	//! Left rotation
	static InnerNode*	leftRotate(InnerNode* root);

	//! Right rotation
	static InnerNode*	rightRotate(InnerNode* root);

	// MemManager
	InnerNode*		createInnerNode(const PKey& key);

	/** Delete node (juste free, no logic) - MemManager
		@param recursive : delete sons
	*/
	void		deleteInnerNode(InnerNode* pNode, bool recursive);

	/* Find node by key and fill stack with traversed nodes "referencies"
		@param TNode Node or const Node !
	*/
	template<class TNode>
	bool	stackFindNode(NodePtrStack<TNode>& nodeStack, const PKey& key);

	//! FindMin which fill a stack of "reference" of traversed nodes
	template<class TNode>
	void	stackTraceToMin(NodePtrStack<TNode>& nodeStack, TNode** root);

	//! Adjust height of nodes and rotate if necessary
	void	balance(NodePtrStack<InnerNode>& nodeStack);

	/* Fill stack with traversed nodes until reaching key
		@param TNode Node or const Node !
	*/
	template<class TNode>
	bool	stackTraceToKey(NodeStack<TNode>& nodeStack, TNode* root, const PKey& key) const;

	/* Fill stack with traversed nodes until reaching min
		@param TNode Node or const Node !
	*/
	template<class TNode>
	static void	stackTraceToMin(NodeStack<TNode>& nodeStack, TNode* root);

	/* Fill stack with traversed nodes until reaching max
		@param TNode Node or const Node !
	*/
	template<class TNode>
	static void	stackTraceToMax(NodeStack<TNode>& nodeStack, TNode* root);

	//! Find which fill a stack of "reference" of traversed nodes
	bool	stackFindNode(NodeStack<InnerNode>& nodeStack, const PKey& key);
	bool	stackFindNode(NodeStack<const InnerNode>& nodeStack, const PKey& key) const;

	//! Find
	template<class TNode>
	TNode	findNode(TNode root, const PKey& key) const;

	/** Add a node if needed
		@return New node
	*/
	InnerNode*	addNode(const PKey& key);

	/** Remove a node
		@return Old node
	*/
	InnerNode*	removeNode(const PKey& key);

	/// Used by constructor, move, ...
	void	reset(void);

public:
	AVLTree(void);
	~AVLTree(void);

	// Copy
	AVLTree(const AVLTree& toCopy);
    AVLTree& operator=(const AVLTree& toCopy);

	// Move
	AVLTree(AVLTree&& toMove);
    AVLTree& operator=(AVLTree&& toMove);

	//! @see C_HasSize
	TSize	size() const;

	//! @see C_HasSize
	bool	isEmpty() const;

	//! @see C_IndexedBag
	void	clear(void);

	//! @see C_IndexedBag
	void	add(const PKey& key, const PValue& value);

	//! @see C_IndexedBag
	void	moveIn(const PKey& key, PValue&& value);

	//! @see C_IndexedBag
	TIterWrite	add(const PKey& key);

	//!	Check if key is present
	PCO_INLINE
	bool	hasKey(const PKey& key) const;

	//!	@see C_IndexedBag TODO: findNearest ...
	PCO_INLINE
	TIterRead	find(const PKey& key) const;
	//!	@see C_IndexedBag
	PCO_INLINE
	TIterWrite	find(const PKey& key);

	//!	Remove key (and item)
	void	remove(const PKey& key);

	//! Find min
	PCO_INLINE
	const PKey*		min(void) const;

	//! Find max
	PCO_INLINE
	const PKey*		max(void) const;

	//! @see C_HasIndexIterRead
	TIterRead	first(void) const;
	//! @see C_HasIndexIterRead
	TIterRead	last(void) const;
	//! @see C_HasIndexIterRead
	TIterRead	iter_r(const PKey& key) const;
	//! @see C_SortedIndexBag
	TIterRead	iter_r_min(const PKey& key) const;
	//! @see C_SortedIndexBag
	TIterRead	iter_r_max(const PKey& key) const;

	//! @see C_HasIndexIterWrite
	TIterWrite	first(void);
	//! @see C_HasIndexIterWrite
	TIterWrite	last(void);
	//! @see C_HasIndexIterWrite
	TIterWrite	iter_w(const PKey& key);

	PCO_INLINE
	const PValue&	operator[](const PKey& key) const;
	PCO_INLINE
	PValue&			operator[](const PKey& key);

	template<class CK, class CM>
	friend std::ostream&	operator<<(std::ostream &stream, const AVLTree<CK, CM>& toEcho);

};

} // N..

#include "imp/AVLTree.cppi"

#endif
