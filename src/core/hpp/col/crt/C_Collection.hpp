/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_COLLECTION_HPP_INCLUDED
#define	LIB_PCO_C_COLLECTION_HPP_INCLUDED

#include "pattern/bag/C_RowDynBag.hpp"
#include "mem/MemSettings.hpp"

namespace NPCO {

/**
	Constraint for collections

*/
template<class PCd, class PType, class PSize = array_size> class C_Collection
	: private C_RowDynBag<PCd, PType, PSize> {

public:
	using TType			= PType;
	using TSize			= PSize;

};

} // N..

#endif
