/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_UNIQUENESS_COLLECTION_HPP_INCLUDED
#define	LIB_PCO_C_UNIQUENESS_COLLECTION_HPP_INCLUDED

#include "pattern/bag/C_KeyBag.hpp"
#include "mem/MemSettings.hpp"

namespace NPCO {

/**
	Constraint for uniqueness collections: no dupliacted value

*/
template<class PCd, class PType, class PSize = array_size>
class C_UniquenessCollection : private C_KeyBag<PCd, PType, PSize> {

public:
	using TType			= PType;
	using TSize			= PSize;

private:

public:

};

} // N..

#endif
