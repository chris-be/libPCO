/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_HAS_HEAD_HPP_INCLUDED
#define	LIB_PCO_C_HAS_HEAD_HPP_INCLUDED

namespace NPCO {

/**
	Constraint for "queue"

*/
template<class PCd, class PType> class C_HasHead {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// Head access
		[[maybe_unused]]	const PType& ct = const_ctd.head();
		[[maybe_unused]]	      PType&  t = ctd.head();

		// Pop head
		ctd.popHead();
		// Pull head value (and pop)
		t = ctd.pullHead();

	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_HasHead(void)
	{
		C_HasHead::ensure();
	}

};

} // N..

#endif
