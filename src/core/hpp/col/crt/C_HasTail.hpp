/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_HAS_TAIL_HPP_INCLUDED
#define	LIB_PCO_C_HAS_TAIL_HPP_INCLUDED

namespace NPCO {

/**
	Constraint for "stack"

*/
template<class PCd, class PType> class C_HasTail {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// Tail access
		[[maybe_unused]]	const PType& ct = const_ctd.tail();
		[[maybe_unused]]	      PType&  t = ctd.tail();

		// Pop tail
		ctd.popTail();
		// Pull tail value (and pop)
		t = ctd.pullTail();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_HasTail(void)
	{
		C_HasTail::ensure();
	}

};

} // N..

#endif
