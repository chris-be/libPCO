/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_SORTED_ASSOCIATION_COLLECTION_HPP_INCLUDED
#define	LIB_PCO_C_SORTED_ASSOCIATION_COLLECTION_HPP_INCLUDED

#include "pattern/bag/C_SortedIndexBag.hpp"
#include "mem/MemSettings.hpp"	// array_size

namespace NPCO {

/**
	Contract for association collections: key => value

*/
template<class PCd, class PKey, class PValue, class PSize = array_size>
class C_SortedAssociationCollection
	: private C_SortedIndexBag<PCd, PKey, PValue, PSize> {

public:
	using TKey			= PKey;
	using TValue		= PValue;
	using TSize			= PSize;

private:

public:

};

} // N..

#endif
