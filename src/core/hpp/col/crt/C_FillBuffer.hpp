/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_FILL_BUFFER_HPP_INCLUDED
#define	LIB_PCO_C_FILL_BUFFER_HPP_INCLUDED

#include "pattern/C_HasSize.hpp"

namespace NPCO {

/*!
	Contract for "Fill buffers" :
	- fixed size (can be resized)
	- gets filled and read (FIFO)

	Design:
		- there is a writePosition and a readPosition which evolves while adding / getting

	@param PType object type
	@param PSize type for size
 */
template<class PType, class PSize>
class IFillBuffer : private C_HasSize<IFillBuffer<PType, PSize>, PSize> {

public:
	using TType				= PType;
	using TSize				= PSize;

public:
	virtual ~IFillBuffer(void)	{ }

	//! @see C_HasSize
	virtual PSize	size(void) const = 0;

	//! @see C_HasSize
	virtual bool	isEmpty(void) const
	{
		return this->size() == 0;
	}

	//! Place "positions" at begining
	virtual void	reset(void) = 0;

	//! Resize buffer
	virtual void	resize(PSize size) = 0;

// Filling buffer with datas

	/** To know how many place left for writing (0 means buffer is FULL)
		@return Place left for writing
	*/
	virtual PSize	leftToWrite(void) const = 0;

	/** Copy pBuffer into Buffer (and adapt writePosition)
		@param pBuffer Buffer to read
		@param size Size of pBuffer (or size to add)
	*/
	virtual void	add(const PType* pBuffer, const PSize size) = 0;

//	/** Copy buffer into Buffer (and adapt writePosition)
//		@param buffer
//	*/
//	virtual void	add(const TBuffer& buffer) = 0;

// Reading datas in buffer

	//! Reset readPosition
	virtual void	resetRead(void) = 0;

	//! Test if something was already added
	virtual bool	isDirty(void) = 0;

	//! To know how many items can be read
	virtual PSize	leftToRead(void) const = 0;

	/** Copy from current readPosition into pBuffer (and adapt readPosition)
		@param pBuffer Buffer to fill
		@param size Size of pBuffer (or size to get)
	*/
	virtual void	get(PType* pBuffer, const PSize size) = 0;

//	/** Copy from current readPosition into into buffer (and adapt readPosition)
//		@param buffer
//	*/
//	virtual void	get(const TBuffer& buffer) = 0;

};

} // N..

#endif
