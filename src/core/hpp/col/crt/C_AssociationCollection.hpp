/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_ASSOCIATION_COLLECTION_HPP_INCLUDED
#define	LIB_PCO_C_ASSOCIATION_COLLECTION_HPP_INCLUDED

#include "crt/bag/C_IndexedBag.hpp"
#include "mem/MemSettings.hpp"

namespace NPCO {

/**
	Contract for association collections: key => value

*/
template<class PCd, class PKey, class PValue, class PSize = array_size> class C_AssociationCollection
	: public C_IndexedBag<PCd, PKey, PValue, PSize> {

private:

public:

};

} // N..

#endif
