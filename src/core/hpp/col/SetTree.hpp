/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_SET_TREE_HPP_INCLUDED
#define	LIB_PCO_SET_TREE_HPP_INCLUDED

#include "crt/C_UniquenessCollection.hpp"
#include "col/trees/AVLTree.hpp"
#include "cvs/EmptyClass.hpp"
#include "cvs/iter/Key_IterRead.hpp"

namespace NPCO {

/*!
	Set Tree
	Usage:
	- sorted items using binary tree

	Tech:
		@see AVLTree, BTree
*/
template<class PKey, class PClassifier = DefaultClassifier<PKey>
		, class PTree = AVLTree<PKey, EmptyClass, PClassifier>>
class SetTree
	: public C_UniquenessCollection<SetTree<PKey, PClassifier>, PKey, typename PTree::TSize> {

public:
	using TSize			= typename PTree::TSize;

	using TIterRead		= Key_IterRead<PKey, EmptyClass, typename PTree::TIterRead>;

protected:
	PTree		tree;

public:
	SetTree(void);
	SetTree(std::initializer_list<PKey> list);

	// Copy
	SetTree(const SetTree& toCopy) = default;
    SetTree& operator=(const SetTree& toCopy) = default;

	// Move
	SetTree(SetTree&& toMove) = default;
    SetTree& operator=(SetTree&& toMove) = default;

	//! @see C_HasSize
	PCO_INLINE
	TSize		size(void) const;

	//! @see C_HasSize
	PCO_INLINE
	bool		isEmpty(void) const;

	//! @see C_KeyBag
	PCO_INLINE
	void	add(const PKey& key);
	//! @see C_KeyBag
	PCO_INLINE
	void	moveIn(PKey&& key);

	//! @see C_KeyBag
	PCO_INLINE
	bool	hasKey(const PKey& key) const;

	//! @see C_KeyBag TODO nearest...
	PCO_INLINE
	TIterRead	find(const PKey& key) const;

	//! @see C_KeyBag
	PCO_INLINE
	void	remove(const PKey& key);

	//! @see C_KeyBag
	PCO_INLINE
	void	clear(void);

	//! Find min
	PCO_INLINE
	const PKey*	min(void) const;

	//! Find max
	PCO_INLINE
	const PKey*	max(void) const;

	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead	first(void) const;
	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead	last(void) const;
	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead	iter_r(const PKey& key) const;

};

} // N..

#include "imp/SetTree.cppi"

#endif