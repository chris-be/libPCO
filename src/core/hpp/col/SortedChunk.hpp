/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_SORTED_CHUNK_HPP_INCLUDED
#define	LIB_PCO_SORTED_CHUNK_HPP_INCLUDED

#include "pattern/bag/C_KeyBag.hpp"
#include "col/FixedHeap.hpp"
#include "cvs/cfr/DefaultClassifier.hpp"
#include "logic/FindResult.hpp"

#include "cvs/use/UseMemoryManager.hpp"
#include "cvs/use/UseClassifier.hpp"

namespace NPCO {

/*!
	This template keeps items sorted as they are added/removed in a heap
	Use it when you need sorted data chunks
*/
template<class PType, class PClassifier = DefaultClassifier<PType>
		, class PMemMgr = SimpleMemoryManager> class SortedChunk
	: public C_KeyBag<SortedChunk<PType, PClassifier>, PType, typename FixedHeap<PType, PMemMgr>::TSize>
	, public UseClassifier<PClassifier> {

public:
	using TSize			= typename FixedHeap<PType, PMemMgr>::TSize;
	using TIterRead		= typename FixedHeap<PType, PMemMgr>::TIterRead;
	using TFindResult	= FindResult<TSize>;

protected:
	FixedHeap<PType, PMemMgr>	heap;

public:
	SortedChunk(void);
	SortedChunk(const TSize maxSize);

	// Copy
	SortedChunk(const SortedChunk& toCopy) = default;
    SortedChunk& operator=(const SortedChunk& toCopy) = default;

	// Move
	SortedChunk(SortedChunk&& toMove) = default;
    SortedChunk& operator=(SortedChunk&& toMove) = default;

	//! Reserved size of chunk
	TSize	maxSize(void) const;

	//! Resize chunk with specified size
	void	resize(const TSize maxSize);

	//! @see C_HasSize
	TSize	size(void) const;

	//! @see C_HasSize
	bool	isEmpty(void) const;

	//! @see C_KeyBag
	void	clear(void);

	/*! Find an item
		@param[out] findResult
	*/
	PCO_INLINE
	void	find(TFindResult& findResult, const PType& toFind) const;

	//! @see C_KeyBag
	PCO_INLINE
	bool	hasKey(const PType& toFind) const;

	//! @see C_KeyBag
	void	add(const PType& toCopy);
	void	moveIn(PType&& toMove);

	//! Remove item at index
	PCO_INLINE
	void	removeAt(const TSize i);

	/*! Remove an item
		@return true if item was removed
	*/
	bool	remove(const PType& toRemove);

	/** Remove item at index (pull remaining items) - note: makes no sense to moveOut a value
		@return Removed item
	*/
	PType	moveOut(const TSize i);

	//! Access to the ith element
	PCO_INLINE
	const PType&	operator[](const TSize i) const;

	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead	first(void) const;
	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead	last(void) const;

	//! @see C_KeyBag
	PCO_INLINE
	TIterRead	iter_r(const PType& toFind) const;

	//! Get pointer - Do not free !!
	PCO_INLINE
	const PType*	_da(void) const;

};

} // N..

#include "imp/SortedChunk.cppi"

#endif