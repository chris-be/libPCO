/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FIXED_HEAP_HPP_INCLUDED
#define	LIB_PCO_FIXED_HEAP_HPP_INCLUDED

#include "pattern/bag/C_RowDynBag.hpp"
#include "mem/Array.hpp"
#include "cvs/use/UseMemoryManager.hpp"

namespace NPCO {

/*!
	This template provides a fixed array which is filled at demand
	Use it when you need to reserve some space and put data in it
*/
template<class PType, class PMemMgr = SimpleMemoryManager> class FixedHeap
	: private C_RowDynBag<FixedHeap<PType, PMemMgr>, PType, array_size>
	, public UseMemoryManager<PMemMgr> {

protected:
	using TArray		= Array<PType, array_size, PMemMgr>;

public:
	using TSize			= typename TArray::TSize;
	using TType			= typename TArray::TType;
	using TIterRead		= typename TArray::TIterRead;
	using TIterWrite	= typename TArray::TIterWrite;

protected:
	//! Array in memory (its size is defined as "maxSize")
	TArray		array;

	//! Number of currently items
	TSize		nbItems;

	//! Returns if index is valid
	PCO_INLINE
	bool	isInBounds(const TSize i) const;

	/** Shift to right values
		@param i Index to start
		@param delta How many shifts to do
	*/
	void shiftRight(const TSize i, const TSize delta);

	/** Shift to left a block of values
		@param i Index to start
		@param delta How many shifts to do
	*/
	void shiftLeft(const TSize i, const TSize delta);

	//! Add an empty cell
	TSize	prepareNew(void);

	//! Insert an empty cell
	void	prepareNew(const TSize i);

public:
	FixedHeap(void);

	/*!
		@param maxSize Max number of items (and reserved space)
	*/
	FixedHeap(const TSize maxSize);

	// Copy
	FixedHeap(const FixedHeap& toCopy) = default;
    FixedHeap& operator=(const FixedHeap& toCopy) = default;

	// Move
	FixedHeap(FixedHeap&& toMove) = default;
    FixedHeap& operator=(FixedHeap&& toMove) = default;

	//! Reserved size of chunk
	TSize	maxSize(void) const;

	//! Resize chunk with specified size
	void	resize(const TSize maxSize);

	//! @see C_HasSize
	TSize	size(void) const;

	//! @see C_HasSize
	bool	isEmpty(void) const;

	//! @see C_DynBag
	void	clear(void);

	//! @see C_DynBag
	void	add(const PType& toCopy);
	void	moveIn(PType&& toMove);

	//! @see C_DynBag
//	void	remove(const TIterWrite& it_w);
//	void	remove(const TIterWrite& low, const TIterWrite& high);

	//! Add an item (push items farther)
	void	add(const PType& toCopy, const TSize i);
	void	moveIn(PType&& toMove, const TSize i);

	//! Remove item at index (pull remaining items)
	void	removeAt(const TSize i);

	/** Remove item at index (pull remaining items)
		@return Removed item
	*/
	PType	moveOut(const TSize i);

	//! Access to the ith element
	PCO_INLINE
	const PType&	operator[](const TSize i) const;

	//! Access to the ith element
	PCO_INLINE
	PType&			operator[](const TSize i);

	//! @see C_HasIterRead
	TIterRead		first(void) const;
	//! @see C_HasIterRead
	TIterRead		last(void) const;

	//! @see C_HasIterWrite
	TIterWrite		first(void);
	//! @see C_HasIterWrite
	TIterWrite		last(void);

	//! @see C_RowDynBag
	PCO_INLINE
	TIterRead		iter_r(const TSize i) const;
	//! @see C_RowDynBag
	PCO_INLINE
	TIterWrite		iter_w(const TSize i);

	//! Get pointer - Do not free !!
	const PType*	_da(void) const;

	//! Get pointer - Do not free !!
	PType*			_da(void);

};

} // N..

#include "imp/FixedHeap.cppi"

#endif
