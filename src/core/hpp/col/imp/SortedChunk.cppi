/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

// #include	"col/SortedChunk.hpp"
#include "logic/cvs/CVS_Find.hpp"

namespace NPCO {

template<class PType, class PClassifier, class PMemMgr>
SortedChunk<PType, PClassifier, PMemMgr>::SortedChunk(void)
{	}

template<class PType, class PClassifier, class PMemMgr>
SortedChunk<PType, PClassifier, PMemMgr>::SortedChunk(const TSize maxSize)
 : heap(maxSize)
{	}

template<class PType, class PClassifier, class PMemMgr>
auto	SortedChunk<PType, PClassifier, PMemMgr>::maxSize(void) const -> TSize
{
	return this->heap.maxSize();
}

template<class PType, class PClassifier, class PMemMgr>
void	SortedChunk<PType, PClassifier, PMemMgr>::resize(const TSize maxSize)
{
	this->heap.resize(maxSize);
}

template<class PType, class PClassifier, class PMemMgr>
auto	SortedChunk<PType, PClassifier, PMemMgr>::size(void) const -> TSize
{
	return this->heap.size();
}

template<class PType, class PClassifier, class PMemMgr>
bool			SortedChunk<PType, PClassifier, PMemMgr>::isEmpty(void) const
{
	return this->heap.isEmpty();
}

template<class PType, class PClassifier, class PMemMgr>
void			SortedChunk<PType, PClassifier, PMemMgr>::clear(void)
{
	return this->heap.clear();
}

template<class PType, class PClassifier, class PMemMgr>
void	SortedChunk<PType, PClassifier, PMemMgr>::find(TFindResult& findResult, const PType& toFind) const
{
	// Find out index
	CVS_Find::logFind(findResult, this->heap._da(), this->heap.size(), this->classifier, toFind);
}

template<class PType, class PClassifier, class PMemMgr>
bool	SortedChunk<PType, PClassifier, PMemMgr>::hasKey(const PType& toFind) const
{
	FindResult<TSize> findResult;
	this->find(findResult, toFind);

	return findResult.found();
}

template<class PType, class PClassifier, class PMemMgr>
void	SortedChunk<PType, PClassifier, PMemMgr>::add(const PType& toAdd)
{
	// Find out index
	FindResult<array_size> findResult;
	this->find(findResult, toAdd);

	this->heap.add(toAdd, findResult.index());
}

template<class PType, class PClassifier, class PMemMgr>
void	SortedChunk<PType, PClassifier, PMemMgr>::moveIn(PType&& toMove)
{
	// Find out index
	FindResult<array_size> findResult;
	this->find(findResult, toMove);

	this->heap.moveIn(std::move(toMove), findResult.index());
}

template<class PType, class PClassifier, class PMemMgr>
void	SortedChunk<PType, PClassifier, PMemMgr>::removeAt(const TSize i)
{
	this->heap.removeAt(i);
}

template<class PType, class PClassifier, class PMemMgr>
bool	SortedChunk<PType, PClassifier, PMemMgr>::remove(const PType& toRemove)
{
	// Find out index
	FindResult<TSize> findResult;
	this->find(findResult, toRemove);

	if(findResult.found())
	{
		this->removeAt(findResult.index());
	}

	return findResult.found();
}

template<class PType, class PClassifier, class PMemMgr>
PType	SortedChunk<PType, PClassifier, PMemMgr>::moveOut(const TSize i)
{
	return this->heap.moveOut(i);
}

template<class PType, class PClassifier, class PMemMgr>
const PType&	SortedChunk<PType, PClassifier, PMemMgr>::operator[](const TSize i) const
{
	return this->heap.operator[](i);
}

template<class PType, class PClassifier, class PMemMgr>
auto	SortedChunk<PType, PClassifier, PMemMgr>::first(void) const -> TIterRead
{
	return this->heap.first();
}

template<class PType, class PClassifier, class PMemMgr>
auto SortedChunk<PType, PClassifier, PMemMgr>::last(void) const -> TIterRead
{
	return this->heap.last();
}

template<class PType, class PClassifier, class PMemMgr>
auto SortedChunk<PType, PClassifier, PMemMgr>::iter_r(const PType& toFind) const -> TIterRead
{
	// Find out index
	FindResult<TSize> findResult;
	this->find(findResult, toFind);

	if(findResult.found())
	{
		return this->heap.iter_r(findResult.index());
	}

	return TIterRead();
}

template<class PType, class PClassifier, class PMemMgr>
const PType*	SortedChunk<PType, PClassifier, PMemMgr>::_da(void) const
{
	return this->heap._da();
}

} // N..
