/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

// #include	"col/MapTree.hpp"

namespace NPCO {

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
MapTreeList<PKey, PValue, PClassifier, PList, PTree>::MapTreeList(void)
 : nbItems(0)
{	}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
MapTreeList<PKey, PValue, PClassifier, PList, PTree>::MapTreeList(std::initializer_list<TInitPair> list)
 : MapTreeList()
{
	for(auto i = list.begin(), e = list.end() ; i != e ; ++i)
	{
		this->add(i->first, i->second);
	}
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::size(void) const -> TSize
{
	return this->nbItems;
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline bool		MapTreeList<PKey, PValue, PClassifier, PList, PTree>::isEmpty(void) const
{
	return this->tree.isEmpty();
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline void	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::add(const PKey& key, const PValue& value)
{
	PList& list = this->add(key).cell();
	list.add(value);
	++this->nbItems;
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline void	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::moveIn(const PKey& key, PValue&& value)
{
	PList& list = this->add(key).cell();
	list.moveIn(std::move(value));
	++this->nbItems;
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::add(const PKey& key) -> TMapIterWrite
{
	return this->tree.add(key);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline bool	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::hasKey(const PKey& key) const
{
	return this->tree.hasKey(key);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline void	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::remove(const PKey& key)
{
	auto it = this->tree.iter_r(key);
	if(it.isValid())
	{
		this->nbItems-= it.cell().size();
	}
	this->tree.remove(key);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline void	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::clear(void)
{
	this->tree.clear();
	this->nbItems = 0;
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::first(void) const -> TIterRead
{
	return TIterRead(this->tree.first(), false);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::last(void) const -> TIterRead
{
	return TIterRead(this->tree.last(), true);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::iter_r(const PKey& key) const -> TIterRead
{
	return TIterRead(this->tree.iter_r(key), false);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::first(void) -> TIterWrite
{
	return TIterWrite(this->tree.first(), false);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::last(void) -> TIterWrite
{
	return TIterWrite(this->tree.last(), true);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::iter_w(const PKey& key) -> TIterWrite
{
	return TIterWrite(this->tree.iter_w(key), false);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::map_iter_r(const PKey& key) const -> TMapIterRead
{
	return this->tree.iter_r(key);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
auto	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::map_iter_w(const PKey& key) -> TMapIterWrite
{
	return this->tree.iter_w(key);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline const PList&	MapTreeList<PKey, PValue, PClassifier, PList, PTree>::operator[](const PKey& key) const
{
	return this->tree.operator[](key);
}

template<class PKey, class PValue, class PClassifier, class PList, class PTree>
inline PList&		MapTreeList<PKey, PValue, PClassifier, PList, PTree>::operator[](const PKey& key)
{
	return this->tree.operator[](key);
}

} // N..
