/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MAP_TREE_HPP_INCLUDED
#define	LIB_PCO_MAP_TREE_HPP_INCLUDED

#include "crt/C_SortedAssociationCollection.hpp"
#include "trees/AVLTree.hpp"

namespace NPCO {

/*!
	Map Tree
	Usage: sorted keys using binary tree with value associated

*/
template<class PKey, class PValue, class PClassifier = DefaultClassifier<PKey>
		, class PTree = AVLTree<PKey, PValue, PClassifier>> class MapTree
	: public C_SortedAssociationCollection<MapTree<PKey, PValue, PClassifier, PTree>, PKey, PValue, typename PTree::TSize> {

public:
//	using TKey			= PKey;
//	using TValue		= PValue;
	using TClassifier	= PClassifier;

	using TSize			= typename PTree::TSize;
	using TIterRead		= typename PTree::TIterRead;
	using TIterWrite	= typename PTree::TIterWrite;
	using TInitPair		= std::pair<const PKey, PValue>;

protected:
	PTree		tree;

public:
	MapTree(void);
	MapTree(std::initializer_list<TInitPair> list);

	// Copy
	MapTree(const MapTree& toCopy) = default;
	MapTree& operator=(const MapTree& toCopy) = default;

	// Move
	MapTree(MapTree&& toMove) = default;
	MapTree& operator=(MapTree&& toMove) = default;

	//! @see C_HasSize
	PCO_INLINE
	TSize	size() const;

	//! @see C_HasSize
	PCO_INLINE
	bool	isEmpty() const;

	//! @see C_IndexedBag
	PCO_INLINE
	void	clear(void);

	//! @see C_IndexedBag
	PCO_INLINE
	void	add(const PKey& key, const PValue& value);

	//! @see C_IndexedBag
	PCO_INLINE
	void	moveIn(const PKey& key, PValue&& value);

	//! Add node with undefined value TODO
	PCO_INLINE
	TIterWrite	add(const PKey& key);

	//! @see C_IndexedBag
	PCO_INLINE
	bool	hasKey(const PKey& key) const;

	//!	@see C_IndexedBag TODO: findNearest ...
	PCO_INLINE
	TIterRead	find(const PKey& key) const;
	//!	@see C_IndexedBag
	PCO_INLINE
	TIterWrite	find(const PKey& key);

	//! @see C_IndexedBag
	PCO_INLINE
	const PValue&	operator[](const PKey& key) const;

	//! @see C_IndexedBag
	PCO_INLINE
	PValue&			operator[](const PKey& key);

	//! @see C_IndexedBag
	PCO_INLINE
	void	remove(const PKey& key);

	//! Find min
	PCO_INLINE
	const PKey*	min(void) const;

	//! Find max
	PCO_INLINE
	const PKey*	max(void) const;

	//! @see C_HasIndexIterRead
	PCO_INLINE
	TIterRead	first(void) const;
	//! @see C_HasIndexIterRead
	PCO_INLINE
	TIterRead	last(void) const;
	//! @see C_HasIndexIterRead
	PCO_INLINE
	TIterRead	iter_r(const PKey& key) const;
	//! @see C_SortedIndexBag
	PCO_INLINE
	TIterRead	iter_r_min(const PKey& key) const;
	//! @see C_SortedIndexBag
	PCO_INLINE
	TIterRead	iter_r_max(const PKey& key) const;

	//! @see C_HasIndexIterWrite
	PCO_INLINE
	TIterWrite	first(void);
	//! @see C_HasIndexIterWrite
	PCO_INLINE
	TIterWrite	last(void);
	//! @see C_HasIndexIterWrite
	PCO_INLINE
	TIterWrite	iter_w(const PKey& key);

};

} // N..

#include "imp/MapTree.cppi"

#endif
