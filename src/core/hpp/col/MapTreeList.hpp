/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_MAP_TREE_LIST_HPP_INCLUDED
#define	LIB_PCO_MAP_TREE_LIST_HPP_INCLUDED

#include "MapTree.hpp"
#include "ChunkList.hpp"
#include "./iter/MapTreeList_IterRead.hpp"
#include "./iter/MapTreeList_IterWrite.hpp"

namespace NPCO {

/*!
	Map Tree List
	Usage: sorted keys using binary tree with multiple values associated

*/
template<class PKey, class PValue, class PClassifier = DefaultClassifier<PKey>
		, class PList = ChunkList<PValue>
		, class PTree = AVLTree<PKey, PList, PClassifier>> class MapTreeList
	// : public C_AssociationCollection<MapTree<PKey, PValue, PClassifier, PTree>, PKey, PValue>
 {

public:
	typedef	PList							TList;
	typedef	typename PList::TSize			TSize;
	typedef	typename PTree::TIterRead		TMapIterRead;
	typedef	typename PTree::TIterWrite		TMapIterWrite;
	typedef std::pair<const PKey, PValue>	TInitPair;

	typedef	MapTreeList_IterRead<PKey, PValue, PTree, PList>	TIterRead;
	typedef	MapTreeList_IterWrite<PKey, PValue, PTree, PList>	TIterWrite;

protected:
	PTree		tree;
	//! Count lists elements
	TSize		nbItems;

public:
	MapTreeList(void);
	MapTreeList(std::initializer_list<TInitPair> list);

	// Copy
	MapTreeList(const MapTreeList& ) = default;
    MapTreeList& operator=(const MapTreeList& ) = default;

	// Move
	MapTreeList(MapTreeList&& ) = default;
    MapTreeList& operator=(MapTreeList&& ) = default;

	//! @see C_HasSize
	TSize	size() const;

	//! @see C_HasSize
	bool	isEmpty() const;

	//! @see C_IndexedBag
	void	add(const PKey& key, const PValue& value);

	//! @see C_IndexedBag
	void	moveIn(const PKey& key, PValue&& value);

	// Add node with undefined value TODO
	TMapIterWrite	add(const PKey& key);

	//! @see C_IndexedBag
	bool	hasKey(const PKey& key) const;

	//! @see C_IndexedBag
	void	remove(const PKey& key);

	//! @see C_IndexedBag
	void	remove(const PKey& key, const PValue& value);

	//! @see C_IndexedBag
	void	clear(void);

	//! @see C_HasIndexIterRead
	TIterRead	first(void) const;
	//! @see C_HasIndexIterRead
	TIterRead	last(void) const;
	//! @see C_HasIndexIterRead - first of list
	TIterRead	iter_r(const PKey& key) const;

	//! @see C_HasIndexIterWrite
	TIterWrite	first(void);
	//! @see C_HasIndexIterWrite
	TIterWrite	last(void);
	//! @see C_HasIndexIterWrite
	TIterWrite	iter_w(const PKey& key);

	//! @see C_HasIndexIterRead - first of list
	TMapIterRead	map_iter_r(const PKey& key) const;
	//! @see C_HasIndexIterWrite
	TMapIterWrite	map_iter_w(const PKey& key);

	const PList&	operator[](const PKey& key) const;
	PList&			operator[](const PKey& key);

};

} // N..

#include "imp/MapTreeList.cppi"

#endif
