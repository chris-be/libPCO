/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHUNK_LIST_HPP_INCLUDED
#define	LIB_PCO_CHUNK_LIST_HPP_INCLUDED

#include "crt/C_Collection.hpp"
// #include "crt/bag/C_DynBag.hpp"

#include "crt/C_HasTail.hpp"
#include "crt/C_HasHead.hpp"
#include "mem/MemSettings.hpp"
#include "mem/mgr/SimpleMemoryManager.hpp"
#include "cvs/use/UseMemoryManager.hpp"

namespace NPCO {

template<class PCell, class PChunkList> class ChunkList_Iter;
template<class PCell, class PChunkList> class ChunkList_IterRead;
template<class PCell, class PChunkList> class ChunkList_IterWrite;

/*!
	Double linked list of chunks
	Usage: simple storage, just iterate (ex: temporary results)

	Tech:
		Each chunk is a memory block which contains :
			- pointer to previous chunk (if exists)
			- pointer to next chunk (if exits)
			- reserved space for items

				[prev][next][i1][i2][i3]...[in]

		First chunk has indexFirstItem to be able to remove from front
		Last chunk has indexLastItem to be able to handle growing/shrinking
		Any intermediate chunk is full.
*/
template<class PType, class PMemMgr = SimpleMemoryManager> class ChunkList
	: public C_Collection<ChunkList<PType, PMemMgr>, PType>
	, private C_HasTail<ChunkList<PType, PMemMgr>, PType>
	, private C_HasHead<ChunkList<PType, PMemMgr>, PType>
	, public UseMemoryManager<PMemMgr> {

public:
	static const chunk_size		DefaultChunkSize;

protected:
	class InnerChunk {
	public:
		InnerChunk*	next;
		InnerChunk*	previous;
		PType*		data;
	};

	friend class ChunkList_Iter<const PType, const ChunkList>;
	friend class ChunkList_Iter<PType, ChunkList>;

public:
	//using TType			= PType;
	using TSize			= typename C_Collection<ChunkList<PType, PMemMgr>, PType, array_size>::TSize;
	using TIterRead		= ChunkList_IterRead<PType, ChunkList>;
	using TIterWrite	= ChunkList_IterWrite<PType, ChunkList>;

protected:
	chunk_size		chunkSize;
	//!
	InnerChunk*		firstChunk;
	chunk_size		indexFirstItem;

	InnerChunk*		lastChunk;
	chunk_size		indexLastItem;

	//! Number of currently items
	TSize			nbItems;

	//! Returns if index is in list
	PCO_INLINE
	bool		isInBounds(const TSize i) const;

	InnerChunk*	createInnerChunk(void);

	void		deleteInnerChunk(InnerChunk* pChunk);

	//! Initialize as empty (doesn't release anything !!)
	void	setEmpty(void);

	/** Grow list at tail
		@return Prepared cell reference
	*/
	PType&	append(void);

	/** Grow list at head
		@return Prepared cell reference
	*/
//	PType&	prepend(void);

public:
	ChunkList(chunk_size chunkSize = DefaultChunkSize);
	// ChunkList(std::initializer_list<PType> list, chunk_size chunkSize = DefaultChunkSize);
	~ChunkList(void);

	// Copy
	ChunkList(const ChunkList& toCopy);
    ChunkList& operator=(const ChunkList& toCopy);

	// Move
	ChunkList(ChunkList&& toMove);
    ChunkList& operator=(ChunkList&& toMove);

	//! @see C_HasSize
	TSize	size(void) const;

	//! @see C_HasSize
	bool	isEmpty(void) const;

	//! @see C_DynBag
	void	clear(void);

	//! @see C_DynBag
	PCO_INLINE
	void	add(const PType& toAdd);

	//! @see C_DynBag
	PCO_INLINE
	void	moveIn(PType&& toMove);

	//! @see C_HasTail
	PCO_INLINE
	const PType&	tail(void) const;
	PCO_INLINE
	PType&			tail(void);

	//! @see C_HasTail
	void			popTail(void);
	PCO_INLINE
	PType			pullTail(void);

/*
	//! C_Stack
	void			addTail(const PType& toCopy),
	void			moveInTail(PType&& toMove)
	//! C_Queue
	void			addHead(const PType& toCopy);
	void			moveInHead(PType& toMove);
*/

	//! @see C_HasHead
	PCO_INLINE
	const PType&	head(void) const;
	//! @see C_HasHead
	PCO_INLINE
	PType&			head(void);

	//! @see C_HasHead
	void			popHead(void);
	//! @see C_HasHead
	PCO_INLINE
	PType			pullHead(void);

	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead		first(void) const;
	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead		last(void) const;

	//! @see C_HasIterWrite
	PCO_INLINE
	TIterWrite		first(void);
	//! @see C_HasIterWrite
	PCO_INLINE
	TIterWrite		last(void);

	//! @see C_DynBag
//	void	remove(const TIterWrite& it_w);
//	void	remove(const TIterWrite& low, const TIterWrite& high);

	//! @see C_RowBag
	const PType&	operator[](const TSize i) const;
	//! @see C_RowBag
	PType&			operator[](const TSize i);

	//! @see C_RowBag
	TIterRead		iter_r(const TSize i) const;
	//! @see C_RowBag
	TIterWrite		iter_w(const TSize i);

};

} // N..

#include "imp/ChunkList.cppi"

#endif
