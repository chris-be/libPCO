/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PROGRAM_ARGS_CONFIG_HPP_INCLUDED
#define	LIB_PCO_PROGRAM_ARGS_CONFIG_HPP_INCLUDED

#include "OperandConfig.hpp"
#include "OptionConfig.hpp"
#include "col/MapTree.hpp"
#include "string/String.hpp"
#include "math/MathClassifier.hpp"

namespace NProgram {

/**
	Program command line arguments configuration.
		- parse and check arguments
*/
class ArgsConfig {

public:
	class OperandXOption : public NPCO::C_SelfClassified<OperandXOption> {
	public:
		int		idOperand;
		int		idOption;

		OperandXOption(void) = default;
		OperandXOption(int idOperand, int idOption);

		//! @see C_SelfClassified
		NPCO::ClassifyValue	classify(const OperandXOption& toCompare) const;
	};

protected:
	using TId2Option			= NPCO::MapTree<int, OptionConfig, NPCO::MathClassifier<int>>;

	using TLetter2IdOption		= NPCO::MapTree<char, int, NPCO::MathClassifier<char>>;
	using TName2IdOption		= NPCO::MapTree<ArgName, int, ArgNameClassifier>;

	using TName2Operand			= NPCO::MapTree<ArgName, OperandConfig, ArgNameClassifier>;
	using TOperandXOptions		= NPCO::MapTree<OperandXOption, bool>;

public:
	using TId2OptionIter		= typename TId2Option::TIterRead;

	// using TLetter2IdOptionIter	= typename TLetter2IdOption::TIterRead;
	// using TName2IdOptionIter	= typename TName2IdOption::TIterRead;

	using TOperandIter			= typename TName2Operand::TIterRead;
	using TXOptionsIter			= typename TOperandXOptions::TIterRead;

protected:
	//! Index options by id
	TId2Option			idOptions;
	//! Index idOption by letter
	TLetter2IdOption	letterOptions;
	//! Name options
	TName2IdOption		nameOptions;
	//! Operand
	TName2Operand		nameOperands;
	//! Operand options association
	TOperandXOptions	operandXOptions;

public:
	ArgsConfig(void);

	void	add(const OperandConfig& operand);
	//! Find operand
	TOperandIter		operand(const ArgName& name) const;

	void	add(const OptionConfig& option);
	//! Find option by id
	TId2OptionIter		optionById(const int id) const;
	//! Find (short) option
	TId2OptionIter		optionByLetter(const char letter) const;
	//! Find option
	TId2OptionIter		optionByName(const ArgName& name) const;

	//! Add option to operand
	void	link(int idOperand, int idOption, bool mandatory);

	//! Add options - @convenience
	void	link(int idOperand, bool mandatory, std::initializer_list<int> idOptions);

	//! Add option to operand
	TXOptionsIter	getLink(int idOperand, int idOption) const;

	//! Check if configuration is valid (ex: no operand use undefined option)
	bool	isValid(void) const;

	//! Show "basic" usage
	void	showUsage(std::ostream& os, const char* programName) const;

	//! Show operand usage
	void	showOperand(std::ostream& os, const char* operand) const;

	//! Add option @convenience
	template<typename... Args>
	void	addOption(Args&&... args)
	{
		OptionConfig o(std::forward<Args>(args)...);
		this->add(o);
	}

	//! Add operand @convenience
	template<typename... Args>
	void	addOperand(Args&&... args)
	{
		OperandConfig o(std::forward<Args>(args)...);
		this->add(o);
	}

};

} // NProgram

#endif
