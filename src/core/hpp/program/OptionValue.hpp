/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PROGRAM_OPTION_VALUE_HPP_INCLUDED
#define	LIB_PCO_PROGRAM_OPTION_VALUE_HPP_INCLUDED

#include "OptionConfig.hpp"
#include "string/String.hpp"

namespace NProgram {

/**
	Option and associated value(s).
*/
class OptionValue {

public:
	using TArgList		= TList<NPCO::String>;

protected:
	//! Option detected
	OptionConfig	config;
	//! Values defined
	TArgList		values;

public:
	OptionValue(void) = default;
	OptionValue(const OptionConfig& config);

	const OptionConfig&	getConfig(void) const;

	void				addValue(const char* value);
	const TArgList&		getValues(void) const;

	// @convenience for option that take only one value
	const NPCO::String&	getValue(void) const;

};

} // NProgram

#endif
