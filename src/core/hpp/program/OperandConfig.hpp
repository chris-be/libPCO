/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PROGRAM_OPERAND_CONFIG_HPP_INCLUDED
#define	LIB_PCO_PROGRAM_OPERAND_CONFIG_HPP_INCLUDED

#include "ProgramType.hpp"

namespace NProgram {

/**
	Configuration for program operand.
		- action program must take (only one action can be run)
*/
class OperandConfig {

protected:
	//! Id for easy "switch"
	int			id;
	//! Operand name
	ArgName		name;

public:
	OperandConfig(void) = default;
	OperandConfig(int id , ArgName name);

	int				getId(void) const;
	const ArgName&	getName(void) const;

};

} // NProgram

#endif
