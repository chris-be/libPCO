/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_ACTION_REPORT_HPP_INCLUDED
#define	LIB_PCO_ACTION_REPORT_HPP_INCLUDED

#include "string/String.hpp"
#include "col/ChunkList.hpp"

namespace NPCO {

/**
	Action report:
		- warnings
		- errors
*/
class ActionReport {

public:
	using TMessage			= NPCO::IStringCarrier;
	using TextList			= ChunkList<NPCO::String>;

	//! Level of info carried
	enum ELevel {
		  nothing	= 0
		// , message ??
		, warning
		, error
	};

protected:
	//! Warnings
	TextList		warnings;
	//! Errors
	TextList		errors;

public:
	ActionReport(void) = default;

	void	clear(void);

	//! Find highest level
	ELevel	evalLevel(void) const;

	void	addWarning(const TMessage& msg);
	const TextList&		getWarnings(void) const;
	bool	hasWarning(void) const;

	void	addError(const TMessage& msg);
	const TextList&		getErrors(void) const;
	bool	hasError(void) const;

};

} // N..

#endif
