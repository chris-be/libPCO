/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_EXEC_COMMAND_HPP_INCLUDED
#define	LIB_PCO_EXEC_COMMAND_HPP_INCLUDED

#include "string/String.hpp"
#include "col/Store.hpp"
#include "low/Process.hpp"
#include "mem/Reference.hpp"

namespace NPCO {

/*!
	Simplify use of Process.
 */
class ExecCommand {

protected:
	using TName		= NPCO::String;
	using TArgs		= Store<TName>;

protected:
	//! Full path with executable name
	TName		exec;
	//! Deduced (or defined) program name - arg0
	TName		arg0;
	//! Arguments
	TArgs		args;

	Reference<Process>	process;

public:
	ExecCommand(void);
	ExecCommand(const IStringCarrier& exec);
	ExecCommand(const IStringCarrier& exec, const IStringCarrier& arg0);

	// Copy
	ExecCommand(const ExecCommand& ) = default;
	ExecCommand& operator=(const ExecCommand& ) = default;

	// Move
	ExecCommand(ExecCommand&& ) = default;
	ExecCommand& operator=(ExecCommand&& ) = default;

	const TName&	getExec(void) const;
	const TName&	getArg0(void) const;
	const TArgs&	getArgs(void) const;

	void	addArg(const IStringCarrier& arg);

	void	clearArgs(void);

	//! @see Process
	bool	execute(bool usePath, bool pipeStdOut, bool pipeStdErr);

	//! @see Process
	bool	isValid(void) const;
	//! @see Process
	int		getExitStatus(void) const;
	//! @see Process
	void	wait(void);

	void	readStdOut(String& out);
	void	readStdErr(String& err);

};

} // N..

#endif
