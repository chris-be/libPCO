/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PROGRAM_TYPE_HPP_INCLUDED
#define	LIB_PCO_PROGRAM_TYPE_HPP_INCLUDED

#include "col/ChunkList.hpp"
#include "string/cfr/CStringClassifier.hpp"

namespace NProgram {

/**
	Type used for program argument.
*/
typedef const char*					ArgName;
typedef NPCO::CStringClassifier		ArgNameClassifier;

template<class PType>
using TList		= NPCO::ChunkList<PType>;

} // NProgram

#endif
