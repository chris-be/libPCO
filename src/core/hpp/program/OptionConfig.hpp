/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PROGRAM_OPTION_CONFIG_HPP_INCLUDED
#define	LIB_PCO_PROGRAM_OPTION_CONFIG_HPP_INCLUDED

#include "ProgramType.hpp"
#include "logic/Capsule.hpp"

namespace NProgram {

/**
	Configuration for program option.
*/
class OptionConfig {

public:
	using TLetter	= NPCO::Capsule<char>;

	// enum class EType {	};

protected:
	//! Id for easy "switch"
	int			id;
	//! One letter option - optional
	TLetter		letter;
	//! Complete name - optional
	ArgName		name;
	//! Flag if option takes value(s)
	bool		value;

public:
	OptionConfig(void) = default;
	OptionConfig(int id, bool value, char letter, ArgName name);
	OptionConfig(int id, bool value, ArgName name);

	int				getId(void) const;

	const TLetter&	getLetter(void) const;

	const ArgName&	getName(void) const;

	bool	hasValue(void) const;

};

} // NProgram

#endif
