/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PROGRAM_COMMAND_HPP_INCLUDED
#define	LIB_PCO_PROGRAM_COMMAND_HPP_INCLUDED

#include "OperandConfig.hpp"
#include "OptionConfig.hpp"
#include "OptionValue.hpp"
#include "col/MapTreeList.hpp"
#include "math/MathClassifier.hpp"

namespace NProgram {

/**
	Program command.
*/
class ProgramCommand {

public:
	//! Index options by id
	using TOptionMap		= NPCO::MapTreeList<int, OptionValue, NPCO::MathClassifier<int>>;
	using TSubArgList		= TList<NPCO::String>;

protected:
	//! Operand to use
	OperandConfig		operand;
	//! Options to use
	TOptionMap			options;
	//! Sub arguments to use
	TSubArgList			subArgs;

public:
	// ProgramCommand(void) = default;

	const OperandConfig&	getOperand(void) const;
	void					setOperand(const OperandConfig& operand);

	void	clear(void);

	void	addOption(const OptionValue& value);
	const TOptionMap&		getOptions(void) const;

	//! Check if option value is set
	bool					hasOptionValue(int id) const;
	//! Get value for option
	const NPCO::String&		getOptionValue(int id) const;

	void					addSubArg(const ArgName& subArg);
	const TSubArgList&		getSubArgs(void) const;

	//! Show command
	void	show(std::ostream& os) const;

};

} // NProgram

#endif
