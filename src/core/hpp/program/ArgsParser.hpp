/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_PROGRAM_ARGS_PARSER_HPP_INCLUDED
#define	LIB_PCO_PROGRAM_ARGS_PARSER_HPP_INCLUDED

#include "ArgsConfig.hpp"
#include "ProgramCommand.hpp"
#include "ActionReport.hpp"

namespace NProgram {

using namespace NPCO;

/**
	Parse arguments and create command.
*/
class ArgsParser {

protected:
	enum class EMode {
		  option		// Parse option
		, operand		// In case of --
		, subArg		// After operand
	};

	class ParseState {
	public:
		//! Current mode
		EMode		mode;
		//! Current argument
		char* const	*arg;
		//! Last argument
		char* const	*lastArg;

		//! Current arg is ok to read
		bool ok(void) const;
	};

protected:
	//! ArgsConfig to use
	const ArgsConfig&	config;
	//! Command to set
	ProgramCommand&		cmd;

public:
	ArgsParser(const ArgsConfig& cfg, ProgramCommand& cmd);

	//! Parse arguments and set command with it, check with config
	ActionReport	parse(int argc, char** argv);

protected:

	//! Parse option
	void	parseOption(ParseState& state, ActionReport& report);

	//! Get option - adjust report
	ArgsConfig::TId2OptionIter	getOption(const char letter, ActionReport& report);

	//! Parse value
	void	parseValue(OptionValue& option, ParseState& state, ActionReport& report);

	//! Parse operand
	void	parseOperand(ParseState& state, ActionReport& report);

	//! Check operand
	void	checkOperand(const char* arg, ActionReport& report);

};

} // NProgram

#endif
