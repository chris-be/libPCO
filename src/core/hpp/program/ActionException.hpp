/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_ACTION_EXCEPTION_HPP_INCLUDED
#define	LIB_PCO_ACTION_EXCEPTION_HPP_INCLUDED

#include "program/ActionReport.hpp"
#include <exception>

namespace NPCO {

/**
	Action exception: forward ActionReport
*/
class ActionException : public std::exception {

protected:
	//! Message
	const char*		msg;
	//! Report
	ActionReport	report;

public:
	ActionException(const char* msg, const ActionReport& report);
	ActionException(const ActionReport& report);

	// Copy
	ActionException(const ActionException& ) = default;
	ActionException& operator=(const ActionException& ) = default;

	// Move does copy
	ActionException(ActionException&& toMove);
	ActionException& operator=(ActionException&& toMove) = delete;

	virtual const char* what(void) const noexcept override;

	const ActionReport&	getReport(void) const;

};

} // N..

#endif
