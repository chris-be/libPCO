/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_UNICODE_DECODER_HPP_INCLUDED
#define	LIB_PCO_UNICODE_DECODER_HPP_INCLUDED

#include "flow/InflowFilterBase.hpp"
#include "string/crt/UTF_Formats.hpp"
#include "string/crt/String_Type.hpp" // char8_t
#include "low/EndianConverter.hpp"

namespace NPCO {

/*
	Unicode Character decoder
	 - read UTF-8, UTF-16 or UTF-32 and convert to UTF-32 at the fly
*/
class UTFDecoder : public InflowFilterBase<char32_t, char8_t> {

protected:
	using TBase		= InflowFilterBase<char32_t, char8_t>;
public:
	using TResource	= typename TBase::TResource;

protected:
	// Avoid switch for each "read"
	typedef bool (UTFDecoder::*ReadFunc)(char32_t& element);

protected:
	ReadFunc			readFunc;
	EndianConverter		endianConverter;
	EUTF				format;

	void	setFormat(EUTF format);

	//! Read char8_t from source (takes care of reinterpret_cast)
	bool	readChar(uw_08& w);

	//! For readU16
	bool	_readWord(uw_16& w);

public:
	UTFDecoder(TResource src, EUTF format);

	//! Throw out_of_range
	bool	readUTF8(char32_t& element);
	//! Throw out_of_range
	bool	readUTF16(char32_t& element);
	//! Throw out_of_range
	bool	readUTF32(char32_t& element);

	/** Ignore "out_of_range", returns CVS_Unicode::ErrorCode instead
		@see IInflow
	*/
	virtual bool	read(char32_t& element) override;

};

} // N..

#endif
