/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_UNICODE_ENCODER_HPP_INCLUDED
#define	LIB_PCO_UNICODE_ENCODER_HPP_INCLUDED

#include "flow/InflowFilterBase.hpp"
#include "Char32Encoder.hpp"

namespace NPCO {

/*
	Unicode Character encoder
	 - convert UTF-32 at the fly to UTF-8, UTF-16 or UTF-32
*/
class UTFEncoder : public InflowFilterBase<char8_t, char32_t> {

protected:
	using TBase		= InflowFilterBase<char8_t, char32_t>;
public:
	using TResource	= typename TBase::TResource;

protected:
	Char32Encoder		encoder;

public:
	UTFEncoder(TResource src, EUTF format);

	//! @see IInflow
	virtual bool read(char8_t& element) override;

};

} // N..

#endif
