/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_FIXED_FIFO_BUFFER_HPP_INCLUDED
#define	LIB_PCO_FIXED_FIFO_BUFFER_HPP_INCLUDED

#include "PCO_Predicate.hpp"

namespace NPCO {

/**
	FixedFifoBuffer: write/read one element after another
*/
template<class PType, int Nb>
class FixedFifoBuffer {

protected:
	PType			buffer[Nb];
	PType			*left;
	PType			*right;

public:
	static const int size = Nb;

	FixedFifoBuffer(void)
	{
		this->clear();
	}

	//! Push value in buffer
	void	push(PType element)
	{
		PCO_ASSERT(this->right < this->buffer + Nb);
		*this->right = element;
		++this->right;
	}

	//! Clear buffer
	void	clear(void)
	{
		this->left = this->buffer;
		this->right= this->buffer;
	}

	//! Check if buffer can be read
	bool	leftToRead(void) const
	{
		return this->right > this->left;
	}

	//! Pull next value - check leftToRead before
	void	pull(PType& element)
	{
		PCO_LOGIC_ERR(this->leftToRead(), "Nothing to read");

		element = *this->left;
		++this->left;
	}

	//! Read
	bool	read(PType& element)
	{
		bool rc = this->leftToRead();
		if(rc)
		{
			this->pull(element);
		}

		return rc;
	}

};

} // N..

#endif
