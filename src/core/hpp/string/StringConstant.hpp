/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STRING_CONSTANT_HPP_INCLUDED
#define	LIB_PCO_STRING_CONSTANT_HPP_INCLUDED

#include "string/crt/I_String.hpp"
// #include "crt/text/I_CharBlock.hpp"
#include "string/CharBlock.hpp"
#include "string/crt/String_Type.hpp" // char8_t

namespace NPCO {

/*!
	Container for constant string: only to hold/forward const char* !
	@see C_String
*/
class StringC : public IString {

public:
	static const StringC	empty;

protected:
	// Use already existing service
	CharBlock	charBlock;
	// Length was set ?
	bool		lengthOk;

	/** Set str and encoding - unset length (will be computed if needed)
		@param str NULL terminated string
	*/
	void	_set(const char* str, EStringEncoding encoding);

	//! Set length and lengthOk
	void	_setLength(array_size length);

public:
	StringC(void);
	StringC(const char* str, EStringEncoding encoding);
	StringC(const char* str);
	StringC(const char8_t* str);
	StringC(const char16_t* str, bool bigEndian);
	StringC(const char32_t* str, bool bigEndian);

	//! @see set
	StringC(const char16_t* str);
	//! @see set
	StringC(const char32_t* str);

	// Copy
	StringC(const StringC& ) = default;
	StringC& operator=(const StringC& ) = default;

	// Move
	StringC(StringC&& ) = default;
	StringC& operator=(StringC&& ) = default;

	void	set(const char* str, EStringEncoding encoding);
	void	set(const char* str);
	void	set(const char8_t* str);
	void	set(const char16_t* str, bool bigEndian);
	void	set(const char32_t* str, bool bigEndian);

	//! Use CPU endian mode
	void	set(const char16_t* str);
	//! Use CPU endian mode
	void	set(const char32_t* str);

	//! @see IStringCarrier
	virtual EStringEncoding	getEncoding(void) const override;

	//! @see IStringCarrier
	virtual CharInflow*		createCharInflow(void) const override;

	//! @see IString
	virtual	array_size		size(void) const override;

	//! @see IString
	virtual CStringHolder	c_str_wrap(void) const override;

};

} // N..

#endif
