/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_CSTYLE_STRING_HPP_INCLUDED
#define	LIB_PCO_C_CSTYLE_STRING_HPP_INCLUDED

namespace NPCO {

/**
	Constraint for a "string" to be C-Style compatible.
	- chars must be continuous in a memory block
	- be null terminated '\0'

	@param PCd Constrained class
*/
template<class PCd> class C_CStyleString {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
		      PCd&	ctd			= *ptr;

		// Pointer for data null terminated
		[[maybe_unused]]	const	char* const_buffer = const_ctd.const_char_ptr();
		[[maybe_unused]]			char* buffer = ctd.char_ptr();

		// Cast (convenience)
		const_buffer =  (const char*)(const_ctd);
		buffer =  (char*)(ctd);
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_CStyleString(void)
	{
		C_CStyleString::ensure();
	}

};

} // N..

#endif
