/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_STRING_HPP_INCLUDED
#define	LIB_PCO_C_STRING_HPP_INCLUDED

#include "C_StringCarrier.hpp"
#include "C_CStyleString.hpp"
#include "mem/MemSettings.hpp"	// array_size

namespace NPCO {

/**
	Constraint for a String:
	- not forced to have continuous chars in memory
	- no need to be null terminated '\0'

	@param PCd Constrained class
*/
template<class PCd> class C_String
	: private C_StringCarrier<PCd> {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
//		      PCd&	ctd			= *ptr;

		// Get length
		[[maybe_unused]]	array_size		l = const_ctd.size();

		// To use C function taking const char*
		[[maybe_unused]]	auto		cstr =	const_ctd.c_str_wrap();
		C_CStyleString<decltype(cstr)>::ensure();
	}

	static void	_ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

public:
	static void	ensure(void)
	{
		C_StringCarrier<PCd>::ensure();
		_ensure();
	}

	C_String(void)
	{
		C_String::_ensure();
	}

};

} // N..

#endif
