/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_STRING_HPP_INCLUDED
#define	LIB_PCO_I_STRING_HPP_INCLUDED

#include "string/crt/I_StringCarrier.hpp"
#include "string/crt/C_String.hpp"
#include "string/CStringHolder.hpp"

namespace NPCO {

/**
	Contract for a String.

	@see C_CharBag
*/
class IString : private C_String<IString>
	, public IStringCarrier {

public:
	virtual ~IString(void) = default;

	//! @see IString
	virtual	array_size		size(void) const = 0;

	// @see C_String To use C function taking const char*
	virtual CStringHolder	c_str_wrap(void) const = 0;

	virtual bool			isEmpty(void) const
	{
		return this->size() == 0;
	}

};

} // N..

#endif
