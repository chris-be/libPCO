/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_C_STRING_CARRIER_HPP_INCLUDED
#define	LIB_PCO_C_STRING_CARRIER_HPP_INCLUDED

#include "StringEncoding.hpp"
#include "pattern/flow/I_Inflow.hpp"

namespace NPCO {

//! Alias
typedef IInflow<char>			CharInflow;

/**
	Constraint for a StringCarrier: container for encoded chars (only to pass data)
	- length: number of bytes (not character/glyphs/...)

	@param PCd Constrained class
*/
template<class PCd> class C_StringCarrier {

private:
	static void constraints(PCd* ptr)
	{
		const PCd&	const_ctd	= *ptr;
//		      PCd&	ctd			= *ptr;

		// Get encoding used
		[[maybe_unused]]	EStringEncoding	e = const_ctd.getEncoding();

		// Get "char" inflow
		[[maybe_unused]]	CharInflow*	f = const_ctd.createCharInflow();
	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_StringCarrier(void)
	{
		C_StringCarrier::ensure();
	}

};

} // N..

#endif
