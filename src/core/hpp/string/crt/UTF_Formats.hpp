/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_UTF_FORMATS_HPP_INCLUDED
#define	LIB_PCO_E_UTF_FORMATS_HPP_INCLUDED

namespace NPCO {

/*!
	Some typedef for "unicode classes"
	Universal Character Set Transformation Format

	Tech:
		- little endian flag with highest bit
		- limit range to 7 bits
*/
enum class EUTF : unsigned char {

	// General state
	  _8		=	0x01
	, _16_BE	=	0x02
	, _16_LE	=	_16_BE | 0x80
	, _16		=	_16_BE

	, _32_BE	=	0x04
	, _32_LE	=	_32_BE | 0x80
	, _32		=	_32_BE

};

} // N..

#endif
