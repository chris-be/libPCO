/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_STRING_CARRIER_HPP_INCLUDED
#define	LIB_PCO_I_STRING_CARRIER_HPP_INCLUDED

#include "string/crt/C_StringCarrier.hpp"

#include <ostream>

namespace NPCO {

/**
	Default contract for C_StringCarrier

	@see C_StringCarrier
*/
class IStringCarrier {

public:
	virtual ~IStringCarrier(void) = default;

	//! @see C_StringCarrier
	virtual EStringEncoding	getEncoding(void) const = 0;

	//! @see C_StringCarrier @return ICharInflow which must be deleted
	virtual CharInflow*		createCharInflow(void) const = 0;

};

// In E_StringEncoding
std::ostream&	operator<<(std::ostream& stream, const IStringCarrier& toEcho);

} // N..

#endif
