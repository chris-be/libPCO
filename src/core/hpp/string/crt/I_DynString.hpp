/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_I_DYNAMIC_STRING_HPP_INCLUDED
#define	LIB_PCO_I_DYNAMIC_STRING_HPP_INCLUDED

#include "string/crt/I_String.hpp"

namespace NPCO {

/**
	Contract for a Dynamic String.

*/
template<class PString>
class IDynString : public IString {

public:
	virtual ~IDynString(void) override = default;

	// Main logic
	PString&	operator=(const IStringCarrier& sc);

	// Shortcuts
	void	set(const char* str, EStringEncoding encoding);

	PString&	operator=(const char* str);
	PString&	operator=(const char8_t* str);
	//! Use CPU endian mode
	PString&	operator=(const char16_t* str);
	//! Use CPU endian mode
	PString&	operator=(const char32_t* str);

};

} // N..

#endif
