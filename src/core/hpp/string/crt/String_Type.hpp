/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STRING_TYPE_HPP_INCLUDED
#define	LIB_PCO_STRING_TYPE_HPP_INCLUDED

#include <cstdint>	// uint8_t

namespace NPCO {

//! Distinguish u8"" strings
typedef		uint8_t		char8_t;

// Shortcut (bad I know :/)
// #define CHAR8_PTR(STR)		(char8_t*)(u8"" STR)
#define U8_STR(STR)			(const char8_t*)(u8"" STR)

} // N..

#endif
