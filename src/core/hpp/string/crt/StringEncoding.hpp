/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STRING_ENCODING_HPP_INCLUDED
#define	LIB_PCO_STRING_ENCODING_HPP_INCLUDED

#include "PCO_Types.hpp"

namespace NPCO {

/*!
	String encoding
	Tech:
		- little endian flag with highest bit
		- limit range to 15 bits for "little endian" flag ( 0x8000 )

*/
enum class EStringEncoding : uw_16 {
// Do not flood namespace
#define LITTLE_ENDIAN_FLAG	0x8000

	// Special flag
	  unknown		=	0x00

	// Unicode - Keep order ! @see e_isUTF
	, UTF_8			=	0x01

	, UTF_16_BE		=	0x02
	, UTF_16_LE		=	UTF_16_BE | LITTLE_ENDIAN_FLAG
	, UTF_16		=	UTF_16_BE

	, UTF_32_BE		=	0x03
	, UTF_32_LE		=	UTF_32_BE | LITTLE_ENDIAN_FLAG
	, UTF_32		=	UTF_32_BE

	// Charset
	, US_ASCII		=	0x04
	, ISO_8859_1

#undef	LITTLE_ENDIAN_FLAG
};

/**
	Some static needs
*/
class StringEncoding {

public:
	// Service only
	StringEncoding(void) = delete;

	/** Get enum from name (http://www.iana.org/assignments/character-sets/character-sets.xhtml)
		@param[out] enc Adjusted if found
		@return false if not found
	*/
	static bool					findEncoding(const char* encodingName, EStringEncoding& enc);

//	//! Get locale encoding name
//	static const char*			getLocaleEncodingName(void);

	//! Set locale encoding
	static const char*			setLocale(int category, const char *locale);

	//! Get locale encoding name
	static const char*			getLocaleName(void);

//
// Cache
//
public:
	//! Get locale encoding - @warning cached value ! @see setLocaleEncoding
	static EStringEncoding		getLocaleEncoding(void);

	//! Set locale encoding - adjust cache
	static void					setLocaleEncoding(int category, const char *locale);

};

} // N..

#endif
