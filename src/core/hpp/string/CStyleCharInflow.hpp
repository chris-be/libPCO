/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CSTYLE_CHAR_INFLOW_HPP_INCLUDED
#define	LIB_PCO_CSTYLE_CHAR_INFLOW_HPP_INCLUDED

#include "string/crt/C_StringCarrier.hpp"	// CharInflow
// #include "flow/crt/I_Inflow.hpp"
#include "string/crt/StringEncoding.hpp"
#include "string/crt/String_Type.hpp"	// char8_t
#include "mem/MemSettings.hpp"	// array_size

namespace NPCO {

/*
	Use C-Style string as inflow
		- read char by char
		- take care of '\0'
*/
class CStyleCharInflow : public CharInflow {

protected:
	const char*		str;
	EStringEncoding	encoding;
	//! Size of character - or number of continuous 0 to detect
	uw_08			charSize;

	const char*		cur;
	//! Number of char to read before test
	uw_08			nbToRead;

	//! Set charSize and toFirst()
	void	_set(const char* str, EStringEncoding encoding);

public:
	CStyleCharInflow(void) = delete;
	CStyleCharInflow(const char* str, EStringEncoding encoding);

	CStyleCharInflow(const char8_t* str);
	CStyleCharInflow(const char16_t* str, bool bigEndian);
	CStyleCharInflow(const char32_t* str, bool bigEndian);
	//! Use current encoding
	CStyleCharInflow(const char* str);
	//! Use CPU endian mode
	CStyleCharInflow(const char16_t* str);
	//! Use CPU endian mode
	CStyleCharInflow(const char32_t* str);

	// Copy
	CStyleCharInflow(const CStyleCharInflow& ) = default;
	CStyleCharInflow& operator=(const CStyleCharInflow& ) = default;

	// Move
	CStyleCharInflow(CStyleCharInflow&& ) = default;
	CStyleCharInflow& operator=(CStyleCharInflow&& ) = default;

	//! Get encoding
	EStringEncoding	getEncoding(void) const;

	//! Get size of a character for encoding
	uw_08	getCharSize(void) const;

	//! strlen like
	array_size	strlen(void) const;

	//! Rewind
	void	toFirst(void);

	//! @see ICharInflow
	virtual bool	read(char& element) override;

	//! Get size of a character for given encoding
	static uw_08	getCharSize(const EStringEncoding encoding);

};

} // N..

#endif
