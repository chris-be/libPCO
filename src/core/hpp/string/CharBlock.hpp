/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHAR_BLOCK_HPP_INCLUDED
#define	LIB_PCO_CHAR_BLOCK_HPP_INCLUDED

#include "string/crt/I_String.hpp"

namespace NPCO {

/*!
	Hold const char* with size
	- assume not NULL terminated

	@see IStringCarrier
*/
class CharBlock : public IString {

protected:
	const char*				str;
	EStringEncoding			encoding;
	array_size				nbBytes;

public:
	CharBlock(void);
	CharBlock(const char* str, EStringEncoding encoding, array_size size);

	// Copy
	CharBlock(const CharBlock& ) = default;
	CharBlock& operator=(const CharBlock& ) = default;

	// Move
	CharBlock(CharBlock&& ) = default;
	CharBlock& operator=(CharBlock&& ) = default;

	void	set(const char* str, EStringEncoding encoding, array_size size);

	//! @see IStringCarrier
	virtual EStringEncoding	getEncoding(void) const override;

	//! @see IStringCarrier
	virtual CharInflow*		createCharInflow(void) const override;

	//! @see IString
	virtual	array_size		size(void) const override;

	//! @see IString
	virtual CStringHolder	c_str_wrap(void) const override;

	//! StringConstant
	const char*	_da(void) const;

	static CharInflow*		createCharInflow(const char* str, array_size size);

};

} // N..

#endif
