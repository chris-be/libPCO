/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CSTRING_HOLDER_HPP_INCLUDED
#define	LIB_PCO_CSTRING_HOLDER_HPP_INCLUDED

#include "string/crt/C_CStyleString.hpp"
#include "string/crt/I_StringCarrier.hpp"
#include "col/Store.hpp"
#include "mem/Reference.hpp"

namespace NPCO {

/*!
	C-Style string holder.
	@see C_CStyleString
*/
class CStringHolder : private C_CStyleString<CStringHolder> {

protected:
	// Permits copy
	using TChars	= Store<char>;
	using TRef		= Reference<TChars>;

protected:
	//!
	const char*			const_str;
	//! In case "copy" must be made
	TRef				str;

	//
	void	set(CharInflow& src, EStringEncoding encoding);

public:
	CStringHolder(void);
	CStringHolder(const char* str, EStringEncoding encoding);
	CStringHolder(const IStringCarrier& sc);

	// Copy
	CStringHolder(const CStringHolder& ) = default;
	CStringHolder& operator=(const CStringHolder& ) = default;

	// Move
	CStringHolder(CStringHolder&& ) = default;
	CStringHolder& operator=(CStringHolder&& ) = default;

	void	set(const char* str, EStringEncoding encoding);
	void	set(const IStringCarrier& sc);

	//! @see C_CStyleString
	const char*		const_char_ptr(void) const;

	//! @see C_CStyleString
	char*			char_ptr(void);

	//! @see C_CStyleString
	PCO_INLINE
	explicit operator const char*(void) const
	{
		return this->const_char_ptr();
	}

	//! @see C_CStyleString
	PCO_INLINE
	explicit operator char*(void)
	{
		return this->char_ptr();
	}

};

} // N..

#endif
