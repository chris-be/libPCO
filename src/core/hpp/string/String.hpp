/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STRING_HPP_INCLUDED
#define	LIB_PCO_STRING_HPP_INCLUDED

#include "string/use/StringBase.hpp"
#include "string/use/SB_Context_UTF8.hpp"
// #include "string/crt/I_String.hpp"
#include "pattern/cfr/C_SelfClassified.hpp"

namespace NPCO {

/*!
	String: UTF-8 encoded string
	@see C_String

	Use:
		- hold string, concat
*/
class String	: public StringBase<SB_Context_UTF8>
				, private C_SelfClassified<String> {

protected:
	using TBase			= StringBase<SB_Context_UTF8>;

public:
	//! Import constructors
	using TBase::StringBase;
	virtual ~String(void) override = default;
	String(const char* str, EStringEncoding encoding);
	String(const IStringCarrier& sc);
	String(const char* str);
	String(const char16_t* str);
	String(const char32_t* str);

	// Copy
	String(const String& ) = default;
	String& operator=(const String& ) = default;

	// Move
	String(String&& ) = default;
	String& operator=(String&& ) = default;

	//! @see C_SelfClassified
	int		classify(const String& toCompare) const;

	//! classify using "case insensitive" comparison
	int		classify_CI(const String& toCompare) const;

	// @convenience
	PCO_INLINE
	bool		operator==(const String& toCompare) const
	{
		return this->classify(toCompare) == 0;
	}

};

String	operator+(const String& left, const String& right);
String	operator+(const String& left, const IStringCarrier& sc);

String	operator+(const String& left, const char8_t* right);
String	operator+(const String& left, const char8_t right);

String	operator+(const String& left, const char* right);
String	operator+(const String& left, const char right);

} // N..

#endif
