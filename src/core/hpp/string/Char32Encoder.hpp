/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_UNICODE_CHAR32_ENCODER_HPP_INCLUDED
#define	LIB_PCO_UNICODE_CHAR32_ENCODER_HPP_INCLUDED

#include "string/crt/UTF_Formats.hpp"
#include "string/crt/String_Type.hpp" // char8_t
#include "low/EndianConverter.hpp"
#include "string/FixedFifoBuffer.hpp"

namespace NPCO {

/*
	Unicode Character encoder
	 - convert UTF-32 to UTF-8, UTF-16 or UTF-32
*/
class Char32Encoder : protected FixedFifoBuffer<uw_08, 4> {

protected:
	// Avoid switch for each "encode"
	typedef void (Char32Encoder::*EncodeFunc)(uw_32 u);

	using TBase			= FixedFifoBuffer<uw_08, 4>;

protected:
	EUTF				format;
	EncodeFunc			encodeFunc;
	EndianConverter		endianConverter;

	// Push value in buffer - toUTF16
	void	push16(uw_16 w);

	void	toUTF8(uw_32 u);
	void	toUTF16(uw_32 u);
	void	toUTF32(uw_32 u);

public:
	Char32Encoder(EUTF format);
	void	setFormat(EUTF format);

	//! Clear buffer
	void	clear(void);

	//! Encode c
	void	encode(char32_t c);

	//! Check if buffer can be read
	bool	leftToRead(void) const;

	//! Pull next char - check leftToRead before
	void	pull(char8_t& element);

	//! Read
	bool	read(char8_t& element);

};

} // N..

#endif
