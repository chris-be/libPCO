/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHARSET_ENCODER_HPP_INCLUDED
#define	LIB_PCO_CHARSET_ENCODER_HPP_INCLUDED

#include "flow/InflowFilterBase.hpp"
#include "string/crt/StringEncoding.hpp"

namespace NPCO {

/*
	Charset Character encoder
	 - convert UTF-32 to ISO-... at the fly
*/
class CharsetEncoder : public InflowFilterBase<char, char32_t> {

protected:
	using TBase		= InflowFilterBase<char, char32_t>;
public:
	using TResource	= typename TBase::TResource;

protected:
	// Avoid switch for each "write"
	typedef bool (CharsetEncoder::*ReadFunc)(char& element);

protected:
	ReadFunc			readFunc;
	EStringEncoding		encoding;

	void	setEncoding(EStringEncoding encoding);

	//! rawConvert, validity check
	uw_32	convert(char32_t c);

	//! US-ASCII (and ISO-8859-1 really ?)
	bool	passThrough(char& element);

public:
	CharsetEncoder(TResource src, EStringEncoding encoding);

	// Move
	CharsetEncoder(CharsetEncoder&& ) = default;
	CharsetEncoder& operator=(CharsetEncoder&& ) = default;

	//! @see IInflow
	virtual bool	read(char& element) override;

};

} // N..

#endif
