/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

// #include	"string/use/StringBase.hpp"
#include "string/StringConstant.hpp"
#include "cvs/bag/E_DynBag.hpp"	// e_addAll

namespace NPCO {

template<class PSBContext>
const StringBase<PSBContext>	StringBase<PSBContext>::empty = StringBase();


template<class PSBContext>
StringBase<PSBContext>::StringBase(const TChar* str)
{
	this->operator=(str);
}

template<class PSBContext>
EStringEncoding	StringBase<PSBContext>::getEncoding(void) const
{
	return PSBContext::encoding;
}

template<class PSBContext>
CharInflow*		StringBase<PSBContext>::createCharInflow(void) const
{
	auto cur = e_cursor(this->chars);
	auto inf = RefRes<IInflow<TChar>>::template from<decltype(cur)>(std::move(cur));
	return PSBContext::charInflow(inf);
}

template<class PSBContext>
CStringHolder	StringBase<PSBContext>::c_str_wrap(void) const
{
	return CStringHolder(*this);
}

template<class PSBContext>
auto	StringBase<PSBContext>::size(void) const -> TSize
{
	return this->chars.size();
}

template<class PSBContext>
bool	StringBase<PSBContext>::isEmpty(void) const
{
	return this->chars.isEmpty();
}

template<class PSBContext>
void	StringBase<PSBContext>::clear()
{
	this->chars.clear();
}

template<class PSBContext>
void	StringBase<PSBContext>::add(const TChar& toAdd)
{
	this->chars.add(toAdd);
}

template<class PSBContext>
void	StringBase<PSBContext>::moveIn(TChar&& toMove)
{
	this->chars.moveIn(std::move(toMove));
}

template<class PSBContext>
void	StringBase<PSBContext>::add(const StringBase& toAdd)
{
	e_addAll(this->chars, toAdd.chars);
}

template<class PSBContext>
void	StringBase<PSBContext>::add(const IStringCarrier& sc)
{
	RefRes<IInflow<TChar>> inf = PSBContext::inflow(sc);
	e_addFlow(this->chars, *inf);
}

template<class PSBContext>
StringBase<PSBContext>&		StringBase<PSBContext>::operator+=(const StringBase& toAdd)
{
	this->add(toAdd);
}

template<class PSBContext>
StringBase<PSBContext>&		StringBase<PSBContext>::operator+=(const TChar* str)
{
	for(const TChar* ptr = str ; (*ptr) != '\0' ; ++ptr)
	{
		this->chars.add(*ptr);
	}

	return *this;
}

template<class PSBContext>
	template<class PCh2
			, std::enable_if_t<!std::is_same<typename StringBase<PSBContext>::TChar, std::decay<PCh2>>::value, bool> >
StringBase<PSBContext>&		StringBase<PSBContext>::operator+=(const PCh2* str)
{
	this->add( StringC(str) );
	return *this;
}

template<class PSBContext>
auto	StringBase<PSBContext>::operator=(const IStringCarrier& sc) -> StringBase&
{
	this->clear();
	this->add(sc);
	return *this;
}

template<class PSBContext>
void	StringBase<PSBContext>::set(const char* str, EStringEncoding encoding)
{
	this->operator=( StringC(str, encoding) );
}

template<class PSBContext>
template<class PCh2>
StringBase<PSBContext>&		StringBase<PSBContext>::operator=(const PCh2* str)
{
	this->clear();
	return this->operator+=(str);
}

template<class PSBContext>
void	StringBase<PSBContext>::add(TCharInflow& inflow)
{
	e_addFlow(this->chars, inflow);
}

template<class PSBContext>
void	StringBase<PSBContext>::set(TCharInflow& inflow)
{
	this->clear();
	this->add(inflow);
}

template<class PSBContext>
auto	StringBase<PSBContext>::first(void) const -> TIterRead
{
	return this->chars.first();
}

template<class PSBContext>
auto	StringBase<PSBContext>::last(void) const -> TIterRead
{
	return this->chars.last();
}

template<class PSBContext>
auto	StringBase<PSBContext>::first(void) -> TIterWrite
{
	return this->chars.first();
}

template<class PSBContext>
auto	StringBase<PSBContext>::last(void) -> TIterWrite
{
	return this->chars.last();
}

/*
template<class PSBContext>
void	StringBase<PSBContext>::remove(const TIterWrite& iter)
{
	TSize i = this->chars._indexOf(iter);
	this->chars.removeAt(i, 1);
}

template<class PSBContext>
void	StringBase<PSBContext>::remove(const TIterWrite& low, const TIterWrite& high)
{
	TSize b = this->chars._indexOf(low);
	TSize e = this->chars._indexOf(high);
	PCO_INV_ARG(b <= e, "invalid range");

	this->chars.removeAt(b, e-b +1);
}
*/

template<class PSBContext>
void	StringBase<PSBContext>::toLower(void)
{
	for(auto r = e_rider(this->chars) ; r.ok() ; ++r)
	{
		PSBContext::toLower(*r);
	}
}

template<class PSBContext>
void	StringBase<PSBContext>::toUpper(void)
{	// WRONG !! TODO
	for(auto r = e_rider(this->chars) ; r.ok() ; ++r)
	{
		PSBContext::toUpper(*r);
	}
}

template<class PSBContext>
void	StringBase<PSBContext>::trimLeft(void)
{
	// TODO: implement remove(it,it) and use e_trimXX
	const TCharSet& patterns = PSBContext::default_trim_chars;
	while(!this->chars.isEmpty() && patterns.hasKey(this->chars.head()))
	{
		this->chars.popHead();
	}
}

template<class PSBContext>
void	StringBase<PSBContext>::trimRight(void)
{
	// TODO: implement remove(it,it) and use e_trimXX
	const TCharSet& patterns = PSBContext::default_trim_chars;
	while(!this->chars.isEmpty() && patterns.hasKey(this->chars.tail()))
	{
		this->chars.popTail();
	}
}

template<class PSBContext>
void	StringBase<PSBContext>::trim(void)
{
	this->trimRight();
	this->trimLeft();
}

} // N..
