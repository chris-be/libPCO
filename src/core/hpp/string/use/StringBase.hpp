/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STRING_BASE_HPP_INCLUDED
#define	LIB_PCO_STRING_BASE_HPP_INCLUDED

#include "C_SB_Context.hpp"
// #include "string/crt/I_StringCarrier.hpp"
#include "string/CStringHolder.hpp"
#include "col/ChunkList.hpp"

namespace NPCO {

/*!
	StringBase: base implementation for (CI)String(16/32)

	@param PChar Character to use
*/
template<class PSBContext>
class StringBase	: public IStringCarrier
					// ,  private C_DynBag<StringBase<PChar, encoding>, PChar, array_size>
					, private C_SB_Context<PSBContext> {

public:
	using TChar			= typename PSBContext::TChar;

protected:
	//typedef Store ? Array<TChar, array_size, BlockMemoryManager>		TChars;
	typedef ChunkList<TChar>		TChars;

	using TCharSet		= typename PSBContext::TCharSet;

public:
	using TType			= typename TChars::TType;
	using TSize			= typename TChars::TSize;
	using TIterRead		= typename TChars::TIterRead;
	using TIterWrite	= typename TChars::TIterWrite;

	using TCharInflow	= IInflow<TChar>;

public:
	static const StringBase		empty;

protected:
	TChars	chars;

public:
	StringBase(void) = default;
	virtual ~StringBase(void) override = default;
	StringBase(const TChar* str);

	// Copy
	StringBase(const StringBase& ) = default;
	StringBase&	operator=(const StringBase& ) = default;

	// Move
	StringBase(StringBase&& ) = default;
	StringBase&	operator=(StringBase&& ) = default;

	//! @see IStringCarrier
	virtual EStringEncoding	getEncoding(void) const override;

	//! @see IStringCarrier
	virtual CharInflow*		createCharInflow(void) const override;

	//! @see IString
	CStringHolder			c_str_wrap(void) const;

	//! @see C_DynBag
	PCO_INLINE
	void	clear(void);

	//! @see C_HasSize
	PCO_INLINE
	TSize	size(void) const;

	//! @see C_HasSize
	PCO_INLINE
	bool	isEmpty(void) const;

	//! see C_DynBag
	PCO_INLINE
	void	add(const TChar& toAdd);

	//! @see C_DynBag - Append a char
	PCO_INLINE
	void	moveIn(TChar&& toMove);

	void	add(const StringBase& toAdd);
	void	add(const IStringCarrier& sc);

	PCO_INLINE
	StringBase&	operator+=(const StringBase& toAdd);
	PCO_INLINE
	StringBase&	operator+=(const TChar* str);

	template<class PCh2
			, std::enable_if_t<!std::is_same<TChar, std::decay<PCh2>>::value, bool> = true>
	StringBase&	operator+=(const PCh2* str);

	void		set(const char* str, EStringEncoding encoding);
	StringBase&	operator=(const IStringCarrier& sc);

	//! Simply call clear and +=
	template<class PCh2>
	PCO_INLINE
	StringBase&	operator=(const PCh2* str);

	PCO_INLINE
	void	add(TCharInflow& inflow);
	PCO_INLINE
	void	set(TCharInflow& inflow);

	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead		first(void) const;
	//! @see C_HasIterRead
	PCO_INLINE
	TIterRead		last(void) const;

	//! @see C_HasIterWrite
	PCO_INLINE
	TIterWrite		first(void);
	//! @see C_HasIterWrite
	PCO_INLINE
	TIterWrite		last(void);

/*
	//! @see C_DynBag
	void	remove(const TIterWrite& iter);

	//! @see C_DynBag
	void	remove(const TIterWrite& low, const TIterWrite& high);
*/

	void	toLower(void);
	void	toUpper(void);

	//! Trim left with spaces
	void	trimLeft(void);
	//! Trim right with spaces
	void	trimRight(void);
	//! trimLeft, trimRight
	void	trim(void);

};

} // N..

#include "imp/StringBase.cppi"

#endif
