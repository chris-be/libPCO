/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STRING_BASE_CONTEXT_HPP_INCLUDED
#define	LIB_PCO_STRING_BASE_CONTEXT_HPP_INCLUDED

//#include "string/crt/StringEncoding.hpp"
#include "string/crt/I_StringCarrier.hpp"
#include "pattern/flow/I_Inflow.hpp"
#include "mem/cvs/RefRes.hpp"

namespace NPCO {

/*!
	Constraint "StringBase Context"

	@param PChar Character to use
	@param encoding Encoding
*/
template<class PCd>
class C_SB_Context
{

private:
	static void constraints(PCd* ptr)
	{
		[[maybe_unused]]	const PCd&	const_ctd = *ptr;

		// TChar defined
		[[maybe_unused]]	typename PCd::TChar	*has_TChar;
		// Encoding defined
		[[maybe_unused]]	EStringEncoding enc = PCd::encoding;

		// TCharSet defined
		[[maybe_unused]]	typename PCd::TCharSet	*has_TCharSet;

		// Default trim chars defined
		[[maybe_unused]]	const typename PCd::TCharSet& dtc = PCd::default_trim_chars;

		using TChar = typename PCd::TChar;
		// toLower / toUpper
		[[maybe_unused]]	TChar* pc = nullptr;
		PCd::toLower(*pc);
		PCd::toUpper(*pc);

		// From IStringCarrier
		const IStringCarrier* sc = nullptr;
		[[maybe_unused]]	IInflow<TChar>*		inf = PCd::inflow(*sc);

		RefRes<IInflow<TChar>>		ri;
		RefRes<IInflow<TChar>>& 	ref_ri = ri;
		[[maybe_unused]]	CharInflow*			ci = PCd::charInflow(ref_ri);

	}

public:
	static void	ensure(void)
	{
		[[maybe_unused]]	void (*func)(PCd* ptr) = constraints;
	}

	C_SB_Context(void)
	{
		C_SB_Context::ensure();
	}


};

} // N..

#endif
