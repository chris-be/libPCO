/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHAR8_SB_CONTEXT_HPP_INCLUDED
#define	LIB_PCO_CHAR8_SB_CONTEXT_HPP_INCLUDED

#include "C_SB_Context.hpp"
#include "string/crt/String_Type.hpp" // char8_t
#include "string/crt/I_StringCarrier.hpp"
// CharSet
#include "col/SetTree.hpp"
#include "math/MathClassifier.hpp"

namespace NPCO {

/*!
	Char8 StringBase Context

	@param PChar Character to use
*/
class SB_Context_UTF8 : private C_SB_Context<SB_Context_UTF8> {

public:
	using TChar			= char8_t;
	using TCharSet		= SetTree<TChar, MathClassifier<TChar>>;

public:
	static const EStringEncoding	encoding;

	static const TCharSet			default_trim_chars;

	static void			toLower(char8_t& c);
	static void			toUpper(char8_t& c);

	static IInflow<char8_t>*	inflow(const IStringCarrier& sc);

	static CharInflow*			charInflow(RefRes<IInflow<char8_t>>& ri);

};

} // N..

#endif
