/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_STRING_CI_CLASSIFIER_HPP_INCLUDED
#define	LIB_PCO_STRING_CI_CLASSIFIER_HPP_INCLUDED

#include "pattern/cfr/C_Classifier.hpp"
#include "string/String.hpp"

namespace NPCO {

/**
	String "case insensitive" classifier.

*/
class StringCIClassifier : public C_Classifier<StringCIClassifier, String> {

public:
	using TClassified	= String;

public:
	StringCIClassifier(void) = default;

	StringCIClassifier(const StringCIClassifier& ) = default;
	StringCIClassifier& operator=(const StringCIClassifier& ) = default;

	StringCIClassifier(StringCIClassifier&& ) = default;
	StringCIClassifier& operator=(StringCIClassifier&& ) = default;

	//! @see C_Classifier
	ClassifyValue	classify(const TClassified& o1, const TClassified& o2) const;

};

} // N..

#endif
