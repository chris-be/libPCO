/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHAR8_CI_CLASSIFIER_HPP_INCLUDED
#define	LIB_PCO_CHAR8_CI_CLASSIFIER_HPP_INCLUDED

#include "pattern/cfr/C_Classifier.hpp"
#include "string/crt/String_Type.hpp" // char8_t

namespace NPCO {

/**
	char8_t case insensitive classifier.
	FIXME !! Wrong !! Only works for ASCII.
	IMPROVE WHEN TIME.
*/
class Char8CIClassifier : private C_Classifier<Char8CIClassifier, char8_t> {

public:
	using TClassified	= char8_t;

public:
	Char8CIClassifier(void) = default;

	//! @see C_Classifier
	ClassifyValue	classify(const char8_t o1, const char8_t o2) const;

};

} // N..

#endif
