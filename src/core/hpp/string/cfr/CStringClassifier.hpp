/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CSTRING_CLASSIFIER_HPP_INCLUDED
#define	LIB_PCO_CSTRING_CLASSIFIER_HPP_INCLUDED

#include "pattern/cfr/C_Classifier.hpp"

namespace NPCO {

/**
	"C string" (const char*) classifier.
		- case sensitive
*/
class CStringClassifier
		: private C_Classifier<CStringClassifier, const char*> {

public:
	using TClassified	= char* const;

public:
	CStringClassifier(void) = default;

	CStringClassifier(const CStringClassifier& ) = default;
	CStringClassifier& operator=(const CStringClassifier& ) = default;

	CStringClassifier(CStringClassifier&& ) = default;
	CStringClassifier& operator=(CStringClassifier&& ) = default;

	//! @see C_Classifier
	ClassifyValue	classify(const char* o1, const char* o2) const;

};

} // N..

#endif
