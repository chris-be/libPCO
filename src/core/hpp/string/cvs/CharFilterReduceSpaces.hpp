/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_CHAR_FILTER_REDUCE_SPACES_HPP_INCLUDED
#define LIB_PCO_CHAR_FILTER_REDUCE_SPACES_HPP_INCLUDED

#include "flow/InflowFilterBase.hpp"
#include "col/SetTree.hpp"
#include "math/MathClassifier.hpp"

namespace NPCO {

/**
	Replace all consecutive separators by only one.
*/
template<class PCell>
class CharFilterReduceSpaces : public InflowFilterBase<PCell, PCell> {

public:
	static const PCell	default_separator;

public:
	using TBase		= InflowFilterBase<PCell, PCell>;
	using TResource	= typename TBase::TResource;

protected:
	using TSet			= SetTree<PCell, MathClassifier<PCell>>;

protected:
	//! Separator to use
	PCell				separator;
	TSet				tokens;
	bool				lastWasSep;

public:
	CharFilterReduceSpaces(TResource& src) : CharFilterReduceSpaces(src, default_separator)
	{	}

	CharFilterReduceSpaces(TResource& src, PCell separator)
		: TBase(src), separator(separator)
		, tokens{ ' ', '\t', '\r', '\n' }
		, lastWasSep(false)
	{	}

	virtual ~CharFilterReduceSpaces(void) = default;

	//! @see IInflow
	virtual bool	read(PCell& element) override
	{
		while(this->src->read(element))
		{
			bool isSep = this->tokens.hasKey(element);
			if(isSep != this->lastWasSep)
			{	// State change
				this->lastWasSep = isSep;
				if(isSep)	element = this->separator;
				return true;
			}
			else if(!isSep)
			{	// ok
				return true;
			}
		}

		return false;
	}

};

template<class PCell>
const PCell	CharFilterReduceSpaces<PCell>::default_separator = ' ';

} // N..

#endif
