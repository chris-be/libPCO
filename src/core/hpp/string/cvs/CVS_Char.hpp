/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CVS_CHAR_HPP_INCLUDED
#define	LIB_PCO_CVS_CHAR_HPP_INCLUDED

#include <initializer_list>

namespace NPCO {

/**
	Convenience Char

*/
class CVS_Char {

private:
	CVS_Char(void) = delete;

	static unsigned char	uc(char c)
	{
		return reinterpret_cast<unsigned char&>(c);
	}

public:
	static bool		isControl(char c);
	static bool		isDigit(char c);
	static bool		isChar(char c);
	static bool		isUpper(char c);
	static bool		isLower(char c);

	//! (get) upper : doesn't use "map" for convert (no cache default ?)
	static char		upper(char c);

	//! (get) lower : doesn't use "map" for convert (no cache default ?)
	static char		lower(char c);

	template<class PChar>
	static PChar	tab(void)
	{	return uc('\t');	}

	template<class PChar>
	static PChar	space(void)
	{	return uc(' ');		}

	template<class PChar>
	static PChar	carriage_return(void)
	{	return uc('\r');	}

	template<class PChar>
	static PChar	line_feed(void)
	{	return uc('\n');	}

/*
	static bool		isControl(char16_t c);
	static bool		isDigit(char16_t c);
	static bool		isChar(char16_t c);
	static bool		isUpper(char16_t c);
	static bool		isLower(char16_t c);

	static bool		isControl(char32_t c);
	static bool		isDigit(char32_t c);
	static bool		isChar(char32_t c);
	static bool		isUpper(char32_t c);
	static bool		isLower(char32_t c);
*/

};

} // N..

#endif
