/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_STRING_ENCODING_HPP_INCLUDED
#define	LIB_PCO_E_STRING_ENCODING_HPP_INCLUDED

#include "string/crt/I_StringCarrier.hpp"
#include "string/crt/UTF_Formats.hpp"
#include "string/crt/String_Type.hpp"
#include "mem/cvs/RefRes.hpp"

namespace NPCO {

// Encoding (format)
bool				e_isUTF(const EStringEncoding enc);

EUTF				e_getUTF(const EStringEncoding enc);
EStringEncoding		e_getStringEncoding(const EUTF enc);

// Transcode

// ICharInflow*	e_transcode(const IStringCarrier& sc, const EStringEncoding toEncoding);


//! Get utf-32 inflow
IInflow<char32_t>*		e_utf32Inflow(RefRes<CharInflow>& src, EStringEncoding fromEncoding);
IInflow<char32_t>*		e_utf32Inflow(const IStringCarrier& sc);

//! Get utf-8 outflow
IInflow<char8_t>*		e_utf8Inflow(RefRes<CharInflow>& src, EStringEncoding fromEncoding);
IInflow<char8_t>*		e_utf8Inflow(const IStringCarrier& sc);


} // N..

#endif
