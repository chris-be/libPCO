/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef LIB_PCO_CHAR_FILTER_ESCAPE_HPP_INCLUDED
#define LIB_PCO_CHAR_FILTER_ESCAPE_HPP_INCLUDED

#include "flow/InflowFilterBase.hpp"
#include "col/SetTree.hpp"
#include "math/MathClassifier.hpp"
#include "cvs/Capsule.hpp"

namespace NPCO {

/**
	Escape all given char.
*/
template<class PCell>
class CharFilterEscape : public InflowFilterBase<PCell, PCell> {

public:
	static const PCell	default_escape;

public:
	using TBase		= InflowFilterBase<PCell, PCell>;
	using TResource	= typename TBase::TResource;

protected:
	using TSet			= SetTree<PCell, MathClassifier<PCell>>;

protected:
	//! Separator to use
	PCell				escape;
	TSet				tokens;
	Capsule<PCell>		lastEsc;

public:
	CharFilterEscape(TResource& src) : CharFilterEscape(src, default_escape)
	{	}

	CharFilterEscape(TResource& src, PCell escape, std::initializer_list<PCell> tokens)
	 : TBase(src), escape(escape), tokens(tokens), lastEsc()
	{	}

	CharFilterEscape(TResource& src, PCell escape)
	 : CharFilterEscape(src, escape, { '"' })
	{	}

	TextFilterEscape(TResource& src, std::initializer_list<PCell> tokens)
	 : TextFilterEscape(src, default_escape, tokens)
	{	}

	virtual ~CharFilterEscape(void)	= default;

	//! @see IInflow
	virtual bool	read(PCell& element) override
	{
		if(this->lastEsc->hasValue())
		{
			element = this->lastEsc.getValue();
			this->lastEsc.unset();
			return true;
		}

		if(this->src.read(element))
		{
			if(this->tokens.hasKey(element))
			{
				this->lastEsc = element;
				element = this->escape;
			}
			return true;
		}

		return false;
	}

};

template<class PCell>
const PCell	CharFilterEscape<PCell>::default_escape = '\\';

} // N..

#endif
