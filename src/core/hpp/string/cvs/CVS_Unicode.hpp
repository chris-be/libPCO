/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CVS_UNICODE_HPP_INCLUDED
#define	LIB_PCO_CVS_UNICODE_HPP_INCLUDED

#include "PCO_Types.hpp"

namespace NPCO {

/**
	Unicode: values, useful tests

*/
class CVS_Unicode {

public:
	// Reserved values (min/max range)
	static const uw_16		ReservedRangeLow;
	static const uw_16		ReservedRangeHigh;
	static const uw_32		MaxValue;

	//! Value indicating problem (while decoding, etc...)
	static const uw_32		ErrorCode;

// utf-08
	// Surrogate codes ?
	//static const uw_08		Surrogate08High;
	//static const uw_08		Surrogate08Low;

	// Max value that can be encoded in utf-8 for "N" bytes (excluded "first octet")
	//static const uw_32		Max08For1;
	static const uw_32		Max08For2;
	static const uw_32		Max08For3;
	static const uw_32		Max08For4;

// utf-16
	// Surrogate codes
	static const uw_16		Surrogate16High;
	static const uw_16		Surrogate16Low;

	static bool		isSurrogate(uw_16 c);
	static bool		isHighSurrogate(uw_16 c);
	static bool		isLowSurrogate(uw_16 c);

// utf-32
	static bool		isValid(uw_32 c);

	static bool		isControl(uw_32 c);
	static bool		isDigit(uw_32 c);
	static bool		isChar(uw_32 c);
	static bool		isUpper(uw_32 c);
	static bool		isLower(uw_32 c);

	static uw_32	upper(uw_32 c);
	static uw_32	lower(uw_32 c);

};

} // N..

#endif
