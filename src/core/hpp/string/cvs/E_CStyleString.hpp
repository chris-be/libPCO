/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_E_CSTYLE_STRING_HPP_INCLUDED
#define	LIB_PCO_E_CSTYLE_STRING_HPP_INCLUDED

#include "string/crt/String_Type.hpp"
// #include "mem/MemSettings.hpp"
#include "mem/MemBlock.hpp"

#include <string>	// strlen

namespace NPCO {

// Copy/free/strlen string - C way

//! strlen
mem_size	e_strlen(const char* toCopy);
mem_size	e_strlen(const char8_t* toCopy);

/** Copy string - C way
	@param toCopy If nullptr, return nullptr, else a copy
	@return A copy e_free after!!
*/
char*		e_strcpy(const char* toCopy);
char8_t*	e_strcpy(const char8_t* toCopy);

/** Free string - C way
	@param toFree Only for (e_)strcpy
*/
void		e_free(char* &toFree);
void		e_free(char8_t* &toFree);

// @convenience
template<class PChar>
static const MemBlock<PChar, size_t>	e_memBlock(const PChar* str)
{
	return MemBlock<PChar, size_t>( const_cast<PChar*>(str), e_strlen(str) );
}

/*
// @convenience
static MemBlock<uw_08, size_t>			e_memBlock08(char* str)
{
	return MemBlock<uw_08, size_t>( reinterpret_cast<uw_08*>(str), strlen(str) );
}
*/

} // N..

#endif
