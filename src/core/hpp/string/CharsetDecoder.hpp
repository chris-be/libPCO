/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHARSET_DECODER_HPP_INCLUDED
#define	LIB_PCO_CHARSET_DECODER_HPP_INCLUDED

#include "flow/InflowFilterBase.hpp"
#include "string/crt/StringEncoding.hpp"

namespace NPCO {

/*
	Charset Character decoder
	 - read ISO-... and convert to UTF-32 at the fly
*/
class CharsetDecoder : public InflowFilterBase<char32_t, char> {

protected:
	using TBase		= InflowFilterBase<char32_t, char>;
public:
	using TResource	= typename TBase::TResource;

protected:
	// Avoid switch for each "read"
	typedef bool (CharsetDecoder::*ReadFunc)(char32_t& element);

protected:
	ReadFunc			readFunc;
	EStringEncoding		encoding;

	void	setEncoding(EStringEncoding encoding);

	//! US-ASCII (and ISO-8859-1 really ?)
	bool	passThrough(char32_t& element);

public:
	CharsetDecoder(TResource src, EStringEncoding encoding);

	// Move
	CharsetDecoder(CharsetDecoder&& ) = default;
	CharsetDecoder& operator=(CharsetDecoder&& ) = default;

	//! @see IInflow
	virtual bool	read(char32_t& element) override;

};

} // N..

#endif
