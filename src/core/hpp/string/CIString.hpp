/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CASE_INSENSITIVE_STRING_HPP_INCLUDED
#define	LIB_PCO_CASE_INSENSITIVE_STRING_HPP_INCLUDED

#include "string/use/StringBase.hpp"
#include "string/use/SB_Context_UTF8.hpp"
#include "pattern/cfr/C_SelfClassified.hpp"

namespace NPCO {

/*!
	String: UTF-8 encoded string
	@see C_String

	Use:
		- hold string, concat
		- use "case insensitive" classifier
*/
class CIString	: public StringBase<SB_Context_UTF8>
				, private C_SelfClassified<CIString> {

protected:
	using TBase			= StringBase<SB_Context_UTF8>;

public:
	//! Import constructors
	using TBase::StringBase;
	virtual ~CIString(void) override = default;
	CIString(const char* str, EStringEncoding encoding);
	CIString(const IStringCarrier& sc);
	CIString(const char* str);
	CIString(const char16_t* str);
	CIString(const char32_t* str);

	// Copy
	CIString(const CIString& ) = default;
	CIString& operator=(const CIString& ) = default;

	// Move
	CIString(CIString&& ) = default;
	CIString& operator=(CIString&& ) = default;

	//! @see C_SelfClassified
	int		classify(const CIString& toCompare) const;

};

CIString	operator+(const CIString& left, const CIString& right);
CIString	operator+(const CIString& left, const IStringCarrier& sc);

CIString	operator+(const CIString& left, const char8_t* right);
CIString	operator+(const CIString& left, const char8_t right);

CIString	operator+(const CIString& left, const char* right);
CIString	operator+(const CIString& left, const char right);

} // N..

#endif
