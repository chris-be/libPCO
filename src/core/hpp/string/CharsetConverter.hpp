/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#ifndef	LIB_PCO_CHARSET_CONVERTER_HPP_INCLUDED
#define	LIB_PCO_CHARSET_CONVERTER_HPP_INCLUDED

#include "flow/InflowFilterBase.hpp"
//#include "string/crt/StringEncoding.hpp"
#include "string/crt/C_StringCarrier.hpp"	// CharInflow

namespace NPCO {

/*

	 - convert at the fly
*/
class CharsetConverter : public InflowFilterBase<char, char> {

protected:
	using TBase		= InflowFilterBase<char, char>;
public:
	using TResource	= typename TBase::TResource;

protected:
	EStringEncoding			fromEncoding;
	EStringEncoding			toEncoding;
	//! Point to converter (or src if no transcode needed)
	CharInflow*				filter;
	//! Transcoder (if needed)
	RefRes<CharInflow>		transcoder;

	// Init
	void	setEncoding(EStringEncoding fromEncoding, EStringEncoding toEncoding);

public:
	CharsetConverter(TResource src, EStringEncoding fromEncoding, EStringEncoding toEncoding);

	// Move
	CharsetConverter(CharsetConverter&& ) = default;
	CharsetConverter& operator=(CharsetConverter&& ) = default;

	EStringEncoding	getFromEncoding(void) const;
	EStringEncoding	getToEncoding(void) const;

	//! @see IInflow
	virtual bool	read(char& element) override;

};

} // N..

#endif
