/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"Stamp_LibPCO.hpp"
#include "dbg/CompilerOptions.hpp"

#define		LIB_PCO_CODE			"libPCO"
#define		LIB_PCO_VERSION			"0.00a"
#define		LIB_PCO_NAME			"'Petite Caisse à Outils'"

Stamp_LibPCO::Stamp_LibPCO(void)
 : CompileStamp(LIB_PCO_CODE, LIB_PCO_VERSION, LIB_PCO_NAME)
{
	this->debug			= LIB_PCO_DEBUG_COMPILED;
	this->predicate		= LIB_PCO_PREDICATE_COMPILED;
}
