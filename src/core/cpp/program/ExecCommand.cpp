/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"program/ExecCommand.hpp"
#include "string/cvs/E_CStyleString.hpp"
#include "cvs/bag/E_DynBag.hpp"
#include "cvs/walk/E_Walker.hpp"
#include "file/FileBuffered.hpp"
#include "file/FileInflow.hpp"

using namespace NPCO;

ExecCommand::ExecCommand(void)
{	}

ExecCommand::ExecCommand(const IStringCarrier& exec)
 : exec(exec)
{
	auto cur = e_cursor(this->exec);
	if(e_rvsFind(cur, '/'))
	{
		++cur;
		e_addAll(this->arg0, cur);
	}
	else
	{
		this->arg0 = exec;
	}
}

ExecCommand::ExecCommand(const IStringCarrier& exec, const IStringCarrier& arg0)
 : exec(exec), arg0(arg0)
{	}

auto	ExecCommand::getExec(void) const -> const TName&
{
	return this->exec;
}

auto	ExecCommand::getArg0(void) const -> const TName&
{
	return this->arg0;
}

auto	ExecCommand::getArgs(void) const -> const TArgs&
{
	return this->args;
}

void	ExecCommand::addArg(const IStringCarrier& arg)
{
	TName ca = arg;
	this->args.add(ca);
}

void	ExecCommand::clearArgs(void)
{
	this->args.clear();
}

bool	ExecCommand::execute(bool usePath, bool pipeStdOut, bool pipeStdErr)
{
	PCO_LOGIC_ERR(this->process.isNull(), "A process is already running");

	// Create copies
	Store<CStringHolder>	argCopy;
	argCopy.add( this->arg0 );
	for(auto cur = e_cursor(this->args) ; cur.ok() ; ++cur)
	{
		argCopy.add( *cur );
	}

	// Create "char*"
	CStringHolder	exec = this->exec.c_str_wrap();
	Store<char*>	argv;
	for(auto r = e_rider(argCopy) ; r.ok() ; ++r)
	{
		argv.add( r->char_ptr() );
	}
	argv.add(nullptr);

	this->process = Reference<Process>::create<Process>(pipeStdOut, pipeStdErr);
	return this->process->execute(exec.const_char_ptr(), usePath, argv._da());
}

bool	ExecCommand::isValid(void) const
{
	return this->process.isValid() && this->process->isValid();
}

int		ExecCommand::getExitStatus(void) const
{
	PCO_LOGIC_ERR(this->process.isValid(), "No process running");
	return this->process->getExitStatus();
}

void	ExecCommand::wait(void)
{
	PCO_LOGIC_ERR(this->process.isValid(), "No process running");
	this->process->wait();
}

void	ExecCommand::readStdOut(String& out)
{
	PCO_LOGIC_ERR(this->process.isValid(), "No process running");

	FileBuffered	fb;
	fb.open(&this->process->getStdOut());
	FileInflow		fi(fb);
	e_addFlow(out, fi);
	fb.close();
}

void	ExecCommand::readStdErr(String& err)
{
	PCO_LOGIC_ERR(this->process.isValid(), "No process running");

	FileBuffered	fb;
	fb.open(&this->process->getStdErr());
	FileInflow		fi(fb);
	e_addFlow(err, fi);
	fb.close();
}
