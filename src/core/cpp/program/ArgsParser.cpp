/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"program/ArgsParser.hpp"
#include "string/StringConstant.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;
using namespace NProgram;

bool	ArgsParser::ParseState::ok(void) const
{
	return this->arg <= this->lastArg;
}

ArgsParser::ArgsParser(const ArgsConfig& cfg, ProgramCommand& cmd)
 : config(cfg), cmd(cmd)
{	}

ActionReport	ArgsParser::parse(int argc, char** argv)
{
	ActionReport	report;

	this->cmd.clear();
	if(argc < 1)
	{
		PCO_ASSERT(false);
		report.addError( StringC("Invalid args") );
		return report;
	}

	// Go through args
	ParseState state;
	// Skip program name
	state.arg = argv + 1;
	state.lastArg = argv + argc - 1;
	state.mode = EMode::option;

	while(state.ok())
	{	//
		const char* arg = *state.arg;
		if(arg == nullptr)
		{
			PCO_ASSERT(false);
			report.addError( StringC("Invalid argument (nullptr)") );
		}
		else
		switch(state.mode)
		{
			case EMode::option:
			{
				this->parseOption(state, report);
			} break;
			case EMode::operand:
			{
				this->parseOperand(state, report);
			} break;
			case EMode::subArg:
			{
				const char* arg = *state.arg;
				cmd.addSubArg(arg);
			} break;
			default:
			{
				PCO_UNK_CASE("New mode ?");
			}
		}

		++state.arg;
	}

	// TODO: check options with detected operand


	return report;
}

void	ArgsParser::parseOption(ParseState& state, ActionReport& report)
{
	PCO_INV_ARG(state.ok(), "");

	const char* arg = *state.arg;
	int len = strlen(arg);
	if(len == 0)
	{	// Ignore empty
		PCO_ASSERT_MSG(false, "Strange state");
		return;
	}

	if(arg[0] != '-')
	{	// Operand
		this->checkOperand(arg, report);
		state.mode = EMode::subArg;
		return;
	}

	if(len == 1)
	{
		report.addError( StringC("Invalid single -") );
		return;
	}

	if(arg[1] == '-')
	{	// --???
		if(len == 2)
		{	// Switch mode
			state.mode = EMode::operand;
			return;
		}
		// Check option
		auto it = this->config.optionByName(arg+2);
		if(!it.isValid())
		{
			String msg = String("Invalid option '") + arg + "'";
			report.addError(msg);
		}
		else
		{
			OptionValue ov(it.cell());
			this->parseValue(ov, state, report);
		}

		return;
	}

	// -??
	char letter = arg[1];
	auto it = this->getOption(letter, report);
	if(!it.isValid())
	{
		return;
	}

	OptionValue ov(it.cell());
	if(it.cell().hasValue())
	{	//
		if(len == 2)
		{
			this->parseValue(ov, state, report);
		}
		else
		{	// Take remain as value
			ov.addValue(arg+2);
		}
		this->cmd.addOption(ov);

		return;
	}

	// Should be a list of option without values
	this->cmd.addOption(ov);

	for(int i = 2 ; i < len ; ++i)
	{
		letter = arg[i];
		// Check option
		auto it = this->getOption(letter, report);
		if(it.isValid())
		{
			if(it.cell().hasValue())
			{
				report.addError( StringC("Mixing option of different kind") );
			}
			else
			{
				OptionValue v(it.cell());
				this->cmd.addOption(v);
			}
		}
	}
}

ArgsConfig::TId2OptionIter	ArgsParser::getOption(const char letter, ActionReport& report)
{
	auto it = this->config.optionByLetter(letter);
	if(!it.isValid())
	{
		String msg = String() + "Invalid option '" + letter + "'";
		report.addError(msg);
	}

	return it;
}

void	ArgsParser::parseValue(OptionValue& option, ParseState& state, ActionReport& report)
{
	PCO_INV_ARG(state.ok(), "");
	++state.arg;
	if(!state.ok())
	{
		report.addError( StringC("Option value expected") );
		return;
	}

	const char* arg = *state.arg;
	option.addValue(arg);
}

void	ArgsParser::parseOperand(ParseState& state, ActionReport& report)
{
	PCO_INV_ARG(state.ok(), "");

	const char* arg = *state.arg;
	int len = strlen(arg);
	if(len == 0)
	{	// Ignore empty
		PCO_ASSERT_MSG(false, "Strange state");
		return;
	}

	// Operand
	this->checkOperand(arg, report);
	state.mode = EMode::subArg;
}

void	ArgsParser::checkOperand(const char* arg, ActionReport& report)
{
	PCO_INV_PTR(arg);
	PCO_INV_ARG(strlen(arg) > 0, "");

	auto it = this->config.operand(arg);
	if(!it.isValid())
	{
		String msg = String() + "Invalid operand '" + arg + "'";
		report.addError(msg);
	}
	else
	{
		this->cmd.setOperand(it.cell());
	}
}
