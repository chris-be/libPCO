/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"program/ProgramCommand.hpp"
#include "cvs/iter/E_HasIndexIter.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;
using namespace NProgram;

const OperandConfig&	ProgramCommand::getOperand(void) const
{
	return this->operand;
}

void	ProgramCommand::setOperand(const OperandConfig& operand)
{
	this->operand = operand;
}

void	ProgramCommand::clear(void)
{
	this->operand = OperandConfig();
	this->options.clear();
	this->subArgs.clear();
}

void	ProgramCommand::addOption(const OptionValue& value)
{
	int id = value.getConfig().getId();
	this->options.add(id, value);
}

auto	ProgramCommand::getOptions(void) const -> const TOptionMap&
{
	return this->options;
}

bool	ProgramCommand::hasOptionValue(int id) const
{
	return this->options.hasKey(id);
}

const NPCO::String&		ProgramCommand::getOptionValue(int id) const
{
	auto it = this->options.iter_r(id);
	PCO_INV_ARG(it.isValid(), "No option");
	const OptionValue& ov = it.cell();
	return ov.getValue();
}

void	ProgramCommand::addSubArg(const ArgName& subArg)
{
	TSubArgList::TType	sa(subArg);
	this->subArgs.moveIn(std::move(sa));
}

auto	ProgramCommand::getSubArgs(void) const -> const TSubArgList&
{
	return this->subArgs;
}

void	ProgramCommand::show(std::ostream& os) const
{
	os << "Operand: " << this->operand.getName();
	if(!this->subArgs.isEmpty())
	{
		os << "\t[";
		bool first = true;
		for(auto c = e_cursor(this->subArgs) ; c.ok() ; ++c)
		{
			if(first)	first = false;
			else		os << ", ";
			os << *c;
		}
		os << "]";
	}
	os << std::endl;

	if(this->options.isEmpty())
	{
		return;
	}

/*
	class TextAligner ....

	// Eval max width
	int maxWidth = 0;
	for(auto c = e_indexCursor(cmd.getOptions()) ; c.ok() ; ++c)
	{
		const OptionConfig& cfg = c.cell().getConfig();
		int w = 2; // -?
		if(cfg.getName() != nullptr)
		{
			w = strlen(cfg.getName()) + 2;
			if(cfg.getLetter().hasValue())
				w+= 5;	// -? | --option_name
		}
		maxWidth = std::max(maxWidth, w);
	}
*/

	os << "Options: ";
	for(auto c = e_indexCursor(this->options) ; c.ok() ; ++c)
	{
		const OptionConfig& cfg = c.cell().getConfig();
		os << std::endl << "  ";

		if(cfg.getLetter().hasValue())
		{
			char l = cfg.getLetter().getValue();
			os << '-' << l;
			if(cfg.getName() != nullptr)
				os << " (--" << cfg.getName() << ")\t";
			else
				os << "\t\t";
		}
		else
		{
			PCO_ASSERT(cfg.getName() != nullptr);
			os << "--" << cfg.getName() << "\t";
		}

		const OptionValue::TArgList& args = c->getValues();
		if(!args.isEmpty())
		{
			os << "\t[";
			bool first = true;
			for(auto v = e_cursor(args) ; v.ok() ; ++v)
			{
				if(first)	first = false;
				else		os << ", ";
				os << *v;
			}
			os << "]";
		}
	}

	os << std::endl;
}
