/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"program/OptionConfig.hpp"

#include "PCO_Predicate.hpp"

using namespace NProgram;

OptionConfig::OptionConfig(int id, bool value, char letter, ArgName name)
{
	this->id	= id;
	this->value	= value;
	this->letter= letter;
	this->name	= name;
}

OptionConfig::OptionConfig(int id, bool value, ArgName name)
{
	PCO_INV_ARG(name != nullptr, "Letter not defined -> name is mandatory");

	this->id	= id;
	this->value	= value;
	this->letter.unset();
	this->name	= name;
}

int		OptionConfig::getId(void) const
{
	return this->id;
}

bool	OptionConfig::hasValue(void) const
{
	return this->value;
}

auto	OptionConfig::getLetter(void) const -> const TLetter&
{
	return this->letter;
}

const ArgName&		OptionConfig::getName(void) const
{
	return this->name;
}
