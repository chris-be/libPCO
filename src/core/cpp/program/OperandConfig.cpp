/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"program/OperandConfig.hpp"

#include "PCO_Predicate.hpp"

using namespace NProgram;

/*
OperandConfig::OperandConfig(TArgName name)
{
	this->name		= name;
}
*/

OperandConfig::OperandConfig(int id, ArgName name)
{
	PCO_INV_ARG(name != nullptr, "Invalid operand name");
	this->id = id;
	this->name = name;
}

int		OperandConfig::getId(void) const
{
	return this->id;
}

const ArgName&		OperandConfig::getName(void) const
{
	return this->name;
}
