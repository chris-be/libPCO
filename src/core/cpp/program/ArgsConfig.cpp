/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"program/ArgsConfig.hpp"
#include "cvs/iter/E_HasIndexIter.hpp"

#include "PCO_Predicate.hpp"

#include <iostream>

using namespace NPCO;
using namespace NProgram;

ArgsConfig::OperandXOption::OperandXOption(int idOperand, int idOption)
 : idOperand(idOperand), idOption(idOption)
{	}

ClassifyValue	ArgsConfig::OperandXOption::classify(const OperandXOption& toCompare) const
{
	ClassifyValue cv;
	cv = this->idOperand - toCompare.idOperand;
	if(cv !=0)	return cv;

	return this->idOption - toCompare.idOption;
}

///

ArgsConfig::ArgsConfig(void)
{	}

void	ArgsConfig::add(const OperandConfig& operand)
{
	ArgName name = operand.getName();
	PCO_ASSERT_MSG(!this->nameOperands.hasKey(name), "Redefining operand");
	this->nameOperands.add(name, operand);
}

auto	ArgsConfig::operand(const ArgName& name) const -> TOperandIter
{
	return this->nameOperands.iter_r(name);
}

void	ArgsConfig::add(const OptionConfig& option)
{
	int id = option.getId();
	PCO_ASSERT_MSG(!this->idOptions.hasKey(id), "Redefining option");
	this->idOptions.add(id, option);

	if(option.getLetter().hasValue())
	{
		char l = option.getLetter().getValue();
		PCO_ASSERT_MSG(!this->letterOptions.hasKey(l), "Redefining option");
		this->letterOptions.add(l, id);
	}

	if(option.getName() != nullptr)
	{
		ArgName name = option.getName();
		PCO_ASSERT_MSG(!this->nameOptions.hasKey(name), "Redefining option");
		this->nameOptions.add(name, id);
	}
}

auto	ArgsConfig::optionById(const int id) const -> TId2OptionIter
{
	return this->idOptions.iter_r(id);
}

auto	ArgsConfig::optionByLetter(const char letter) const -> TId2OptionIter
{
	auto it = this->letterOptions.iter_r(letter);
	return !it.isValid() ? TId2OptionIter() : this->optionById(it.cell());
}

auto	ArgsConfig::optionByName(const ArgName& name) const -> TId2OptionIter
{
	auto it =this->nameOptions.iter_r(name);
	return !it.isValid() ? TId2OptionIter() : this->optionById(it.cell());
}

void	ArgsConfig::link(int idOperand, int idOption, bool mandatory)
{
	OperandXOption key(idOperand, idOption);
	this->operandXOptions.add(key, mandatory);
}

void	ArgsConfig::link(int idOperand, bool mandatory, std::initializer_list<int> idOptions)
{
	for(auto i = idOptions.begin(), e = idOptions.end() ; i != e ; ++i)
	{
		OperandXOption key(idOperand, *i);
		this->operandXOptions.add(key, mandatory);
	}
}

auto	ArgsConfig::getLink(int idOperand, int idOption) const -> TXOptionsIter
{
	OperandXOption key(idOperand, idOption);
	return this->operandXOptions.iter_r(key);
}

bool	ArgsConfig::isValid(void) const
{
	return true;
}

void	ArgsConfig::showUsage(std::ostream& os, const char* progName) const
{
	PCO_INV_PTR(progName);

	os << "Usage of '" << progName << "': ";
	if(this->nameOperands.size() == 0)
	{
		os << "no argument";
	}
	else
	{
		os << "options operand 'sub arguments'";
		os << std::endl << "More info with: help 'operand name'";
		os << std::endl << std::endl << "Available operands: ";
		bool first = true;
		for(auto cur = e_indexCursor(this->nameOperands) ; cur.ok() ; ++cur)
		{
			if(first)		first = false;
			else			os << ", ";
			os << cur.key();
		}
		os << std::endl;
	}

	os << std::endl;
}

void	ArgsConfig::showOperand(std::ostream& os, const char* operand) const
{
	PCO_INV_PTR(operand);

	os << "Usage of '" << operand << "': ";
	auto it = this->nameOperands.iter_r(operand);
	if(!it.isValid())
	{
		os << "not a valid operand" << std::endl;
		return;
	}

	int idOperand = it.cell().getId();
	OperandXOption key(idOperand, 0);
	auto it_op = this->operandXOptions.iter_r_min(key);
	if(!it_op.isValid() || it_op.key().idOperand != idOperand)
	{
		os << "no options" << std::endl;
		return;
	}

    os << std::endl << "Options (! for mandatory):";
	do
	{
		int idOption = it_op.key().idOption;
		bool mandatory = it_op.cell();

		auto it_cfg = this->optionById(idOption);
		PCO_ASSERT(it_cfg.isValid());

		const OptionConfig& cfg = it_cfg.cell();
		os << std::endl << (mandatory ? "! " : "  ");

		if(cfg.getLetter().hasValue())
		{
			char l = cfg.getLetter().getValue();
			os << '-' << l;
			if(cfg.getName() != nullptr)
				os << " (--" << cfg.getName() << ")\t";
			else
				os << "\t\t";
		}
		else
		{
			PCO_ASSERT(cfg.getName() != nullptr);
			os << "--" << cfg.getName() << "\t";
		}

		++it_op;
	} while(it_op.isValid() && it_op.key().idOperand == idOperand);

	os << std::endl;
}
