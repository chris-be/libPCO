/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include "program/OptionValue.hpp"

using namespace NProgram;

OptionValue::OptionValue(const OptionConfig& config)
 : config(config)
{	}

const OptionConfig&	OptionValue::getConfig(void) const
{
	return this->config;
}

void	OptionValue::addValue(const char* value)
{
	this->values.add(value);
}

auto	OptionValue::getValues(void) const -> const TArgList&
{
	return this->values;
}

const NPCO::String&	OptionValue::getValue(void) const
{
	PCO_ASSERT(this->values.size() == 1);
	return this->values.first().cell();
}
