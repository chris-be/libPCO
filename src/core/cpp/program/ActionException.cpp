/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"program/ActionException.hpp"

using namespace NPCO;

ActionException::ActionException(const char* msg, const ActionReport& report)
 : msg(msg), report(report)
{	}

ActionException::ActionException(const ActionReport& report)
 : ActionException("", report)
{	}

ActionException::ActionException(ActionException&& toMove)
{
	const ActionException& toCopy = toMove;
	this->operator=(toCopy);
}

const char* ActionException::what(void) const noexcept
{
	return this->msg;
}

const ActionReport&	ActionException::getReport(void) const
{
	return this->report;
}
