/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"program/ActionReport.hpp"

using namespace NPCO;

void	ActionReport::clear(void)
{
	this->warnings.clear();
	this->errors.clear();
}

auto	ActionReport::evalLevel(void) const -> ELevel
{
	if(this->hasError())
	{
		return ELevel::error;
	}
	if(this->hasWarning())
	{
		return ELevel::warning;
	}

	return ELevel::nothing;
}

void	ActionReport::addWarning(const TMessage& msg)
{
	this->warnings.add(msg);
}

auto	ActionReport::getWarnings(void) const -> const TextList&
{
	return this->warnings;
}

bool	ActionReport::hasWarning(void) const
{
	return !this->warnings.isEmpty();
}

void	ActionReport::addError(const TMessage& msg)
{
	this->errors.add(msg);
}

auto	ActionReport::getErrors(void) const -> const TextList&
{
	return this->errors;
}

bool	ActionReport::hasError(void) const
{
	return !this->errors.isEmpty();
}
