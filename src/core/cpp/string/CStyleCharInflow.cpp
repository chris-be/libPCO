/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/CStyleCharInflow.hpp"
#include "low/Endian.hpp"
#include "string/cvs/E_StringEncoding.hpp" // e_getCurrentEncoding

#include "PCO_Predicate.hpp"

using namespace NPCO;

uw_08	CStyleCharInflow::getCharSize(const EStringEncoding enc)
{
	switch(enc)
	{
		case EStringEncoding::UTF_8:
		{
			return 1;
		} break;
		case EStringEncoding::UTF_16_BE:
		case EStringEncoding::UTF_16_LE:
		{
			return 2;
		} break;
		case EStringEncoding::UTF_32_BE:
		case EStringEncoding::UTF_32_LE:
		{
			return 4;
		} break;
		case EStringEncoding::US_ASCII:
		case EStringEncoding::ISO_8859_1:
		{
			return 1;
		} break;
		default:
		{
			PCO_UNK_CASE("");
			return 1;
		}
	}
}

void	CStyleCharInflow::_set(const char* str, EStringEncoding encoding)
{
	PCO_INV_PTR(str);
	this->str		= str;
	this->encoding	= encoding;
	this->charSize	= CStyleCharInflow::getCharSize(encoding);
	this->toFirst();
}

CStyleCharInflow::CStyleCharInflow(const char* str, EStringEncoding encoding)
{
	this->_set(str, encoding);
}

/*
CStyleCharInflow::CStyleCharInflow(const ICharBag& cb)
{
	this->_set(cb._da(), cb.getEncoding());
}
*/

CStyleCharInflow::CStyleCharInflow(const char* str)
{
	EStringEncoding cenc = StringEncoding::getLocaleEncoding();
	this->_set(str, cenc);
}

CStyleCharInflow::CStyleCharInflow(const char8_t* str)
{
	const char* s = reinterpret_cast<const char*>(str);
	this->_set(s, EStringEncoding::UTF_8);
}

CStyleCharInflow::CStyleCharInflow(const char16_t* str, bool bigEndian)
{
	const char* s = reinterpret_cast<const char*>(str);
	EStringEncoding e = bigEndian ? EStringEncoding::UTF_16_BE : EStringEncoding::UTF_16_LE;
	this->_set(s, e);
}

CStyleCharInflow::CStyleCharInflow(const char32_t* str, bool bigEndian)
{
	const char* s = reinterpret_cast<const char*>(str);
	EStringEncoding e = bigEndian ? EStringEncoding::UTF_32_BE : EStringEncoding::UTF_32_LE;
	this->_set(s, e);
}

CStyleCharInflow::CStyleCharInflow(const char16_t* str)
 : CStyleCharInflow(str, (Endian::detect() == EEndianMode::eBig))
{	}

CStyleCharInflow::CStyleCharInflow(const char32_t* str)
 : CStyleCharInflow(str, (Endian::detect() == EEndianMode::eBig))
{	}

EStringEncoding	CStyleCharInflow::getEncoding(void) const
{
	return this->encoding;
}

uw_08	CStyleCharInflow::getCharSize(void) const
{
	return this->charSize;
}

array_size	CStyleCharInflow::strlen(void) const
{
	int s = 0;

	switch(this->encoding)
	{
		case EStringEncoding::UTF_32_LE:
		case EStringEncoding::UTF_32_BE:
		{
			for(const uw_32* t = (uw_32*)this->str ; *t != '\0' ; ++s, ++t);
		} break;

		case EStringEncoding::UTF_16_LE:
		case EStringEncoding::UTF_16_BE:
		{
			for(const uw_16* t = (uw_16*)this->str ; *t != '\0' ; ++s, ++t);
		} break;

		case EStringEncoding::UTF_8:
		{
			for(const uw_08* t = (uw_08*)this->str ; *t != '\0' ; ++s, ++t);
		} break;

		default:
		{
			PCO_UNK_CASE("");
			for(const uw_08* t = (uw_08*)this->str ; *t != '\0' ; ++s, ++t);
		} break;
	}

	return s;
}

void	CStyleCharInflow::toFirst(void)
{
	this->cur = this->str;
	this->nbToRead = 0;
}

bool	CStyleCharInflow::read(char& element)
{
	if(nbToRead == 0)
	{	// Check if end of string
		switch(this->charSize)
		{
			case 1:
			{
				if( *this->cur == '\0')
					return false;
			} break;
			case 2:
			{
				if( *(const uw_16*)(this->cur) == '\0' )
					return false;
				nbToRead = 1;
			} break;
			case 4:
			{
				if( *(const uw_32*)(this->cur) == '\0' )
					return false;
				nbToRead = 3;
			} break;
			default:
				PCO_UNK_CASE("??");
		}
	}
	else
	{
		--nbToRead;
	}

	// Move next
	element = *(this->cur);
	++this->cur;
	return true;
}
