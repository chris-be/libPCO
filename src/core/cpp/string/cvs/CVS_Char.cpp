/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/cvs/CVS_Char.hpp"

using namespace NPCO;

bool	CVS_Char::isControl(char c)
{
	return c < 32 || c == 127;
}

bool	CVS_Char::isDigit(char c)
{
	return (c >= '0') && (c <= '9');
}

bool	CVS_Char::isChar(char c)
{
	return CVS_Char::isLower(c) || CVS_Char::isUpper(c);
}

bool	CVS_Char::isUpper(char c)
{
	return (c >= 'A') && (c <= 'Z');
}

bool	CVS_Char::isLower(char c)
{
	return (c >= 'a') && (c <= 'z');
}

char	CVS_Char::upper(char c)
{
	if((c < 'a') || (c > 'z'))
	{
		return c;
	}

	return c ^ (1 << 5);
}

char	CVS_Char::lower(char c)
{
	if((c < 'A') || (c > 'Z'))
	{
		return c;
	}

	return c ^ (1 << 5);
}
