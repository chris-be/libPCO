/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/cvs/CVS_Unicode.hpp"

using namespace NPCO;

const uw_16		CVS_Unicode::ReservedRangeLow		= 0xD800;
const uw_16		CVS_Unicode::ReservedRangeHigh		= 0xDC00;

const uw_32		CVS_Unicode::MaxValue				= 0x10FFFF;

const uw_16		CVS_Unicode::Surrogate16High		= 0xD800;
const uw_16		CVS_Unicode::Surrogate16Low			= 0xDC00;

//const uw_32 Max08For1			= (1 << 8)-1;	// 7
const uw_32		CVS_Unicode::Max08For2			= (1 << 11)-1;	// 5+6
const uw_32		CVS_Unicode::Max08For3			= (1 << 16)-1;	// 4+6+6
const uw_32		CVS_Unicode::Max08For4 			= (1 << 21)-1;	// 3+6+6+6


const uw_32		CVS_Unicode::ErrorCode			= 0xFFFD0000;

bool	CVS_Unicode::isSurrogate(uw_16 c)
{
	// 0b11011xxxxxxxxxxx
	return ((c & 0xF800) == Surrogate16Low);
}

bool	CVS_Unicode::isHighSurrogate(uw_16 c)
{
	// 0b110110xxxxxxxxxx
	return ((c & 0xFC00) == Surrogate16High);
}

bool	CVS_Unicode::isLowSurrogate(uw_16 c)
{
	// 0b110111xxxxxxxxxx
	return ((c & 0xFC00) == Surrogate16Low);
}

bool	CVS_Unicode::isValid(uw_32 c)
{
	// Trivial check (also should be more often than greater)
	if(c < ReservedRangeLow)
	{
		return true;
	}

	return (c > ReservedRangeHigh) && (c <= MaxValue);
}

bool	CVS_Unicode::isControl(uw_32 c)
{
	return true; // iswcntrl(;
}

bool	CVS_Unicode::isDigit(uw_32 c)
{
	return (c >= '0') && (c <= '9');
}

bool	CVS_Unicode::isChar(uw_32 c)
{
	if(c >= ('a'-1))
	{
		// "convert" to lower "range"
		c = c & 0x19;
	}

	return CVS_Unicode::isUpper(c);
}

bool	CVS_Unicode::isUpper(uw_32 c)
{
	return (c >= 'A') && (c <= 'Z');
}

bool	CVS_Unicode::isLower(uw_32 c)
{
	return (c >= 'a') && (c <= 'z');
}

uw_32	CVS_Unicode::upper(uw_32 c)
{
	if((c < 'a') || (c > 'z'))
	{
		return c;
	}

	return c ^ (1 << 5);
}

uw_32	CVS_Unicode::lower(uw_32 c)
{
	if((c < 'A') || (c > 'Z'))
	{
		return c;
	}

	return c ^ (1 << 5);
}
