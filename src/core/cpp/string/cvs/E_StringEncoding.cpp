/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/cvs/E_StringEncoding.hpp"
#include "string/CharsetDecoder.hpp"
#include "string/UTFDecoder.hpp"
#include "string/UTFEncoder.hpp"
#include "flow/TransformInflow.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;

bool	NPCO::e_isUTF(const EStringEncoding enc)
{
	const uw_16	mask	= 0x7FFF; // ~LITTLE_ENDIAN_FLAG;
	const uw_16	low		= (uw_16)EStringEncoding::UTF_8;
	const uw_16	high	= (uw_16)EStringEncoding::UTF_32_BE;

	// Remove any LITTLE_ENDIAN_FLAG
	uw_16 test	= ((uw_16)enc) & mask;
	// Take care of "unknown" value
	return (test <= high) && (test >= low);
}

EUTF	NPCO::e_getUTF(const EStringEncoding enc)
{
	PCO_RANGE_ERR(e_isUTF(enc), "No utf format");

	switch(enc)
	{
		case EStringEncoding::UTF_32_LE:
		{
			return EUTF::_32_LE;
		} break;
		case EStringEncoding::UTF_32_BE:
		{
			return EUTF::_32_BE;
		} break;

		case EStringEncoding::UTF_16_LE:
		{
			return EUTF::_16_LE;
		} break;
		case EStringEncoding::UTF_16_BE:
		{
			return EUTF::_16_BE;
		} break;

		case EStringEncoding::UTF_8:
		{
			return EUTF::_8;
		} break;

		default:
		{
			PCO_UNK_CASE("Wrong argument");
		}
	}

	throw std::runtime_error("Wrong argument");
}

EStringEncoding		NPCO::e_getStringEncoding(const EUTF utf)
{
	switch(utf)
	{
		case EUTF::_32_LE:
		{
			return EStringEncoding::UTF_32_LE;
		} break;
		case EUTF::_32_BE:
		{
			return EStringEncoding::UTF_32_BE;
		} break;

		case EUTF::_16_LE:
		{
			return EStringEncoding::UTF_16_LE;
		} break;
		case EUTF::_16_BE:
		{
			return EStringEncoding::UTF_16_BE;
		} break;

		case EUTF::_8:
		{
			return EStringEncoding::UTF_8;
		} break;

	}

	PCO_UNK_CASE("Adapt code");
}

IInflow<char32_t>*	NPCO::e_utf32Inflow(RefRes<CharInflow>& src, EStringEncoding fromEncoding)
{
	IInflow<char32_t>*	decoder;

	if(e_isUTF(fromEncoding))
	{
		TransformInflow<char8_t, char>	ti(src, rawConvert<char8_t>);
		auto resInf = RefRes<IInflow<char8_t>>::from(std::move(ti));
		decoder = new UTFDecoder(resInf, e_getUTF(fromEncoding));
	}
	else
	{
		decoder = new CharsetDecoder(src, fromEncoding);
	}

	return decoder;
}

IInflow<char32_t>*	NPCO::e_utf32Inflow(const IStringCarrier& sc)
{
	RefRes<CharInflow>	src( sc.createCharInflow() );
	return e_utf32Inflow(src, sc.getEncoding());
}

IInflow<char8_t>*	NPCO::e_utf8Inflow(RefRes<CharInflow>& src, EStringEncoding fromEncoding)
{
	IInflow<char8_t>*	encoder;

	if(fromEncoding == EStringEncoding::UTF_8)
	{
		encoder = new TransformInflow<char8_t, char>(src, rawConvert<char8_t>);
	}
	else
	{
		RefRes<IInflow<char32_t>> decoder = e_utf32Inflow(src, fromEncoding);
		encoder = new UTFEncoder(decoder, EUTF::_8);
	}

	return encoder;
}

IInflow<char8_t>*	NPCO::e_utf8Inflow(const IStringCarrier& sc)
{
	RefRes<CharInflow>	src( sc.createCharInflow() );
	return e_utf8Inflow(src, sc.getEncoding());
}

std::ostream&	NPCO::operator<<(std::ostream& stream, const IStringCarrier& toEcho)
{
	//TODO switch(toEcho.getEncoding()) CharsetConverter ...

	CharInflow* ci = toEcho.createCharInflow();
	AutoDelete<CharInflow> cleaner(ci);

	char c;
	while(ci->read(c))
	{
		stream << c;
	};

	return stream;
}
