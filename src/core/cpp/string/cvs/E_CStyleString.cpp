/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/cvs/E_CStyleString.hpp"
#include "mem/Mem.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;

mem_size	NPCO::e_strlen(const char* str)
{
	return ::strlen(str);
}

mem_size	NPCO::e_strlen(const char8_t* str)
{
	return ::strlen( reinterpret_cast<const char*>(str) );
}

template<class PChar>
PChar*		_strcpy(const PChar* toCopy)
{
	if(toCopy == nullptr)
	{
		return nullptr;
	}

	PChar* str;
	int size = e_strlen(toCopy);
	Mem::malloc(str, size+1);
	Mem::memcpy(str, toCopy, size+1);

	return str;
}

char*		NPCO::e_strcpy(const char* toCopy)
{
	return _strcpy<char>(toCopy);
}

char8_t*	NPCO::e_strcpy(const char8_t* toCopy)
{
	return _strcpy<char8_t>(toCopy);
}

void		NPCO::e_free(char8_t* &toFree)
{
	Mem::free(toFree);
}

void		NPCO::e_free(char* &toFree)
{
	Mem::free(toFree);
}
