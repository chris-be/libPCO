/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include "string/CStringHolder.hpp"
#include "string/StringConstant.hpp"
#include "string/CharsetConverter.hpp"
#include "string/cvs/E_StringEncoding.hpp"
#include "cvs/bag/E_DynBag.hpp"
#include "cvs/res/AutoDelete.hpp"

using namespace NPCO;

void	CStringHolder::set(CharInflow& src, EStringEncoding encoding)
{
	if(this->str.isNull())
	{
		TChars chars;
		this->str = TRef::from(std::move(chars));
	}
	else
	{
		this->str->clear();
	}

	// Copy data to current encoding
	EStringEncoding		cenc = StringEncoding::getLocaleEncoding();
	CharsetConverter	csc(src, encoding, cenc);

	e_addFlow(*this->str, csc);
	auto it_r = this->str->last();
	if(!it_r.isValid() || (*it_r != '\0'))
	{	// Ensure zero
		this->str->add('\0');
	}

	// Simply point to buffer
	this->const_str = (*this->str)._da();

}

CStringHolder::CStringHolder(void)
{
	this->const_str = "";
}

CStringHolder::CStringHolder(const char* str, EStringEncoding encoding)
{
	this->set(str, encoding);
}

CStringHolder::CStringHolder(const IStringCarrier& sc)
{
	this->set(sc);
}

void	CStringHolder::set(const char* str, EStringEncoding encoding)
{
	PCO_INV_PTR(str);
	EStringEncoding cenc = StringEncoding::getLocaleEncoding();
	if(cenc == encoding)
	{	// No need to convert
		this->const_str = str;
		this->str.unset();
		return;
	}

	// Convert
	this->set( StringC(str, encoding) );
}

void	CStringHolder::set(const IStringCarrier& sc)
{
	CharInflow*	ci = sc.createCharInflow();
	AutoDelete<CharInflow>	cleaner(ci);
	this->set(*ci, sc.getEncoding());
}

const char*		CStringHolder::const_char_ptr(void) const
{
	return this->const_str;
}

// CStringHolder::operator const char*(void) const

char*		CStringHolder::char_ptr(void)
{
	if(this->str.isNull())
	{	// Create a copy
		StringC	src(this->const_str);
		this->set(src);
	}

	return this->str->_da();
}

// operator	CStringHolder::char*(void)
