/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/CharBlock.hpp"
#include "string/cvs/E_StringEncoding.hpp"
#include "mem/cvs/CVS_Mem.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;

CharBlock::CharBlock(void)
{
	EStringEncoding cenc = StringEncoding::getLocaleEncoding();
	this->set("", cenc, 0);
}

CharBlock::CharBlock(const char* str, EStringEncoding encoding, array_size size)
{
	this->set(str, encoding, size);
}

void	CharBlock::set(const char* str, EStringEncoding encoding, array_size size)
{
	PCO_INV_PTR(str);
	this->str = str;
	this->encoding = encoding;
	this->nbBytes = size;
}

EStringEncoding	CharBlock::getEncoding(void) const
{
	return this->encoding;
}

array_size	CharBlock::size(void) const
{
	return this->nbBytes;
}

CharInflow*	CharBlock::createCharInflow(void) const
{
	return CharBlock::createCharInflow(this->str, this->nbBytes);
}

CStringHolder	CharBlock::c_str_wrap(void) const
{
	return CStringHolder(*this);
}

const char*	CharBlock::_da(void) const
{
	return this->str;
}

CharInflow*	CharBlock::createCharInflow(const char* str, array_size size)
{
	auto cur = CVS_Mem::cursor<char>(str, size);
	return new decltype(cur)(std::move(cur));
}
