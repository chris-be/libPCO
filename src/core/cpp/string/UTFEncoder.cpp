/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/UTFEncoder.hpp"
#include "string/cvs/CVS_Unicode.hpp"

#include "PCO_Predicate.hpp"

#include <stdexcept> // out_of_range

using namespace NPCO;

UTFEncoder::UTFEncoder(TResource src, EUTF format)
 : TBase(src), encoder(format)
{	}

bool	UTFEncoder::read(char8_t& element)
{
	if(!this->encoder.leftToRead())
	{
		char32_t c;
		if(!this->src->read(c))
		{
			return false;
		}

		this->encoder.encode(c);
	}

	this->encoder.pull(element);
	return true;
}
