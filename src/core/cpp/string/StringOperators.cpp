/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include "string/String.hpp"
#include "string/CIString.hpp"
#include "string/StringConstant.hpp"

using namespace NPCO;

// Generic

template<class PString>
PCO_INLINE
PString	operatorConcat(const PString& left, const PString& right)
{
	PString	concat(left);
	concat.add(right);
	return concat;
}

template<class PString>
PCO_INLINE
PString	operatorConcat(const PString& left, const IStringCarrier& right)
{
	PString	concat(left);
	concat.add(right);
	return concat;
}

template<class PString, class PChar>
PCO_INLINE
PString	operatorConcat(const PString& left, const PChar* right)
{
	PString	concat(left);
	StringC	stc(right);
	concat.add(stc);
	return concat;
}

template<class PString, class PChar = typename PString::TChar>
PCO_INLINE
PString	operatorConcat(const PString& left, const PChar right)
{
	PString	concat(left);
	concat.add(right);
	return concat;
}

// String

String	NPCO::operator+(const String& left, const String& right)
{
	return operatorConcat(left, right);
}

String	NPCO::operator+(const String& left, const IStringCarrier& right)
{
	return operatorConcat(left, right);
}

String	NPCO::operator+(const String& left, const char8_t* right)
{
	return operatorConcat(left, right);
}

String	NPCO::operator+(const String& left, const char8_t right)
{
	return operatorConcat(left, right);
}

String	NPCO::operator+(const String& left, const char* right)
{
	return operatorConcat(left, right);
}

String	NPCO::operator+(const String& left, const char right)
{
	return operatorConcat(left, right);
}

// CIString

CIString	NPCO::operator+(const CIString& left, const CIString& right)
{
	return operatorConcat(left, right);
}

CIString	NPCO::operator+(const CIString& left, const IStringCarrier& right)
{
	return operatorConcat(left, right);
}

CIString	NPCO::operator+(const CIString& left, const char8_t* right)
{
	return operatorConcat(left, right);
}

CIString	NPCO::operator+(const CIString& left, const char8_t right)
{
	return operatorConcat(left, right);
}

CIString	NPCO::operator+(const CIString& left, const char* right)
{
	return operatorConcat(left, right);
}

CIString	NPCO::operator+(const CIString& left, const char right)
{
	return operatorConcat(left, right);
}
