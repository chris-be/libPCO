/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/CIString.hpp"
#include "string/cfr/Char8CIClassifier.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;

CIString::CIString(const char* str, EStringEncoding encoding)
{
	TBase::set(str, encoding);
}

CIString::CIString(const IStringCarrier& sc)
{
	TBase::operator=(sc);
}

CIString::CIString(const char* str)
{
	TBase::operator=(str);
}

CIString::CIString(const char16_t* str)
{
	TBase::operator=(str);
}

CIString::CIString(const char32_t* str)
{
	TBase::operator=(str);
}

int		CIString::classify(const CIString& toCompare) const
{	//WRONG !!
	return e_lexiSameCompare(this->chars, toCompare.chars, Char8CIClassifier());
}
