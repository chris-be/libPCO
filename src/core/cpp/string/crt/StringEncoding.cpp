/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/crt/StringEncoding.hpp"

#include "col/MapTree.hpp"
#include "string/cfr/CStringClassifier.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;

#include "PCO_CompilerPlatform.hpp"
#if defined FOR_POSIX || defined FOR_LINUX
	#include "imp/POSIX_StringEncoding.cppi"
#elif defined FOR_WINDOWS
	#error	Unsupported now
	#include "imp/WIN_StringEncoding.cppi"
#else
	#error	Unspecified or Unknown Platform
#endif

using TName2Encoding	= MapTree<const char*, EStringEncoding, CStringClassifier>;

// Ensure initialization done at first call
class Encodings {

	using TP		= typename TName2Encoding::TInitPair;

public:
	static const TName2Encoding& getName2Encoding(void)
	{
		// Created at first call
		static const TName2Encoding name2Encoding =
		{
			  TP("UTF-8"			, EStringEncoding::UTF_8)
			, TP("UTF-16"			, EStringEncoding::UTF_16)
			, TP("UTF-32"			, EStringEncoding::UTF_32)

			, TP("ANSI_X3.4-1968"	, EStringEncoding::US_ASCII)
			, TP("ISO-8859-1"		, EStringEncoding::ISO_8859_1)
		};

		return name2Encoding;
	}

	//! Encoding chosen - default getLocaleEncoding state at construction
	class EncodingCache {
	public:
		EStringEncoding		encoding;

		EncodingCache(void)
		{
			const char* encName = StringEncoding::getLocaleName();
			[[maybe_unused]] bool rc = StringEncoding::findEncoding(encName, this->encoding);
			PCO_ASSERT_MSG(rc, "Bad state");
		}
	};

	static EncodingCache& getEncodingCache(void)
	{
		// Created at first call
		static EncodingCache encCache;

		return encCache;
	}
};

bool		StringEncoding::findEncoding(const char* encodingName, EStringEncoding& enc)
{
	PCO_INV_PTR(encodingName);

	const TName2Encoding& n2e = Encodings::getName2Encoding();
	auto it_r = n2e.find(encodingName);
	bool rc = it_r.isValid();
	if(rc)
	{
		enc = *it_r;
	}

	return rc;
}

EStringEncoding		StringEncoding::getLocaleEncoding(void)
{
	const Encodings::EncodingCache& ec = Encodings::getEncodingCache();
	return ec.encoding;
}

void				StringEncoding::setLocaleEncoding(int category, const char *locale)
{
	const char* encName = StringEncoding::setLocale(category, locale);

	Encodings::EncodingCache& ec = Encodings::getEncodingCache();
	bool rc = StringEncoding::findEncoding(encName, ec.encoding);
	if(!rc)
	{
		throw std::runtime_error("Bad state");
	}
}
