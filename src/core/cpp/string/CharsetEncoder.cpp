/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/CharsetEncoder.hpp"
#include "string/cvs/E_StringEncoding.hpp"
#include "string/cvs/CVS_Unicode.hpp"

using namespace NPCO;

void	CharsetEncoder::setEncoding(EStringEncoding encoding)
{
	PCO_INV_ARG(!e_isUTF(encoding), "Only for ISO-... charsets");
	this->encoding = encoding;
	switch(encoding)
	{
		case EStringEncoding::US_ASCII:
		case EStringEncoding::ISO_8859_1:
		{
			this->readFunc = &CharsetEncoder::passThrough;
		} break;
		default:
		{
			PCO_UNK_CASE("Not implemented yet");
			this->readFunc = &CharsetEncoder::passThrough;
		}
	}
}

uw_32	CharsetEncoder::convert(char32_t c)
{
	uw_32 code = rawConvert<uw_32>(c);
	if(!CVS_Unicode::isValid(code))
	{
		throw std::out_of_range("Invalid codepoint value");
	}

	return code;
}

bool	CharsetEncoder::passThrough(char& element)
{
	char32_t c;
	if(!this->src->read(c))
	{
		return false;
	}

	uw_32 code = this->convert(c);
	if(code > 255)
	{
		PCO_LOGIC_ERR(false, "What to do ?");
	}

	element = rawConvert<char>( (uw_08)code );
	return true;
}

CharsetEncoder::CharsetEncoder(TResource src, EStringEncoding encoding)
 : InflowFilterBase(src)
{
	this->setEncoding(encoding);
}

bool	CharsetEncoder::read(char& element)
{
	PCO_LOGIC_ERR(this->readFunc != nullptr, "");

	return (this->*readFunc)(element);
}
