/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/cfr/CStringClassifier.hpp"
// #include <string>

#include <string.h>

using namespace NPCO;

ClassifyValue	CStringClassifier::classify(const char* o1, const char* o2) const
{
	return ::strcmp(o1, o2);
}
