/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/cfr/Char8CIClassifier.hpp"
#include "string/cvs/CVS_Char.hpp"
#include "PCO_Types.hpp"

using namespace NPCO;

ClassifyValue	Char8CIClassifier::classify(const char8_t o1, char8_t o2) const
{
	char c1 = rawConvert<char>(o1);
	char c2 = rawConvert<char>(o2);

	return CVS_Char::lower(c1) - CVS_Char::lower(c2);
}
