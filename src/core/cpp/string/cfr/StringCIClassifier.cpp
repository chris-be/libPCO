/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/cfr/StringCIClassifier.hpp"
#include "string/cvs/CVS_Char.hpp"

using namespace NPCO;

ClassifyValue	StringCIClassifier::classify(const String& s1, const String& s2) const
{
	return s1.classify_CI(s2);
}
