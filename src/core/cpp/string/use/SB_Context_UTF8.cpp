/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/use/SB_Context_UTF8.hpp"
#include "string/cvs/E_StringEncoding.hpp" // e_utf
#include "flow/TransformInflow.hpp"
#include "string/cvs/CVS_Char.hpp"

using namespace NPCO;

const EStringEncoding				SB_Context_UTF8::encoding = EStringEncoding::UTF_8;

const SB_Context_UTF8::TCharSet		SB_Context_UTF8::default_trim_chars(
	{
		  CVS_Char::space<char8_t>()
		, CVS_Char::tab<char8_t>()
		, CVS_Char::carriage_return<char8_t>()
		, CVS_Char::line_feed<char8_t>()
	}
);

void	SB_Context_UTF8::toLower(char8_t& c)
{
	if(c < 128)
	{	// WRONG !! TODO
		char t = rawConvert<char>(c);
		t = CVS_Char::lower(t);
		c = rawConvert<char8_t>(t);
	}
}

void	SB_Context_UTF8::toUpper(char8_t& c)
{
	if(c < 128)
	{
		char t = rawConvert<char>(c);
		t = CVS_Char::upper(t);
		c = rawConvert<char8_t>(t);
	}
}

IInflow<char8_t>*	SB_Context_UTF8::inflow(const IStringCarrier& sc)
{
    return e_utf8Inflow(sc);
}

CharInflow*			SB_Context_UTF8::charInflow(RefRes<IInflow<char8_t>>& ri)
{
	return new TransformInflow<char, char8_t>(ri, rawConvert<char>);
}
