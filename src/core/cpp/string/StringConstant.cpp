/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/StringConstant.hpp"
#include "low/Endian.hpp"
#include "string/CStyleCharInflow.hpp"
#include "string/cvs/E_StringEncoding.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;

static const char8_t* StringC_Empty		= U8_STR("");

const StringC		StringC::empty( StringC_Empty );

void	StringC::_set(const char* str, EStringEncoding encoding)
{
	this->charBlock.set(str, encoding, 0);
	// Compute when needed
	this->lengthOk = false;
}

void	StringC::_setLength(array_size length)
{
	this->charBlock.set(this->charBlock._da(), this->charBlock.getEncoding(), length);
	this->lengthOk = true;
}

StringC::StringC(void)
 : StringC(StringC_Empty)
{	}

StringC::StringC(const char* str, EStringEncoding encoding)
{
	this->set(str, encoding);
}

StringC::StringC(const char* str)
{
	this->set(str);
}

StringC::StringC(const char8_t* str)
{
	this->set(str);
}

StringC::StringC(const char16_t* str, bool bigEndian)
{
	this->set(str, bigEndian);
}

StringC::StringC(const char32_t* str, bool bigEndian)
{
	this->set(str, bigEndian);
}

StringC::StringC(const char16_t* str)
{
	this->set(str);
}

StringC::StringC(const char32_t* str)
{
	this->set(str);
}

void	StringC::set(const char* str, EStringEncoding encoding)
{
	this->_set(str, encoding);
}

void	StringC::set(const char* str)
{
	EStringEncoding cenc = StringEncoding::getLocaleEncoding();
	this->set(str, cenc);
}

void	StringC::set(const char8_t* str)
{
	this->_set( (const char*) str, EStringEncoding::UTF_8);
}

void	StringC::set(const char16_t* str, bool bigEndian)
{
	EStringEncoding e = bigEndian ? EStringEncoding::UTF_16_BE : EStringEncoding::UTF_16_LE;
	this->_set( (const char*) str, e);
}

void	StringC::set(const char32_t* str, bool bigEndian)
{
	EStringEncoding e = bigEndian ? EStringEncoding::UTF_32_BE : EStringEncoding::UTF_32_LE;
	this->_set( (const char*) str, e);
}

void	StringC::set(const char16_t* str)
{
	this->set(str, (Endian::detect() == EEndianMode::eBig));
}

void	StringC::set(const char32_t* str)
{
	this->set(str, (Endian::detect() == EEndianMode::eBig));
}

EStringEncoding	StringC::getEncoding(void) const
{
	return this->charBlock.getEncoding();
}

array_size	StringC::size(void) const
{
	if(!this->lengthOk)
	{
		CStyleCharInflow	si(this->charBlock._da(), this->charBlock.getEncoding());
		array_size		l = si.strlen();
		// const cheat to set length at first call
		StringC& sc = const_cast<StringC&>(*this);
		sc._setLength(l);
	}

	return this->charBlock.size();
}

CharInflow*	StringC::createCharInflow(void) const
{
	return new CStyleCharInflow(this->charBlock._da(), this->charBlock.getEncoding());
}

CStringHolder	StringC::c_str_wrap(void) const
{
	const char* p = this->charBlock._da();
	return CStringHolder(p, this->getEncoding());
}
