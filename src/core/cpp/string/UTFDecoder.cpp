/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/UTFDecoder.hpp"
#include "string/cvs/CVS_Unicode.hpp"

#include "PCO_Predicate.hpp"

#include <stdexcept> // out_of_range

using namespace NPCO;

UTFDecoder::UTFDecoder(TResource src, EUTF format)
 : InflowFilterBase(src)
 , endianConverter(EEndianMode::eBig)	// Bytes are read in "big endian" mode.
{
	this->setFormat(format);
}

void	UTFDecoder::setFormat(EUTF format)
{
	this->format = format;
	switch(format)
	{
		case EUTF::_8:
		{
			this->readFunc = &UTFDecoder::readUTF8;
		} break;
		case EUTF::_16_BE:
		{
			this->readFunc = &UTFDecoder::readUTF16;
			this->endianConverter.setMode(EEndianMode::eBig);
		} break;
		case EUTF::_16_LE:
		{
			this->readFunc = &UTFDecoder::readUTF16;
			this->endianConverter.setMode(EEndianMode::eLittle);
		} break;
		case EUTF::_32_BE:
		{
			this->readFunc = &UTFDecoder::readUTF32;
			this->endianConverter.setMode(EEndianMode::eBig);
		} break;
		case EUTF::_32_LE:
		{
			this->readFunc = &UTFDecoder::readUTF32;
			this->endianConverter.setMode(EEndianMode::eLittle);
		} break;
		default:
		{
			PCO_UNK_CASE("Unicode format");
			this->readFunc = &UTFDecoder::readUTF8;
		}
	}
}

bool	UTFDecoder::readChar(uw_08& w)
{
	char8_t c;
	if(!this->src->read(c))
	{
		return false;
	}

	w = rawConvert<char8_t>(c);
	return true;
}

bool	UTFDecoder::_readWord(uw_16& w)
{
	uw_08 c;
	if(!this->readChar(c))
	{
		return false;
	}

	w = c;
	if(!this->readChar(c))
	{
		throw std::out_of_range("Less bytes than excepted");
	}

	w = (w << 8) | c;
	this->endianConverter.fromOutside(w);

	return true;
}

bool	UTFDecoder::readUTF8(char32_t& element)
{
	uw_08 c;
	if(!this->readChar(c))
	{
		return false;
	}

	if((c & 0x80) == 0)
	{	// 0b0xxxxxxx : ASCII => keep it
		element = rawConvert<char32_t, uw_32>(c);
		return true;
	}

	uw_32 u;
/*
	// Compute number of byte to read
	int	length;
	if( (c & 0xF8) == 0xF0 )
	{	// 0b11110xxx
		u = c & 0x07;
		length = 3;
	}
	else if( (c & 0xF0) == 0xE0 )
	{	// 0b1110xxxx
		u = c & 0x0F;
		length = 2;
	}
	else if( (c & 0xE0) == 0xC0 )
	{	// 0b110xxxxx
		u = c & 0x1F;
		length = 1;
	}
	else
	{	// Something wrong
		throw std::out_of_range("Bad code (utf-8)");
	}

	for( ; length > 0 ; --length)
	{
		if(!this->src.read(c))
		{
			throw std::out_of_range("Less bytes than excepted");
		}

		if( (c & 0xC0) != 0x80 )
		{	// ! 0b10xxxxxx
			throw std::out_of_range("Bad code (utf-8)");
		}

		u = (u << 6) | (c & 0x3F);
	}

	best case:	5 tests	+ 2&	for 3 bytes
	worst case:	5 tests	+ 3&	for 1 byte
*/

	u = c;
	// 0b11110xxx -> _111 0xxx
	// 0b1110xxxx -> _110 xxxx
	// 0b110xxxxx -> _10x xxxx
	// Keep the 3 significant bits (and ensure end of loop)
	unsigned int flags = c & 0x70;

	unsigned int witness = 0x40;
	for( ; (flags & witness) > 0 ; witness >>= 1)
	{	// Test them "one by one"
		if(!this->readChar(c))
		{
			throw std::out_of_range("Less bytes than excepted");
		}

		if( (c & 0xC0) != 0x80 )
		{	// ! 0b10xxxxxx
			throw std::out_of_range("Bad code (utf-8)");
		}

		u = (u << 6) | (c & 0x3F);
	}

	// Clear "code" garbage
	switch(witness)
	{
		case 0x40:
		{	// 0b10xxxxxx
			throw std::out_of_range("Bad code (utf-8)");
		}
		case 0x20:
		{	// 1 time
			u = u & CVS_Unicode::Max08For2;
		} break;
		case 0x10:
		{	// 2 times
			u = u & CVS_Unicode::Max08For3;
		} break;
		case 0x08:
		{	// 3 times
			if((u & (1 << 21)) > 0)
			{	// 0b11111xxx
				throw std::out_of_range("Bad code (utf-8)");
			}
			u = u & CVS_Unicode::Max08For4;
		} break;
	}
/*
	best case:	1 test	+ 1 switch	+ 2&	for 1 byte
	worst case:	4 tests	+ 1 switch	+ 5&	for 3 bytes
	=> should be statically better (even with switch implementation efficiency)
*/

	element = rawConvert<char32_t, uw_32>(u);
	return true;
}

bool	UTFDecoder::readUTF16(char32_t& element)
{
	uw_16 word;
	if(!this->_readWord(word))
	{	// End
		return false;
	}

	if(!CVS_Unicode::isSurrogate(word))
	{	// No surrogate => valid codepoint
		element = rawConvert<char32_t, uw_32>(word);
		return true;
	}

	if(!CVS_Unicode::isHighSurrogate(word))
	{	// Not high surogate
		throw std::out_of_range("High surrogate expected (utf-16)");
	}

	uw_32 u = (word - 0xD800) << 10;
	if(!this->_readWord(word))
	{	// Not enough data
		throw std::out_of_range("Less bytes than excepted");
	}

	if(!CVS_Unicode::isLowSurrogate(word))
	{	// Not low surrogate => invalid codepoint
		throw std::out_of_range("Low surrogate expected (utf-16)");
	}

	u = u | (word - 0xDC00);
	u = u + 0x10000;
	element = rawConvert<char32_t, uw_32>(u);
	return true;
}

bool	UTFDecoder::readUTF32(char32_t& element)
{
	uw_08 c;
	if(!this->readChar(c))
	{
		return false;
	}

	uw_32 u = c;
	for(int i = 3 ; i > 0 ; --i)
	{
		if(!this->readChar(c))
		{
			throw std::out_of_range("Less bytes than excepted");
		}

		u = (u << 8) | c;
	}

	this->endianConverter.fromOutside(u);
	element = rawConvert<char32_t>(u);
	return true;
}

bool	UTFDecoder::read(char32_t& element)
{
	PCO_LOGIC_ERR(this->readFunc != nullptr, "");

	bool rc;
	try
	{
		rc = (this->*readFunc)(element);
	}
	catch(std::out_of_range e)
	{	// Bad data or partially loaded data (less bytes than it should be)
		element = CVS_Unicode::ErrorCode;
		rc = true;
	}

	return rc;
}
