/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/Char32Encoder.hpp"
#include "string/cvs/CVS_Unicode.hpp"

#include "PCO_Predicate.hpp"

#include <stdexcept> // out_of_range

using namespace NPCO;

Char32Encoder::Char32Encoder(EUTF format)
{
	this->setFormat(format);
}

void	Char32Encoder::setFormat(EUTF format)
{
	this->clear();
	this->format = format;

	switch(format)
	{
		case EUTF::_8:
		{
			this->encodeFunc = &Char32Encoder::toUTF8;
		} break;
		// Buffer is written in
		case EUTF::_16_BE:
		{
			this->encodeFunc = &Char32Encoder::toUTF16;
			this->endianConverter.setMode(EEndianMode::eBig);
		} break;
		case EUTF::_16_LE:
		{
			this->encodeFunc = &Char32Encoder::toUTF16;
			this->endianConverter.setMode(EEndianMode::eLittle);
		} break;
		case EUTF::_32_BE:
		{
			this->encodeFunc = &Char32Encoder::toUTF32;
			this->endianConverter.setMode(EEndianMode::eBig);
		} break;
		case EUTF::_32_LE:
		{
			this->encodeFunc = &Char32Encoder::toUTF32;
			this->endianConverter.setMode(EEndianMode::eLittle);
		} break;
		default:
		{
			PCO_UNK_CASE("Unicode format");
			this->encodeFunc = &Char32Encoder::toUTF8;
		}
	}
}

void	Char32Encoder::push16(uw_16 w)
{
	this->endianConverter.toOutside(w);

	PCO_LOGIC_ERR(this->right < this->buffer + TBase::size, "Too much writes");
	uw_16* ptr = reinterpret_cast<uw_16*>(this->right);
	*ptr = w;
	this->right+= 2;
/*
				this->push(w);
	w >>= 8;	this->push(w);
*/
}

void	Char32Encoder::toUTF8(uw_32 u)
{
	this->clear();

	uw_08 c;
	if(u < 0x80)
	{	// 0b0xxxxxxx : ASCII => keep it
		c = u;
	}
	else
	{
		// Reminder: nothing to do with endianness here => successive "u >>= 6" will reverse order !
		if(u > CVS_Unicode::Max08For3)
		{	// 4 bytes
			// 0b1111 0xxx
			c = ((u >> 18) & 0x3F) | 0xF0;		this->push(c);
			c = ((u >> 12) & 0x3F) | 0x80;		this->push(c);
			c = ((u >> 6) & 0x3F) | 0x80;		this->push(c);
			c = (u & 0x3F) | 0x80;
		}
		else if(u > CVS_Unicode::Max08For2)
		{	// 3 bytes
			// 0b1110 xxxx
			c = (u >> 12) | 0xE0;				this->push(c);	// & 0x3F
			c = ((u >> 6) & 0x3F) | 0x80;		this->push(c);
			c = (u & 0x3F) | 0x80;
		}
		else
		{	// 2 bytes
			// 0b110x xxxx
			c = (u >> 6) | 0xC0;				this->push(c);	// & 0x3F
			c = (u & 0x3F) | 0x80;
		}
	}

	this->push(c);
}

void	Char32Encoder::toUTF16(uw_32 u)
{
	this->clear();

	uw_16 word;
	if(u < 0x10000)
	{
		word = u;	// & 0x0000FFFF
		this->push16(word);
		return;
	}

	// Encode
	u = u - 0x10000;
	// High surrogate
	word = (u >> 10) + 0xD800;
	this->push16(word);
	// Low surrogate
	word = (u & 0x03FF) + 0xDC00;
	this->push16(word);
}

void	Char32Encoder::toUTF32(uw_32 u)
{
	this->endianConverter.toOutside(u);
/*
	this->clear();
				this->push(u);
	u >>= 8;	this->push(u);
	u >>= 8;	this->push(u);
	u >>= 8;	this->push(u);
*/
	this->left	= this->buffer;
	this->right	= this->buffer + 4;
	uw_32* ptr = reinterpret_cast<uw_32*>(this->buffer);
	*ptr = u;
}

void	Char32Encoder::clear(void)
{
	TBase::clear();
}

void	Char32Encoder::encode(char32_t c)
{
	PCO_LOGIC_ERR(this->encodeFunc != nullptr, "");

	uw_32 u = rawConvert<uw_32>(c);
	if(!CVS_Unicode::isValid(u))
	{
		throw std::out_of_range("Invalid codepoint value");
	}

	(this->*encodeFunc)(u);
}

bool	Char32Encoder::leftToRead(void) const
{
	return TBase::leftToRead();
}

void	Char32Encoder::pull(char8_t& element)
{
	uw_08 w;
	TBase::pull(w);
	element = rawConvert<char8_t>(w);
}

bool	Char32Encoder::read(char8_t& element)
{
	bool rc = TBase::leftToRead();
	if(rc)
	{
		this->pull(element);
	}

	return rc;
}
