/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/String.hpp"
#include "string/cfr/Char8CIClassifier.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;

String::String(const char* str, EStringEncoding encoding)
{
	TBase::set(str, encoding);
}

String::String(const IStringCarrier& sc)
{
	TBase::operator=(sc);
}

String::String(const char* str)
{
	TBase::operator=(str);
}

String::String(const char16_t* str)
{
	TBase::operator=(str);
}

String::String(const char32_t* str)
{
	TBase::operator=(str);
}

int		String::classify(const String& toCompare) const
{
	return e_lexiMathCompare(this->chars, toCompare.chars);
}

int		String::classify_CI(const String& toCompare) const
{	//WRONG !!
	return e_lexiSameCompare(this->chars, toCompare.chars, Char8CIClassifier());
}
