/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/CharsetDecoder.hpp"
#include "string/cvs/E_StringEncoding.hpp"
#include "string/cvs/CVS_Unicode.hpp"

using namespace NPCO;

void	CharsetDecoder::setEncoding(EStringEncoding encoding)
{
	PCO_INV_ARG(!e_isUTF(encoding), "Only for ISO-... charsets");
	this->encoding = encoding;

	switch(encoding)
	{
		case EStringEncoding::US_ASCII:
		case EStringEncoding::ISO_8859_1:
		{
			this->readFunc = &CharsetDecoder::passThrough;
		} break;
		default:
		{
			PCO_UNK_CASE("Not implemented yet");
			this->readFunc = &CharsetDecoder::passThrough;
		}
	}
}

bool	CharsetDecoder::passThrough(char32_t& element)
{
	char c;
	bool rc = this->src->read(c);
	if(rc)
	{
		uw_32 tmp = rawConvert<uw_08>(c);
		element = rawConvert<char32_t>(tmp);
	}

	return rc;
}

CharsetDecoder::CharsetDecoder(TResource src, EStringEncoding encoding)
 : InflowFilterBase(src)
{
	this->setEncoding(encoding);
}

bool	CharsetDecoder::read(char32_t& element)
{
	PCO_LOGIC_ERR(this->readFunc != nullptr, "");

	bool rc;
	try
	{
		rc = (this->*readFunc)(element);
	}
	catch(std::out_of_range e)
	{	// Bad data or partially loaded data (less bytes than it should be)
		element = CVS_Unicode::ErrorCode;
		rc = true;
	}

	return rc;
}
