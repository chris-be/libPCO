/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"string/CharsetConverter.hpp"
#include "string/cvs/E_StringEncoding.hpp"
#include "string/CharsetDecoder.hpp"
#include "string/CharsetEncoder.hpp"

#include "string/UTFDecoder.hpp"
#include "string/UTFEncoder.hpp"

#include "flow/TransformInflow.hpp"

using namespace NPCO;

void	CharsetConverter::setEncoding(EStringEncoding fromEncoding, EStringEncoding toEncoding)
{
	this->fromEncoding	= fromEncoding;
	this->toEncoding	= toEncoding;

	if(fromEncoding == toEncoding)
	{
		this->filter = this->src._da();
		return;
	}

	RefRes<IInflow<char32_t>>	decoder;

	if(e_isUTF(fromEncoding))
	{
		TransformInflow<char8_t, char>	ti(this->src, rawConvert<char8_t>);
		auto	resInf = RefRes<IInflow<char8_t>>::from(std::move(ti));
		decoder = new UTFDecoder(resInf, e_getUTF(fromEncoding));
	}
	else
	{
		decoder = new CharsetDecoder(this->src, fromEncoding);
	}

	//TODO UTFNormalizer()

	if(e_isUTF(toEncoding))
	{
		UTFEncoder	encoder(decoder, e_getUTF(toEncoding));
		auto	resInf = RefRes<IInflow<char8_t>>::from(std::move(encoder));
		this->transcoder = new TransformInflow<char, char8_t>(resInf, rawConvert<char>);
	}
	else
	{
		CharsetEncoder	encoder = CharsetEncoder(decoder, toEncoding);
		auto resInf = RefRes<IInflow<char>>::from(std::move(encoder));

		this->transcoder = resInf;
	}

	PCO_LOGIC_ERR(!this->transcoder.isNull(), "");
	this->filter = this->transcoder._da();
}

CharsetConverter::CharsetConverter(TResource src, EStringEncoding fromEncoding, EStringEncoding toEncoding)
 : InflowFilterBase(src)
{
	this->setEncoding(fromEncoding, toEncoding);
}

EStringEncoding	CharsetConverter::getFromEncoding(void) const
{
	return this->fromEncoding;
}

EStringEncoding	CharsetConverter::getToEncoding(void) const
{
	return this->toEncoding;
}

bool	CharsetConverter::read(char& element)
{
	PCO_LOGIC_ERR(this->filter != nullptr, "Filter not initialized !!");
	return this->filter->read(element);
}
