/*!
	Socket.

	Class to handle network UDP/TCP.
*/

#include "Socket.h"

#ifdef FOR_WIN32
	#define		socklen_t			int

	#define		READ				revc
	#define		WRITE				send
	#define		CLOSE				closesocket
	#define		IOCTL				ioctlsocket

#elif defined FOR_POSIX
	#include <netdb.h>
	#include <unistd.h>
	#include <fcntl.h>

	#define		READ				read
	#define		WRITE				write
	#define		CLOSE				close
	#define		IOCTL				ioctl
#else
	#error Unspecified or Unknown Platform
#endif

namespace NIO {

// Not thread safe !!
// Counts how many sockets was inited
uw_16		CSocket::nbSocket = 0;


// Call each time a CSocket is created
// Needed to init windows WSA...
void	CSocket::incNbSocket(void)
{
#ifdef FOR_WIN32
	if(CSocket::nbSocket == 0)
	{	// Time to init
		WSADATA		wsdata;
		if(WSAStartup(0x0101,&wsdata) != 0)
		{	// Ask version 01.01
			// Error :/
			Error(EerrorIO, EsuberrUndefined, "WSA service must be at least 01.01");
		}
	}
#endif

	CSocket::nbSocket++;
	// One more

}

// Call each time a CSocket is destroyed
// Needed to init windows WSA...
void	CSocket::decNbSocket(void)
{

	CSocket::nbSocket--;
	// One less

#ifdef FOR_WIN32
	if(CSocket::nbSocket == 0)
	{	// Time to stop
		if(WSACleanup() != 0)
		{	// Error :(
			ASSERT0;
			CSocket::nbSocket = 1;
		}
	}
#endif

}


// Constructor
CSocket::CSocket(void)
{
	this->socketStruct.sin_family		=	AF_INET;
	this->socketStruct.sin_port			=	htons(0);
	this->socketStruct.sin_addr.s_addr	=	INADDR_ANY;

	this->socketDescriptor			=	-1;

	// Clean host name
	memset(this->hostName, 0, sizeof(this->hostName));

	// No errors
	CSocket::incNbSocket();
}

// Destructor
CSocket::~CSocket(void)
{
	if(this->socketDescriptor != -1)
	{	// If socket was opened, than close it
		CLOSE(this->socketDescriptor);
	}
	//
	CSocket::decNbSocket();
}

// Set name, if hostName is empty than use name of current machine
// Also get IP of name
void	CSocket::setHostName(const CString &hostName)
{
	if(hostName.size() == 0)
	{   // No name given, get current name
		char	buffer[512];
		if(gethostname(buffer, sizeof(buffer)) == -1)
		{	// Error
			ASSERT0;
			Error(EerrorIO, EsuberrUndefined, "Get HostName");
		}
		//
		this->hostName = buffer;
	}
	else
	{
		this->hostName = hostName;
	}

	{	// Get IP
		const char	*pName = (const char*)( this->hostName );
		struct hostent	*pComputerInfos = gethostbyname(pName);
		if(pComputerInfos == NULL)
		{	// Error while getting IP
			Error(EerrorIO, EsuberrUndefined, "Get IP");;
		}

		memcpy(&(this->socketStruct.sin_addr.s_addr), &(pComputerInfos->h_addr[0]), sizeof(long int));
	}

}

// Adjust in non blocking mode when trying to read or write
// ONLY available on Unix
int		CSocket::setNonBlock(void)
{
	ASSERT(this->socketDescriptor != -1);
	// Error :/

#ifndef WIN32
// Doesn't work on windows grmbl !! >_<
	if(fcntl(this->socketDescriptor, F_SETFL, O_NONBLOCK) == -1)
	{
		ASSERT0;
	}

#endif

	return 0;
}

// Get IP
long int	CSocket::getIP(void)
{
	ASSERT(this->socketStruct.sin_addr.s_addr != INADDR_ANY);
	return this->socketStruct.sin_addr.s_addr;
}

// Set IP
void		CSocket::setIP(long int ip)
{
	this->socketStruct.sin_addr.s_addr = ip;
}

// Get port
unsigned short int	CSocket::getPort(void)
{
	ASSERT(this->socketStruct.sin_port != htons(0));
	return htons(this->socketStruct.sin_port);
}

// Set port
void	CSocket::setPort(unsigned short int port)
{
	this->socketStruct.sin_port = htons(port);
}

// Get socket descriptor
int		CSocket::getSocketDescriptor(void)
{
	ASSERT(this->socketDescriptor != -1);
	// Error :/
	return this->socketDescriptor;
}

// Get name of connected computer
const CString&	CSocket::getConnectedName(void) const
{
	return this->hostName;
}

// Bind socket with UDP or TCP
void	CSocket::bind(tSocketProtocol protocol)
{
	if(this->socketStruct.sin_addr.s_addr == 0)
	{	// Error, socket not initialized
		ASSERT0;
		Error(EerrorIO, EsuberrUndefined, "Bind on uninitialized socket");
	}

	// Get a descriptor
	this->socketDescriptor = ::socket(AF_INET, protocol, 0);
	if(this->socketDescriptor == -1)
	{	// Couldn't get a descriptor
		ASSERT0;
		Error(EerrorIO, EsuberrUndefined, "Bind get descriptor");
	}

	if(::bind(this->socketDescriptor, (struct sockaddr*)(&(this->socketStruct)), sizeof(this->socketStruct)) == -1)
	{	// Can't bind
		ASSERT0;
		Error(EerrorIO, EsuberrUndefined, "Bind error");
	}

	{
		socklen_t		tmp = sizeof(this->socketStruct);
		if(getsockname(this->socketDescriptor, (struct sockaddr*)(&(this->socketStruct)), &tmp) == -1)
		{	// Error while getting port number
			ASSERT0;
			Error(EerrorIO, EsuberrUndefined, "Bind");
		}
	}

}


/*
// Send message
int		CSocket::Send(const CSocket *pExternalSocket, char *pBuffer, int SizeOfBuffer)
{
	if(sendto(this->SocketDescriptor, pBuffer, SizeOfBuffer, 0, (struct sockaddr*)&(pExternalSocket->Socket), sizeof(pExternalSocket->Socket)) == -1)
	{	// Error sending
		ASSERT0;
		return -1;
	}

	return 0;
}

// Receive a message
int		CSocket::Receive(CSocket *pExternalSocket, char *pBuffer, int SizeOfBuffer)
{
	socklen_t	SizeOfDist		=	sizeof(pExternalSocket->Socket);

	if(recvfrom(this->SocketDescriptor, pBuffer, SizeOfBuffer, 0, (struct sockaddr*)(&pExternalSocket->Socket), &SizeOfDist) == -1)
	{	// Error receiving
		return -1;
	}

	return 0;
}
*/

}	// namespace NIO