/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/FileMemory.hpp"
#include "mem/Mem.hpp"
#include "mem/mgr/SimpleMemoryManager.hpp"
#include "low/Bit.hpp"
#include "PCO_Predicate.hpp"

// TODO templatize ?
using namespace NPCO;

io_offset	FileMemory::evalSeekPosition(io_offset offset, ESeek origin)
{
	this->_checkOpened();

	io_offset newOffset;

	switch(origin)
	{
		case ESeek::fromStart	:	{ newOffset = offset;					} break;
		case ESeek::fromCurrent	:	{ newOffset = currentOffset + offset;	} break;
		case ESeek::fromEnd		:	{ newOffset = length - offset;			} break;
		default:
		{
			PCO_UNK_CASE("origin");
		} break;
	}

	if(newOffset < 0)
	{
		PCO_ASSERT(false);
		throw std::runtime_error("seek too low");
	}

	if(newOffset > this->length)
	{
		PCO_ASSERT(false);
		throw std::runtime_error("seek too far");
	}

	return newOffset;
}

FileMemory::FileMemory(void)
: FileMemory(new SimpleMemoryManager())
{	}

FileMemory::FileMemory(IMemoryManager* memManager) : AFile()
{
	this->memManager = memManager;
	// Means nothing for the moment
	this->pArray = 0;
	this->length = this->currentOffset = 0;
}

FileMemory::~FileMemory(void)
{
	if(this->isOpened())
	{
		this->close();
	}
}

/*
bool	FileMemory::isOpened(void) const
{
	return (this->pArray != 0);
}
*/

void	FileMemory::open(uw_08 *pBuffer, io_offset size, io_open_mode how)
{
	PCO_INV_SIZE(size > 0);
	PCO_INV_ARG(how.isOn(eRead) || how.isOn(eWrite), "no read nor write ?");
	this->_checkUnopened();

	this->setState(how);

	if(pBuffer == nullptr)
	{	// If pBuffer = NULL => allocation
		this->memManager->reserve(this->pArray, size);
	}
	else
	{	// Else : pBuffer assumed to have Size bytes !!
		pArray = pBuffer;
	}
	this->length = size;

	this->currentOffset = 0;
}

void	FileMemory::open(const uw_08 *pBuffer, io_offset size)
{
	PCO_INV_PTR(pBuffer);
	PCO_INV_SIZE(size > 0);
	this->_checkUnopened();

	this->setState(eRead);

	// pBuffer assumed to have Size bytes !!
	pArray = const_cast<uw_08*>(pBuffer);
	this->length = size;

	this->currentOffset = 0;
}

void	FileMemory::close(void) noexcept
{
	// Free memory
	this->_checkOpened();

	this->memManager->release(this->pArray);
	this->length = this->currentOffset = 0;
	this->reset();
}

void	FileMemory::release(uw_08* &pBuffer, io_offset &size)
{
	this->_checkOpened();

	pBuffer = this->pArray;			this->pArray = 0;
	size = this->length;
	this->reset();
}

io_offset	FileMemory::seek(io_offset offset, ESeek origin)
{
	io_offset newOffset = this->evalSeekPosition(offset, origin);

	currentOffset	=	newOffset;
	return currentOffset;
}

io_offset	FileMemory::where(void) const
{
	this->_checkOpened();
	return this->currentOffset;
}

bool		FileMemory::isEnd(void) const
{
	this->_checkOpened();
	return(this->currentOffset >= this->length);
}

io_offset	FileMemory::size(void) const
{
	this->_checkOpened();
	return this->length;
}

io_size	FileMemory::read(uw_08* pBuffer, io_size size)
{
	PCO_INV_PTR(pBuffer != nullptr);
	PCO_INV_SIZE(size > 0);
	this->_checkRead();

	{
		io_offset		test	=	 currentOffset + size;
		if(test > this->length)
		{	// To much to read !! We can't
			//	ASSERT0;
			size = static_cast<io_size>(this->length - currentOffset);	// No possible loss of data
		}
	}

	Mem::memcpy(pBuffer, pArray + currentOffset, size);

	currentOffset += size;
	return size;
}

// Write functions
io_size	FileMemory::write(const uw_08* pBuffer, io_size size)
{
	PCO_INV_PTR(pBuffer != nullptr);
	PCO_INV_SIZE(size > 0);
	this->_checkWrite();

	{
		io_offset		test	=	currentOffset + size;

		if(test > this->length)
		{	// growing array !!
			this->memManager->resize(pArray, this->length, test);
			this->length	=	test;
		}
	}

	Mem::memcpy(pArray + currentOffset, pBuffer, size);
	currentOffset	+=	size;

	return size;
}

void	FileMemory::flush(void)
{	// Simply nothing to do
	this->_checkOpened();
}
