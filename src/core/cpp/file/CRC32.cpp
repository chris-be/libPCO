/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/CRC32.hpp"

using namespace NPCO;

CRC32::CRC32(void)
 : table(256)
{
	// Compute table
	for(unsigned short int i = 0 ; i < 256 ; ++i)
    {	// Compute the 256 values
		crc_32	crc = static_cast<uw_32>(i);
		for (int j = 0 ; j < 8 ; ++j)
		{
			if (crc & 1)
				crc = 0xedb88320L ^ (crc >> 1);
			else
				crc = crc >> 1;
		}

		table[i] = crc;
	}
}

CRC32::crc_32	CRC32::compute(const uw_08 *pBuffer, int size) const
{
	crc_32	crc = 0xffffffffL;

	const uw_08 *cur = pBuffer;
    for(int i = 0; i < size ; ++i, ++cur)
    {
    	crc = table[(crc ^ *cur) & 0xff] ^ (crc >> 8);
    }

    return (crc ^ 0xffffffffL);
}
