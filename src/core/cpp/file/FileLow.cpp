/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/FileLow.hpp"
#include "file/FileInfo.hpp"

#include "PCO_Predicate.hpp"

#include "PCO_CompilerPlatform.hpp"
#if defined FOR_POSIX || defined FOR_LINUX
	#include <unistd.h>
	// Since not used in Unix/Linux
	#define		O_BINARY	0
#elif defined FOR_WIN32
	#include <io.h>
#else
	#error Unknown platform
#endif

#include <stdexcept> // runtime_error, logic_error
#include <fcntl.h>	// O_BINARY, RD_ONLY ...

using namespace NPCO;

template<class POffset>
POffset	file_seek(int handle, POffset offset, int seekOptions)
{
	return ::lseek(handle, offset, seekOptions);
}

#if defined FOR_LINUX

template<__off64_t>
__off64_t	file_seek(int handle, __off64_t offset, int seekOptions)
{
	return ::lseek64(handle, offset, seekOptions);
}

#endif

//////////////

FileLow::FileLow(void) : AFile(), handle(-1), atEOF(false)
{	}

FileLow::~FileLow(void)
{
	if(this->isOpened())
	{
		this->close();
	}
}

/*
bool	FileLow::isOpened(void) const
{
	return (this->handle != -1);
}
*/

void	FileLow::open(const char* filename, EStateFlags how)
{
	PCO_INV_PTR(filename);
	this->_checkUnopened();

	int	flags	=	O_BINARY;

	if(how & (eRead | eWrite) )	flags |= O_RDWR;
	else
		if(how & eWrite)	flags |= O_WRONLY;
		else				flags |= O_RDONLY;

	int	handle = ::open(filename, flags, 0);

	this->open(handle, how);
}

void	FileLow::open(int handle, EStateFlags how)
{
	this->_checkUnopened();

	if(handle == -1)
	{
		PCO_ASSERT(false);
		throw std::runtime_error( PCO_ERR_MSG("open file") );
	}

	this->handle = handle;
	this->setState(how);
	this->atEOF = false;
}

void	FileLow::close(void) noexcept
{
	if(this->handle != -1)
	{
		::close(this->handle);
		this->handle = -1;
	}

	this->reset();
}

io_offset	FileLow::seek(io_offset offset, ESeek origin)
{
	this->_checkOpened();

	int		fromOpt;
	switch(origin)
	{
		case ESeek::fromStart:
		{
			fromOpt = SEEK_SET;
		} break;
		case ESeek::fromCurrent:
		{
			fromOpt = SEEK_CUR;
		} break;
		case ESeek::fromEnd:
		{
			fromOpt = SEEK_END;
		} break;
	}

	io_offset newOffset = file_seek(this->handle, offset, SEEK_SET);
	if(newOffset == -1)
	{
		PCO_ASSERT(false);
		throw std::runtime_error( PCO_ERR_MSG("seek in file") );
	}

	return newOffset;
}

io_offset	FileLow::where(void) const
{
	this->_checkOpened();

	// Hackish :/
	return file_seek(this->handle, 0, SEEK_CUR);
}

bool	FileLow::isEnd(void) const
{
	return this->atEOF;
}

io_offset	FileLow::size(void) const
{
	this->_checkOpened();

	// Get size of file
	FileInfo	info;
	if(! info.getFileInfos(this->handle) )
	{
		PCO_ASSERT(false);
		throw std::runtime_error( PCO_ERR_MSG("get file size") );
	}

	return info.size();
}

io_size	FileLow::read(uw_08* pBuffer, io_size size)
{
	PCO_INV_PTR(pBuffer != nullptr);
	PCO_INV_SIZE(size > 0);
	this->_checkRead();

	io_size effRead = ::read(this->handle, pBuffer, size);
	if(effRead <= 0)
	{
		if(effRead == 0)
		{	// Reached end of file
			this->atEOF = true;
			return 0;
		}
		PCO_ASSERT(false);
		throw std::runtime_error( PCO_ERR_MSG("read file") );
	}

	return effRead;
}

io_size	FileLow::write(const uw_08* pBuffer, io_size size)
{
	PCO_INV_PTR(pBuffer != nullptr);
	PCO_INV_SIZE(size > 0);
	this->_checkWrite();

	if(::write(this->handle, pBuffer, size) != size)
	{
		PCO_ASSERT(false);
		throw std::runtime_error( PCO_ERR_MSG("write file") );
	}

	return size;
}

void	FileLow::flush(void)
{	// Simply nothing to do
	this->_checkOpened();
}
