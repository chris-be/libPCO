/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/FilePipe.hpp"

#include "PCO_Predicate.hpp"

#include <unistd.h>
#include <errno.h>

using namespace NPCO;

/*
// Platform dependent
#include "PCO_CompilerPlatform.hpp"
#ifdef FOR_POSIX
	#include "imp/POSIX_FilePipe.cppi"
#elif FOR_WIN32
	#error	Unsupported now
//	#include "imp/WIN_FilePipe.cppi"
#else
	#error Unknown platform
#endif
*/

FilePipe::FilePipe(void)
{
	this->pipeFd[0] = 0;
	this->pipeFd[1] = 0;
}

void	FilePipe::open(void)
{
	int ret = pipe(this->pipeFd);
	if(ret != 0)
	{
		PCO_ASSERT(false);
		throw std::runtime_error( PCO_ERR_MSG("create pipe") );
	}

	this->read.open(this->pipeFd[0], eRead);
	this->write.open(this->pipeFd[1], eWrite);
}

FileLow&	FilePipe::getRead(void)
{
	return this->read;
}

FileLow&	FilePipe::getWrite(void)
{
	return this->write;
}

void	FilePipe::closeRead(void)
{
	this->read.close();
}

void	FilePipe::closeWrite(void)
{
	this->write.close();
}

void	FilePipe::routeWrite(int handle)
{
	PCO_INV_ARG(handle != -1, "Invalid file handle");
	int ret;
	do
	{
		ret = dup2(this->pipeFd[1], handle);
	} while( (ret == -1) && (errno == EINTR) );
}

void	FilePipe::routeStdOut(void)
{
	this->routeWrite(STDOUT_FILENO);
}

void	FilePipe::routeStdErr(void)
{
	this->routeWrite(STDERR_FILENO);
}
