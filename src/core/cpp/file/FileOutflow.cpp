/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/FileOutflow.hpp"

namespace NPCO {

FileOutflow::FileOutflow(IFile& file)
 : file(file)
{	}

void	FileOutflow::write(const uw_08& element)
{
	this->file.write(&element, 1);
}

} // N..
