/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/AFile.hpp"
#include	"low/Bit.hpp"
#include "PCO_Predicate.hpp"

using namespace NPCO;

void	AFile::reset(void)
{
	this->state = 0;
}

void	AFile::setState(io_open_mode how)
{
	this->state = how;
}

bool	AFile::_verifyOpened(void) const
{
	return (this->state != 0);
}

bool	AFile::_verifyUnopened(void) const
{
	return (this->state == 0);
}

bool	AFile::_verifyRead(void) const
{
	return this->state.isOn(eRead);
}

bool	AFile::_verifyWrite(void) const
{
	return this->state.isOn(eWrite);
}

void	AFile::_checkOpened(void) const
{
	if(!this->_verifyOpened())
	{	// open before
		throw std::runtime_error( "AFile: not opened");
	}
}

void	AFile::_checkUnopened(void) const
{
	if(!this->_verifyUnopened())
	{	// close before
		// If close was called, check if derived class adjust state in it...
		throw std::runtime_error( "AFile: already opened" );
	}
}

void	AFile::_checkRead(void) const
{
	if(!this->_verifyRead())
	{	// Not opened for read
		throw std::runtime_error( "AFile: no read function allowed");
	}
}

void	AFile::_checkWrite(void) const
{
	if(!this->_verifyWrite())
	{	// Not opened for write
		throw std::runtime_error( "AFile: no write function allowed");
	}
}

AFile::AFile(void)
{
	this->reset();
}

AFile::~AFile(void)
{
	PCO_ASSERT_MSG(!this->isOpened(), "Close resource before destroying object");
}

io_open_mode	AFile::getState(void) const
{
	return this->state;
}

bool	AFile::isOpened(void) const noexcept
{
	return this->state & (eRead | eWrite);
}

/*
void	AFile::close(void)
{
	PCO_RUNTIME_ERR(false, "Override and implement");
}
*/

io_size	AFile::read(TBuffer& buffer)
{
	// this->verifyRead();
	return this->read(buffer._da(), buffer.size());
}

io_size	AFile::write(const TBuffer& buffer)
{
	// this->verifyWrite();
	return this->write(buffer._da(), buffer.size());
}
