/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/FileInflow.hpp"

namespace NPCO {

FileInflow::FileInflow(IFile& file) : file(file)
{	}

bool	FileInflow::read(uw_08& element)
{
	if(this->file.isEnd())
	{
		return false;
	}

	return (this->file.read(&element, 1) == 1);
}

} // N..
