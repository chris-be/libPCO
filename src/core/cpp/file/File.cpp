/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/File.hpp"
#include "PCO_Predicate.hpp"

using namespace NPCO;

File::File(void)
{	}

File::~File(void)
{
	if(this->isOpened())
	{
		this->close();
	}
}

void	File::open(const char* filename, EStateFlags how, TBufferSize bufferSize)
{
	this->fileLow.open(filename, how);
	this->file.open(&this->fileLow, bufferSize);
}

void	File::open(int handle, EStateFlags how, TBufferSize bufferSize)
{
	this->fileLow.open(handle, how);
	this->file.open(&this->fileLow, bufferSize);
}

io_open_mode	File::getState(void) const
{
	return this->file.getState();
}

io_offset	File::seek(io_offset offset, ESeek origin)
{
	return this->file.seek(offset, origin);
}

io_offset	File::where(void) const
{
	return this->file.where();
}

bool		File::isEnd(void) const
{
	return this->file.isEnd();
}

io_offset	File::size(void) const
{
	return this->file.size();
}

bool	File::isOpened(void) const noexcept
{
	return this->file.isOpened();
}

void	File::close(void) noexcept
{
	this->file.close();
	this->fileLow.close();
}

io_size	File::read(uw_08* pBuffer, io_size size)
{
	return this->file.read(pBuffer, size);
}

io_size	File::write(const uw_08* pBuffer, io_size size)
{
	return this->file.write(pBuffer, size);
}

void	File::flush(void)
{
	this->file.flush();
}
