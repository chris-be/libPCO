/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/FileBuffered.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;

const FileBuffered::TBufferSize FileBuffered::default_buffer_size = 4096;

void	FileBuffered::flushNoCheck(void)
{
	PCO_ASSERT(this->lastAction == ELastAction::write);

	// Flush action used by write, flush
	array_size toWrite	= this->buffer.leftToRead();
	if(toWrite > 0)
	{
		this->file->write(this->buffer._da_read(), toWrite);
	}
}

FileBuffered::FileBuffered(void) : AFile(), file(), buffer(0)
{
	this->lastAction = ELastAction::none;
}

FileBuffered::~FileBuffered(void)
{
	if(this->isOpened())
	{
		this->close();
	}
}

void	FileBuffered::open(IFile *pFile, TBufferSize bufferSize)
{
	PCO_INV_PTR(pFile != nullptr);
	PCO_INV_SIZE(bufferSize > 0);
	this->_checkUnopened();

	this->file			= pFile;
	this->setState(pFile->getState() | eBuffered);

	this->buffer.resize(bufferSize);
	this->lastAction	= ELastAction::none;
}

void	FileBuffered::close(void) noexcept
{
	this->flush();

	this->file			= nullptr;
	this->buffer.reset();
	this->lastAction	= ELastAction::none;

	this->reset();
}

io_offset	FileBuffered::seek(io_offset offset, ESeek origin)
{
	this->_checkOpened();

	if(this->lastAction == ELastAction::write)
	{	// Last action was write => flush
		this->flushNoCheck();
	}
	this->buffer.reset();
	this->lastAction = ELastAction::none;

	return this->file->seek(offset, origin);
}

io_offset	FileBuffered::where(void) const
{
	this->_checkOpened();

	array_size delta;
	if(this->lastAction == ELastAction::write)
	{
		delta = this->buffer.position();
	}
	else if(this->lastAction == ELastAction::read)
	{
		delta = this->buffer.position();
	}
	else
	{
		delta = 0;
	}

	return this->file->where() + delta;
}

bool		FileBuffered::isEnd(void) const
{
	this->_checkOpened();

	if(this->lastAction == ELastAction::write)
	{
		return false;
	}

	if(this->lastAction == ELastAction::read)
	{
		if(this->buffer.leftToRead() > 0)
			return false;
	}

	return this->file->isEnd();
}

io_offset	FileBuffered::size(void) const
{
	this->_checkOpened();

	return this->file->size();
}

io_size	FileBuffered::read(uw_08* pBuffer, io_size size)
{
	PCO_INV_PTR(pBuffer != nullptr);
	this->_checkRead();

	if(this->lastAction == ELastAction::write)
	{	// Last action was write => really flush
		this->flushNoCheck();
		this->buffer.reset();
	}
	this->lastAction = ELastAction::read;

	io_size		retRead =	0;	// What was really read
	io_size		toGet	=	this->buffer.leftToRead();
	if(toGet > 0)
	{	// If buffer has info left, get them first !!
		if(size < toGet)
		{	// If we want to have less than buffer contains, get it
			toGet	=	size;
		}
		// Else get all what buffer contains
		this->buffer.get(pBuffer, toGet);

		pBuffer	+=	toGet;		size				-=	toGet;
		retRead	+=	toGet;
	}

	if(size == 0)
	{	// Read enough ? Good bye
		return retRead;
	}

	// Here Buffer is empty
			toGet	= this->buffer.size();
	io_size	i		= size / toGet;
	for( ; i > 0 ; i--)
	{	// A lot to read => read without buffer but by (i*size of buffer) to optimize reading
		io_size	efGot = this->file->read(pBuffer, toGet);
		pBuffer	+=	efGot;		size				-=	efGot;
		retRead	+=	efGot;

		if( efGot < toGet )
		{	// May be the end
			return retRead;
		}
	}

	toGet	=	size;
	if(toGet > 0)
	{	// Something less to read ? => fill buffer !!
		io_size	efGot = this->file->read(this->buffer._da_write(), this->buffer.leftToWrite() );
		this->buffer.emuleWrite(efGot);	// Say we filled directly buffer
		if(efGot < toGet)
		{	// we didn't get enough => adjust
			toGet = efGot;
		}

		// Put what is asked, pBuffer was adjusted before, now nothing to worry
		this->buffer.get(pBuffer, toGet);
		//	pBuffer	+=	toGet;	// useless
								size				-=	toGet;
		retRead	+=	toGet;
	}

	return retRead;
}

io_size	FileBuffered::write(const uw_08* pBuffer, io_size size)
{
	PCO_INV_PTR(pBuffer != nullptr);
	this->_checkWrite();

	if(this->lastAction == ELastAction::read)
	{	// Last action was read => clean buffer
		this->buffer.reset();
	}
	this->lastAction = ELastAction::write;

	// What was really written
	io_size	retWritten	=	0;
	io_size	toPut		=	this->buffer.leftToWrite();
	if(toPut > 0)
	{	// If buffer has place left, complete it first !!
		if(size < toPut)
		{	// If what we have to write is less than buffer place left => prevent filling to much
			toPut	=	size;
		}
		// We can fill buffer
		this->buffer.add(pBuffer, toPut);

		pBuffer		+=	toPut;		size				-=	toPut;
		retWritten	+=	toPut;
	}

	if(size > 0)
	{	// More to write => buffer is full
		this->flushNoCheck();

				toPut	=	buffer.size();
		io_size	i		= size / toPut;
		for( ; i > 0 ; i--)
		{	// A lot to write => write without buffer but by (i*size of buffer) to optimize writing
			io_size	efPut = this->file->write(pBuffer, toPut);

			pBuffer		+=	efPut;		size				-=	efPut;
			retWritten	+=	efPut;
		}

		toPut	=	size;
		if(toPut > 0)
		{	// Something less to write ? => fill buffer !!
			this->buffer.add(pBuffer, toPut);
			// pBuffer was adjusted before, no worry :)
			// pBuffer		+=	toPut;		// Useless
										size				-=	toPut;
			retWritten	+=	toPut;
		}
	}

	return retWritten;
}

void	FileBuffered::flush(void)
{
	this->_checkOpened();

	if(this->lastAction == ELastAction::write)
	{	// Last action was write => really flush
		this->flushNoCheck();
		this->buffer.reset();

		this->lastAction = ELastAction::none;
	}
}
