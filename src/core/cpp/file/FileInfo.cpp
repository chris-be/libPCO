/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"file/FileInfo.hpp"

#include "PCO_CompilerPlatform.hpp"
#if defined FOR_POSIX || defined FOR_LINUX
	#include "imp/POSIX_FileInfo.cppi"
#elif defined FOR_WIN32
	#error	Unsupported now
//	#include "imp/WIN_FileInfo.cppi"
#else
	#error Unknown platform
#endif
