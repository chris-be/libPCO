/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"low/Process.hpp"
#include "col/Store.hpp"

#include "PCO_Predicate.hpp"

#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>		// perror

using namespace NPCO;

Process::Process(void)
 : Process(false, false)
{	}

Process::Process(bool pipeStdOut, bool pipeStdErr)
 : pipeStdOut(pipeStdOut), pipeStdErr(pipeStdErr), pid(0)
{	}

bool	Process::isPipeStdOut(void) const
{
	return this->pipeStdOut;
}

bool	Process::isPipeStdErr(void) const
{
	return this->pipeStdErr;
}

FileLow&	Process::getStdOut(void)
{
	PCO_ASSERT(this->isValid() && this->isPipeStdOut());
	return this->stdOut.getRead();
}

FileLow&	Process::getStdErr(void)
{
	PCO_ASSERT(this->isValid() && this->isPipeStdErr());
	return this->stdErr.getRead();
}

bool	Process::isValid(void) const
{
	return this->pid > 0;
}

int		Process::getExitStatus(void) const
{
	return WEXITSTATUS(this->status);
}

void	Process::shell(const char* cmd)
{
	this->status = ::system(cmd);
}

bool	Process::execute(const char* exec, bool usePath, char* const argv[])
{
	this->status = 0;
	if(this->pipeStdOut)	this->stdOut.open();
	if(this->pipeStdErr)	this->stdErr.open();

	this->pid = ::fork();
	if(this->pid < 0)
	{	// Couldn't fork
		return false;
	}

	if(this->pid > 0)
	{	// Parent
		if(this->pipeStdOut)	this->stdOut.closeWrite();
		if(this->pipeStdErr)	this->stdErr.closeWrite();

		return true;
	}

	// Child :)
	if(this->pipeStdOut)
	{
		this->stdOut.closeRead();	this->stdOut.routeStdOut();
	}
	if(this->pipeStdErr)
	{
		this->stdErr.closeRead();	this->stdErr.routeStdErr();
	}

	// Only return if error
	if(usePath)		::execvp(exec, argv);
	else			::execv(exec, argv);
	// Something went wrong
	perror("Process::execute");
	std::exit(1);
}

void	Process::wait(void)
{
	PCO_LOGIC_ERR(this->isValid(), "Invalid process");

	int options = 0;
	::waitpid(this->pid, &this->status, options);
}
