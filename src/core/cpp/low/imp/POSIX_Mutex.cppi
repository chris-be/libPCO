/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"low/Mutex.hpp"
#include "PCO_Predicate.hpp"

using namespace NPCO;

const bool Mutex::Default_Check_Recursive_Lock =
												#ifdef NDEBUG
													false
												#else
													true
												#endif
											;

Mutex::Mutex(bool checkRecursiveLock)
{
	this->lockedState = false;
	this->initState = 0;

	int rCode;
	rCode = pthread_mutexattr_init(&this->attribute);
	if(rCode != 0)
	{
		PCO_ASSERT(false);
		throw std::runtime_error("Mutex : init attribute");
	}
	this->initState = 1;

	if(checkRecursiveLock)
	{
		rCode = pthread_mutexattr_settype(&this->attribute, PTHREAD_MUTEX_ERRORCHECK);
		if(rCode != 0)
		{
			PCO_ASSERT(false);
			throw std::runtime_error("Mutex : settype attribute");
		}
	}

	rCode = pthread_mutex_init(&this->mutex, &this->attribute);
	if(rCode != 0)
	{
		throw std::runtime_error("Error : init mutex");
	}
	this->initState = 2;

}

Mutex::~Mutex(void)
{
	PCO_ASSERT_MSG(!this->isLocked(), "Delete locked resource");

	if(this->initState > 1)
	{
		[[maybe_unused]]	int rc = pthread_mutex_destroy(&this->mutex);
		PCO_ASSERT_MSG(rc == 0, "Mutex : problem while cleaning");
	}

	if(this->initState > 0)
	{
		[[maybe_unused]]	int rc = pthread_mutexattr_destroy(&this->attribute);
		PCO_ASSERT_MSG(rc == 0, "Mutex : problem while cleaning");
	}
}

bool	Mutex::tryLock(void)
{
	int rCode = pthread_mutex_trylock(& this->mutex);
	if(rCode != 0)
	{
		if(rCode == EBUSY)
		{	// Couldn't lock
			return false;
		}
		// Technical problem
		PCO_ASSERT(false);
		throw std::runtime_error("Mutex : trylock");
	}

	this->lockedState = true;
	return true;
}

bool	Mutex::isLocked(void) const noexcept
{
	return this->lockedState;
}

void	Mutex::lock(void)
{
	int rCode = pthread_mutex_lock(& this->mutex);
	if(rCode != 0)
	{
		// Technical problem
		PCO_ASSERT(false);
		throw std::runtime_error("Mutex : lock");
	}

	this->lockedState = true;
}

void	Mutex::unlock(void) noexcept
{
	int rCode = pthread_mutex_unlock(& this->mutex);
	if(rCode != 0)
	{
		// Technical problem
		PCO_ASSERT(false);
		return;
	}

	this->lockedState = false;
}
