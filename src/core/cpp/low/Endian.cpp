/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"low/Endian.hpp"
#include "mem/cvs/E_MemBlock.hpp"
// #include "PCO_Types.hpp"

#include "PCO_Predicate.hpp"

using namespace NPCO;

void	Endian::swap(unsigned char *dst, mem_size size)
{
	e_mirror(dst, size);
}

void	Endian::noSwap([[maybe_unused]] unsigned char *dst, [[maybe_unused]] mem_size size)
{
	PCO_INV_PTR(dst != nullptr);
	PCO_INV_SIZE(size > 0);
}

EEndianMode	Endian::detect(void)
{
	unsigned char		witness[2] = { 0, 1 };

	// unsigned short int	test =  *( reinterpret_cast<unsigned short int*>(witness) );
	uw_16				test =  *( reinterpret_cast<unsigned short int*>(witness) );
	if(test == 1)
	{	// Same order => big endian
		return EEndianMode::eBig;
	}

	// Not the same order => little endian
	return EEndianMode::eLittle;
}

Endian::Endian(void)
{
	switch(Endian::detect())
	{
		case EEndianMode::eBig:
		{
			this->fctToLittle = swap;			this->fctToBig = noSwap;
		} break;
		case EEndianMode::eLittle:
		{
			this->fctToLittle = noSwap;			this->fctToBig = swap;
		} break;
		default:
		{
			PCO_UNK_CASE("Unhandled");
		}
	}
}
