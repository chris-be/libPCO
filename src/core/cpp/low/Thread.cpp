/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"low/Thread.hpp"

#if defined FOR_POSIX || defined FOR_LINUX
	#include "imp/POSIX_Thread.cppi"
#elif defined FOR_WINDOWS
	#error	Unsupported now
	#include "imp/WIN_Thread.cppi"
#else
	#error	Unspecified or Unknown Platform
#endif
