/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"low/EndianConverter.hpp"
#include "low/Endian.hpp"
#include "PCO_Predicate.hpp"

using namespace NPCO;

EndianConverter::EndianConverter(void)
 : EndianConverter( Endian::detect() )
{	}

EndianConverter::EndianConverter(const EEndianMode insideMode)
 : insideMode(insideMode)
{
	this->setMode(insideMode);
}

EEndianMode	EndianConverter::getInsideMode(void) const
{
	return this->insideMode;
}

EEndianMode	EndianConverter::getMode(void) const
{
	return this->outsideMode;
}

void	EndianConverter::setMode(const EEndianMode oMode)
{
	this->outsideMode = oMode;

	if(this->insideMode == this->outsideMode)
	{
		this->fctFrom	= Endian::noSwap;
		this->fctTo		= Endian::noSwap;
	}
	else
	{
		if((this->outsideMode == EEndianMode::eMiddle) || (this->insideMode == EEndianMode::eMiddle))
		{
			PCO_UNK_CASE("Unhandled format");
		}

		this->fctFrom	= Endian::swap;
		this->fctTo		= Endian::swap;
	}
}
