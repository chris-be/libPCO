/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"mem/mgr/BlockMemoryManager.hpp"

using namespace NPCO;

const mem_size	BlockMemoryManager::DefaultBlockSize = 8;

BlockMemoryManager::BlockMemoryManager(mem_size blockSize)
{
	this->setBlockSize(blockSize);
}

BlockMemoryManager::~BlockMemoryManager(void)
{	}

mem_size BlockMemoryManager::getBlockSize(void) const
{
	return this->blockSize;
}

void BlockMemoryManager::setBlockSize(mem_size size)
{
	if(size <= 0)
	{
		size = BlockMemoryManager::DefaultBlockSize;
	}

	this->blockSize = size;
}

mem_size	BlockMemoryManager::computeSize(mem_size size)
{
	if(size < (MEM_SIZE_MAX-this->blockSize))
	{	// Can allocate more
		mem_size delta = size % this->blockSize;
		if(delta > 0)
		{	// "Round" size with a block
			size+= this->blockSize - delta;
		}
	}
	else
	{	// Take max
		size = MEM_SIZE_MAX;
	}

	return size;
}

void* BlockMemoryManager::basic_alloc(mem_size size)
{
	mem_size realSize = this->computeSize(size);
	return SimpleMemoryManager::basic_alloc(realSize);
}

void* BlockMemoryManager::basic_realloc(void* src, mem_size currentSize, mem_size newSize)
{
	if(!this->basic_hasToMove(src, currentSize, newSize))
	{
		return src;
	}

	mem_size realSize = this->computeSize(newSize);
	return SimpleMemoryManager::basic_realloc(src, currentSize, realSize);
}

bool BlockMemoryManager::basic_hasToMove([[maybe_unused]] const void* src, mem_size currentSize, mem_size newSize)
{
	return newSize > this->computeSize(currentSize);
}
