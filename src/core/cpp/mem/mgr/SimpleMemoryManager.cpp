/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"mem/mgr/SimpleMemoryManager.hpp"

#include <cstdlib>	// malloc
#include <new>		// bad_alloc, placement new

using namespace NPCO;

SimpleMemoryManager::~SimpleMemoryManager(void)
{	}

void* SimpleMemoryManager::basic_alloc(mem_size size)
{
	PCO_INV_SIZE(size > 0);

	void    *dst = ::malloc(size);
#ifndef NDEBUG
	if( (dst == nullptr) && (size > 0) )
	{	// An exception should already been thrown
		throw std::bad_alloc();
	}
#endif
    return dst;
}

void* SimpleMemoryManager::basic_realloc(void* src, [[maybe_unused]] mem_size currentSize, mem_size newSize)
{
	PCO_INV_PTR(src != nullptr);
	PCO_INV_SIZE(currentSize > 0);
	PCO_INV_SIZE(newSize > 0);

	void    *dst = ::realloc(src, newSize);
#ifndef NDEBUG
	if( ( dst == 0) && (newSize > 0) )
	{	// An exception should already been thrown
		throw std::bad_alloc();
	}
#endif
    return dst;
}

void SimpleMemoryManager::basic_free(void* toFree)
{
    ::free(toFree);
}

bool SimpleMemoryManager::basic_hasToMove([[maybe_unused]] const void* src, mem_size currentSize, mem_size newSize)
{
	PCO_INV_PTR(src != nullptr);
	PCO_INV_SIZE(currentSize > 0);
	PCO_INV_SIZE(newSize > 0);

	return newSize > currentSize;
}
