/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"mem/mgr/I_MemoryManager.hpp"

using namespace NPCO;

// Avoid [-Wweak-vtables]
IMemoryManager::~IMemoryManager(void)
{ }
