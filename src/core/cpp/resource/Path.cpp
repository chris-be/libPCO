/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"resource/Path.hpp"
#include "string/cvs/E_StringEncoding.hpp"
#include "cvs/bag/E_DynBag.hpp" // e_addAll
#include "cvs/bag/E_RowBag.hpp" // e_cursor
#include "col/cvs/E_Tail.hpp" // e_tail
#include "col/cvs/E_Head.hpp" // e_head
#include "cvs/res/AutoDelete.hpp"

using namespace NPCO;

// #include "PCO_CompilerPlatform.hpp"

const char8_t	Path::separator	= '/';

inline
static bool	isSeparator(const char8_t c)
{
	// Windows '\\' ??
	return c == Path::separator;
}

bool	Path::getRelative(void) const
{
	return this->relative;
}

bool	Path::parseAdd(const IStringCarrier& sc)
{
	IInflow<char8_t>* toParse = e_utf8Inflow(sc);
	AutoDelete<IInflow<char8_t>> cleaner(toParse);

	String tmp;
	char8_t c;
	if(!toParse->read(c))
	{	// Empty
		return false;
	}

	bool rc = isSeparator(c);
	if(!rc)	tmp.add(c);

	while(toParse->read(c))
	{
		if(isSeparator(c))
		{	// Keep empty parts
			this->parts.add(tmp);
			tmp.clear();
		}
		else
		{
			tmp.add(c);
		}
	}

	if(!tmp.isEmpty())
	{	// Keep only last part if not empty
		this->parts.add(tmp);
	}

	return rc;
}

Path::Path(bool relative)
 : relative(relative)
{	}

Path::Path(const IStringCarrier& path)
{
	this->set(path);
}

void	Path::setRelative(bool value)
{
	this->relative = value;
}

bool	Path::isRelative(void) const
{
	return this->relative && !this->parts.isEmpty();
}

auto	Path::size(void) const -> TSize
{
	return this->parts.size();
}

bool	Path::isEmpty(void) const
{
	return this->parts.isEmpty();
}

void	Path::clear(void)
{
	this->parts.clear();
}

void	Path::add(const TType& toAdd)
{
	this->parts.add(toAdd);
}

void	Path::moveIn(TType&& toMove)
{
	this->parts.moveIn(std::move(toMove));
}

void	Path::set(const IStringCarrier& path)
{
	this->clear();
	this->relative = !this->parseAdd(path);
}

void	Path::add(const Path& toAdd)
{
	e_addAll(this->parts, toAdd.parts);
}

void	Path::add(const IStringCarrier& path)
{
	this->parseAdd(path);
}

String	Path::createString(void) const
{
	String rc;
	if(!this->isRelative())
	{	// Begin with /
		rc.add(Path::separator);
	}

	if(!this->parts.isEmpty())
	{
		auto cur = e_cursor(this->parts);
		rc.add(*cur);
		for(++cur ; cur.ok() ; ++cur)
		{
			rc.add(Path::separator);
			rc.add(*cur);
		}
	}

	return rc;
}

auto	Path::first(void) const -> TIterRead
{
	return this->parts.first();
}

auto	Path::last(void) const -> TIterRead
{
	return this->parts.last();
}

auto	Path::first(void) -> TIterWrite
{
	return this->parts.first();
}

auto	Path::last(void) -> TIterWrite
{
	return this->parts.last();
}

auto	Path::operator[](const TSize i) const -> const TType&
{
	return this->parts[i];
}

auto	Path::operator[](const TSize i) -> TType&
{
	return this->parts[i];
}

auto	Path::iter_r(const TSize i) const -> TIterRead
{
	return this->parts.iter_r(i);
}

auto	Path::iter_w(const TSize i) -> TIterWrite
{
	return this->parts.iter_w(i);
}

auto	Path::head(void) const -> const TType&
{
	return e_head(this->parts);
}

auto	Path::head(void) -> TType&
{
	return e_head(this->parts);
}

void	Path::popHead(void)
{
	e_popHead(this->parts);
}

auto	Path::pullHead(void) -> TType
{
	return e_pullHead(this->parts);
}

auto	Path::tail(void) const -> const TType&
{
	return e_tail(this->parts);
}

auto	Path::tail(void) -> TType&
{
	return e_tail(this->parts);
}

void	Path::popTail(void)
{
	e_popTail(this->parts);
}

auto	Path::pullTail(void) -> TType
{
	return e_pullTail(this->parts);
}

ClassifyValue	Path::classify(const Path& toCompare) const
{
	return e_lexiSameCompare(this->parts, toCompare.parts);
}
