/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"resource/Filename.hpp"
#include "cvs/bag/E_Bag.hpp"
#include "cvs/bag/E_DynBag.hpp"	// e_addAll
// #include "cvs/walk/CursorIter.hpp"

using namespace NPCO;

const char8_t		Filename::extension_separator = '.';

Filename::Filename(const Path& path)
 : path(path)
{	}

Filename::Filename(const Path& path, const TType& name)
 : path(path), name(name)
{	}

Filename::Filename(const IStringCarrier& path, const IStringCarrier& name)
 : path(path), name(name)
{	}

Filename::Filename(const IStringCarrier& name)
{
	this->set(name);
}

const Path&	Filename::getPath(void) const
{
	return this->path;
}

Path&		Filename::getPath(void)
{
	return this->path;
}

auto	Filename::getName(void) const -> const TType&
{
	return this->name;
}

auto	Filename::getName(void) -> TType&
{
	return this->name;
}

void	Filename::clear(void)
{
	this->path.clear();
	this->name.clear();
}

void	Filename::set(const IStringCarrier& name)
{
	this->path.set(name);
	if(this->path.isEmpty())
	{
		this->name.clear();
	}
	else
	{
		this->name = this->path.pullTail();
	}
}

ClassifyValue	Filename::classify(const Filename& toCompare) const
{
	ClassifyValue cv = this->path.classify(toCompare.path);
	if(cv != 0)
		return cv;

	return this->name.classify(toCompare.name);
}

String		Filename::createString(void) const
{
    String tmp = this->path.createString();
	if(!this->name.isEmpty())
	{
		tmp.add(Path::separator);
		tmp.add(this->name);
	}

	return tmp;
}

CStringHolder	Filename::CFilename(void) const
{
    String tmp = this->createString();
	return tmp.c_str_wrap();
}

auto	Filename::extractName(const TType& name) -> TType
{
	TType res;
	auto first = name.first();
	auto last = name.last();
	using TCursor = CursorIter<decltype(first)>;

	TCursor cur(first, last);
	for(cur.toFirst() ; cur.ok() && (*cur != Filename::extension_separator) ; ++cur)
	{	// Keep everything before separator
		res.add(*cur);
	}

	if(cur.ok())
	{	// Find last '.' on the remaining
		cur.limitRangeLast();
		cur.toLast();
		[[maybe_unused]]	bool y = e_rvsFind(cur, Filename::extension_separator);
		PCO_LOGIC_ERR(y, "At least where cur stopped");
		if(cur.getCurrent() != cur.getFirst())
		{
			cur.limitRangeFirst();
			cur.toFirst();
			e_addAll(res, cur);
		}
	}

	return res;
}

auto	Filename::extractExtension(const TType& name) -> TType
{
	TType ext;
	auto cur = e_cursor(name);
	if(e_rvsFind(cur, extension_separator))
	{
		++cur;
		e_addAll(ext, cur);
	}

	return ext;
}

auto	Filename::createFullname(const TType& name, const TType& extension) -> TType
{
	TType res = name;
	if(!extension.isEmpty())
	{
		res.add(Filename::extension_separator);
		res.add(extension);
	}

	return res;
}
