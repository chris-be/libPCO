/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"resource/Scheme.hpp"
#include "cvs/cfr/E_Classifier.hpp" // e_isSame

using namespace NPCO;

const Scheme		Scheme::mem_scheme		= U8_STR("mem");
const Scheme		Scheme::file_scheme		= U8_STR("file");
const Scheme		Scheme::ftp_scheme		= U8_STR("ftp");
const Scheme		Scheme::ssh_scheme		= U8_STR("ssh");
const Scheme		Scheme::http_scheme		= U8_STR("http");
const Scheme		Scheme::https_scheme	= U8_STR("https");

Scheme::Scheme(const char8_t* name)
 : scheme(name)
{	}

Scheme::Scheme(const IStringCarrier& scheme)
{
	this->set(scheme);
}

void	Scheme::normalize(void)
{
	this->scheme.toLower();
}

void	Scheme::clear(void)
{
	this->scheme.clear();
}

const String&	Scheme::get(void) const
{
	return this->scheme;
}

void	Scheme::set(const IStringCarrier& scheme)
{
 	this->scheme = scheme;
	this->normalize();
}

bool	Scheme::isValid(void) const
{
	return false;
}

int		Scheme::classify(const Scheme& toCompare) const
{
	return this->scheme.classify_CI(toCompare.scheme);
}

auto	Scheme::findType(void) const -> EType
{
    if(e_isSame(*this, Scheme::mem_scheme))
	{
		return EType::mem;
	}
	else if(e_isSame(*this, Scheme::file_scheme))
	{
		return EType::file;
	}
	else if(e_isSame(*this, Scheme::ftp_scheme))
	{
		return EType::ftp;
	}
	else if(e_isSame(*this, Scheme::http_scheme))
	{
		return EType::http;
	}
	else if(e_isSame(*this, Scheme::https_scheme))
	{
		return EType::https;
	}
	else if(e_isSame(*this, Scheme::ssh_scheme))
	{
		return EType::ssh;
	}

	return EType::unknown;
}
