/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"resource/IRI.hpp"
#include "cvs/bag/E_DynBag.hpp" // e_addAll
// #include "string/StringConstant.hpp"

using namespace NPCO;

enum ESeparators : char8_t {
	  eScheme		= ':'
	, eAuthority	= '@'
	, eQuery		= '?'
	, eFragment		= '#'
};

IRI::IRI(const String& uri)
{
	this->decode(uri);
}

IRI::IRI(const Scheme& scheme, const IStringCarrier& authority, const Path& path, const IStringCarrier& query, const IStringCarrier& fragment)
 : scheme(scheme), authority(authority), path(path), query(query), fragment(fragment)
{	}

IRI::IRI(const IStringCarrier& scheme, const IStringCarrier& authority, const IStringCarrier& path, const IStringCarrier& query, const IStringCarrier& fragment)
 : scheme(scheme), authority(authority), path(path), query(query), fragment(fragment)
{	}

IRI::IRI(const Scheme& scheme, const IStringCarrier& authority, const Filename& filename)
 : scheme(scheme), authority(authority), path(filename.createString()), query(), fragment()
{	}

const Scheme&	IRI::getScheme(void) const
{
	return this->scheme;
}

void			IRI::setScheme(const Scheme& v)
{
	this->scheme = v;
}

auto	IRI::getAuthority(void) const -> const TString&
{
	return this->authority;
}

void	IRI::setAuthority(const IStringCarrier& v)
{
	this->authority = v;
}

const Path&	IRI::getPath(void) const
{
	return this->path;
}

void		IRI::setPath(const Path& v)
{
	this->path = v;
}

auto	IRI::getQuery(void) const -> const TString&
{
	return this->query;
}

void	IRI::setQuery(const IStringCarrier& v)
{
	this->query = v;
}

auto	IRI::getFragment(void) const -> const TString&
{
	return this->fragment;
}

void	IRI::setFragment(const IStringCarrier& v)
{
	this->fragment = v;
}

void	IRI::clear(void)
{
	this->scheme.clear();
	this->authority.clear();
	this->path.clear();
	this->query.clear();
	this->fragment.clear();
}

bool	IRI::isValid(void) const
{
	return false;
}

void	IRI::decode(const String& uri)
{
	this->clear();

	auto cur = e_cursor(uri);
	// Detect scheme
	String tmp;
	for( ; cur.ok() && (*cur != ESeparators::eScheme) ; ++cur)
	{
		tmp.add(*cur);
	}

	if(!cur.ok())
	{	// Error
		return;
	}

	this->scheme.set(tmp);

	// Parse left
	tmp.clear();

	// Detect hier-path
	for( ; cur.ok() && (*cur != ESeparators::eQuery) ; ++cur)
	{
		auto c = *cur;
		if(c == ESeparators::eAuthority)
		{
			this->authority = tmp;
			tmp.clear();
		}
		else
		{
			tmp.add(c);
		}
	}

	this->path.set(tmp);

	// Detect query
	for( ; cur.ok() && (*cur != ESeparators::eFragment) ; ++cur)
	{
		this->query.add(*cur);
	}

	// Read fragment
	e_addAll(this->fragment, cur);
}

String	IRI::encode(void) const
{
	String res = this->scheme.get();
	res.add(ESeparators::eScheme);

	if(!this->authority.isEmpty())
	{
		res.add(this->authority);
		res.add(ESeparators::eAuthority);
	}

	res.add(this->path.createString());

	res.add(ESeparators::eQuery);
	res.add(this->query);

	if(!this->fragment.isEmpty())
	{
		res.add(ESeparators::eFragment);
		res.add(this->fragment);
	}

	return res;
}

Filename	IRI::filename(void) const
{
	return Filename(this->path);
}

ClassifyValue	IRI::classify(const IRI& toCompare) const
{
	ClassifyValue cv;
	cv = this->scheme.classify(toCompare.scheme);
	if(cv != 0)	return cv;

	cv = this->authority.classify_CI(toCompare.authority);
	if(cv != 0)	return cv;

	cv = this->path.classify(toCompare.path);
	if(cv != 0)	return cv;

	cv = this->query.classify(toCompare.query);
	if(cv != 0)	return cv;

	return this->fragment.classify(toCompare.fragment);
}

//
//
//

IRI		IRI::forFile(const Filename& filename)
{
	return IRI(Scheme::file_scheme, String::empty, filename);
}

IRI		IRI::forMem(const Filename& name)
{
	return IRI(Scheme::mem_scheme, String::empty, name);
}
