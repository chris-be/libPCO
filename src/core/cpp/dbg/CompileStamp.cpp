/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"dbg/CompileStamp.hpp"
#include "dbg/Debug.hpp"

#include <iostream>

using namespace NPCO;

CompileStamp::CompileStamp(const char* codeName, const char* version, const char* name)
{
	PCO_ASSERT_MSG(codeName != nullptr && version != nullptr, "nullptr");

	this->codeName		= codeName;
	this->version		= version;
	this->name			= (name != nullptr) ? name : "#-#";

	this->compileDate	=	__DATE__;
	this->compileTime	=	__TIME__;
}

const char*	CompileStamp::getCodeName(void) const
{
	return this->codeName;
}

const char*	CompileStamp::getVersion(void) const
{
	return this->version;
}

const char*	CompileStamp::getName(void) const
{
	return this->name;
}

const char*	CompileStamp::getCompilationDate(void) const
{
	return this->compileDate;
}

const char*	CompileStamp::getCompilationTime(void) const
{
	return this->compileTime;
}

bool	CompileStamp::hasDebugInfo(void) const
{
	return this->debug;
}

bool	CompileStamp::hasPredicate(void) const
{
	return this->predicate;
}

void	CompileStamp::echoInfo(bool fullName) const
{
	const char* mode = this->debug		? "DBG" : "REL";
	const char* pred = this->predicate	? "SEC" : "SPD";

	std::cout << this->codeName << " [" << this->version << "]";
	if(fullName)
	{
		std::cout << " - " << this->name;
	}

	std::cout << std::endl << "\tCOMPILATION { ";

		std::cout << this->compileDate << " : " << this->compileTime << "\t[";
		std::cout << mode  << "|" << pred << "]";

	std::cout << " }" << std::endl << std::flush;
}
