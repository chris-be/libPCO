/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"dbg/DebugInfo.hpp"
#include "string/cvs/E_CStyleString.hpp"

using namespace NPCO;

DebugInfo::DebugInfo(void)
{
	this->info	= nullptr;
	this->file	= nullptr;
	this->line	= 0;
}

void	DebugInfo:: clear(void)
{
	e_free(this->info);
	e_free(this->file);
}

DebugInfo::DebugInfo(const char* info, const char* file, unsigned int line)
{
	this->info	= e_strcpy(info);
	this->file	= e_strcpy(file);
	this->line	= line;
}

DebugInfo::~DebugInfo(void)
{
	this->clear();
}

DebugInfo::DebugInfo(const DebugInfo& toCopy)
 : DebugInfo(toCopy.info, toCopy.file, toCopy.line)
{	}

DebugInfo&	DebugInfo::operator=(const DebugInfo& toCopy)
{
	this->clear();

	this->info	= e_strcpy(toCopy.info);
	this->file	= e_strcpy(toCopy.file);
	this->line	= toCopy.line;

	return *this;
}

const char* 	DebugInfo::getInfo(void) const noexcept
{
	return this->info != nullptr ? this->info : "undefined";
}

const char* 	DebugInfo::getFile(void) const noexcept
{
	return this->file != nullptr ? this->file : "undefined";
}

unsigned int	DebugInfo::getLine(void) const noexcept
{
	return this->line;
}
