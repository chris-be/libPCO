/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"dbg/Debug.hpp"

#include <iostream>

using namespace NPCO;

void	Debug::do_assert(const char* msg, const char* file, const int line)
{
	std::cerr
		<< "ASSERT: " << std::endl
		<< msg << std::endl
		<< "\tFile [" << file << "]" << std::endl
		<< "\tLine [" << line << "]" << std::endl
		<< std::flush;

	std::abort();
	// __asm int 3
}
