/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"dbg/DebugException.hpp"

using namespace NPCO;

DebugException::DebugException(const char* info, const char* file, unsigned int line)
 : DebugInfo(info, file, line)
{	}

DebugException::DebugException(const DebugInfo& info)
 : DebugInfo(info)
{	}


DebugException::~DebugException(void)
{	}

DebugException::DebugException(DebugException&& toMove)
 : DebugInfo()
{
	const DebugException& toCopy = toMove;
	this->operator=(toCopy);
}

const char* DebugException::what(void) const noexcept
{
	return this->getInfo(); // "DebugException - see info";
}
