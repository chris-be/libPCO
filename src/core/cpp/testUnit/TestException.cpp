/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"testUnit/TestException.hpp"

using namespace NTestUnit;
// using namespace NPCO;

TestException::TestException(const char* info, const char* file, unsigned int line)
 : DebugInfo(info, file, line)
{	}

TestException::TestException(const DebugInfo& info)
 : DebugInfo(info)
{	}


TestException::~TestException(void)
{	}

TestException::TestException(const TestException& toCopy)
 : TestException(toCopy.info, toCopy.file, toCopy.line)
{	}

TestException&	TestException::operator=(const TestException& toCopy)
{
	DebugInfo::operator=(toCopy);

	return *this;
}

TestException::TestException(TestException&& toMove)
 : DebugInfo()
{
	const TestException& toCopy = toMove;
	this->operator=(toCopy);
}

const char* TestException::what(void) const noexcept
{
	return this->getInfo(); // "TestException - see info";
}
