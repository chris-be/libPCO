/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"testUnit/TestContainer.hpp"
#include "testUnit/TestDebug.hpp"
#include "cvs/iter/E_HasIndexIter.hpp"

#include <exception>

using namespace NPCO;
using namespace NTestUnit;

const chunk_size	TestContainer::BlockSize = 8;


TestContainer::TestContainer(void)
{	}

TestContainer::~TestContainer(void)
{
	this->clear();
}

void	TestContainer::clear(void)
{
	this->factory.clear();
	this->hierarchy.clear();
}

void	TestContainer::addTestClass(const TestName& name, FacFunc func)
{
	if(name.isEmpty())
	{
		throw std::invalid_argument("Invalid name provided");
	}

	if(func == nullptr)
	{
		throw std::invalid_argument("Invalid factory provided");
	}

	if(this->factory.hasKey(name))
	{
		throw std::invalid_argument("Already registered test name");
	}

	this->factory.add(name, func);
}

bool	TestContainer::isDependancy(const TestName& root, const TestName& leaf) const
{
	auto it_r = this->hierarchy.find(root);
	if(!it_r.isValid())
	{	// Nothing defined
		return false;
	}

	// Test all successors
	const TNameList& names = it_r.cell();
	for(auto cursor = e_cursor(names) ; cursor.ok() ; ++cursor)
	{
		if(cursor.cell() == leaf)
		{
			return true;
		}
		if(this->isDependancy(cursor.cell(), leaf))
		{
			return true;
		}
	}

	return false;
}

void	TestContainer::setDependancy(const TestName& previous, const TestName& name)
{
	if(previous.isEmpty())
	{
		throw std::invalid_argument("Invalid previous name provided");
	}

	if(name.isEmpty())
	{
		throw std::invalid_argument("Invalid name provided");
	}

	if(!this->factory.hasKey(previous))
	{
		throw std::invalid_argument("Unknown/unregistered previous name");
	}

	if(!this->factory.hasKey(name))
	{
		throw std::invalid_argument("Unknown/unregistered test name");
	}

	// Check loop
	if(this->isDependancy(name, previous))
	{
		throw std::invalid_argument("Creating cyclic dependancy");
	}

	// Add
    auto it_r = this->hierarchy.find(previous);
	if(!it_r.isValid())
	{
		it_r = this->hierarchy.add(previous);
		PCO_ASSERT(it_r.isValid());
	}

	it_r.cell().add(name);
}

TestClass*	TestContainer::newTest(const TestName& name) const
{
	if(!this->factory.hasKey(name))
	{
		throw std::invalid_argument("Invalid test name provided");
	}

	return this->factory[name]();
}

/////

typename TestContainer::TNameList	TestContainer::findRoots() const
{
	TNameList	roots;

	auto cursor = e_indexCursor(this->factory);
	for( ; cursor.ok() ; ++cursor)
	{
		TestName name = cursor.key();
		roots.add( name );
	}

	return roots;
}
