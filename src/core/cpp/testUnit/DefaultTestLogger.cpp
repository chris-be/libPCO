/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"testUnit/DefaultTestLogger.hpp"
#include "string/String.hpp"
#include "string/cvs/E_StringEncoding.hpp"

#include <iostream>

using namespace NPCO;
using namespace NTestUnit;

DefaultTestLogger::~DefaultTestLogger()
{	}

void	DefaultTestLogger::setLogLevel(const TChannel& channel, LogLevel level)
{
	//@todo
}

void	DefaultTestLogger::enterSubSection(const TChannel& channel, const TMessage& name)
{
	//@todo
}

void	DefaultTestLogger::leaveSubSection(const TChannel& channel)
{
	//@todo
}

void	DefaultTestLogger::logMessage(const TChannel& channel, LogLevel level, const TMessage& message)
{
	String	c(channel);
	if(c == ITestLogger::ErrorChannel)
	{
		std::cerr << message << std::endl;
	}
	else
	{
		std::cout << channel << '[' << level << ']' << message << std::endl;
	}
}
