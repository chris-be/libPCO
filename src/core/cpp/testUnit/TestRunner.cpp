/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"testUnit/TestRunner.hpp"
#include "testUnit/TestDebug.hpp"
#include "testUnit/DefaultTestLogger.hpp"
#include "mem/Mem.hpp"
#include "string/String.hpp"
#include "cvs/res/AutoDelete.hpp"

#include "testUnit/TestException.hpp"
#include "dbg/DebugException.hpp"

#include <exception>

using namespace NPCO;
using namespace NTestUnit;

const TestName	TestRunner::ErrorMessage_AddingTestClass	= U8_STR("Error adding test class: ");
const TestName	TestRunner::ErrorMessage_RunningTest		= U8_STR("Error running test: ");


TestRunner::TestRunner(ITestLogger*	testLogger)
{
	if(testLogger == nullptr)
	{
		testLogger = new DefaultTestLogger();
	}

	this->logger = testLogger;
}

TestRunner::~TestRunner(void)
{
	Mem::_delete(this->logger);
}

int	TestRunner::run(ETestMode mode)
{
	// Ok by default
	int rc = 0;

	try
	{
		// Extract roots
		TestContainer::TNameList	roots = this->container.findRoots();

		for(auto cur = e_cursor(roots) ; cur.ok() ; ++cur)
		{
			//@todo thread by root...
			const TestName& name = *cur;
			// Adjust rc
			rc |= this->runTest(name,mode);
		}
	}
	catch(const std::exception& ex)
	{
		this->logger->logError(1, ErrorMessage_RunningTest + ex.what());
		rc = -1;
	}
	catch(...)
	{
		this->logger->logError(1, ErrorMessage_RunningTest + "unknown");
		rc = -1;
	}

	return rc;
}

inline
static void	_runTest(TestClass& testClass, const ETestMode mode)
{
	switch(mode)
	{
		case ETestMode::correctness:
		{
			testClass.correctness();
		} break;
		case ETestMode::speed:
		{
			testClass.speed();
		} break;
		case ETestMode::volumetry:
		{
			testClass.volumetry();
		} break;
		default:
		{
			PCO_UNK_CASE("Unkown test mode");
		}
	}
}

int		TestRunner::runTest(const TestName& name, const ETestMode mode)
{
	// Wrong by default
	int rc = 1;

	this->logger->logMessage(3, String("Instantiate test: ") + name);
	{	// Auto
		TestClass* testClass = this->container.newTest(name);
		AutoDelete<TestClass>	autoTest(testClass);

		this->logger->logMessage(3, StringC(U8_STR("Tests")) );

		try
		{
			_runTest(*testClass, mode);
			rc = 0;
		}
		catch(const TestException& ex)
		{
			this->logger->logError(1, ErrorMessage_RunningTest + ex.getInfo());
			String	msg;
					msg = msg + U8_STR("\tdetails(") + ex.getFile() + U8_STR(" [") ; // + ex.getLine() + "]";
			this->logger->logError(1, msg);
		}
		catch(const DebugException& ex)
		{
			this->logger->logError(1, ErrorMessage_RunningTest + ex.getInfo());
		}
	}
	this->logger->logMessage(3, StringC(U8_STR("Test cleaned")) );

	return rc;
}
