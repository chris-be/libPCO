/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"testUnit/ITestLogger.hpp"

using namespace NTestUnit;

const StringC	ITestLogger::MessageChannel		= (char8_t*)u8"default";
const StringC	ITestLogger::ErrorChannel		= (char8_t*)u8"error";
const StringC	ITestLogger::VolumetryChannel	= (char8_t*)u8"volumetry";
