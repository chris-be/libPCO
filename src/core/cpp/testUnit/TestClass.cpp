/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"testUnit/TestClass.hpp"
#include "testUnit/TestDebug.hpp"
#include "testUnit/TestException.hpp"

#include "low/Bit.hpp"
#include "dbg/DebugException.hpp"

using namespace NPCO;
using namespace NTestUnit;

TestClass::~TestClass(void)
{	}

void TestClass::predicate(bool condition, const char* msg, const char* file, unsigned int line) const
{
	if(condition)
	{	// Ok
		return;
	}

	PCO_ASSERT(false);
	throw DebugException(msg, file, line);
}

void TestClass::check(bool condition, const char* msg, const char* file, unsigned int line) const
{
	if(condition)
	{	// Ok
		return;
	}

	PCO_ASSERT(false);
	throw TestException(msg, file, line);
}

void TestClass::correctness(void)
{	}

void TestClass::speed(void)
{	}

void TestClass::volumetry(void)
{	}

/*
void TestClass::run(ETestMode mode)
{
	switch(mode)
	{
		case ETestMode::correctness:
		{
			this->correctness();
		} break;
		case ETestMode::speed:
		{
			this->speed();
		} break;
		case ETestMode::volumetry:
		{
			this->volumetry();
		} break;
		default:
		{
			PCO_UNK_CASE("Unkown test mode");
		}
	}
}
*/
