/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"time/Timer.hpp"
#include "mem/Mem.hpp"
#include "cvs/cfr/E_Classifier.hpp" // e_isAfter


#include <time.h>
#include <errno.h>

using namespace NPCO;

Timer::Timer(void)
{
	this->delay.set(0, 0);
}

Timer::Timer(time_t secDelay, long nanoDelay)
{
	this->setDelay(secDelay, nanoDelay);
}

Timer::Timer(const Nanotime& delay)
{
	this->setDelay(delay);
}

Timer::~Timer(void)
{	}

void	Timer::top(void)
{
	this->start.setNow();
	this->stop = this->start;
	this->stop+= this->delay;
}

bool	Timer::isStopOverlapped(void) const
{
	Nanotime	now;

	now.setNow();
	return(e_isAfter(now, this->stop));
}

bool	Timer::sleep(void) const
{
	Nanotime	now;

	now.setNow();
	if(e_isAfter(now, this->stop))
	{	// Nothing to do
		return false;
	}

	// Should sleep
	Nanotime	delta( this->stop );
				delta-= now;

	timespec remain;
	Mem::structCpy(remain, delta.getTime());

	int ret =0;
	do
	{
		timespec limit;
		Mem::structCpy(limit, remain);
		//LOG_DBG( "Go sleeping " + StringUtil::intToString(limit.tv_sec) + " s, " + StringUtil::longToString(limit.tv_nsec) + " ns.." );
		ret = nanosleep(&limit, &remain);
		//LOG_DBG( "Wakeup");
	} while( (ret < 0) && (errno == EINTR) );

	return true;
}
