/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"time/Nanotime.hpp"
#include "time/Temporal.hpp"

using namespace NPCO;

const long	ONE_SECOND_IN_MILLISECONDS		= Temporal::Second::In_Milliseconds;
const long	ONE_SECOND_IN_MICROSECONDS		= Temporal::Second::In_Microseconds;
const long	ONE_SECOND_IN_NANOSECONDS		= Temporal::Second::In_Nanoseconds;

const long	ONE_MICROSECOND_IN_NANOSECONDS	= Temporal::MicroSecond::In_Nanoseconds;

Nanotime::Nanotime(void)
{
#ifndef NDEBUG
	this->time.tv_sec	= 0;
	this->time.tv_nsec	= 0;
#endif
}

Nanotime::Nanotime(time_t seconds, long nanos)
{
	this->time.tv_sec	= seconds;
	this->time.tv_nsec	= nanos;
}

void	Nanotime::setNow(void)
{
	timeval		now;
	gettimeofday(&now, 0);

	this->set(now.tv_sec, now.tv_usec * ONE_MICROSECOND_IN_NANOSECONDS);
}

void	Nanotime::set(time_t seconds, long nanos)
{
	this->time.tv_sec	= seconds;
	this->time.tv_nsec	= nanos;
}

Nanotime&	Nanotime::operator+=(const Nanotime &toAdd)
{
	this->time.tv_nsec	+= toAdd.time.tv_nsec;

	time_t	seconds		= (time_t)(this->time.tv_nsec / ONE_SECOND_IN_NANOSECONDS);
	this->time.tv_nsec	= this->time.tv_nsec % ONE_SECOND_IN_NANOSECONDS;
	this->time.tv_sec	+= toAdd.time.tv_sec + seconds;

	return *this;
}

Nanotime&	Nanotime::operator-=(const Nanotime &toSub)
{
	this->time.tv_nsec	-=	toSub.time.tv_nsec;

	time_t	seconds = 0;
	if(this->time.tv_nsec < 0)
	{
		this->time.tv_nsec	= -this->time.tv_nsec;
		seconds				= (time_t)(this->time.tv_nsec / ONE_SECOND_IN_NANOSECONDS);
		this->time.tv_nsec	= this->time.tv_nsec % ONE_SECOND_IN_NANOSECONDS;
	}
	this->time.tv_sec -= toSub.time.tv_sec + seconds;

	return *this;
}

Nanotime&	Nanotime::operator*=(unsigned int factor)
{
	long overflow		= (this->time.tv_nsec * factor);

	this->time.tv_sec	*= factor;
	this->time.tv_sec	+= overflow / ONE_SECOND_IN_NANOSECONDS;
	this->time.tv_nsec	= overflow % ONE_SECOND_IN_NANOSECONDS;

	return *this;
}

Nanotime&	Nanotime::operator/=(unsigned int factor)
{
	long remain			= (this->time.tv_sec % factor) * ONE_SECOND_IN_NANOSECONDS;
	this->time.tv_sec	/= factor;

	this->time.tv_nsec	+= remain;
	this->time.tv_nsec	/= factor;

	return *this;
}

int		Nanotime::classify(const Nanotime &toCompare) const
{
	int tmp = this->time.tv_sec - toCompare.time.tv_sec;
	if(tmp != 0)	return tmp;

	return this->time.tv_nsec - toCompare.time.tv_nsec;
}

Nanotime	NPCO::operator+(const Nanotime& left, const Nanotime& right)
{
	Nanotime delta(left);
	delta+= right;
	return delta;
}

Nanotime	NPCO::operator-(const Nanotime& left, const Nanotime& right)
{
	Nanotime delta(left);
	delta-= right;
	return delta;
}
