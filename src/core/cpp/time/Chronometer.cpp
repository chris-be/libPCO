/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"time/Chronometer.hpp"

// #include "PCO_Predicate.hpp"

using namespace NPCO;

void	Chronometer::start(void)
{
	this->begin.setNow();
}

void	Chronometer::stop(void)
{
	this->end.setNow();
}

Nanotime	Chronometer::elapsed(void) const
{
	// PCO_ASSERT(this->start >= this->stop);
	return this->end - this->begin;
}

Nanotime	Chronometer::liveElapsed(void) const
{
	Nanotime now;
	now.setNow();
	now-= this->begin;
	return now;
}
