/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include "text/cvs/CVS_Regex.hpp"

using namespace NPCO;

bool	CVS_Regex::match(const IStringCarrier& expr, const IStringCarrier& toParse, bool ignoreCase)
{
	Regex	reg;
			reg.prepare(expr, false, ignoreCase);

	return reg.match(toParse);
}
