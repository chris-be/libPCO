/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include "text/RegexSplitResult.hpp"

using namespace NPCO;

MatchOffset::MatchOffset(void)
 : first(-1), last(-1)
{	}

MatchOffset::MatchOffset(int first, int last)
{
	PCO_INV_ARG( ((first >= 0) && (last >= 0)) || ((first < 0) && (last < 0)), "");

	this->first	= first;
	this->last	= last;
}

bool	MatchOffset::isValid(void) const
{
	return this->first > -1;
}

auto	MatchOffset::getFirst(void) const -> TOffset
{
	return this->first;
}

auto	MatchOffset::getLast(void) const -> TOffset
{
	return this->last;
}

//
//
//

void	RegexSplitResult::clear()
{
	this->result.clear();
}

void	RegexSplitResult::addEmpty(void)
{
	MatchOffset mo;
	this->result.moveIn(std::move(mo));
}

void	RegexSplitResult::add(int first, int last)
{
	MatchOffset mo(first, last);
	this->result.moveIn(std::move(mo));
}
