/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"text/TextPosition.hpp"
#include "PCO_Types.hpp" // rawConvert

using namespace NPCO;

// TODO handle win, linux, ...

void	TextPosition::handleControlCode(char control)
{
	switch(control)
	{
		case  0: // NULL
		case  1: // Start of header
		case  2: // Start of text
		case  3: // End of Text
		case  4: // End of Trans.
		case  5: // Enquiry
		case  6: // Acknowledgement
		case  7: // Bell
		{	} break;
		case  8: // Backspace
		{
			--this->column;
		} break;
		case  9: // Horizontal Tab
		case 11: // Vertical Tab
		case 12: // Form feed
		case 14: // Shift Out
		case 15: // Shift In
		case 16: // Data link escape
		case 17: // Device control 1
		case 18: // Device control 2
		case 19: // Device control 3
		case 20: // Device control 4
		case 21: // Negative acknowl.
		case 22: // Synchronous idle
		case 23: // End of trans. block
		case 24: // Cancel
		case 25: // End of medium
		case 26: // Substitute
		case 27: // Escape
		case 28: // File separator
		case 29: // Group separator
		case 30: // Record separator
		case 31: // Unit separator
		case 127: // Delete
		{

		} break;
		case 10: // Line feed
		{
			++this->line;
			this->column = 0;
		} break;
		case 13: // Carriage return
		{
			this->column = 0;
		} break;
		default:
		{
			++this->column;
		}
	}
}

void	TextPosition::newLine(void)
{
	++this->line;
	this->column = 0;
}

TextPosition::TextPosition(void)
{
	this->reset();
}

TextPosition::TextPosition(TSize line, TSize column)
 : line(line), column(column)
{	}

void	TextPosition::reset(void)
{
	this->line	= 0;
	this->column= 0;
}

void	TextPosition::set(TSize line, TSize column)
{
	this->line = line;
	this->column = column;
}

auto	TextPosition::getLine(void) const -> TSize
{
	return this->line;
}

void	TextPosition::setLine(TSize line)
{
	this->line = line;
}

auto	TextPosition::getColumn(void) const -> TSize
{
	return this->column;
}

void	TextPosition::setColumn(TSize column)
{
	this->column = column;
}

void	TextPosition::add(TChar c)
{
	if(c > 32)
	{
		++this->column;
	}
	else if(c == '\r')
	{
		this->column = 0;
	}
	else if(c == '\n')
	{
		this->newLine();
	}
	else
	{
		++this->column;
	}
}

/*
void	TextPosition::add(AChar c)
{
	this->add(c.getCode());
}

void	TextPosition::add(UChar c)
{
	char32_t code = c.getCode();
	this->add(code);
}

void	TextPosition::move(AChar c)
{
	if(c.isControl())
	{
		this->handleControlCode(c.getCode());
	}
	else
	{
		++this->row;
	}
}

void	TextPosition::move(UChar c)
{
	if(c.isControl())
	{
		char ctrl = (char)c.getCode();
		this->handleControlCode(ctrl);
	}
	else
	{
		++this->row;
	}
}
*/

bool	TextPosition::operator==(const TextPosition& toCompare) const
{
	return	   (this->line == toCompare.line)
			&& (this->column == toCompare.column);
}
