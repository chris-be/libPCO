/*
  Copyright 2018 Christophe Marc BERTONCINI

  This file is part of PCO - "Petite Caisse à Outils".
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

#include	"text/Regex.hpp"
#include "string/CStringHolder.hpp"
#include "mem/cvs/CVS_Mem.hpp"
#include "cvs/res/AutoDeleteBlock.hpp"

using namespace NPCO;

#include <string>

static std::string	getError(int ec, const regex_t* regex)
{
	std::string	msg;
	// Query length
	int size = ::regerror(ec, regex, nullptr, static_cast<size_t>(0));

	// Use temp buffer
	Array<char>	buffer(size);			char* ptr = buffer._da();
	::regerror(ec, regex, ptr, size);

	msg.append(ptr, size);
	return msg;
}

void	Regex::throwError(int ec, const char* msg) const
{
	std::string err = msg;
	err = err + ": " + getError(ec, &this->regex);
	throw std::runtime_error(err);
}

Regex::Regex(void)
 : prepared(false)
{	}

Regex::Regex(const IStringCarrier& expr, bool forSplit, bool ignoreCase)
 : Regex()
{
	this->prepare(expr, forSplit, ignoreCase);
}

bool	Regex::isOpened(void) const noexcept
{
	return this->prepared;
}

void	Regex::close(void) noexcept
{
	if(this->prepared == true)
	{
		regfree(&this->regex);
		this->prepared = false;
	}
}

void	Regex::prepare(const IStringCarrier& expr, bool forSplit, bool ignoreCase)
{
	PCO_LOGIC_ERR(!this->prepared, "Close before");

	int				flags = REG_EXTENDED;
	if(!forSplit)	flags |= REG_NOSUB;
	if(ignoreCase)	flags |= REG_ICASE;

	CStringHolder	pattern(expr);
	int	ec	= regcomp(&this->regex, pattern.const_char_ptr(), flags);
	if(ec != 0)
	{
		this->throwError(ec, PCO_ERR_MSG("Regex compilation"));
	}

	this->prepared = true;
}

bool	Regex::match(const IStringCarrier& toParse) const
{
	PCO_LOGIC_ERR(this->prepared, "Prepare before");
	CStringHolder	test(toParse);

	bool	rc;
	int		ec = regexec(&this->regex, test.const_char_ptr(), 0, 0, 0);
	switch(ec)
	{
		case 0:
		{
			rc = true;
		} break;
		case REG_NOMATCH:
		{
			rc = false;
		} break;
		default:
		{
			this->throwError(ec, PCO_ERR_MSG("Regex match"));
			rc = false;
		}
	}

	return rc;
}

void	Regex::split(RegexSplitResult &result, const IStringCarrier& toParse) const
{
	PCO_LOGIC_ERR(this->prepared, "Prepare before");
	CStringHolder	test(toParse);

	// +1 since [0] is full expression
	size_t			nb = this->regex.re_nsub+1;
	regmatch_t*		matches = new regmatch_t[nb];
	AutoDeleteBlock<regmatch_t>	cleaner(matches);

	int		ec = regexec(&this->regex, test.const_char_ptr(), nb, matches, 0);
	switch(ec)
	{
		case 0:
		{
			result.clear();
			// Ignore full expression
			for(auto cur = CVS_Mem::cursor(matches+1, nb-1) ; cur.ok() ; ++cur)
			{
				const regmatch_t&	match = *cur;
				regoff_t	start = match.rm_so;
				if(start != -1)
				{
					result.add(start, match.rm_eo-1);
				}
				else
				{
					result.addEmpty();
				}
			}
			result.setFound(true);
		} break;
		case REG_NOMATCH:
		{
			result.setFound(false);
		} break;
		default:
		{
			this->throwError(ec, PCO_ERR_MSG("Regex split"));
		}
	}

}
