# Urgent / Imperative
- bags: remove moveIn call in "add" ! Add "add()"
	- e_moveIn(bag, T&& tm) { bag.add() = std::move(tm); } ?

- MemManager:
	- remove "virtual" / C_??
	- add grow / reduce logic (see Store)
	- MemManager<AllocStrategy, ResizeStrategy> ??

- write tests:
	- LogicTree Bijection....

- IString: remove ?
- StringBase:
	- C_SelfClassified<TChar>
	- operator==(const TChar* )
	- explicit constructor(IStringCarrier) ??

- improve Reference

- Regex UTF: NFC normalization ? lib PCRE2 for regex ?

- Chunklist :
	- implement remove
	- MetaHolderBlock ?
	- update TestChunkList to use Test_DynBag

- use char instead of uw_8 for file reading ?

- cvs/bag/E_Bag : e_lexiCompare... -> cvs/cfr/ ??

- E_Classifier Classifier<TWith>
	- operator== ?? return compare() == 0 ...
- DefaultClassifier: math for primitives, SelfClassified else

- work on "beta"

- CVS_Mem + E_MemBlock -> E_Mem

- CVS_Unicode: unicode complience, not only ASCII


## Missing
- Convert from "encoding"
	- not implemented ! no time to waste with this awful thing !!!!! Just prepar it
	- charset map ?


- file:
	- O_CREAT, O_EXCL, O_NOCTTY et O_TRUNC, O_ASYNC, ...
	- review code
	- add "select" method: test if read/write can be done (for async op)

# Review ??
- CVS_Find / E_Find ?
	- use MemBlock ?

- CVS_Random
	- FreeBSD: RAND_MAX < <int>max()
- time/Temporal
	- add/sub mul/div (BigInteger ??)
- time/Chronometer -> Temporal

- SingletonOnDemand: traits ? remove ?

- DebugException/TestException

- Tests:
	- wrong dependancy run: no use of Hierachy at this time
	- add tests for namespace NTest !!

## Improvement
- assert : ask user to ignore and continue (default handler that can be changed)
- log channel (DEBUG)

- use of constraints !
	- math/C_Number, remove INumber
	- IFillBuffer -> C_FillBuffer

- math min/max traits/policy ??

## Print
- TextAligner (tab/spaces / indent)

## Program arguments
- mandatory options to operand
- option value constraint (if any)

## ?
- Map<, SortedChunk> for mini memory footprints => libData

- MemSettings ??
- remove type ?


# beta
## Text ??
- UCharray : interop with IString
- ACharray : interop with IString (only ASCII !!!)
- IString / Charray
- C_Charray
	void	add(IInflow<PChar>& inflow);
	void	set(IInflow<PChar>& inflow);
	int		compare(IInflow<PChar>& inflow) const;

## Remind
AutoDelete<std::remove_pointer<decltype(src)>::type>

-Wno-unused-const-variable
-Wno-unused-variable
-Wno-unused-parameter

