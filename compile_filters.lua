#!lua
-- Written with TAB as 4 spaces

	-- Define MACROS depending on system

	-- Check MacOS X POSIX complience
	filter "system:bsd or system:macosx"
		defines { "PREMAKE_POSIX" }

	-- Linux not so POSIX ?
	filter "system:linux"
		defines { "PREMAKE_LINUX" }

	filter "system:windows"
		defines { "PREMAKE_WINDOWS" }

	-- Not forget to stop filter action
	filter { }
