#!/bin/sh

readonly mapsDir='charmap'

create_map() {
	encoding=$1
	printf ' %s' $encoding

	# -f: from encoding
	# -t: to encoding
	# -o: output file
	# -s: silent
	uconv -s --from-callback substitute -f $encoding -t utf-32be -o "$mapsDir/charset-$encoding.txt" 128-255-charset.txt

}

create_maps() {

	for enc in $(uconv -l | cut -d/ -f1); do
		create_map $enc
	done 
}

mkdir -p "$mapsDir"
printf 'Generating:'
# create_map 'iso-8859-1'
create_maps

