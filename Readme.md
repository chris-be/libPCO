# libPCO : "Petite Caisse à Outils"
Copyright (c) 2018 Christophe Marc BERTONCINI (see LICENSE text file).

## Why libPCO ?
Simply because I started programming on my own. First BASIC on a "Commodore 64". Then ASM on x86, later C. Meanwhile I always tried to discover/understand/test things, that's why I "reinvented the wheel" as it's commonly said.
The first langage I learned at "school" (university) and not by my own was C++: and I decided that "object was cool".
Than I worked with a variety of langages and ecosystems... but I always had to code same things for the same needs...
So this is a synthesis of my knowledge and needs.

## How to **use it** ?
1. *Buid it* (see below).
2. Main library classes are in 'NPCO' namespace. With some fun it can be pronounced "un poco" (which means "a little bit" in spanish).
3. There is a NTestUnit namespace which contains some classes used to create ... "test units", yes :)
4. src/core contains library, src/test contains tests for "core".

## How to **build** ?
### Minimum requirements
C++ compiler compatible with "C++17"
libc++ installed (or modify premake files)

### Use of Premake 5
This project use [Premake 5](https://premake.github.io/) to get rid of project management.
So it's up to you to choose what you want (depending on Premake options).

#### Example for generating solution/project for CodeLite
> premake5 --cc=gcc codelite
> #				--os=linux

## How to **clean** ?
Generated folders are :
- ide : where project files are stored (Makefile, Codelite, Eclipse, ...)
- bin : where binaries are generated

> They can be simply cleaned like this :
> rm -rf bin
> rm -rf ide
